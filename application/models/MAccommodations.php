<?php
class MAccommodations extends CI_Model{

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		$this->load->library('Ical');
  }

	function create($data_posted){
		$this->db->trans_start();
		$randName = uniqid();
		$pathName = "./uploads/ics/".$randName.".ics";
		$this->ical->get($data_posted['ics_url'], $pathName);

		$data = array(
			'name' => $data_posted['name'],
			'address' => $data_posted['address'],
			'website' => $data_posted['website'],
			'youtube' => $data_posted['youtube'],
			'lat' => $data_posted['lat'],
			'lng' => $data_posted['lng'],
			'description' => $data_posted['description'],
			'accommodation_types_id' => $data_posted['accommodation_types_id'],
			'locations_id' => $data_posted['locations_id'],
			'tags' => $data_posted['tags'],
			'ics_url' => $data_posted['ics_url'],
			'ics' => $pathName,
			'class' => $data_posted['class'],
			'created_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);


		if($this->db->insert('accommodations', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodation has been added",
				'id' => $this->db->insert_id(),
				'name' => $data_posted['name'],
				'description' => $data_posted['description'],
				'photos' => array()
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		/*Get last id*/
		$insert_id = $this->db->insert_id();
		/*Upload photos*/
		$photos = $this->upload_photos();
		/*Create data photos*/
		$response['photos'] = $this->create_photos($photos, $data_posted, $insert_id);
		/*Check and reformat services*/
		$services = array();
		if(isset($data_posted['services'])){
			foreach($data_posted['services'] as $service){
				$ser = array(
					'services_id' => $service,
					'rate' => $data_posted['price_service-'.$service]
				);
				array_push($services, $ser);
			}
			/*Create data services*/
			$response['services'] = $this->create_services($services, $insert_id);
		}



		// print_r($response);
		$this->db->trans_complete();
		return $response;

	}

	function create_services($services, $insert_id){
		$responses = array();
		for($i=0; $i< count($services); $i++){
			$service['accommodations_id'] = $insert_id;
			$service['services_id'] = $services[$i]['services_id'];
			$service['rate'] = $services[$i]['rate'];
			$service['created_on'] = date('Y-m-d H:i:s');
			if($this->db->insert('mm_accommodations_services', $service)){
				$response = array(
					'error' => 0,
					'message' => "Service has been added",
					'id' => $this->db->insert_id()
				);
				array_push($responses, $response);
			}else{
				$response = array(
					'error' => 1,
					'message' => $this->db->error()['message']
				);
				array_push($responses, $response);
			}
		}
		return $responses;
	}

	function create_service($service, $insert_id){
		if($this->db->insert('mm_accommodations_services', $service)){
			$response = array(
				'error' => 0,
				'message' => "Service has been added",
				'id' => $this->db->insert_id()
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function create_photos($photos, $dataPosted, $insert_id){
		$responsephotos = array();
		for($i=0; $i< count($photos); $i++){
			$photos[$i]['accommodations_id'] = $insert_id;
			$photos[$i]['created_on'] = date('Y-m-d H:i:s');
			if($this->db->insert('photos', $photos[$i])){
				$photo = array(
					'error' => 0,
					'message' => "Photo has been added",
					'id' => $this->db->insert_id(),
					'name' => $dataPosted['name'],
					'description' => $dataPosted['description'],
				);
				array_push($responsephotos, $photo);
			}else{
				$photo = array(
					'error' => 1,
					'message' => $this->db->error()['message']
				);
				array_push($response->photos, $photo);
			}
		}
		return $responsephotos;
	}

	function upload_photos(){
		$photos = array();
		// print_r($_FILES);
		if(file_exists($_FILES['files']['tmp_name'][0])){
			// echo 'ada foto';
				//Photos Not Null
				$filesCount = count($_FILES['files']['name']);
				for($i = 0; $i < $filesCount; $i++){
						$_FILES['userFile']['name'] = $_FILES['files']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['files']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['files']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['files']['size'][$i];
						$new_name = uniqid();
						$uploadPath = 'uploads/photos/accommodations';
						$config['upload_path'] = $uploadPath;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['file_name'] = $new_name;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('userFile')){
								$fileData = $this->upload->data();
								$ty = explode('/', $_FILES['userFile']['type']);
								if($ty[1] == "jpeg"){
									$type = "jpg";
								}else{
									$type = $ty[1];
								}
								$photo['photo'] = "uploads/photos/accommodations/"."".$config['file_name'].".".$type;
								array_push($photos, $photo);
						}else{
							echo $this->upload->display_errors("<span class='error'>", "</span>");
						}
				}
		}else{
			// echo "gada foto";
			// Photos Null
		}
		return $photos;
	}

	function read_all(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.active,

			accommodations.class,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left_outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left_outer');
		$query = $this->db->where('accommodations.active != 2');


		if($query = $this->db->get()){
			$accommodations = $query->result();
		}



		echo $this->db->error()['message'];
		return $accommodations;
	}

	function readAllIncludingRooms(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.code,
			accommodations.flag_ics_valid,
			accommodations.address,
			accommodations.website,
			accommodations.active,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id');
		$query = $this->db->where('accommodations.active != 2');
		if($query = $this->db->get()){
			$accommodations = $query->result();
		}

		for($i=0; $i<sizeof($accommodations); $i++){
			$query  = $this->db->select('
				accommodation_rooms.id,
				accommodation_rooms.name,
			');
			$query = $this->db->from('accommodation_rooms');
			$query = $this->db->where('accommodation_rooms.accommodations_id', $accommodations[$i]->id);
			if($query = $this->db->get()){
				$rooms = $query->result();
			}
			$accommodations[$i]->rooms = $rooms;
		}
		return $accommodations;
	}

	function readIncludingRoomsSearch(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.code,
			accommodations.flag_ics_valid,
			accommodations.address,
			accommodations.website,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id');
		$query = $this->db->where('accommodations.active != 2');

		if($this->input->get("bedrooms")){
			$bedrooms = $this->input->get("bedrooms");
			if(sizeof($bedrooms)==1){
				$queryBedroom = "accommodations.id IN ";
				$queryBedroom .= "(SELECT accommodation_rooms.accommodations_id FROM accommodation_rooms WHERE ";
				$queryBedroom .= "accommodation_rooms.total_rooms ".$bedrooms[0].")";
				$this->db->where("(".$queryBedroom.")");
			}else{
				$this->db->group_start();
				for($room = 0; $room<sizeof($bedrooms); $room++){
					$queryBedroom = "accommodations.id IN ";
					$queryBedroom .= "(SELECT accommodation_rooms.accommodations_id FROM accommodation_rooms WHERE ";
					$queryBedroom .= "accommodation_rooms.total_rooms ".$bedrooms[$room].")";
					$this->db->or_where("(".$queryBedroom.")");
				}
				$this->db->group_end();
			}
		}
		if($this->input->get("name")){
			$query = $this->db->where("accommodations.name LIKE '%".$this->input->get("name")."%'");
		}
		if($this->input->get("locations")){
			$locations = $this->input->get("locations");
			if(sizeof($locations)==1){
				$query = $this->db->where("accommodations.locations_id", $locations[0]);
			}else{
				$this->db->group_start();
				for($location = 0; $location < sizeof($locations); $location++){
					$query = $this->db->or_where("accommodations.locations_id", $locations[$location]);
				}
				$this->db->group_end();
			}
		}
		if($this->input->get("rates")){
			if($this->input->get("rates") != "all"){
				$class = $this->input->get("rates");
				$query = $this->db->where("accommodations.class", $class);
			}
		}


		if($query = $this->db->get()){
			$accommodations = $query->result();
		}


		for($i=0; $i<sizeof($accommodations); $i++){
			$query  = $this->db->select('
				accommodation_rooms.id,
				accommodation_rooms.name,
			');
			$query = $this->db->from('accommodation_rooms');
			$query = $this->db->where('accommodation_rooms.accommodations_id', $accommodations[$i]->id);
			if($query = $this->db->get()){
				$rooms = $query->result();
			}
			$accommodations[$i]->rooms = $rooms;
		}
		// echo $this->db->last_query();
		return $accommodations;
	}

	function read_id($id){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.youtube,
			accommodations.lat,
			accommodations.lng,
			accommodations.description,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.ics,
			accommodations.created_on,
			accommodations.updated_on,
			locations.name as location,
			accommodations.locations_id,
			accommodation_types.name as type,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->where('accommodations.id', $id);
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');

		if($query = $this->db->get()){
			$accommodations = $query->result()[0];
			// $accommodations->photos = $this->read_accommodation_photos_by_id($id);
		}
		return $accommodations;
	}

	function read_accommodation_photos_by_id($id){
		$query  = $this->db->select('
			photos.id,
			photos.photo,
			photos.description,
			photos.tags
		');
		$query = $this->db->from('photos');
		$query = $this->db->where('accommodations_id', $id);
		if($query = $this->db->get()){
			$photos = $query->result();
		}
		return $photos;
	}

	function read_like($word){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			locations.name as location,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
		$query = $this->db->where("accommodations.name LIKE '%$word%'");

		if($query = $this->db->get()){
			$results = $query->result();
		}
		$accommodations = array();

		foreach($results as $result){
			$accomodation = new stdClass();
			$accomodation->value = $result->id;
			$accomodation->label = $result->name.", ".$result->location;
			array_push($accommodations, $accomodation);
		}
		return $accommodations;
	}

	function read_accommodation_services($id){
		$query  = $this->db->select('
			mm_accommodations_services.id,
			mm_accommodations_services.rate,
			mm_accommodations_services.services_id,
			services.name as service
		');
		$query = $this->db->from('mm_accommodations_services');
		$query = $this->db->join('services', 'mm_accommodations_services.services_id = services.id');
		$query = $this->db->where('accommodations_id', $id);
		if($query = $this->db->get()){
			$services = $query->result();
		}
		return $services;
	}

	function read_rooms_by_id($id){
		$query  = $this->db->select('
			accommodation_rooms.id,
			accommodation_rooms.uid,
			accommodation_rooms.name,
			accommodation_rooms.total_rooms,
			accommodation_rooms.min_stay
		');
		$query = $this->db->from('accommodation_rooms');
		$query = $this->db->where('accommodations_id', $id);
		if($query = $this->db->get()){
			$rooms = $query->result();
		}
		return $rooms;
	}

	function updateCalendarFile($id){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.ics_url,
			accommodations.ics,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->where('accommodations.id', $id);

		if($query = $this->db->get()){
			$accommodation = $query->result()[0];
		}

		$this->ical->get($accommodation->ics_url, $accommodation->ics);
	}

	function updateTes(){

	}

	function update($data_posted){
		$this->db->trans_start();

		$randName = uniqid();
		$accm = $this->read_id($data_posted['idtoupdate']);
		unlink($accm->ics);
		$pathName = "./uploads/ics/".$randName.".ics";
		$this->ical->get($data_posted['ics_url'], $pathName);


		$data = array(
			'name' => $data_posted['name'],
			'address' => $data_posted['address'],
			'website' => $data_posted['website'],
			'youtube' => $data_posted['youtube'],
			'lat' => $data_posted['lat'],
			'lng' => $data_posted['lng'],
			'description' => $data_posted['description'],
			'accommodation_types_id' => $data_posted['accommodation_types_id'],
			'locations_id' => $data_posted['locations_id'],
			'tags' => $data_posted['tags'],
			'ics_url' => $data_posted['ics_url'],
			'ics' => $pathName,
			'created_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);

		$this->db->where('id', $data_posted['idtoupdate']);
		$this->db->update('accommodations', $data);

		$ics = $this->ical->create($pathName);
		$vvent = $this->ical->events($ics);
		// print_r($this->ical->events($ics));
		// for($i=0; $i < count($vvent); $i++){
		// 	print_r($vvent[$i]);
		// 	echo "<br>";
		// 	echo date("Y-m-d", strtotime($vvent[$i]['DTSTART']));
		// 	echo "<br>";
		// 	echo date("Y-m-d", strtotime($vvent[$i]['DTEND']));
		// 	echo "<br>";
		// 	echo $vvent[$i]['UID'];
		// 	echo "<br>";
		// 	echo $vvent[$i]['SUMMARY'];
		// 	echo "<br><br>";
		// }

		/*Upload photos*/
		$photos = $this->upload_photos();
		/*Create data photos*/
		$response['photos'] = $this->create_photos($photos, $data_posted, $data_posted['idtoupdate']);
		/*Reformat services*/
		$selected_services = $this->read_accommodation_services($data_posted['idtoupdate']);
		$services = array();
		if(isset($data_posted['services'])){
			foreach($data_posted['services'] as $service){
				$ser = array(
					'id' => $data_posted['service-'.$service],
					'services_id' => $service,
					'rate' => $data_posted['price_service-'.$service]
				);
				array_push($services, $ser);
			}
			$response['services'] = $this->recheck_services($services, $selected_services, $data_posted['idtoupdate']);
		}else{
			$this->delete_all_mm_accommodations_services_by_accommodations_id($data_posted['idtoupdate']);
		}

		print_r($response);
		$this->db->trans_complete();
		return $data;
	}

	function recheck_services($services, $selected_services, $insert_id){
		$count;
		echo "<br>Services = <br>";
		print_r($services);
		echo "<br>";
		echo "Selected = <br>";
		print_r($selected_services);
		echo "<br>";
		if(count($selected_services) == 0){
			echo "Create Services";
			$response = $this->create_services($services, $insert_id);
			echo "<br>";
			print_r($response);
		}else{
			// Do insert
			for($i=0; $i< count($services); $i++){
				$count = 0;
				for($j=0; $j< count($selected_services); $j++){
					$count++;
					if($services[$i]['services_id'] != $selected_services[$j]->services_id){
						if($count == count($selected_services)){
							$service = array(
								'services_id' => $services[$i]['services_id'],
								'accommodations_id' => $insert_id,
								'rate' => $services[$i]['rate'],
								'created_on' => date('Y-m-d H:i:s')
							);
							$this->create_service($service, $insert_id);
						}
					}else{
						break;
					}

				}
			}
			//Do delete
			for($i=0; $i< count($selected_services); $i++){
				$count = 0;
				for($j=0; $j< count($services); $j++){
					echo $selected_services[$i]->services_id." == ".$services[$j]['services_id']."??";
					echo "<br>";
					$count++;
					if($selected_services[$i]->services_id != $services[$j]['services_id']){
						if($count == count($services)){
							echo "hapus ini ".$selected_services[$i]->id."<br>";
							$this->db->where('id', $selected_services[$i]->id);
							if($this->db->delete('mm_accommodations_services')){
								$response = array(
									'error' => 0,
									'message' => "Services has been deleted",
									'id' => $selected_services[$i]->id
								);
							}else{
								$response = array(
									'error' => 1,
									'message' => $this->db->error()['message']
								);
							}
						}
					}else{
						break;
					}

				}
			}
			//Do Update
			for($i=0; $i< count($services); $i++){
				$data = array(
					'rate' => $services[$i]['rate'],
					'updated_on' => date('Y-m-d H:i:s')
				);
				$this->db->where('id', $services[$i]['id']);
				$this->db->update('mm_accommodations_services', $data);
				echo $this->db->last_query()."<br>";
			}
		}

	}

	function delete($id){
		$this->db->where('id', $id);
		if($this->db->delete('accommodations')){
			$response = array(
				'error' => 0,
				'message' => "Accommodation has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function delete_mm_accommodations_services($id){
		$this->db->where('id', $id);
		if($this->db->delete('mm_accommodations_services')){
			$response = array(
				'error' => 0,
				'message' => "Service has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function delete_all_mm_accommodations_services_by_accommodations_id($id){
		$this->db->where('accommodations_id', $id);
		if($this->db->delete('mm_accommodations_services')){
			$response = array(
				'error' => 0,
				'message' => "Services has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function count(){
		$this->db->where('active', 1);
		$this->db->from('accommodations');
		return $this->db->count_all_results();
	}

	function api_read_by_id($id){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.youtube,
			accommodations.lat,
			accommodations.lng,
			accommodations.description,
			accommodations.tags,
			accommodations.locations_id,
			accommodations.created_on,
			accommodations.updated_on,
			accommodation_types.name as type,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id');
		$query = $this->db->where('accommodations.active != 2');
		$query = $this->db->where('accommodations.id', $id);

		if($query = $this->db->get()){
			$accommodations = $query->result()[0];
		}
		return $accommodations;
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null && $value!=""){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

}
?>
