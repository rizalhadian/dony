<?php
class MAccommodationRooms extends CI_Model{

	public $id;
	public $code;
	public $old_code;
	public $name;
	public $total_rooms;
	public $occupancy;
	public $min_stay;
	public $max_guest_normal;
	public $max_guest_extra;
	public $max_guest_infant;
	public $total_bathrooms;
	public $description;
	public $tags;
	public $accommodations_id;
	public $created_on;
	public $updated_on;
	public $active;


	function __construct(){
  	parent::__construct();

  }

	function create($data_posted){
		$this->db->trans_start();
		$data = array(
			'name' => $data_posted['name'],
			'total_rooms' => $data_posted['total_rooms'],
			'total_bathrooms' => $data_posted['total_bathrooms'],
			'min_stay' => $data_posted['min_stay'],
			'max_guest_normal' => $data_posted['max_guest_normal'],
			'max_guest_extra' => $data_posted['max_guest_extra'],
			'description' => $data_posted['description'],
			'tags' => $data_posted['tags'],
			'accommodations_id' => $data_posted['accommodations_id'],
			'created_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);

		echo "<br>";
		echo "Data Posted";
		echo "<br>";
		print_r($data_posted);
		echo "<br>";

		if($this->db->insert('accommodation_rooms', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodations Room has been added",
				'id' => $this->db->insert_id(),
				'name' => $data_posted['name'],
				'description' => $data_posted['description'],
				'photos' => array(),
				'facilities' => array()
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		/*Get last id*/
		$insert_id = $this->db->insert_id();
		/*Upload photos*/
		$photos = $this->upload_photos();
		/*Create data photos*/
		$response['photos'] = $this->create_photos($photos, $data_posted, $insert_id);
		/*Check and reformat facilities*/
		$facilities = array();
		if(isset($data_posted['facilities'])){
			foreach($data_posted['facilities'] as $f){
				$fac = array(
					'facilities_id' => $f,
				);
				array_push($facilities, $fac);
			}

			/*Create data facilities*/
			$response['facilities'] = $this->create_facilities($facilities, $insert_id);
		}

		// print_r($response);

		// echo $this->db->last_query();
		$this->db->trans_complete();
		return $data;
	}

	function create_do_on_accommodation($data_posted){
		$this->db->trans_start();
		$data = array(
			'name' => $data_posted['name'],
			'total_rooms' => $data_posted['total_rooms'],
			'total_bathrooms' => $data_posted['total_bathrooms'],
			'min_stay' => $data_posted['min_stay'],
			'old_code' => $data_posted['old_code'],
			'max_guest_normal' => $data_posted['max_guest_normal'],
			'max_guest_extra' => $data_posted['max_guest_extra'],
			'description' => $data_posted['description'],
			'accommodations_id' => 0,
			'created_on' => date('Y-m-d H:i:s'),
			'active' => 0
		);

		if($this->db->insert('accommodation_rooms', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodations Room has been added",
				'id' => $this->db->insert_id(),
				'name' => $data_posted['name'],
				'total_rooms' => $data_posted['total_rooms'],
				'total_bathrooms' => $data_posted['total_bathrooms'],
				'photos' => array(),
				'facilities' => array()
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		/*Get last id*/
		$insert_id = $this->db->insert_id();

		/*Upload photos*/
		// $photos = $this->upload_photos();
		/*Create data photos*/
		// $response['photos'] = $this->create_photos($photos, $data_posted, $insert_id);
		/*Check and reformat facilities*/

		$facilities = array();
		if(isset($data_posted['facilities'])){
			foreach($data_posted['facilities'] as $f){
				$fac = array(
					'facilities_id' => $f,
				);
				array_push($facilities, $fac);
			}

			/*Create data facilities*/
			$response['facilities'] = $this->create_facilities($facilities, $insert_id);
		}

		// print_r($response);

		// echo $this->db->last_query();
		$this->db->trans_complete();
		return $response;
	}

	function create_facilities($facilities, $insert_id){
		// echo "<br>===== facilities after reformat =============<br>";
		// print_r($facilities);
		// echo "<br>=========================<br>";
		$responses = array();
		for($i=0; $i< count($facilities); $i++){
			$facility['accommodation_rooms_id'] = $insert_id;
			$facility['facilities_id'] = $facilities[$i]['facilities_id'];
			$facility['created_on'] = date('Y-m-d H:i:s');
			if($this->db->insert('mm_accommodation_rooms_facilities', $facility)){
				$response = array(
					'error' => 0,
					'message' => "Service has been added",
					'id' => $this->db->insert_id()
				);
				array_push($responses, $response);
			}else{
				$response = array(
					'error' => 1,
					'message' => $this->db->error()['message']
				);
				array_push($responses, $response);
			}
		}
		return $responses;
	}

	function create_facility($facility, $insert_id){
		if($this->db->insert('mm_accommodation_rooms_facilities', $facility)){
			$response = array(
				'error' => 0,
				'message' => "Facility has been added",
				'id' => $this->db->insert_id()
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function create_photos($photos, $dataPosted, $insert_id){
		$responsephotos = array();
		for($i=0; $i< count($photos); $i++){
			$photos[$i]['accommodation_rooms_id'] = $insert_id;
			$photos[$i]['created_on'] = date('Y-m-d H:i:s');
			if($this->db->insert('photos', $photos[$i])){
				$photo = array(
					'error' => 0,
					'message' => "Photo has been added",
					'id' => $this->db->insert_id(),
					'name' => $dataPosted['name'],
					'description' => $dataPosted['description'],
				);
				array_push($responsephotos, $photo);
			}else{
				$photo = array(
					'error' => 1,
					'message' => $this->db->error()['message']
				);
				array_push($response->photos, $photo);
			}
		}
		return $responsephotos;
	}

	function upload_photos(){
		$photos = array();
		print_r($_FILES);
		if(file_exists($_FILES['files']['tmp_name'][0])){
			// echo 'ada foto';
				//Photos Not Null
				$filesCount = count($_FILES['files']['name']);
				for($i = 0; $i < $filesCount; $i++){
						$_FILES['userFile']['name'] = $_FILES['files']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['files']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['files']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['files']['size'][$i];
						$new_name = uniqid();
						$uploadPath = 'uploads/photos/accommodations';
						$config['upload_path'] = $uploadPath;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['file_name'] = $new_name;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('userFile')){
								$fileData = $this->upload->data();
								$ty = explode('/', $_FILES['userFile']['type']);
								if($ty[1] == "jpeg"){
									$type = "jpg";
								}else{
									$type = $ty[1];
								}
								$photo['photo'] = "uploads/photos/accommodations/"."".$config['file_name'].".".$type;
								array_push($photos, $photo);
						}else{
							echo $this->upload->display_errors("<span class='error'>", "</span>");
						}
				}
		}else{
			// echo "gada foto";
			// Photos Null
		}
		return $photos;
	}



	function read_all(){
		$query  = $this->db->select('
			accommodation_rooms.id,
			accommodation_rooms.name,
			accommodations.name as accommodation,
			locations.name as location
		');
		$query = $this->db->from('accommodation_rooms');
		$query = $this->db->join('accommodations', 'accommodation_rooms.accommodations_id = accommodations.id');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
		$query = $this->db->where('accommodation_rooms.active', 1);


		if($query = $this->db->get()){
			$accommodations = $query->result();
		}
		return $accommodations;
	}

	function read_by_id($id){
		$query  = $this->db->select('
			accommodation_rooms.id,
			accommodation_rooms.name,
			accommodation_rooms.total_rooms,
			accommodation_rooms.total_bathrooms,
			accommodation_rooms.min_stay,
			accommodation_rooms.max_guest_normal,
			accommodation_rooms.max_guest_extra,
			accommodation_rooms.description,
			accommodation_rooms.tags,
			accommodation_rooms.accommodations_id,
			accommodations.name as accommodation,
			locations.name as location
		');
		$query = $this->db->from('accommodation_rooms');
		$query = $this->db->join('accommodations', 'accommodation_rooms.accommodations_id = accommodations.id');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
		$query = $this->db->where('accommodation_rooms.active', 1);
		$query = $this->db->where('accommodation_rooms.id', $id);
		if($query = $this->db->get()){
			$room = $query->result()[0];
			$room->photos = $this->read_room_photos_by_id($id);
			$room->facilities = $this->read_room_facilities_by_id($id);
		}
		return $room;
	}



	function read_room_photos_by_id($id){
		$query  = $this->db->select('
			photos.id,
			photos.photo,
			photos.description,
			photos.tags
		');
		$query = $this->db->from('photos');
		$query = $this->db->where('accommodation_rooms_id', $id);
		if($query = $this->db->get()){
			$photos = $query->result();
		}
		return $photos;
	}

	function read_room_facilities_by_id($id){
		$query  = $this->db->select('
			mm_accommodation_rooms_facilities.id,
			mm_accommodation_rooms_facilities.facilities_id,
			facilities.name
		');
		$query = $this->db->from('mm_accommodation_rooms_facilities');
		$query = $this->db->join('facilities', 'mm_accommodation_rooms_facilities.facilities_id = facilities.id');
		$query = $this->db->where('accommodation_rooms_id', $id);
		if($query = $this->db->get()){
			$facilities = $query->result();
		}
		return $facilities;
	}

	function read_by_accommodations_id($id){
		$query  = $this->db->select('
			accommodation_rooms.id,
			accommodation_rooms.name,
			accommodation_rooms.total_rooms
		');
		$query = $this->db->from('accommodation_rooms');
		// $query = $this->db->where('accommodation_rooms.active', 1);
		$query = $this->db->where('accommodation_rooms.accommodations_id', $id);
		if($query = $this->db->get()){
			$rooms = $query->result();
		}

		return $rooms;
	}


	function read_like($word){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			locations.name as location,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
		$query = $this->db->where("accommodations.name LIKE '%$word%'");

		if($query = $this->db->get()){
			$results = $query->result();
		}
		$accommodations = array();

		foreach($results as $result){
			$accomodation = new stdClass();
			$accomodation->value = $result->id;
			$accomodation->label = $result->name.", ".$result->location;
			array_push($accommodations, $accomodation);
		}
		return $accommodations;
	}

	function update($data_posted){
		$this->db->trans_start();
		$data = array(
			'name' => $data_posted['name'],
			'total_rooms' => $data_posted['total_rooms'],
			'total_bathrooms' => $data_posted['total_bathrooms'],
			'min_stay' => $data_posted['min_stay'],
			'max_guest_normal' => $data_posted['max_guest_normal'],
			'max_guest_extra' => $data_posted['max_guest_extra'],
			'description' => $data_posted['description'],
			'tags' => $data_posted['tags'],
			'accommodations_id' => $data_posted['accommodations_id'],
			'updated_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);
		$this->db->where('id', $data_posted['idtoupdate']);
		$this->db->update('accommodation_rooms', $data);
		// echo "<br>".$this->db->last_query()."<br>";
		/*Upload photos*/
		$photos = $this->upload_photos();
		/*Create data photos*/
		$response['photos'] = $this->create_photos($photos, $data_posted, $data_posted['idtoupdate']);
		/*Reformat services*/
		$selected_facilities = $this->read_accommodation_room_facilities($data_posted['idtoupdate']);
		print_r($selected_facilities);
		$facilities = array();
		if(isset($data_posted['facilities'])){
			foreach($data_posted['facilities'] as $f){
				$f = array(
					'id' => $data_posted['facility-'.$f],
					'facilities_id' => $f,
				);
				array_push($facilities, $f);
			}
			$response['facilities'] = $this->recheck_facilities($facilities, $selected_facilities, $data_posted['idtoupdate']);
		}else{
			$this->delete_all_mm_accommodation_rooms_facilities_by_accommodations_id($data_posted['idtoupdate']);
		}

		print_r($response);
		$this->db->trans_complete();
		return $data;
	}

	function update_accommodations_id($id, $accommodations_id){
		$data = array(
			'accommodations_id' => $accommodations_id,
			'updated_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('accommodation_rooms', $data);
	}

	function recheck_facilities($facilities, $selected_facilities, $insert_id){
		$count;
		echo "<br>Facilities = <br>";
		print_r($facilities);
		echo "<br>";
		echo "Selected = <br>";
		print_r($selected_facilities);
		echo "<br>";
		if(count($selected_facilities) == 0){
			echo "Create Facilities";
			$response = $this->create_facilities($facilities, $insert_id);
			echo "<br>";
			print_r($response);
		}else{
			// Do Insert
			for($i=0; $i< count($facilities); $i++){
				$count = 0;
				for($j=0; $j< count($selected_facilities); $j++){
					$count++;
					if($facilities[$i]['facilities_id'] != $selected_facilities[$j]->facilities_id){
						if($count == count($selected_facilities)){
							$facility = array(
								'facilities_id' => $facilities[$i]['facilities_id'],
								'accommodation_rooms_id' => $insert_id,
								'created_on' => date('Y-m-d H:i:s')
							);
							$this->create_facility($facility, $insert_id);
						}
					}else{
						break;
					}
				}
			}
			//Do Delete
			for($i=0; $i< count($selected_facilities); $i++){
				$count = 0;
				for($j=0; $j< count($facilities); $j++){
					echo $selected_facilities[$i]->facilities_id." == ".$facilities[$j]['facilities_id']."??";
					echo "<br>";
					$count++;
					if($selected_facilities[$i]->facilities_id != $facilities[$j]['facilities_id']){
						if($count == count($facilities)){
							echo "hapus ini ".$selected_facilities[$i]->id."<br>";
							$this->db->where('id', $selected_facilities[$i]->id);
							if($this->db->delete('mm_accommodation_rooms_facilities')){
								$response = array(
									'error' => 0,
									'message' => "Facility has been deleted",
									'id' => $selected_facilities[$i]->id
								);
							}else{
								$response = array(
									'error' => 1,
									'message' => $this->db->error()['message']
								);
							}
						}
					}else{
						break;
					}

				}
			}

		}
	}

	function read_accommodation_room_facilities($id){
		$query  = $this->db->select('
			mm_accommodation_rooms_facilities.id,
			mm_accommodation_rooms_facilities.facilities_id,
			facilities.name as facility

		');
		$query = $this->db->from('mm_accommodation_rooms_facilities');
		$query = $this->db->join('facilities','mm_accommodation_rooms_facilities.facilities_id = facilities.id');
		$query = $this->db->where('mm_accommodation_rooms_facilities.accommodation_rooms_id', $id);
		if($query = $this->db->get()){
			$facilities = $query->result();
		}
		return $facilities;
	}

	function delete($id){
		$this->db->where('id', $id);
		if($this->db->delete('accommodation_rooms')){
			$response = array(
				'error' => 0,
				'message' => "Room has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		$this->delete_all_mm_accommodation_rooms_facilities_by_accommodations_id($id);
		return $response;
	}

	function delete_all_mm_accommodation_rooms_facilities_by_accommodations_id($id){
		$this->db->where('accommodation_rooms_id', $id);
		if($this->db->delete('mm_accommodation_rooms_facilities')){
			$response = array(
				'error' => 0,
				'message' => "Facilities has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function count(){
		$this->db->where('active', 1);
		$this->db->from('accommodation_rooms');
		return $this->db->count_all_results();
	}

	function api_read_by_id($id){
		$query  = $this->db->select('
			accommodation_rooms.id,
			accommodation_rooms.name,
			accommodation_rooms.total_rooms,
			accommodation_rooms.total_bathrooms,
			accommodation_rooms.max_guest_normal,
			accommodation_rooms.max_guest_extra,
			accommodation_rooms.max_guest_infant,
			accommodation_rooms.description,
			accommodation_rooms.tags,
			accommodation_rooms.accommodations_id,
			accommodations.id as accommodations_id,
			accommodations.name as accommodations_name,
			accommodations.address as address,
			accommodations.website as website,
			accommodations.lat as lat,
			accommodations.lng as lng,
			accommodations.description as accommodations_description,
			accommodations.ics_url as ics_url,
			accommodations.ics as ics,
			accommodations.accommodation_types_id as accommodation_types_id,
			accommodations.locations_id as locations_id
		');
		$query = $this->db->from('accommodation_rooms');
		$query = $this->db->where('accommodation_rooms.active', 1);
		$query = $this->db->where('accommodation_rooms.id', $id);
		$query = $this->db->join('accommodations', 'accommodation_rooms.accommodations_id = accommodations.id');
		if($query = $this->db->get()){
			$room = $query->result()[0];
		}
		return $room;
	}

	function api_read_prices_by_id(){

	}

}
?>
