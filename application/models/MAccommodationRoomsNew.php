<?php
class MAccommodationRoomsNew extends CI_Model{

	public $id;
	public $code;
	public $old_code;
	public $name;
	public $total_rooms;
	public $total_beds;
	public $occupancy;
	public $min_stay;
	public $max_guest_normal;
	public $max_guest_extra;
	public $max_guest_infant;
	public $total_bathrooms;
	public $description;
	public $ics_url;
	public $ics;
	public $tags;
	public $accommodations_id;
	public $renting_type;
	public $synced;
	public $airbnb_id;
	public $created_on;
	public $updated_on;
	public $active;


	function __construct(){
  	parent::__construct();

  }

	public function create(){
		if($this->db->insert('accommodation_rooms', $this)){
			$this->id = $this->db->insert_id();
			$response = $this;
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// print_r($response);
		return $response;
	}

	public function readById(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('accommodation_rooms');
		$query = $this->db->where('accommodation_rooms.id', $this->id);

		if($query = $this->db->get()){
			$room_settings = $query->result()[0];
			$response = $room_settings;
		}else{
			$response = $this->db->error()['message'];
		}

		$query_facilities  = $this->db->select('
			*
		');
		$query_facilities = $this->db->from('mm_accommodation_rooms_facilities');
		$query_facilities = $this->db->where('mm_accommodation_rooms_facilities.accommodation_rooms_id', $this->id);
		if($query_facilities = $this->db->get()){
			$room_facilities = $query_facilities->result();
			$response->facilities  = $room_facilities;
		}
		return $response;
	}

	public function readByAccommodationsId(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('accommodation_rooms');
		$query = $this->db->where('accommodation_rooms.accommodations_id', $this->accommodations_id);
		$query = $this->db->order_by("accommodation_rooms.total_rooms", "asc");

		if($query = $this->db->get()){
			$room_settings = $query->result();
			return $room_settings;
		}else{
			return $this->db->error()['message'];
		}
	}
	

	function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodation_rooms', $data)){
			$response = array(
				'error' => 0,
				'message' => "Room has been updated",
				'id' => $this->id,
				'name' => $this->name,
				'total_rooms' => $this->total_rooms,
				'total_beds' => $this->total_beds,
				'total_bathrooms' => $this->total_bathrooms,
				'min_stay' => $this->min_stay,
				'max_guest_normal' => $this->max_guest_normal,
				'max_guest_extra' => $this->max_guest_extra,
				'renting_type' => $this->renting_type,
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null && $value!=""){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

	// public function update(){
	// 	$this->db->where('id', $this->id);
	// 	if($this->db->update('accommodation_rooms', $this)){
	// 		$response = array(
	// 			'error' => 0,
	// 			'message' => "Room has been updated",
	// 			'id' => $this->id,
	// 			'name' => $this->name,
	// 			'description' => $this->description,
	// 		);
	// 	}else{
	// 		$response = array(
	// 			'error' => 1,
	// 			'message' => $this->db->error()['message']
	// 		);
	// 	}
	// 	return $response;
	// }

	public function updateAirbnbId(){
		$data = array(
			'airbnb_id' => $this->airbnb_id
		);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodation_rooms', $data)){
			$response = array(
				'error' => 0,
				'message' => "Airbnb Id has been added"
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function updateAccommodationsId(){
		$data = array(
			'accommodations_id' => $this->accommodations_id
		);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodation_rooms', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodations Id has been added"
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

}
?>
