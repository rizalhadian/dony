<?php
class MProduk extends CI_Model{

	public $id;
	public $userid;
	public $produk_jenis_2_id;
	public $nama;
	public $stock;
	public $harga;
	public $berat;
	public $foto;
	public $deskripsi;
	public $view_count;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  		parent::__construct();
		// $this->load->library('Ical');
  	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('produk', $this)){
			$response = array(
				'error' => 0,
				'message' => "Produk has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readAll($page){
		// $query  = $this->db->distinct('produk.id');
		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.view_count,
			produk.deskripsi,
			produk_jenis_2.nama as produk_jenis_2_nama,
			produk_jenis_1.nama as produk_jenis_1_nama,
			photos.url as photo,
			users.username as toko,
			users.province as province,
			users.city as city,

		');
		$limit = 16;
		$start = $page;
		$query = $this->db->from('produk');
		$query = $this->db->group_by('produk.id');
		$query = $this->db->join('produk_jenis_2', 'produk.produk_jenis_2_id = produk_jenis_2.id');
		$query = $this->db->join('produk_jenis_1', 'produk_jenis_2.produk_jenis_1_id = produk_jenis_1.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');
		$query = $this->db->join('users', 'produk.userid = users.id');
		if(isset($this->session->userdata('user')->id)){
			$query = $this->db->where('produk.userid != '.$this->session->userdata('user')->id);
		}

		

		$query = $this->db->limit($limit, $start);
		// $query = $this->db->where('accommodations.active != 2');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function count(){
		$query  = $this->db->select('
			COUNT(produk.id) as count,
		');
		$query = $this->db->from('produk');

		if($query = $this->db->get()){
			$response = $query->result()[0]->count;
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}


	public function readById($id){
		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.deskripsi,
			produk.view_count,
			produk_jenis_2.nama as produk_jenis_2_nama,
			produk_jenis_1.nama as produk_jenis_1_nama,
			users.username as toko,
			users.id as tokoid,
			users.province_id as province_id,
			users.city_id as city_id,
			users.phone,
			users.photo,
			users.deskripsi as catatan_pelapak,
			users.created_on as user_created_on
		');
		$query = $this->db->from('produk');
		$query = $this->db->join('produk_jenis_2', 'produk.produk_jenis_2_id = produk_jenis_2.id');
		$query = $this->db->join('produk_jenis_1', 'produk_jenis_2.produk_jenis_1_id = produk_jenis_1.id');
		$query = $this->db->join('users', 'produk.userid = users.id');

		$query = $this->db->where('produk.id', $id);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByTokoDanProduk($toko, $produk){
		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.deskripsi,
			produk.view_count,
			produk_jenis_2.nama as produk_jenis_2_nama,
			produk_jenis_1.nama as produk_jenis_1_nama,
			users.username as toko,
			users.id as tokoid,
			users.province_id as province_id,
			users.city_id as city_id,
			users.phone,
			users.photo,
			users.deskripsi as catatan_pelapak,
			users.created_on as user_created_on
		');
		$query = $this->db->from('produk');
		$query = $this->db->join('produk_jenis_2', 'produk.produk_jenis_2_id = produk_jenis_2.id');
		$query = $this->db->join('produk_jenis_1', 'produk_jenis_2.produk_jenis_1_id = produk_jenis_1.id');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$query = $this->db->where('users.username', $toko);
		$query = $this->db->where('produk.nama', $produk);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByUserId($user_id){
		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.deskripsi,
			produk.view_count,
			produk_jenis_2.nama as produk_jenis_2_nama,
			produk_jenis_1.nama as produk_jenis_1_nama,
			photos.url as photo,
			users.username as toko,
			users.id as tokoid,
			users.deskripsi as catatan_pelapak
		');
		$query = $this->db->from('produk');
		$query = $this->db->join('produk_jenis_2', 'produk.produk_jenis_2_id = produk_jenis_2.id');
		$query = $this->db->join('produk_jenis_1', 'produk_jenis_2.produk_jenis_1_id = produk_jenis_1.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$this->db->group_by('produk.id');


		$query = $this->db->where('produk.userid', $user_id);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}



	public function readLikeByName($name){
		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.view_count,
			produk.deskripsi,
			produk_jenis_2.nama as produk_jenis_2_nama,
			produk_jenis_1.nama as produk_jenis_1_nama,
			photos.url as photo,
			users.username as toko,
			users.id as tokoid,
			users.province as province,
			users.city as city,

		');
		$query = $this->db->from('produk');
		$query = $this->db->join('produk_jenis_2', 'produk.produk_jenis_2_id = produk_jenis_2.id');
		$query = $this->db->join('produk_jenis_1', 'produk_jenis_2.produk_jenis_1_id = produk_jenis_1.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$query = $this->db->like('produk.nama', $name);
		if(isset($this->session->userdata('user')->id)){
			$query = $this->db->where('produk.userid != '.$this->session->userdata('user')->id);
		}
		$this->db->group_by('produk.id');


		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByKategori($kat){
		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.view_count,
			produk.deskripsi,
			photos.url as photo,
			users.username as toko,
			users.id as tokoid,
			users.province as province,
			users.city as city,
			kategori.nama as kategori_nama

		');
		$query = $this->db->from('produk');
		$query = $this->db->join('produk_kategori', 'produk.id = produk_kategori.produk_id');
		$query = $this->db->join('kategori', 'produk_kategori.kategori_id = kategori.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$query = $this->db->where('kategori.id', $kat);
		if(isset($this->session->userdata('user')->id)){
			$query = $this->db->where('produk.userid != '.$this->session->userdata('user')->id);
		}

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByKategori2($kat1, $kat2){
		$this->db->distinct();

		$query  = $this->db->select('
			produk.id,
			produk.userid,
			produk.nama,
			produk.stock,
			produk.harga,
			produk.berat,
			produk.foto,
			produk.view_count,
			produk.deskripsi,
			photos.url as photo,
			users.username as toko,
			users.id as tokoid,
			users.province as province,
			users.city as city,
			

		');
		$query = $this->db->from('produk');
		// $query = $this->db->join('produk_kategori', 'produk.id = produk_kategori.produk_id');
		// $query = $this->db->join('kategori', 'produk_kategori.kategori_id = kategori.id');
		// $query = $this->db->join('kategori as k2', 'produk_kategori.kategori_id = k2.id');
		// $query = $this->db->join('kategori as kat2', 'produk_kategori.kategori_id = kat2.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$query = $this->db->where("exists(SELECT * FROM produk as p2 JOIN produk_kategori ON p2.id = produk_kategori.produk_id JOIN kategori ON produk_kategori.kategori_id = kategori.id  WHERE produk.id = p2.id AND kategori.nama = '".$kat1."')");
		$query = $this->db->where("exists(SELECT * FROM produk as p3 JOIN produk_kategori ON p3.id = produk_kategori.produk_id JOIN kategori as k2 ON produk_kategori.kategori_id = k2.id  WHERE produk.id = p3.id AND k2.nama = '".$kat2."')");
		
		// $query = $this->db->where('kat1.nama = "'.$kat1.'" AND kat2.nama = "'.$kat2.'"');
		// $query = $this->db->where('kat1.nama IN ("'.$kat1.'","'.$kat2.'")');

		if(isset($this->session->userdata('user')->id)){
			// $query = $this->db->where('produk.userid != '.$this->session->userdata('user')->id);
		}
		$this->db->distinct();
		$this->db->group_by('produk.id');
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		// return $this->db->last_query();
		return $response;
	}



	

	public function create_photos($photos, $dataPosted, $insert_id){
		$responsephotos = array();
		for($i=0; $i< count($photos); $i++){
			$photos[$i]['accommodations_id'] = $insert_id;
			$photos[$i]['created_on'] = date('Y-m-d H:i:s');
			if($this->db->insert('photos', $photos[$i])){
				$photo = array(
					'error' => 0,
					'message' => "Photo has been added",
					'id' => $this->db->insert_id(),
					'name' => $dataPosted['name'],
					'description' => $dataPosted['description'],
				);
				array_push($responsephotos, $photo);
			}else{
				$photo = array(
					'error' => 1,
					'message' => $this->db->error()['message']
				);
				array_push($response->photos, $photo);
			}
		}
		return $responsephotos;
	}

	function upload_photos(){
		$photos = array();
		// print_r($_FILES);
		if(file_exists($_FILES['files']['tmp_name'][0])){
			// echo 'ada foto';
				//Photos Not Null
				$filesCount = count($_FILES['files']['name']);
				for($i = 0; $i < $filesCount; $i++){
						$_FILES['userFile']['name'] = $_FILES['files']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['files']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['files']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['files']['size'][$i];
						$new_name = uniqid();
						$uploadPath = 'uploads/photos/accommodations';
						$config['upload_path'] = $uploadPath;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['file_name'] = $new_name;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('userFile')){
								$fileData = $this->upload->data();
								$ty = explode('/', $_FILES['userFile']['type']);
								if($ty[1] == "jpeg"){
									$type = "jpg";
								}else{
									$type = $ty[1];
								}
								$photo['photo'] = "uploads/photos/accommodations/"."".$config['file_name'].".".$type;
								array_push($photos, $photo);
						}else{
							echo $this->upload->display_errors("<span class='error'>", "</span>");
						}
				}
		}else{
			// echo "gada foto";
			// Photos Null
		}
		return $photos;
	}

	public function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('produk', $data)){
			$response = array(
				'error' => 0,
				'message' => "Produk has been updated",
				'id' => $this->id,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	public function delete(){
		$this->db->where('id', $this->id);
		if($this->db->delete('produk')){
			$response = array(
				'error' => 0,
				'message' => "Produk has been deleted",
				'id' => $this->id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
