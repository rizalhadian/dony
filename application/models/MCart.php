<?php
class MCart extends CI_Model{

	public $id;
    public $sessionid;
    public $userid;
    public $total_harga;
    public $status;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		// $this->load->library('Ical');
  }

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('cart', $this)){
			$response = array(
				'error' => 0,
				'message' => "Cart has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readBySessionId($sessionid){
		$query  = $this->db->select('
            cart.id,
            cart.sessionid,
            cart.userid,
            cart.total_harga,
            cart.status,
            cart.created_on,
            cart.updated_on
		');
		$query = $this->db->from('cart');
		$query = $this->db->where('cart.sessionid', $sessionid);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
    }
    

	public function readByUserId($userid){
		$query  = $this->db->select('
			cart.id,
            cart.sessionid,
            cart.userid,
            cart.total_harga,
            cart.status,
            cart.created_on,
            cart.updated_on
		');
		$query = $this->db->from('cart');
		$query = $this->db->where('cart.userid', $userid);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('cart', $data)){
			$response = array(
				'error' => 0,
				'message' => "Cart has been updated",
				'id' => $this->id,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

	

	

}
?>
