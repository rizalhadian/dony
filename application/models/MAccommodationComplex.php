<?php

class MAccommodationComplex extends CI_Model{

  public $id;
  public $name;
  public $total_accommodations;
  public $total_rooms;
  public $total_beds;
  public $locations_id;
  public $address;
  public $lat;
  public $lng;
  public $partners_id;
  public $created_on;
  public $updated_on;

  function __construct(){
  	parent::__construct();

  }

  public function index(){

  }

  public function create(){
    $data = $this->getDataNotNullOnly($this);
    if($this->db->insert('accommodation_complex', $data)){
			$response = $this;
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
  }

  public function readById(){
    $query  = $this->db->select('
      accommodation_complex.id,
      accommodation_complex.name,
      accommodation_complex.total_accommodations,
      accommodation_complex.total_rooms,
      accommodation_complex.total_beds,
      accommodation_complex.locations_id,
      accommodation_complex.address,
      accommodation_complex.lat,
      accommodation_complex.lng,
      accommodation_complex.partners_id,
      accommodation_complex.created_on,
      accommodation_complex.updated_on,
      locations.name as location
    ');
		$query = $this->db->from('accommodation_complex');
    $query = $this->db->join('locations', 'accommodation_complex.locations_id = locations.id', 'left outer');
		$query = $this->db->where('accommodation_complex.id', $this->id);
		if($query = $this->db->get()){
			$partner = $query->result()[0];
		}
		return $partner;
  }

  public function readByPartnersId(){
    $query  = $this->db->select('
      accommodation_complex.id,
      accommodation_complex.name,
      accommodation_complex.total_accommodations,
      accommodation_complex.total_rooms,
      accommodation_complex.total_beds,
      accommodation_complex.locations_id,
      accommodation_complex.address,
      accommodation_complex.lat,
      accommodation_complex.lng,
      accommodation_complex.partners_id,
      accommodation_complex.created_on,
      accommodation_complex.updated_on,
      locations.name as location
    ');
		$query = $this->db->from('accommodation_complex');
    $query = $this->db->join('locations', 'accommodation_complex.locations_id = locations.id', 'left outer');
		$query = $this->db->where('accommodation_complex.partners_id', $this->partners_id);
		if($query = $this->db->get()){
			$partner = $query->result();
		}
		return $partner;
  }

  public function update(){
    $data = $this->getDataNotNullOnly($this);
    $this->db->where('id', $this->id);
		$this->db->update('accommodation_complex', $data);
    return $this;
  }

  public function delete(){
    $this->db->where('id', $this->id);
		if($this->db->delete('accommodation_complex')){
			$response = array(
				'error' => 0,
				'message' => "Complex has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
  }

  private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null && $value!=""){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

}
?>
