<?php
class MProdukJenis2 extends CI_Model{

	public $id;
	public $userid;
	public $produk_jenis_1_id;
	public $nama;
	public $stock;
	public $harga;
	public $foto;
	public $deskripsi;
	public $view_count;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		$this->load->library('Ical');
  }

	function readAll(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('produk_jenis_2');
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	function readAllByProdukJenis1Id(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('produk_jenis_2');
		$query = $this->db->where('produk_jenis_2.produk_jenis_1_id', $this->produk_jenis_1_id);
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

}
?>
