<?php
class MProdukTag extends CI_Model{

	public $id;
	public $produkid;
	public $tagid;
	public $is_from_admin;
	
	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
		parent::__construct();
			
	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('produk_tag', $this)){
			$response = array(
				'error' => 0,
				'message' => "Produk Tag has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readByProdukId(){
		$query  = $this->db->select('
			produk_tag.id,
			produk_tag.produkid,
			produk_tag.tagid,
			produk_tag.is_from_admin,
			tag.name
		');
		$query = $this->db->from('produk_tag');
		$query = $this->db->where('produk_tag.produkid', $this->produkid);
		$query = $this->db->join('tag', 'produk_tag.tagid = tag.id');
		
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	

}
?>
