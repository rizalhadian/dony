<?php
class MTag extends CI_Model{

	public $id;
	public $name;
	
	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
		parent::__construct();
			
	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('tag', $this)){
			$response = array(
				'error' => 0,
				'message' => "Tag has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function isExistByName(){
		$query  = $this->db->select('
			COUNT(tag.id) as count
		');
		$query = $this->db->from('tag');
		$query = $this->db->where('tag.name', $this->name);

		if($query = $this->db->get()){
			$response = $query->result()[0]->count;
			if($response==1){
				return true;
			}elseif($response==0){
				return false;
			}
		}else{
			$response = $this->db->error()['message'];
		}
		// return $response;
	}

	public function readByName(){
		$query  = $this->db->select('
			tag.id,
			tag.name,
			tag.is_from_admin,
		');
		$query = $this->db->from('tag');
		$query = $this->db->where('tag.name', $this->name);

		if($query = $this->db->get()){
			$response = $query->result()[0];
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	

}
?>
