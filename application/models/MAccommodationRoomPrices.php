<?php
class MAccommodationRoomPrices extends CI_Model{

	function __construct(){
  	parent::__construct();

  }

	function create($dataPosted){
		$this->db->trans_start();
		$data = array(
			'name' => $dataPosted['name'],
			'rate' => $dataPosted['rate'],
			'rate_mature_extra' => $dataPosted['rate_mature_extra'],
			'rate_children_extra' => $dataPosted['rate_children_extra'],
			'rate_infant_extra' => $dataPosted['rate_infant_extra'],
			'type' => $dataPosted['type'],
			'date_start' => date('Y-m-d', strtotime($dataPosted['date_start'])),
			'date_end' => date('Y-m-d', strtotime($dataPosted['date_end'])),
			'accommodation_rooms_id' => $dataPosted['accommodation_rooms_id'],
			'accommodations_id' => $dataPosted['accommodations_id'],
			'description' => $dataPosted['description'],
			'created_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);

		if($dataPosted['type'] == 0){
			$data['name'] = "Base Rate";
			$data['date_start'] = '0000-00-00';
			$data['date_end'] = '0000-00-00';
		}

		if($this->db->insert('rates', $data)){
			$response = array(
				'error' => 0,
				'message' => "Price has been added",
				'id' => $this->db->insert_id(),
				'name' => $data['name'],
				'type' => $dataPosted['type'],
				'rate' => $dataPosted['rate'],
				'rate_mature_extra' => $dataPosted['rate_mature_extra'],
				'rate_children_extra' => $dataPosted['rate_children_extra'],
				'rate_infant_extra' => $dataPosted['rate_infant_extra'],
				'date_start' => date('Y-m-d', strtotime($dataPosted['date_start'])),
				'date_end' => date('Y-m-d', strtotime($dataPosted['date_end'])),
				'description' => $dataPosted['description'],
			);

			if($dataPosted['type'] == 0){
				$response['name'] = "Base Rate";
				$response['type'] = "Base Rate";
				$response['date_start'] = '0000-00-00';
				$response['date_end'] = '0000-00-00';
			}elseif($dataPosted['type'] == 1){
				$response['type'] = "Seasonal";
			}elseif($dataPosted['type'] == 2){
				$response['type'] = "Promo";
			}


		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// echo $this->db->last_query();
		$this->db->trans_complete();
		return $response;

	}

	function read_all(){
		$query  = $this->db->select('
			rates.id,
			rates.name,
			rates.rate_mature,
			rates.rate_mature_owner,
			rates.date_start,
			rates.date_end,
			rates.type,
			rates.description,
			accommodation_rooms.name as room,
			accommodations.name as accommodation,
			locations.name as location
		');
		$query = $this->db->from('rates');
		$query = $this->db->join('accommodation_rooms', 'accommodation_rooms.id = rates.accommodation_rooms_id');
		$query = $this->db->join('accommodations', 'accommodations.id = accommodation_rooms.accommodations_id');
		$query = $this->db->join('locations', 'locations.id = accommodations.locations_id');
		// $query = $this->db->where('rates.active', 1);


		if($query = $this->db->get()){
			$accommodations = $query->result();
		}

		print_r($this->db->error()['message']);

		return $accommodations;
	}

	function read_all_by_accommodation_rooms_id($id){
		$query  = $this->db->select('
			rates.id,
			rates.name,
			rates.rate,
			rates.rate_owner,
			rates.date_start,
			rates.date_end,
			rates.type,
			rates.description');
		$query = $this->db->from("rates");
		$query = $this->db->where('rates.accommodation_rooms_id', $id);

		if($query = $this->db->get()){
			$rates = $query->result();
		}
		return $rates;
	}

	function read_by_accommodations_id_base_rate($id){
		$query  = $this->db->select('
			rates.id,
			rates.name,
			rates.rate,
			rates.accommodation_rooms_id,
		');
		$query = $this->db->from('rates');
		$query = $this->db->where('rates.active', 1);
		$query = $this->db->where('rates.type', 0);
		$query = $this->db->where('rates.accommodations_id', $id);

		if($query = $this->db->get()){
			$accommodations = $query->result();
		}
		return $accommodations;
	}



	// function read_like($word){
	// 	$query  = $this->db->select('
	// 		accommodations.id,
	// 		accommodations.name,
	// 		locations.name as location,
	// 	');
	// 	$query = $this->db->from('rates');
	// 	$query = $this->db->join('locations', 'accommodations.locations_id = locations.id');
	// 	$query = $this->db->where("accommodations.name LIKE '%$word%'");
	//
	// 	if($query = $this->db->get()){
	// 		$results = $query->result();
	// 	}
	// 	$accommodations = array();
	//
	// 	foreach($results as $result){
	// 		$accomodation = new stdClass();
	// 		$accomodation->value = $result->id;
	// 		$accomodation->label = $result->name.", ".$result->location;
	// 		array_push($accommodations, $accomodation);
	// 	}
	// 	return $accommodations;
	// }

	function update($dataPosted){
		$data = array(
			'name' => $dataPosted['name'],
			'description' => $dataPosted['description'],
			'updated_on' => date('Y-m-d H:i:s')
		);
		$this->db->where('id', $dataPosted['id']);
		if($this->db->update('rates', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodation has been updated",
				'id' => $dataPosted['id'],
				'name' => $dataPosted['name'],
				'description' => $dataPosted['description'],
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function delete($id){
		$this->db->where('id', $id);
		if($this->db->delete('rates')){
			$response = array(
				'error' => 0,
				'message' => "Rate has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function delete_by_accommodation_rooms_id($accommodation_rooms_id){
		$this->db->where('accommodation_rooms_id', $accommodation_rooms_id);
		if($this->db->delete('rates')){
			$response = array(
				'error' => 0,
				'message' => "Rate has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function count(){
		$this->db->where('active', 1);
		$this->db->from('rates');
		return $this->db->count_all_results();
	}

	function api_read_by_accommodation_rooms_id($id){
		$query  = $this->db->select('
			rates.id,
			rates.name,
			rates.rate,
			rates.rate_extra,
			rates.rate_owner,
			rates.rate_mature,
			rates.rate_mature_p,
			rates.rate_children,
			rates.rate_children_p,
			rates.rate_infant,
			rates.rate_infant_p,
			rates.minimum_stay,
			rates.date_start,
			rates.date_end,
			rates.type,
			rates.description
		');

		$query = $this->db->where('rates.active', 1);
		$query = $this->db->where('rates.accommodation_rooms_id ', $id);
		$query = $this->db->from("rates");


		if($query = $this->db->get()){
			$rates = $query->result();
		}
		return $rates;
	}

}
?>
