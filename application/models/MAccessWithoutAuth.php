<?php
class MAccessWithoutAuth extends CI_Model{

	public $id;
	public $unique_id;
	public $accommodations_id;
	public $partners_id;
	public $unique_key;
	public $created_on;
	public $updated_on;

	function __construct(){
  	parent::__construct();
  }

	public function create(){

		if($this->accommodations_id != ""){
			$query  = $this->db->select('
				*
			');
			$query = $this->db->from('access_without_auth');
			$query = $this->db->where('access_without_auth.accommodations_id', $this->accommodations_id);
			if($query = $this->db->get()){
					if(isset($query->result()[0])){
						$response = $query->result()[0];
					}else{
						if($this->db->insert('access_without_auth', $this)){
							$this->id = $this->db->insert_id();
							$response = $this;
						}else{
							$response = array(
								'error' => 1,
								'message' => $this->db->error()['message']
							);
						}
					}
			}
		}

		if($this->partners_id != ""){
			$query  = $this->db->select('
				*
			');
			$query = $this->db->from('access_without_auth');
			$query = $this->db->where('access_without_auth.partners_id', $this->partners_id);
			if($query = $this->db->get()){
					if(isset($query->result()[0])){
						$response = $query->result()[0];
					}else{
						if($this->db->insert('access_without_auth', $this)){
							$this->id = $this->db->insert_id();
							$response = $this;
						}else{
							$response = array(
								'error' => 1,
								'message' => $this->db->error()['message']
							);
						}
					}
			}
		}



		return $response;
		// return $this->accommodations_id;
	}

	public function readByUniqueId(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('access_without_auth');
		$query = $this->db->where('access_without_auth.unique_id', $this->unique_id);
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	private function isExistedByAccommodationsId(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('access_without_auth');
		$query = $this->db->where('access_without_auth.accommodations_id', $this->accommodations_id);
	}

	private function isExistedByPartnersId(){

	}



	public function hasUniqueId(){
    if($this->input->post("unique_id") == null){
			return false;
    }else{
      $this->unique_id = $this->input->post("unique_id");
      $response = $this->readByUniqueId();
      if($response != null){
        return true;
      }else{
        return false;
      }
    }
  }



}
?>
