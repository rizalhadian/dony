<?php
class MMessageRooms extends CI_Model{

		public $id;
		public $created_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  		parent::__construct();
		// $this->load->library('Ical');
  	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('message_rooms', $this)){
			$response = array(
				'error' => 0,
				'message' => "room has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}
	

    public function readByUserid($guest1, $guest2){
				// $query2 = $this->db;
				// $query2->select('COUNT(message_rooms2.id)');
				// $query2->from('message_rooms as message_rooms2');
				// $query2->join('message_room_guests as message_room_guests2', 'message_rooms2.id = message_room_guests2.messageroomid');
				// $query2->where('message_room_guests2.userid', $guest2);
				// $query2->distinct()
				
				$query = $this->db;
				$query->select('
					message_rooms.id,
				
				');
				
				$query->from('message_rooms');
				$query->join('message_room_guests', 'message_rooms.id = message_room_guests.messageroomid');
				$query->join('message_room_guests as message_room_guests2', 'message_rooms.id = message_room_guests2.messageroomid');
				// $query = $this->db->join('messages', 'messages.messageroomid = message_rooms.id');
				
				
				
				// $query->where('(message_room_guests.userid = "'.$guest1.'" AND message_room_guests.userid = "'.$guest2.'")');
				$query->where('(message_room_guests.userid = "'.$guest1.'")');
				$query->where('(message_room_guests2.userid = "'.$guest2.'")');
				// $query->distinct();
				// $query->order_by("id", "asc");
				// return $this->pengirimid." ".$this->penerimaid;

				if($query_response = $query->get()){
					$response = $query_response->result();
					return $response;
				}else{
					return  $this->db->error()['message'];
				}
	
		}
		
		public function readByUserid2($guest){
			$query  = $this->db->select('
				message_rooms.id,
			
			');
		$query = $this->db->from('message_rooms');
		$query = $this->db->join('message_room_guests', 'message_rooms.id = message_room_guests.messageroomid');
		// $query = $this->db->join('messages', 'messages.messageroomid = message_rooms.id');
		$query = $this->db->where('(message_room_guests.userid = "'.$guest.'")');
		$this->db->distinct();
		$this->db->order_by("id", "asc");
		// return $this->pengirimid." ".$this->penerimaid;

		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}

	}
    
    public function readMessages(){
		$query  = $this->db->select('
            messages.id,
            messages.pengirimid,
            messages.penerimaid,
            messages.message,
            messages.created_on,
            messages.is_read,
            pengirim.username as usernamepengirim,
            penerima.username as usernamepenerima
		');
		$query = $this->db->from('messages');
		$query = $this->db->join('users as pengirim', 'messages.pengirimid = pengirim.id');
		$query = $this->db->join('users as penerima', 'messages.penerimaid = penerima.id');
		// $query = $this->db->where('messages.pengirimid = "'.$this->penerimaid.'" AND messages.penerimaid = "'.$this->pengirimid.'"');
		$query = $this->db->where('(messages.pengirimid = "'.$this->pengirimid.'" OR messages.penerimaid = "'.$this->pengirimid.'")');
		$this->db->distinct();
		$this->db->order_by("messages.created_on", "desc");


		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}
    }

	

	



	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
