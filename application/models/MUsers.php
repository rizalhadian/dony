<?php
class MUsers extends CI_Model{

	public $id;
	public $first_name;
	public $last_name;
	public $email;
	public $phone;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		$this->load->library('Ical');
  }

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('toko', $this)){
			$response = array(
				'error' => 0,
				'message' => "Toko has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	function readById($id){
		$query  = $this->db->select('
			users.id,
			users.type_id,
			users.ip_address,
			users.username,
			users.password,
			users.salt,
			users.email,
			users.first_name,
			users.last_name,
			users.phone,
			users.photo,
			users.province_id,
			users.city_id,
			users.lat,
			users.lng,
			users.address,
			users.deskripsi,
			users.is_detail_setted,
			users.created_on
		');
		$query = $this->db->from('users');
		$query = $this->db->where('users.id', $id);
		
		if($query = $this->db->get()){
			$accommodations = $query->result()[0];
			// $accommodations->photos = $this->read_accommodation_photos_by_id($id);
		}
		return $accommodations;
	}

	function readByUsername($username){
		$query  = $this->db->select('
			users.id,
			users.type_id,
			users.ip_address,
			users.username,
			users.password,
			users.salt,
			users.email,
			users.first_name,
			users.last_name,
			users.phone,
			users.photo,
			users.province_id,
			users.city_id,
			users.lat,
			users.lng,
			users.deskripsi,

			users.address,
			users.is_detail_setted,
			users.created_on

		');
		$query = $this->db->from('users');
		$query = $this->db->where('users.username', $username);
		
		if($query = $this->db->get()){
			$accommodations = $query->result()[0];
			// $accommodations->photos = $this->read_accommodation_photos_by_id($id);
		}
		return $accommodations;
	}

	function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('users', $data)){
			$response = array(
				'error' => 0,
				'message' => "User Profile has been updated",
				'id' => $this->id,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;

	}

	function delete($id){
		$this->db->where('id', $id);
		if($this->db->delete('accommodations')){
			$response = array(
				'error' => 0,
				'message' => "Accommodation has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function count(){
		$this->db->where('active', 1);
		$this->db->from('accommodations');
		return $this->db->count_all_results();
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null && $value!=""){
				$new_data->$key = $value;
			}
		}
		$new_data->updated_on = date("Y-m-d h:m:s");
		return $new_data;
	}

}
?>
