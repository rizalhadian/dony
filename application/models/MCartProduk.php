<?php
class MCartProduk extends CI_Model{

	public $id;
	public $sessid;
	public $userid;
	public $userid_produk;
	public $invoiceid;
	public $cartid;
  public $produkid;
  public $qty;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		// $this->load->library('Ical');
  }

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('cart_produk', $this)){
			$response = array(
				'error' => 0,
				'message' => "Produk has been added to Cart ",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readBySessId($sessid){
		$query  = $this->db->select('
			cart_produk.id,
			cart_produk.sessid,
			cart_produk.userid,
			cart_produk.produkid,
			cart_produk.qty,
			cart_produk.created_on,
			cart_produk.updated_on,
			produk.nama as nama,
			produk.harga as harga,
			produk.berat,
			produk.userid as userid_toko,
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.sessid', $sessid);
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->order_by('produk.userid');
		// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	

	public function readByUserId($userid){
		$query  = $this->db->select('
			cart_produk.id,
			cart_produk.sessid,
			cart_produk.userid,
			cart_produk.produkid,
			cart_produk.qty,
			cart_produk.created_on,
			cart_produk.updated_on,
			produk.nama as nama,
			produk.harga as harga,
			produk.berat,
			produk.userid as userid_toko,
			
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.userid', $userid);
		$query = $this->db->where('cart_produk.invoiceid', 0);
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->order_by('produk.userid');

		// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}


	public function readByInvoiceId(){
		$query  = $this->db->select('
			cart_produk.id,
			cart_produk.sessid,
			cart_produk.userid,
			cart_produk.produkid,
			cart_produk.qty,
			cart_produk.total_ongkir,
			cart_produk.total_berat,
			cart_produk.ongkir_perkilo,
			cart_produk.created_on,
			cart_produk.updated_on,
			produk.nama as nama,
			produk.harga as harga,
			produk.berat,
			produk.userid as userid_toko,
			users.username as toko_username,
			invoice.kode as invoice_kode,
			invoice.total as invoice_total,
			invoice.provinsi as invoice_provinsi,
			invoice.kota as invoice_kota,
			invoice.alamat as alamat,
			

			
		');
		$query = $this->db->from('cart_produk');
		if($this->userid!=0){
			$query = $this->db->where('cart_produk.userid', $this->userid);
		}
		$query = $this->db->where('cart_produk.invoiceid', $this->invoiceid);
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$query = $this->db->join('invoice', 'cart_produk.invoiceid = invoice.id');
		$query = $this->db->order_by('produk.userid');

		// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByUserIdDistinctToko($userid){
		$query  = $this->db->select('
			DISTINCT(produk.userid)		
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.userid', $userid);
		$query = $this->db->where('cart_produk.invoiceid', 0);

		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->order_by('produk.userid');
		// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readBySessIdDistinctToko($sessid){
		$query  = $this->db->select('
			DISTINCT(produk.userid)		
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.sessid', $sessid);
		$query = $this->db->where('cart_produk.invoiceid', 0);

		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->order_by('produk.userid');
		// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByUseridProduk($userid_produk){
		// $query  = $this->db->select('DISTINCT id');
		$query  = $this->db->select('
			
			cart_produk.invoiceid,
			
			invoice.kode as kode_invoice,
			
			users.username as username,
		');
		$query = $this->db->from('cart_produk');
		$this->db->distinct();
		$query = $this->db->where('cart_produk.userid_produk', $userid_produk);
		$query = $this->db->where('cart_produk.invoiceid != 0');
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('users', 'cart_produk.userid = users.id');
		$query = $this->db->join('invoice', 'cart_produk.invoiceid = invoice.id');
		$query = $this->db->order_by('cart_produk.invoiceid'); 

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readAllByUseridProduk($userid_produk){
		$query  = $this->db->select('
			
			cart_produk.id,
			cart_produk.sessid,
			cart_produk.userid,
			cart_produk.produkid,
			cart_produk.qty,
			cart_produk.invoiceid,
			cart_produk.total_ongkir,
			cart_produk.total_berat,
			cart_produk.ongkir_perkilo,
			cart_produk.status,
			cart_produk.is_read,
			cart_produk.created_on,
			cart_produk.updated_on,
			invoice.kode as kode_invoice,
			invoice.alamat as alamat,
			invoice.provinsi as provinsi,
			invoice.kota as kota,
			produk.nama as nama,
			produk.harga as harga,
			produk.berat,
			produk.userid as userid_toko,
			users.username as username,
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.userid_produk', $userid_produk);
		$query = $this->db->where('cart_produk.invoiceid != 0');
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('users', 'cart_produk.userid = users.id');
		$query = $this->db->join('invoice', 'cart_produk.invoiceid = invoice.id');
		

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readAllInvoiceByUseridProduk($userid_produk){
		$query  = $this->db->select('
			
			
			invoice.kode as kode_invoice,
			invoice.id as invoiceid,
			invoice.alamat as alamat,
			invoice.provinsi as provinsi,
			invoice.kota as kota,
			users.username as username,
			
		');
		$this->db->distinct();
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.userid_produk', $userid_produk);
		$query = $this->db->where('cart_produk.invoiceid != 0');
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('users', 'cart_produk.userid = users.id');
		$query = $this->db->join('invoice', 'cart_produk.invoiceid = invoice.id');
		

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}



	public function countUnread($userid_produk){
		$query  = $this->db->select('
			cart_produk.invoiceid,
				
			invoice.kode as kode_invoice,
			
			users.username as username,
		');
		$query = $this->db->from('cart_produk');
		$this->db->distinct();
		$query = $this->db->where('cart_produk.userid_produk', $userid_produk);
		$query = $this->db->where('cart_produk.invoiceid != 0');
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('users', 'cart_produk.userid = users.id');
		$query = $this->db->join('invoice', 'cart_produk.invoiceid = invoice.id');
		$query = $this->db->order_by('cart_produk.invoiceid'); 

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function countProdukByToko($tokoid){
		$query  = $this->db->select('
			COUNT(produk.id) as count,
			users.username,
			users.province_id,
			users.city_id
		');
		$query = $this->db->from('cart_produk');
		
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('users', 'produk.userid = users.id');
		$query = $this->db->where('produk.userid', $tokoid);
		// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	// public function readTokosUnique(){
	// 	$query  = $this->db->select('
	// 		cart_produk.id,
	// 		cart_produk.sessid,
	// 		cart_produk.userid,
	// 		cart_produk.produkid,
	// 		cart_produk.qty,
	// 		cart_produk.created_on,
	// 		cart_produk.updated_on,
	// 		produk.nama as nama,
	// 		produk.harga as harga,
	// 		produk.berat,
	// 		produk.userid as userid_toko,
			
	// 	');
	// 	$query = $this->db->from('cart_produk');
	// 	$query = $this->db->where('cart_produk.userid', $userid);
	// 	$query = $this->db->where('cart_produk.invoiceid', 0);
	// 	$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
	// 	$query = $this->db->order_by('produk.userid');

	// 	// $query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

	// 	if($query = $this->db->get()){
	// 		$response = $query->result();
	// 	}else{
	// 		$response = $this->db->error()['message'];
	// 	}
	// 	return $response;
	// }

	public function readById($id){
		$query  = $this->db->select('
			cart_produk.id,
			cart_produk.produkid,
			cart_produk.qty,
			cart_produk.created_on,
			cart_produk.updated_on,
			produk.nama as nama,
			produk.harga as harga,
			produk.berat,
			produk.userid as userid_toko,
			photos.url as photo,
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.id', $id);
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readByCartId($cartid){
		$query  = $this->db->select('
			cart_produk.id,
			cart_produk.produkid,
			cart_produk.qty,
			cart_produk.created_on,
			cart_produk.updated_on,
			produk.nama as nama,
			produk.harga as harga,
			produk.berat,
			produk.userid as userid_toko,
			photos.url as photo,
		');
		$query = $this->db->from('cart_produk');
		$query = $this->db->where('cart_produk.cartid', $cartid);
		$query = $this->db->join('produk', 'cart_produk.produkid = produk.id');
		$query = $this->db->join('photos', 'produk.id = photos.produkid', 'left');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}
	
	public function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('cart_produk', $data)){
			$response = array(
				'error' => 0,
				'message' => "Cart Produk has been updated",
				'id' => $this->id,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	public function updateUserIdBySessid(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('sessid', $this->sessid);
		if($this->db->update('cart_produk', $data)){
			$response = array(
				'error' => 0,
				'message' => "Cart Produk has been updated",
				'id' => $this->id,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	public function delete(){
		$this->db->where('id', $this->id);
		if($this->db->delete('cart_produk')){
			$response = array(
				'error' => 0,
				'message' => "Cart Produk has been deleted",
				'id' => $selected_services[$i]->id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
	}

	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

}
?>
