<?php
class MAccommodationsNew extends CI_Model{

	public $id;
	public $code;
	public $name;
	public $address;
	public $website;
	public $email;
	public $phone;
	public $facebook;
	public $instagram;
	public $youtube;
	public $lat;
	public $lng;
	public $description;
	public $tags;
	public $ics_url;
	public $ics;
	public $chanel_booking_com;
	public $chanel_airbnb_com;
	public $chanel_flipkey_com;
	public $chanel_other;
	public $accommodation_types_id;
	public $accommodation_complex_id;
	public $renting_type;
	public $locations_id;
	public $created_on;
	public $updated_on;
	public $management; // 0:Other Management ; 1:Total Bali Management
	public $partners_id;
	public $include_tax; // 0:Not ; 1:Included
	public $tax_percentage;
	public $class; // 0:Affordable ; 1:Luxury ; 2:Elite;

	// public $viewed_count;
	// public $booked_count;
	// public $active;

	public $total_rooms;
	public $total_bathrooms;
	public $total_beds;
	public $building_size;
	public $land_size;
	public $flag_ics_valid;
	public $ics_updated_on;
	public $ics_status;


	function __construct(){
  	parent::__construct();
		$this->load->library('Ical');
  }

	function create(){
		//Download & Create ICS File
		$randName = uniqid();
		$pathName = "./uploads/ics/".$randName.".ics";
		if($this->ics_url != null || $this->ics_url != ""){
			$this->ical->get($this->ics_url, $pathName);
			$this->ics = $pathName;
		}
		//
		if($this->db->insert('accommodations', $this)){
			$this->id = $this->db->insert_id();
			$response = $this;
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function readAll(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.code,
			accommodations.address,
			accommodations.website,
			accommodations.description,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.class,
			accommodations.lat,
			accommodations.lng,
			accommodations.accommodation_complex_id,
			accommodations.active,
			accommodations.status,

			locations.name as location,
			accommodation_types.name as type,
			partners.name as partners_name,
			partners.email as partners_email

		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('partners', 'accommodations.partners_id = partners.id', 'left outer');
		$query = $this->db->where('accommodations.active != 2');



		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readRandomByICSUpdated(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodation_complex.name as complex,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.ics,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		// $query = $this->db->where('accommodations.active', 1);
		$query = $this->db->where('accommodations.ics_updated_on < DATE_ADD(NOW(), INTERVAL -12 HOUR)');
		// $query = $this->db->order_by('rand()');
		$query = $this->db->limit(1);

		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readAllActive(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.code,
			accommodations.address,
			accommodations.website,
			accommodations.description,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.class,
			accommodations.lat,
			accommodations.lng,
			accommodations.accommodation_complex_id,
			accommodations.active,
			accommodations.status,

			locations.name as location,
			accommodation_types.name as type,
			partners.name as partners_name,
			partners.email as partners_email

		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('partners', 'accommodations.partners_id = partners.id', 'left outer');
		// $query = $this->db->where('accommodations.active != 2');
		$query = $this->db->where('accommodations.active', 1);



		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readAllNeedApproval(){
		$query  = $this->db->select('
		accommodations.id,
		accommodations.name,
		accommodations.code,
		accommodations.address,
		accommodations.website,
		accommodations.chanel_booking_com,
		accommodations.chanel_airbnb_com,
		accommodations.chanel_flipkey_com,
		accommodations.chanel_other,
		accommodations.locations_id,
		accommodations.management,
		accommodations.total_rooms,
		accommodations.total_beds,
		accommodations.total_bathrooms,
		accommodations.class,
		accommodations.active,
		accommodations.accommodation_complex_id,
		accommodations.status,

		locations.name as location,
		accommodation_types.name as type,
		partners.name as partners_name,
		partners.email as partners_email
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('partners', 'accommodations.partners_id = partners.id', 'left outer');
		$query = $this->db->where('accommodations.active', 2);


		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readById(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.code,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.renting_type,
			accommodations.land_size,
			accommodations.building_size,
			accommodations.accommodation_complex_id,
			locations.name as location,
			accommodation_types.name as type,
			partners.email as partners_email,
			partners.reservation_email as partners_reservation_email
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left');
		$query = $this->db->join('partners', 'accommodations.partners_id = partners.id', 'left');
		$query = $this->db->where('accommodations.id', $this->id);


		if($query = $this->db->get()){
			if(isset($query->result()[0])){
				$accommodations = $query->result()[0];
			}else{
				$accommodations = null;
			}

			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	public function readByPartnersId(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodation_complex.name as complex,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		$query = $this->db->where('accommodations.partners_id', $this->partners_id);


		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function update(){
		$randName = uniqid();
		if($this->ics_url!=null && $this->ics_url!=""){
			$accm = $this->readById($this->id);
			$this->ics = "./uploads/ics/".$randName.".ics";
			if($accm->ics != null || $accm->ics != ""){
				// $this->ical->get($this->ics_url, $accm->ics);
				if($this->ical->get($this->ics_url, $accm->ics)){
					$this->flag_ics_valid = 1;
				}else{
					$this->flag_ics_valid = 0;
				}

			}else{
				// $this->ical->get($this->ics_url, $this->ics);
				if($this->ical->get($this->ics_url, $accm->ics)){
					$this->flag_ics_valid = 1;
				}else{
					$this->flag_ics_valid = 0;
				}
			}
		}

		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodations', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodations has been updated",
				'id' => $this->id,
				'name' => $this->name,
				'description' => $this->description,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function approve(){
		$data = array(
			'active' => 1
		);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodations', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodation Approved",
				'id' => $this->id,
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function activate(){
		$data = array(
			'active' => 1
		);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodations', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodation Active",
				'id' => $this->id,
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function deactivate(){
		$data = array(
			'active' => 0
		);
		$this->db->where('id', $this->id);
		if($this->db->update('accommodations', $data)){
			$response = array(
				'error' => 0,
				'message' => "Accommodation Deactive",
				'id' => $this->id,
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function count(){
		$this->db->from('accommodations');
		$this->db->where('accommodations.active', 1);
		return $this->db->count_all_results();
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

	function readByCode(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.code,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodation_complex.name as complex,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		$query = $this->db->where('accommodations.code', $this->code);
		$query = $this->db->where('accommodations.active', 1);


		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readRandomHadPromo(){
		$sub_query_2 = $this->db->select('
			rates.accommodation_rooms_id
		');
		$sub_query_2 = $this->db->from('rates');
		$sub_query_2 = $this->db->where('rates.type', 2);
		$sub_query_2 = $this->db->where('accommodations.active', 1);
		$sub_query_2 = $this->db->_compile_select();
		$sub_query_2 = $this->db->_reset_select();

		$sub_query_1 = $this->db->select('
			accommodation_rooms.accommodations_id
		');
		$sub_query_1 = $this->db->from('accommodation_rooms');
		$sub_query_1 = $this->db->where_in($sub_query_2);
		$sub_query_1 = $this->db->_compile_select();
		$sub_query_1 = $this->db->_reset_select();

		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.code,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type,
			accommodation_complex.name as complex,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		$query = $this->db->where('accommodations.active', 1);
		$query = $this->db->where_in($sub_query_1);
		$query = $this->db->limit(6);
		$query = $this->db->order_by("RAND()");

		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readWithNoCode(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodation_complex.name as complex,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		$query = $this->db->where('accommodations.code', "");


		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readActiveWithLimitAndStart($limit, $start){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodation_complex.name as complex,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.ics,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		$query = $this->db->where('accommodations.active', 1);
		$query = $this->db->limit($limit, $start);


		if($query = $this->db->get()){
			$accommodations = $query->result();
			return $accommodations;
		}else{
			echo $this->db->error()['message'];
			return $this->db->error()['message'];
		}
	}

	function readIncludingRoomsSearch(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.code,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.partners_id,
			accommodations.include_tax,
			accommodations.tax_percentage,
			accommodations.accommodation_types_id,
			accommodations.total_rooms,
			accommodations.total_beds,
			accommodations.total_bathrooms,
			accommodations.lat,
			accommodations.lng,
			accommodations.class,
			accommodations.description,
			accommodations.facebook,
			accommodations.instagram,
			accommodations.youtube,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.renting_type,
			locations.name as location,
			accommodation_types.name as type,
			accommodation_complex.name as complex,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->join('accommodation_complex', 'accommodations.accommodation_complex_id = accommodation_complex.id', 'left outer');
		$query = $this->db->where('accommodations.active', 1);

		if($this->input->get("date_stay") != date('m-d-Y')." - ".date('m-d-Y')  && $this->input->get("date_stay") != ""){
			$daterange = explode('-', $this->input->get('date_stay'));
			$newCheckin = date("Y-m-d", strtotime($daterange[0]));
			$newCheckout = date("Y-m-d", strtotime($daterange[1]));
			$queryd = "accommodations.id NOT IN";
			$queryd .= " (SELECT ics_date.accommodations_id from ics_date WHERE";
			$queryd .= " (checkin_date <=  '".$newCheckin."' AND checkout_date > '".$newCheckin."') ";
			$queryd .= " OR (checkin_date < '".$newCheckout."' AND checkout_date > '".$newCheckout."')";
			$queryd .= " OR (checkin_date > '".$newCheckin."' AND checkout_date < '".$newCheckout."')";
			$queryd .= ")";
			$query = $this->db->where($queryd);
		}
		// $query = $this->db->where('accommodations.active', 1);

		if($this->input->get("bedrooms")){
			$bedrooms = $this->input->get("bedrooms");
			if(sizeof($bedrooms)==1){
				$queryBedroom = "accommodations.id IN ";
				$queryBedroom .= "(SELECT accommodation_rooms.accommodations_id FROM accommodation_rooms WHERE ";
				$queryBedroom .= "accommodation_rooms.total_rooms ".$bedrooms[0].")";
				$this->db->where("(".$queryBedroom.")");
			}else{
				$this->db->group_start();
				for($room = 0; $room<sizeof($bedrooms); $room++){
					$queryBedroom = "accommodations.id IN ";
					$queryBedroom .= "(SELECT accommodation_rooms.accommodations_id FROM accommodation_rooms WHERE ";
					$queryBedroom .= "accommodation_rooms.total_rooms ".$bedrooms[$room].")";
					$this->db->or_where("(".$queryBedroom.")");
				}
				$this->db->group_end();
			}
		}
		if($this->input->get("name")){
			$query = $this->db->where("accommodations.name LIKE '%".$this->input->get("name")."%'");
		}
		if($this->input->get("page")){
			$quantity = 6;
			$query = $this->db->limit($quantity, ($quantity*$this->input->get("page")-$quantity));
		}

		if($this->input->get("locations")){
			$locations = $this->input->get("locations");
			if(sizeof($locations)==1){
				$query = $this->db->where("accommodations.locations_id", $locations);
			}else{
				$this->db->group_start();
				for($location = 0; $location < sizeof($locations); $location++){
					$query = $this->db->or_where("accommodations.locations_id", $locations[$location]);
				}
				$this->db->group_end();
			}
		}



		if($query = $this->db->get()){
			$accommodations = $query->result();
		}


		for($i=0; $i<sizeof($accommodations); $i++){
			$query  = $this->db->select('
				accommodation_rooms.id,
				accommodation_rooms.name,
			');
			$query = $this->db->from('accommodation_rooms');
			$query = $this->db->where('accommodation_rooms.accommodations_id', $accommodations[$i]->id);
			if($query = $this->db->get()){
				$rooms = $query->result();
			}
			$accommodations[$i]->rooms = $rooms;
		}
		// echo $this->db->last_query();
		return $accommodations;
	}
}
?>
