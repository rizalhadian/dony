<?php
class MKategori extends CI_Model{

	public $id;
    public $parent_id;
    public $nama;
    public $deskripsi;


	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
    	parent::__construct();
		// $this->load->library('Ical');
    }

	public function create(){
		
		if($this->db->insert('kategori', $this)){
			$response = array(
				'error' => 0,
				'message' => "Kategori has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readAll(){
		$query  = $this->db->select('
			kat.id,
            kat.parent_id,
            kat.nama,
            kat.deskripsi,
            parent.nama as nama_parent,
		');
		$query = $this->db->from('kategori as kat');
		$query = $this->db->join('kategori as parent', 'parent.id = kat.parent_id', 'left');
        
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		// return $this->db->last_query();
		return $response;
	}

	public function readAllParent(){
		$query  = $this->db->select('
			kat.id,
            kat.parent_id,
            kat.nama,
            kat.deskripsi,
            parent.nama as nama_parent,
		');
		$query = $this->db->from('kategori as kat');
		$query = $this->db->join('kategori as parent', 'parent.id = kat.parent_id', 'left');
		$query = $this->db->where('kat.parent_id', null);

        
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		// return $this->db->last_query();
		return $response;
	}

	public function readById(){
		$query  = $this->db->select('
			kat.id,
            kat.parent_id,
            kat.nama,
            kat.deskripsi,
            parent.nama as nama_parent,
		');
		$query = $this->db->from('kategori as kat');
		$query = $this->db->join('kategori as parent', 'parent.id = kat.parent_id', 'left');
		$query = $this->db->where('kat.id', $this->id);
        
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;

	}

	public function readLikeByName($name){
		$query  = $this->db->select('
            kategori.id,
            kategori.parent_id,
            kategori.nama,
            kategori.deskripsi
		');
		$query = $this->db->from('kategori');
		$query = $this->db->like('kategori.nama', $name);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readLikeByName2($name1, $name2){
		$query  = $this->db->select('
			kat1.id,
			kat1.parent_id,
			kat1.nama,
			kat1.deskripsi,
			kat2.id as parent_id,
			kat2.nama as parent_name
		');
		$query = $this->db->from('kategori as kat1');
		$query = $this->db->like('kat1.nama', $name1);
		$query = $this->db->join('kategori as kat2', 'kat1.parent_id = kat2.id');
		$query = $this->db->like('kat2.nama', $name2);		


		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	
	
	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
