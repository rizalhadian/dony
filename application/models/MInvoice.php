<?php

class MInvoice extends CI_Model{

	public $id;
	public $kode;
	public $userid;
	public $total;
	public $is_paid;
	public $created_on;
	public $updated_on;


	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		// $this->load->library('Ical');
  }

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('invoice', $this)){
			$response = array(
				'error' => 0,
				'message' => "Invoice has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readByUserId(){
		$query  = $this->db->select('
			invoice.id,
			invoice.kode,
			invoice.userid,
			invoice.total,
			invoice.provinsi,
			invoice.kota,
			invoice.alamat,
			invoice.nama_penerima,
			invoice.telp_penerima,
			invoice.is_paid,
			invoice.created_on,
			invoice.updated_on
			
		');
		$query = $this->db->from('invoice');
		$query = $this->db->where('invoice.userid', $this->userid);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readById(){
		$query  = $this->db->select('
			invoice.id,
			invoice.kode,
			invoice.userid,
			invoice.total,
			invoice.provinsi,
			invoice.kota,
			invoice.alamat,
			invoice.nama_penerima,
			invoice.telp_penerima,
			invoice.is_paid,
			invoice.created_on,
			invoice.updated_on
			
		');
		$query = $this->db->from('invoice');
		$query = $this->db->where('invoice.userid', $this->userid);
		$query = $this->db->where('invoice.id', $this->id);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	


	

	

	

	

	
	

}
?>
