<?php
class MMessageRoomGuests extends CI_Model{

		public $id;
		public $userid;
		public $messageroomid;
		public $created_on;


	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  		parent::__construct();
		// $this->load->library('Ical');
  	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('message_room_guests', $this)){
			$response = array(
				'error' => 0,
				'message' => "guest has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}
	

    public function readByMessageRoomId($authid){
        $query  = $this->db->select('
					message_room_guests.userid,
					users.username as username,
					users.photo as photo
				');
				$query = $this->db->from('message_room_guests');
				$query = $this->db->where('message_room_guests.messageroomid', $this->messageroomid);
				$query = $this->db->where('message_room_guests.userid != "'.$authid.'"');
				$query = $this->db->join('users', 'users.id = message_room_guests.userid');


					if($query = $this->db->get()){
						$response = $query->result();
						return $response;
					}else{
						return  $this->db->error()['message'];
					}
    }
    
    public function readMessages(){
		$query  = $this->db->select('
            messages.id,
            messages.pengirimid,
            messages.penerimaid,
            messages.message,
            messages.created_on,
            messages.is_read,
            pengirim.username as usernamepengirim,
            penerima.username as usernamepenerima
		');
		$query = $this->db->from('messages');
		$query = $this->db->join('users as pengirim', 'messages.pengirimid = pengirim.id');
		$query = $this->db->join('users as penerima', 'messages.penerimaid = penerima.id');
		// $query = $this->db->where('messages.pengirimid = "'.$this->penerimaid.'" AND messages.penerimaid = "'.$this->pengirimid.'"');
		$query = $this->db->where('(messages.pengirimid = "'.$this->pengirimid.'" OR messages.penerimaid = "'.$this->pengirimid.'")');
		$this->db->distinct();
		$this->db->order_by("messages.created_on", "desc");


		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}
    }

	

	



	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
