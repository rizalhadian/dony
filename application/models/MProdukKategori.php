<?php
class MProdukKategori extends CI_Model{

	public $id;
    public $produk_id;
    public $kategori_id;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
    	parent::__construct();
		// $this->load->library('Ical');
    }

	public function create(){
		if($this->db->insert('produk_kategori', $this)){
			$response = array(
				'error' => 0,
				'message' => "Produk Kategori has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readByProdukId(){
		$query  = $this->db->select('
			produk_kategori.id,
			produk_kategori.produk_id,
			produk_kategori.kategori_id,
			kategori.nama
		');
		$query = $this->db->from('produk_kategori');
		$query = $this->db->where('produk_id', $this->produk_id);
		$query = $this->db->join('kategori', 'produk_kategori.kategori_id = kategori.id');

        
		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function deleteByProdukId(){
		$this->db->where('produk_id', $this->produk_id);
		if($this->db->delete('produk_kategori')){
			$response = array(
				'error' => 0,
				'message' => "Produk Kategori has been deleted",
				'id' => $this->produk_id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	
	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
