<?php
class MMessages extends CI_Model{

	public $id;
    public $messageroomid;
    public $pengirimid;
    public $message;
    public $created_on;
    public $is_read;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  		parent::__construct();
		// $this->load->library('Ical');
  	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('messages', $this)){
			$response = array(
				'error' => 0,
				'message' => "message has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readByMessageRoomId(){
		$query  = $this->db->select('
            messages.id,
            messages.messageroomid,
            messages.pengirimid,
            messages.message,
            messages.created_on,
			messages.is_read,
			users.username as username,
			users.photo as photo
		');
		$query = $this->db->from('messages');
		$query = $this->db->where('messages.messageroomid', $this->messageroomid);
		$query = $this->db->join('users', 'users.id = messages.pengirimid');

		$this->db->order_by("messages.created_on", "desc");
		

		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}
	}
	
	public function readByMessageRoomIdLimit(){
		$query  = $this->db->select('
            messages.id,
            messages.messageroomid,
            messages.pengirimid,
            messages.message,
            messages.created_on,
			messages.is_read,
			users.username as username,
			users.photo as photo
		');
		$query = $this->db->from('messages');
		$query = $this->db->where('messages.messageroomid', $this->messageroomid);
		$query = $this->db->join('users', 'users.id = messages.pengirimid');
		$query = $this->db->limit(1, 0);

		$this->db->order_by("messages.created_on", "desc");
		

		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}
	}


	

    public function readByUserid(){
        $query  = $this->db->select('
            messages.id,
            messages.pengirimid,
            messages.penerimaid,
            messages.message,
            messages.created_on,
            messages.is_read,
            pengirim.username as usernamepengirim,
            penerima.username as usernamepenerima
		');
		$query = $this->db->from('messages');
		$query = $this->db->join('users as pengirim', 'messages.pengirimid = pengirim.id');
		$query = $this->db->join('users as penerima', 'messages.penerimaid = penerima.id');
		// $query = $this->db->where('messages.pengirimid = "'.$this->penerimaid.'" AND messages.penerimaid = "'.$this->pengirimid.'"');
		$query = $this->db->where('(messages.pengirimid = "'.$this->pengirimid.'" AND messages.penerimaid = "'.$this->penerimaid.'") OR (messages.pengirimid = "'.$this->penerimaid.'" AND messages.penerimaid = "'.$this->pengirimid.'")');
		$this->db->order_by("messages.created_on", "desc");
		// return $this->pengirimid." ".$this->penerimaid;

		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}
	
    }
    
    public function readMessages(){
		$query  = $this->db->select('
            messages.id,
            messages.pengirimid,
            messages.penerimaid,
            messages.message,
            messages.created_on,
            messages.is_read,
            pengirim.username as usernamepengirim,
            penerima.username as usernamepenerima
		');
		$query = $this->db->from('messages');
		$query = $this->db->join('users as pengirim', 'messages.pengirimid = pengirim.id');
		$query = $this->db->join('users as penerima', 'messages.penerimaid = penerima.id');
		// $query = $this->db->where('messages.pengirimid = "'.$this->penerimaid.'" AND messages.penerimaid = "'.$this->pengirimid.'"');
		$query = $this->db->where('(messages.pengirimid = "'.$this->pengirimid.'" OR messages.penerimaid = "'.$this->pengirimid.'")');
		$this->db->distinct();
		$this->db->order_by("messages.created_on", "desc");


		if($query = $this->db->get()){
			$response = $query->result();
			return $response;
		}else{
			return  $this->db->error()['message'];
		}
    }

	

	



	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
