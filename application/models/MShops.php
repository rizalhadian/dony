<?php
class MShops extends CI_Model{

	public $id;
	public $nama;
	public $userid;
	public $postalcode;
	public $address;
	public $phone;
	public $lat;
	public $lng;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  	parent::__construct();
		$this->load->library('Ical');
  }

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('toko', $this)){
			$response = array(
				'error' => 0,
				'message' => "Toko has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function checkHadToko(){
		$query  = $this->db->select('
			*
		');
		$query = $this->db->from('toko');
		$query = $this->db->where('toko.userid', $this->userid);

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}

		if(isset($response[0])){
			return true;
		}else{
			if(isset($response['error'])){
				return $response;
			}else{
				return false;
			}
		}
		return $response;
	}

	public function create_photos($photos, $dataPosted, $insert_id){
		$responsephotos = array();
		for($i=0; $i< count($photos); $i++){
			$photos[$i]['accommodations_id'] = $insert_id;
			$photos[$i]['created_on'] = date('Y-m-d H:i:s');
			if($this->db->insert('photos', $photos[$i])){
				$photo = array(
					'error' => 0,
					'message' => "Photo has been added",
					'id' => $this->db->insert_id(),
					'name' => $dataPosted['name'],
					'description' => $dataPosted['description'],
				);
				array_push($responsephotos, $photo);
			}else{
				$photo = array(
					'error' => 1,
					'message' => $this->db->error()['message']
				);
				array_push($response->photos, $photo);
			}
		}
		return $responsephotos;
	}

	function upload_photos(){
		$photos = array();
		// print_r($_FILES);
		if(file_exists($_FILES['files']['tmp_name'][0])){
			// echo 'ada foto';
				//Photos Not Null
				$filesCount = count($_FILES['files']['name']);
				for($i = 0; $i < $filesCount; $i++){
						$_FILES['userFile']['name'] = $_FILES['files']['name'][$i];
						$_FILES['userFile']['type'] = $_FILES['files']['type'][$i];
						$_FILES['userFile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
						$_FILES['userFile']['error'] = $_FILES['files']['error'][$i];
						$_FILES['userFile']['size'] = $_FILES['files']['size'][$i];
						$new_name = uniqid();
						$uploadPath = 'uploads/photos/accommodations';
						$config['upload_path'] = $uploadPath;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['file_name'] = $new_name;
						$this->load->library('upload', $config);
						$this->upload->initialize($config);
						if($this->upload->do_upload('userFile')){
								$fileData = $this->upload->data();
								$ty = explode('/', $_FILES['userFile']['type']);
								if($ty[1] == "jpeg"){
									$type = "jpg";
								}else{
									$type = $ty[1];
								}
								$photo['photo'] = "uploads/photos/accommodations/"."".$config['file_name'].".".$type;
								array_push($photos, $photo);
						}else{
							echo $this->upload->display_errors("<span class='error'>", "</span>");
						}
				}
		}else{
			// echo "gada foto";
			// Photos Null
		}
		return $photos;
	}

	function readAll(){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.chanel_booking_com,
			accommodations.chanel_airbnb_com,
			accommodations.chanel_flipkey_com,
			accommodations.chanel_other,
			accommodations.locations_id,
			accommodations.management,
			accommodations.active,

			accommodations.class,
			locations.name as location,
			accommodation_types.name as type
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left_outer');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left_outer');
		$query = $this->db->where('accommodations.active != 2');


		if($query = $this->db->get()){
			$accommodations = $query->result();
		}
		// echo $this->db->error()['message'];
		return $accommodations;
	}

	function readById($id){
		$query  = $this->db->select('
			accommodations.id,
			accommodations.name,
			accommodations.address,
			accommodations.website,
			accommodations.youtube,
			accommodations.lat,
			accommodations.lng,
			accommodations.description,
			accommodations.tags,
			accommodations.ics_url,
			accommodations.ics,
			accommodations.created_on,
			accommodations.updated_on,
			locations.name as location,
			accommodations.locations_id,
			accommodation_types.name as type,
		');
		$query = $this->db->from('accommodations');
		$query = $this->db->join('accommodation_types', 'accommodations.accommodation_types_id = accommodation_types.id', 'left outer');
		$query = $this->db->where('accommodations.id', $id);
		$query = $this->db->join('locations', 'accommodations.locations_id = locations.id', 'left outer');

		if($query = $this->db->get()){
			$accommodations = $query->result()[0];
			// $accommodations->photos = $this->read_accommodation_photos_by_id($id);
		}
		return $accommodations;
	}

	function update($data_posted){
		$this->db->trans_start();

		$randName = uniqid();
		$accm = $this->read_id($data_posted['idtoupdate']);
		unlink($accm->ics);
		$pathName = "./uploads/ics/".$randName.".ics";
		$this->ical->get($data_posted['ics_url'], $pathName);


		$data = array(
			'name' => $data_posted['name'],
			'address' => $data_posted['address'],
			'website' => $data_posted['website'],
			'youtube' => $data_posted['youtube'],
			'lat' => $data_posted['lat'],
			'lng' => $data_posted['lng'],
			'description' => $data_posted['description'],
			'accommodation_types_id' => $data_posted['accommodation_types_id'],
			'locations_id' => $data_posted['locations_id'],
			'tags' => $data_posted['tags'],
			'ics_url' => $data_posted['ics_url'],
			'ics' => $pathName,
			'created_on' => date('Y-m-d H:i:s'),
			'active' => 1
		);

		$this->db->where('id', $data_posted['idtoupdate']);
		$this->db->update('accommodations', $data);

		$ics = $this->ical->create($pathName);
		$vvent = $this->ical->events($ics);
		// print_r($this->ical->events($ics));
		// for($i=0; $i < count($vvent); $i++){
		// 	print_r($vvent[$i]);
		// 	echo "<br>";
		// 	echo date("Y-m-d", strtotime($vvent[$i]['DTSTART']));
		// 	echo "<br>";
		// 	echo date("Y-m-d", strtotime($vvent[$i]['DTEND']));
		// 	echo "<br>";
		// 	echo $vvent[$i]['UID'];
		// 	echo "<br>";
		// 	echo $vvent[$i]['SUMMARY'];
		// 	echo "<br><br>";
		// }

		/*Upload photos*/
		$photos = $this->upload_photos();
		/*Create data photos*/
		$response['photos'] = $this->create_photos($photos, $data_posted, $data_posted['idtoupdate']);
		/*Reformat services*/
		$selected_services = $this->read_accommodation_services($data_posted['idtoupdate']);
		$services = array();
		if(isset($data_posted['services'])){
			foreach($data_posted['services'] as $service){
				$ser = array(
					'id' => $data_posted['service-'.$service],
					'services_id' => $service,
					'rate' => $data_posted['price_service-'.$service]
				);
				array_push($services, $ser);
			}
			$response['services'] = $this->recheck_services($services, $selected_services, $data_posted['idtoupdate']);
		}else{
			$this->delete_all_mm_accommodations_services_by_accommodations_id($data_posted['idtoupdate']);
		}

		print_r($response);
		$this->db->trans_complete();
		return $data;
	}

	function delete($id){
		$this->db->where('id', $id);
		if($this->db->delete('accommodations')){
			$response = array(
				'error' => 0,
				'message' => "Accommodation has been deleted",
				'id' => $id
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function count(){
		$this->db->where('active', 1);
		$this->db->from('accommodations');
		return $this->db->count_all_results();
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null && $value!=""){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

}
?>
