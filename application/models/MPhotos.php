<?php
class MPhotos extends CI_Model{

	public $id;
	public $name;
	public $produkid;
	public $url;
	public $urutan;


	public function __construct(){
  	parent::__construct();
  }

	public function create(){
		if($this->db->insert('photos', $this)){
			$this->id = $this->db->insert_id();
			return $this;
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
			return $response;
		}
	}

	public function readById($id){
		$query  = $this->db->select('
			photos.id,
			photos.produkid,
			photos.url,
			photos.urutan,
		');
		$query = $this->db->from('photos');
		$query = $this->db->where('photos.id', $this->id);

		if($query = $this->db->get()){
			$photo = $query->result()[0];
		}
		return $photo;
	}

	public function readByProdukId(){
		$query  = $this->db->select('
			photos.id,
			photos.name,
			photos.produkid,
			photos.url,
			photos.urutan,
		');
		$query = $this->db->from('photos');
		$query = $this->db->where('photos.produkid', $this->produkid);
		$query = $this->db->order_by('photos.urutan', 'asc');

		if($query = $this->db->get()){
			$photos = $query->result();
		}else{
			$photos = $this->db->error()['message']	;
		}
		return $photos;
	}


	public function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('photos', $data)){
			$response = array(
				'error' => 0,
				'message' => "Photo has been updated",
				'id' => $this->id,
				'name' => $this->name,
				'title' => $this->title,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	public function updateAccommodationsId(){
		$data = array(
			'accommodations_id' => $this->accommodations_id
		);
		$this->db->where('id', $this->id);
		if($this->db->update('photos', $data)){
			$response = array(
				'error' => 0,
				'message' => "Photo has been updated",
				'id' => $this->id,
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	public function delete(){
		$photo = $this->readById($this->id);

		$photo_arr = explode("accommodations/", $photo->path) ;
		$new_path = $photo_arr[0]."accommodations/thumbnails/".$photo_arr[1];
		unlink('./'.$photo->path);
		unlink('./'.$new_path);

		$this->db->where('id', $this->id);
		if($this->db->delete('photos')){
			$response = array(
				'error' => 0,
				'message' => "Photo has been deleted",
				'id' => $this->id,
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null && $value!=""){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}

}
?>
