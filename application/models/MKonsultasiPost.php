<?php
class MKonsultasiPost extends CI_Model{

	public $id;
	public $userid;
	public $judul;
	public $deskripsi;
	public $jawaban;
	public $is_answered;
	public $view_count;
	public $created_on;
	public $updated_on;

	// echo (base_url().'assets/fileuploader/src/class.fileuploader.php');

	function __construct(){
  		parent::__construct();
		
  	}

	public function create(){
		// $this->db->trans_start();
		if($this->db->insert('konsultasi_post', $this)){
			$response = array(
				'error' => 0,
				'message' => "Konsultasi has been added",
				'id' => $this->db->insert_id(),
				'data' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		// $this->db->trans_complete();
		return $response;
	}

	public function readAll($page){
		$query  = $this->db->select('
			konsultasi_post.id,
			konsultasi_post.userid,
			konsultasi_post.judul,
			konsultasi_post.deskripsi,
			konsultasi_post.jawaban,
			konsultasi_post.is_answered,
			konsultasi_post.view_count,
			konsultasi_post.created_on,
			konsultasi_post.updated_on,
			users.username,
			users.photo
		');
		$limit = 16;
		$start = $page;
		$query = $this->db->from('konsultasi_post');
		$query = $this->db->join('users', 'konsultasi_post.userid = users.id');
		$query = $this->db->order_by("konsultasi_post.is_answered", "desc");
		$query = $this->db->limit($limit, $start);
		// $query = $this->db->where('accommodations.active != 2');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readAnsweredNewest($page){
		$query  = $this->db->select('
			konsultasi_post.id,
			konsultasi_post.userid,
			konsultasi_post.judul,
			konsultasi_post.deskripsi,
			konsultasi_post.jawaban,
			konsultasi_post.is_answered,
			konsultasi_post.view_count,

			konsultasi_post.created_on,
			konsultasi_post.updated_on,
			users.username,
			users.photo
		');
		$limit = 16;
		$start = $page;
		$query = $this->db->from('konsultasi_post');
		$query = $this->db->join('users', 'konsultasi_post.userid = users.id');
		$query = $this->db->order_by("konsultasi_post.created_on", "desc");
		$query = $this->db->limit($limit, $start);
		$query = $this->db->where('konsultasi_post.is_answered = 1');

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readAnsweredLike($page, $keywords){
		$query  = $this->db->select('
			konsultasi_post.id,
			konsultasi_post.userid,
			konsultasi_post.judul,
			konsultasi_post.deskripsi,
			konsultasi_post.jawaban,
			konsultasi_post.is_answered,
			konsultasi_post.view_count,

			konsultasi_post.created_on,
			konsultasi_post.updated_on,
			users.username,
			users.photo
		');
		$limit = 16;
		$start = $page;
		$query = $this->db->from('konsultasi_post');
		$query = $this->db->join('users', 'konsultasi_post.userid = users.id');
		$query = $this->db->order_by("konsultasi_post.created_on", "desc");
		$query = $this->db->limit($limit, $start);
		$query = $this->db->where('konsultasi_post.is_answered = 1');

		for($i=0; $i<sizeof($keywords); $i++){
			$query = $this->db->like('konsultasi_post.judul', $keywords[$i]);
		}

		if($query = $this->db->get()){
			$response = $query->result();
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function readById(){
		$query  = $this->db->select('
			konsultasi_post.id,
			konsultasi_post.userid,
			konsultasi_post.judul,
			konsultasi_post.deskripsi,
			konsultasi_post.jawaban,
			konsultasi_post.is_answered,
			konsultasi_post.view_count,

			konsultasi_post.created_on,
			konsultasi_post.updated_on,
			users.username,
			users.photo
		');
		$query = $this->db->from('konsultasi_post');
		$query = $this->db->join('users', 'konsultasi_post.userid = users.id');
		$query = $this->db->order_by("konsultasi_post.created_on", "asc");
		$query = $this->db->where("konsultasi_post.id", $this->id);

		if($query = $this->db->get()){
			$response = $query->result()[0];
		}else{
			$response = $this->db->error()['message'];
		}
		return $response;
	}

	public function update(){
		$data = $this->getDataNotNullOnly($this);
		$this->db->where('id', $this->id);
		if($this->db->update('konsultasi_post', $data)){
			$response = array(
				'error' => 0,
				'message' => "Konsultasi has been updated",
				'id' => $this->id,
				'object' => $this
			);
		}else{
			$response = array(
				'error' => 1,
				'message' => $this->db->error()['message']
			);
		}
		return $response;
	}

	
	private function getDataNotNullOnly($data){
		$new_data = new stdClass();
		$data_array = (array) $data;
		foreach($data_array as $key=>$value){
			if($value!=null || $value!="" || $value===0){
				$new_data->$key = $value;
			}
		}
		return $new_data;
	}
}
?>
