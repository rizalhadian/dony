<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['search/map'] = 'Search/map';
$route['accommodation'] = 'Accommodations';
$route['accommodation/(:any)'] = 'Accommodations/readByCode/$1';
$route['test_doku'] = 'Accommodations/test_doku';
$route['send-enquiry'] = 'Accommodations/sendEnquiry';
$route['updateicstobooking/(:any)'] = 'ICSToBooking/updateICSToBooking/$1';
$route['pull-from-other-res-random'] = 'API/readCreateAccommodationFromTBV';
$route['update-booking-from-ics-random'] = 'ICSToBooking/updateBookingFromIcsRandom';
$route['api/count-accommodation-active'] = 'ICSToBooking/countAccommodationsActive';



/* Admin */
$route['admin'] = 'admin';

$route['blogs'] = 'Blog';
$route['blogs/read/(:any)'] = 'Blog/read/$1';

$route['konsultasi'] = 'KonsultasiContr';
$route['konsultasi/add'] = 'KonsultasiContr/createForm';
$route['konsultasi/create'] = 'KonsultasiContr/create';
$route['konsultasi/answer/(:any)'] = 'KonsultasiContr/answerForm/$1';
$route['konsultasi/answering'] = 'KonsultasiContr/answer';
$route['konsultasi/read/(:any)'] = 'KonsultasiContr/readById/$1';
// $route['konsultasi/'] = 'KonsultasiContr/';


$route['superadmin'] = 'Admin/index';
$route['superadmin/blog'] = 'AdminBlog/index';
$route['superadmin/blog/addnew'] = 'AdminBlog/addnew';
$route['superadmin/blog/create'] = 'AdminBlog/create';
$route['superadmin/promo'] = 'AdminPromo/index';
$route['superadmin/promo/addnew'] = 'AdminPromo/addnew';
$route['superadmin/headline/addnew'] = 'AdminHeadline/addnew';
$route['superadmin/headline/create'] = 'AdminHeadline/create';
$route['superadmin/kategori'] = 'AdminKategori/index';
$route['superadmin/kategori/addnew'] = 'AdminKategori/addnew';
$route['superadmin/kategori/addnewsub/(:any)'] = 'AdminKategori/addNewSub/$1';
// $route['superadmin/kategori/addparent'] = 'AdminKategori/addParent';
$route['superadmin/kategori/create'] = 'AdminKategori/create';
$route['superadmin/kategori/readlike'] = 'AdminKategori/readLike';


$route['profile'] = 'Users/preview';
$route['profile/(:any)'] = 'Users/previewOther/$1';
$route['profile/getCities/(:any)'] = 'Users/getCities/$1';
$route['profile_edit'] = 'Users/edit';
$route['profile_update'] = 'Users/update';
$route['change_password'] = 'Users/change_password';
$route['update_password'] = 'Users/update_password';

// $route['superadmin'] = 'Admin';

$route['rajaongkir/getCities/(:any)'] = 'Ongkir/getCities/$1';
$route['rajaongkir/getCosts'] = 'Ongkir/getCosts';

$route['cart'] = 'Cart/index';
$route['cart/addtocart'] = 'Cart/addToCart';
$route['cart/checkout'] = 'Cart/checkout';
$route['cart/getCosts'] = 'Cart/getCosts';
$route['cart/removefromcart/(:any)'] = 'Cart/removeFromCart/$1';

$route['invoice'] = 'Invoice/index';
$route['invoice/read/(:any)'] = 'Invoice/readById/$1';

$route['order'] = 'Cart/readOrders';
$route['order/(:any)'] = 'Cart/readOrder/$1';


$route['upload'] = 'Uploads/upload';
$route['upload-single'] = 'Uploads/uploadSingle';
$route['upload-profile-picture'] = 'Uploads/uploadProfilePicture';

$route['shop'] = 'Shops/index';
$route['shop/addnew'] = 'Shops/createNew';
$route['shop/create'] = 'Shops/create';

$route['u/(:any)'] = 'Shops/readByUsername/$1';
$route['u/(:any)/(:any)'] = 'PublicProduk/readProdukNew/$1/$2';

$route['produk/addnew'] = 'Produk/createNew';
$route['produk/tambah-foto/(:any)'] = 'Produk/addPhotosNew/$1';
$route['produk/create'] = 'Produk/create';
$route['produk/edit/(:any)'] = 'Produk/edit/$1';
$route['produk/delete/(:any)'] = 'Produk/delete/$1';
$route['produk/update'] = 'Produk/update';
$route['produk/upload-photos'] = 'Produk/uploadPhotos';
$route['produk/jenis-2/(:any)'] = 'Produk/readProdukJenis2ByProdukJeni1Id/$1';

$route['search'] = 'Search/index';

$route['register'] = 'Auth/register';
$route['register_do'] = 'Home/registerDo';


$route['p/(:any)/(:any)'] = 'PublicProduk/readKategori2/$1/$2';
$route['p/(:any)'] = 'PublicProduk/readKategori1/$1';

$route['messages'] = "Messages/readAll";
$route['message/(:any)'] = "Messages/readByUserId/$1";
$route['send_message'] = "Messages/create";










/* Ion Auth */
$route['login'] = 'auth';
$route['logout'] = 'Auth/logout';
