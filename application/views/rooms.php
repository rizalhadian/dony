<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Ini Judul</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.min.css"> -->
    <!-- jQuery 3.1.1 -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

    <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- Content -->
        <section class="content-header">

            <h1>
    Add New Accomodation
  </h1>
            <div id="alertInformations">
            </div>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="text-align:center; font-size: 150%;">Start from $<b style="font-size: 125%;"><?php echo $cheapest_rate; ?></b>/night</h3>
                            &nbsp;&nbsp;
                            <a class="btn btn-default btn-sm" data-html="true"
                            data-toggle="popover" data-placement="bottom"
                            data-content="
                            <table class='table table-bordered table-striped'>
                              <?php foreach($promos as $promo){ ?>
                                <tr>
                                  <td width='300px'>
                                    <center><?php echo date("d M Y", strtotime($promo->date_start));?> <br>-<br> <?php echo date("d M Y", strtotime($promo->date_end));?></center>
                                  </td>
                                  <td style='vertical-align:middle;'>
                                    $<b><?php echo $promo->rate;?></b> / Night
                                  </td>
                                </tr>
                              <?php } ?>

                            </table>">
                            <i class="fa fa-tags" aria-hidden="true"></i> Promo
                            </a>


                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">

                            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/create" method="POST">
                                <input type="hidden" name="id" id="id" value="<?php echo $room->id; ?>">

                                <div class="form-group col-md-12 col-xs-12">
                                    <label style="text-align:center; font-size: 150%;">Checkin - Checkout</label>
                                    <input style="text-align:center; font-size: 150%;" type="text" class="form-control pull-right" id="checkinout" value="">
                                </div>
                                <div class="form-group col-md-12 col-xs-12">
                                    <label style="text-align:center; font-size: 150%;">Guest</label>
                                    <div class="form-group">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td style="text-align:center; font-size: 150%;">Mature </td>
                                                        <td style="text-align:right">
                                                            <button type="button" class="btn btn-default" id="btn_mature_reduce">-</button>
                                                        </td>
                                                        <td style="text-align:center; font-size: 150%;" width="5%"><span id="txt_guest_mature">xx</span></td>
                                                        <td style="text-align:left">
                                                            <button type="button" class="btn btn-default" id="btn_mature_add">+</button>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:center; font-size: 150%;">Children </td>
                                                        <td style="text-align:right">
                                                            <button type="button" class="btn btn-default" id="btn_children_reduce">-</button>
                                                        </td>
                                                        <td style="text-align:center; font-size: 150%;" width="5%"><span id="txt_guest_children">xx</span>
                                                            <input type="hidden" id="guest_children" name="guest_children">
                                                        </td>
                                                        <td style="text-align:left">
                                                            <button type="button" class="btn btn-default" id="btn_children_add">+</button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:center; font-size: 150%;">Infant </td>
                                                        <td style="text-align:right">
                                                            <button type="button" class="btn btn-default" id="btn_infant_reduce">-</button>
                                                        </td>
                                                        <td style="text-align:center; font-size: 150%;" width="5%"><span id="txt_guest_infant">xx</span>
                                                            <input type="hidden" id="guest_infant" name="guest_infant">
                                                            </span>
                                                        </td>
                                                        <td style="text-align:left">
                                                            <button type="button" class="btn btn-default" id="btn_infant_add">+</button>
                                                        </td>
                                                    </tr>
                                                    <tr class="danger">
                                                        <td style="text-align:center; font-size: 150%;">Extra </td>
                                                        <td style="text-align:right">
                                                            <button type="button" class="btn btn-default" id="btn_extra_reduce">-</button>
                                                        </td>
                                                        <td style="text-align:center; font-size: 150%;" width="5%"><span id="txt_guest_extra">xx</span>
                                                            <input type="hidden" id="guest_extra" name="guest_extra">
                                                        </td>
                                                        <td style="text-align:left">
                                                            <button type="button" class="btn btn-default" id="btn_extra_add">+</button>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12 col-xs-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body " style="font-size: 150%;">
                                            <label>Total Prices</label>
                                            <table class="table table-bordered" id="total_prices_table">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 col-xs-12">
                                    <button  class="col-md-12 col-sm-12 col-xs-12  btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        <b>Add your details</b>
                                    </button>
                                </div>
                                <div class="collapse" id="collapseExample">
                                  <div class="form-group col-md-12 col-xs-12">
                                      <label style="text-align:center; font-size: 150%;">Name</label>
                                      <input type="text" class="form-control" name="name">
                                  </div>
                                  <div class="form-group col-md-12 col-xs-12">
                                      <label style="text-align:center; font-size: 150%;">Email</label>
                                      <input type="text" class="form-control" name="email">
                                  </div>
                                  <div class="form-group col-md-12 col-xs-12">
                                      <label style="text-align:center; font-size: 150%;">Retype Email</label>
                                      <input type="text" class="form-control" name="email_retype">
                                  </div>
                                  <div class="form-group col-md-12 col-xs-12">
                                      <label style="text-align:center; font-size: 150%;">Phone</label>
                                      <input type="text" class="form-control" name="phone">
                                  </div>
                                  <div class="form-group col-md-12 col-xs-12">
                                      <label style="text-align:center; font-size: 150%;">Message</label>
                                      <textarea class="form-control" rows="3"></textarea>
                                  </div>
                                </div>
                                <div class="form-group col-md-12 col-xs-12">
                                    <button type="submit" class="col-md-12 col-sm-12 col-xs-12  btn btn-lg btn-success">
                                        Booking
                                    </button>

                                </div>

                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
            </div>

    </div>

    <br>
    <br>

    </form>

    <!-- /.box -->
    </section>
    <!-- right col -->
    </div>
    <!-- /.row (main row) -->
    </section>

    <!-- Footer -->
    <?php
  // include('footer.php');
?>
        <div>

            <script>
                var base_url = "<?php echo base_url();?>";
            </script>

            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script>
                $.widget.bridge('uibutton', $.ui.button);
            </script>

            <!-- Bootstrap 3.3.6 -->
            <!-- <script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script> -->
            <!-- Morris.js charts -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

            <!-- daterangepicker -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

            <!-- FastClick -->
            <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>

            <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

            <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
            <!-- Bootstrap 3.3.6 -->
            <script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
            <script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
            <!-- SlimScroll -->
            <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
            <!-- FastClick -->
            <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/app.min.js"></script>
            <!-- AdminLTE for demo purposes -->
            <script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/demo.js"></script>

            <!-- daterange picker -->
            <link rel="stylesheet" href="<?php echo base_url();?>assets/css/daterangepicker.css">
            <script src="<?php echo base_url();?>assets/js/daterangepicker.js"></script>
            <script>

                // alert($.datepicker.formatDate('mm/dd/yy', new Date('01/01/2018')));
                //
                // if(new Date('08/03/2017') >= new Date('08/01/2017')){
                //   alert("Uweeee");
                // }else{
                //   alert('Noooo');
                // }

                $("[data-toggle=popover]").popover();
                var guest_max = 0;
                var guest_max_extra = 0;
                var guest_infant_max = 0;
                var guest_mature = 1;
                var guest_children = 0;
                var guest_infant = 0;
                var guest_extra = 0;
                var id = $('#id').val();
                var rates;
                var disabledArr;
                request = $.ajax({
                    url: base_url + "api/rooms/" + id + "?get_rates&get_booked_dates",
                    type: "GET",
                    dataType: 'json'
                });
                request.done(function(response, textStatus, jqXHR) {
                    // alert(response.booked_dates);
                    rates = response.rates;
                    disabledArr = response.booked_dates;
                    for (j = 0; j < rates.length; j++) {
                        if (rates[j].type == 0) {
                            $("#rate_start_from").text(rates[j].rate);
                        }
                    }
                });
                $(function() {
                    // Define the disabled date array
                    $("#checkinout").daterangepicker({
                        "opens": "center",
                        isInvalidDate: function(arg) {
                            console.log(arg);

                            // Prepare the date comparision
                            var thisMonth = arg._d.getMonth() + 1; // Months are 0 based
                            if (thisMonth < 10) {
                                thisMonth = "0" + thisMonth; // Leading 0
                            }
                            var thisDate = arg._d.getDate();
                            if (thisDate < 10) {
                                thisDate = "0" + thisDate; // Leading 0
                            }
                            var thisYear = arg._d.getYear() + 1900; // Years are 1900 based

                            var thisCompare = thisMonth + "/" + thisDate + "/" + thisYear;
                            console.log(thisCompare);

                            if ($.inArray(thisCompare, disabledArr) != -1) {
                                console.log("      ^--------- DATE FOUND HERE");
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });
                    $('#checkinout').on('hide.daterangepicker', function(ev, picker) {
                        check_prices();
                    });
                });

                function check_prices() {
                    //do something, like clearing an input
                    $('#total_prices_table').html('');
                    var checkinout = $("#checkinout").val();
                    var checkinoutsplit = checkinout.split(" - ");
                    var checkin = checkinoutsplit[0];
                    var checkout = checkinoutsplit[1];
                    var total_details = [];
                    var total_detail = [];
                    var total_extra_details = []
                    var total_extra_detail = []

                    var count_days = Math.round((new Date(checkout) - new Date(checkin)) / (1000 * 60 * 60 * 24));
                    for (i = 0; i < count_days; i++) {
                      var special_rate_founded = false;
                        for (j = 0; j < rates.length; j++) {
                            if (rates[j].type == 0) {
                                j++;
                            }
                            var rate_date_start = $.datepicker.formatDate('mm/dd/yy', new Date(rates[j].date_start));
                            var rate_date_end = $.datepicker.formatDate('mm/dd/yy', new Date(rates[j].date_end));
                            var cind = new Date(checkin);
                            var d = new Date(cind).setDate(cind.getDate() + i);
                            var date = $.datepicker.formatDate('mm/dd/yy', new Date(d));

                            if ((new Date(date) >= new Date(rate_date_start)) && (new Date(date) <= new Date(rate_date_end))) {
                                special_rate_founded = true;
                                // alert("Price is $ "+rates[j].rate+". Extra is $"+rates[j].rate_extra);
                                // alert(JSON.stringify(rates[j]));
                                total_detail = [date, rates[j].rate];
                                total_extra_detail = [date, rates[j].rate_extra * guest_extra];
                                total_details.push(total_detail);
                                total_extra_details.push(total_extra_detail);
                                // alert(total_details);
                            }
                        }
                        if(special_rate_founded == false){
                          // alert('walaa');
                          for (k = 0; k < rates.length; k++) {
                              if (rates[k].type == 0) {
                                  //Base Rate
                                  total_detail = [date, rates[k].rate];
                                  total_extra_detail = [date, rates[k].rate_extra * parseInt(guest_extra)];
                                  total_details.push(total_detail);
                                  total_extra_details.push(total_extra_detail);
                                  // alert(total_details);
                              }
                          }
                        }
                        special_rate_founded = false;
                    }
                    var total_per_night = 0;
                    var total_extra_per_night = 0;

                    for (i = 0; i < total_details.length; i++) {
                        total_per_night = total_per_night + parseInt(total_details[i][1]);
                        total_extra_per_night = total_extra_per_night + parseInt(total_extra_details[i][1])
                    }

                    var total = total_per_night + total_extra_per_night;

                    $('#total_prices_table').append('<tr><td>Total Per Night</td><td style="text-align:right">$' + total_per_night + '</td></tr>');
                    if (guest_extra > 0) {
                        $('#total_prices_table').append('<tr><td>Total Extra Per Night</td><td style="text-align:right">$' + total_extra_per_night + '</td></tr>');
                    }
                    $('#total_prices_table').append('<tr><td><b>Total</b></td><td style="text-align:right;border-top: thin double #000000;">$<b>' + total + '</b></td></tr>');
                }
            </script>
            <script>
                var room_info;
                request = $.ajax({
                    url: base_url + "/api/rooms/" + id + "?get_booked_dates",
                    type: "GET",
                    dataType: 'json'
                });
                request.done(function(response, textStatus, jqXHR) {
                    // alert(response.booked_dates);
                    room_info = response.info;
                    guest_max = parseInt(room_info.max_guest_normal);
                    guest_max_extra = parseInt(room_info.max_guest_extra);
                    guest_infant_max = parseInt(room_info.max_guest_infant);
                });

                function check_total_guest() {
                    if ((guest_mature + guest_children) == guest_max) {
                        $('#btn_mature_add').prop('disabled', true);
                        $('#btn_children_add').prop('disabled', true);
                    }
                    if ((guest_mature + guest_children) < guest_max) {
                        $('#btn_mature_add').prop('disabled', false);
                        $('#btn_children_add').prop('disabled', false);
                    }
                    if (guest_mature == 1) {
                        $('#btn_mature_reduce').prop('disabled', true);
                    } else {
                        $('#btn_mature_reduce').prop('disabled', false);
                    }
                    if (guest_children == 0) {
                        $('#btn_children_reduce').prop('disabled', true);
                    } else {
                        $('#btn_children_reduce').prop('disabled', false);
                    }
                    if (guest_infant == guest_infant_max) {
                        $('#btn_infant_add').prop('disabled', true);
                    } else {
                        $('#btn_infant_add').prop('disabled', false);
                    }
                    if (guest_infant == 0) {
                        $('#btn_infant_reduce').prop('disabled', true);
                    } else {
                        $('#btn_infant_reduce').prop('disabled', false);
                    }
                    if (guest_extra == guest_max_extra) {
                        $('#btn_extra_add').prop('disabled', true);
                    } else {
                        $('#btn_extra_add').prop('disabled', false);
                    }
                    if (guest_extra == 0) {
                        $('#btn_extra_reduce').prop('disabled', true);
                    } else {
                        $('#btn_extra_reduce').prop('disabled', false);
                    }

                }
                $(function() {
                    $('#guest_mature').val(guest_mature);
                    $('#guest_children').val(guest_children);
                    $('#guest_infant').val(guest_infant);
                    $('#guest_extra').val(guest_extra);

                    $('#txt_guest_mature').text(guest_mature);
                    $('#txt_guest_children').text(guest_children);
                    $('#txt_guest_infant').text(guest_infant);
                    $('#txt_guest_extra').text(guest_extra);

                    $('#btn_mature_reduce').prop('disabled', true);
                    $('#btn_children_reduce').prop('disabled', true);
                    $('#btn_infant_reduce').prop('disabled', true);
                    $('#btn_extra_reduce').prop('disabled', true);

                    $('#btn_mature_add').on('click', function() {
                        guest_mature++;
                        $('#txt_guest_mature').text(guest_mature);
                        $('#guest_mature').val(guest_mature);
                        check_total_guest();
                    });
                    $('#btn_mature_reduce').on('click', function() {
                        guest_mature--;
                        $('#txt_guest_mature').text(guest_mature);
                        $('#guest_mature').val(guest_mature);
                        check_total_guest();
                    });
                    $('#btn_children_add').on('click', function() {
                        guest_children++;
                        $('#txt_guest_children').text(guest_children);
                        $('#guest_children').val(guest_children);
                        check_total_guest();
                    });
                    $('#btn_children_reduce').on('click', function() {
                        guest_children--;
                        $('#txt_guest_children').text(guest_children);
                        $('#guest_children').val(guest_children);
                        check_total_guest();
                    });
                    $('#btn_infant_add').on('click', function() {
                        guest_infant++;
                        $('#txt_guest_infant').text(guest_infant);
                        $('#guest_infant').val(guest_infant);
                        check_total_guest();
                    });
                    $('#btn_infant_reduce').on('click', function() {
                        guest_infant--;
                        $('#txt_guest_infant').text(guest_infant);
                        $('#guest_infant').val(guest_infant);
                        check_total_guest();
                    });
                    $('#btn_extra_add').on('click', function() {
                        guest_extra++;
                        $('#txt_guest_extra').text(guest_extra);
                        $('#guest_extra').val(guest_extra);
                        check_total_guest();
                        check_prices();
                    });
                    $('#btn_extra_reduce').on('click', function() {
                        guest_extra--;
                        $('#txt_guest_extra').text(guest_extra);
                        $('#guest_extra').val(guest_extra);
                        check_total_guest();
                        check_prices();
                    });
                });
            </script>

</body>

</html>
