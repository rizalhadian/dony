<section class="content-header">


  <h1>
    Add New Price
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">General Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/rooms/prices/create" method="POST">
            <div class="form-group">
              <label>Type</label>
              <select class="form-control select2" name="type" style="width: 100%;">
                <option selected="selected" value="0">Base Rate</option>
                <option value="1">Seasonal</option>
                <option value="2">Promo</option>
              </select>
            </div>
            <div class="form-group">
              <label>Price Name <span class="label label-warning">"Base Rate" doesn't need name</span></label>
              <input type="text" name="name" class="form-control" id="exampleInputEmail1" >
            </div>
            <div class="form-group">
              <label>Accommodation</label>
              <input type="text" name="accommodations_id" id="accommodations_id" ></input>
              <input type="text" id="accommodationsNameAuto" class="form-control">
            </div>
            <div class="form-group">
              <label>Room</label>
              <select class="form-control select2" name="accommodation_rooms_id" id="accommodation_rooms_id" style="width: 100%;" disabled>
                <option selected="selected" value="0">Choose Room</option>
              </select>
            </div>

            <div class="form-group col-md-6">
              <label>Rate</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-usd"></i>
                </div>
              <input type="text" name="rate" class="form-control">
              </div>
            </div>
            <div class="form-group col-md-6">
              <label>Owner Rate</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-usd"></i>
                </div>
              <input type="text" name="rate_p" class="form-control">
              </div>
            </div>
            <div class="form-group col-md-6">
              <label>Date Valid Start</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date_start" class="form-control pull-right" id="datepicker1">
                </div>
            </div>
            <div class="form-group col-md-6">
              <label>Date Valid End</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="date_end" class="form-control pull-right" id="datepicker2">
                </div>
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
            </div>


          </div>
          <!-- /.box-body -->


      </div>
    </div>

  </div>







  <br>
  <br>
  <button type="submit" id="btnConfirmSave" class="hidden-xs hidden-sm navbar-fixed-bottom btn btn-lg btn-success" style="margin-left:250px; padding-left:500px;padding-right:500px;margin-bottom:10px;">
    Submit
  </button>

  <a id="smBtnConfirmSave"  class="hidden-lg hidden-md navbar-fixed-bottom btn btn-lg btn-success btn-block" ><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp Submit</a>
  </form>

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
