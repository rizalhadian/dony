<section class="content-header">

  <h1>
    Facilities & Amenities
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
        <button class="btn btn-default" data-toggle="modal" data-target="#modalFacility" id="btnFacilityAdd"><i class="fa fa-plus" aria-hidden="true" ></i> Add</button>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Name</th>
          <th>Type</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
        <?php foreach($facilities as $facility){ ?>
          <tr id="row<?php echo $facility->id; ?>">
            <td id="dataName<?php echo $facility->id; ?>"><?php echo $facility->name; ?></td>
            <td id="dataName<?php echo $facility->id; ?>"><?php if($facility->type == 0){ echo "Facilty"; }else{ echo "Amenity"; } ?></td>
            <td id="dataDescription<?php echo $facility->id; ?>"><?php echo $facility->description; ?></td>
            <td width="50px">
              <button class="btn btn-default btn-xs "  id="btnFacilityUpdate<?php echo $facility->id; ?>" value="<?php echo $facility->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
              <button class="btn btn-danger btn-xs"  id="btnFacilityDelete<?php echo $facility->id; ?>" value="<?php echo $facility->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
            </td>
          </tr>
        <?php } ?>
        </tbody>

        <!-- <tfoot>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
        </tfoot> -->

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->
<div class="modal fade" id="modalFacility" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalFacilityLabel">Add</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formFacility" role="form">
      <div class="modal-body">
        <div class="form-group">
          <label>Name</label>
          <input type="hidden" name="id" id="idUpdate" class="form-control" placeholder="">
          <input type="text" name="name" id="nameUpdate" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Accommodations Type</label>
          <select class="form-control " name="type" style="width: 100%;">
            <option value="0">Facility</option>
            <option value="1">Amenity</option>
          </select>
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="descriptionUpdate" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
