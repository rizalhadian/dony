<section class="content-header">


  <h1>
    Add New Room
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">General Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/rooms/create_do" method="POST">

            <div class="form-group">
              <label>Name</label>
              <input type="text" name="name" class="form-control" >
            </div>
            <div class="form-group">
              <label>Accommodation</label>
              <input type="text" name="accommodations_id" id="accommodations_id" ></input>
              <input type="text" id="accommodationsNameAuto" class="form-control">
            </div>

            <div class="form-group col-md-4">
              <label>Total Rooms</label>
              <input type="text" name="total_rooms" class="form-control" >
            </div>
            <div class="form-group col-md-4">
              <label>Total Bathrooms</label>
              <input type="text" name="total_bathrooms" class="form-control" >
            </div>
            <div class="form-group col-md-4">
              <label>Minimum Stay</label>
              <input type="text" name="min_stay" class="form-control" >
            </div>

            <div class="form-group col-md-6">
              <label>Maximum Guest</label>
              <input type="text" name="max_guest_normal" class="form-control" >
            </div>
            <div class="form-group col-md-6">
              <label>Maximum Extra Guest </label>
              <input type="text" name="max_guest_extra" class="form-control" >
            </div>

            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
            </div>
            <div class="form-group">
              <label>Meta / Tags</label>
              <input type="text" name="tags" class="form-control">
            </div>

          </div>
          <!-- /.box-body -->


      </div>
    </div>

  </div>

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">

          <h3 class="box-title" style="color:black">Facilities<!-- Button trigger modal -->

          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <?php foreach($facilities as $facility){ ?>
            <div class="col-md-3">
              <table class="table table-bordered">
                <tr>
                  <td style="width:100px;">
                    <input name="facilities[]" id="facilities" value="<?php echo $facility->id; ?>" type="checkbox" class="flat-red"> <?php echo $facility->name; ?></input>
                  </td>
                </tr>
              </table>
            </div>
          <?php } ?>


          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">

          <h3 class="box-title" style="color:black">Photos<!-- Button trigger modal -->

          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">


              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group well" id="imagewell">
                <input type="file" name="files[]" id="files" class="btn btn-block btn-primary" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
              </div>
            </div>


            <!-- /.col -->

            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>

  <br>
  <br>
  <button type="submit" id="btnConfirmSave" class="hidden-xs hidden-sm navbar-fixed-bottom btn btn-lg btn-success" style="margin-left:250px; padding-left:500px;padding-right:500px;margin-bottom:10px;">
    Submit
  </button>

  <a id="smBtnConfirmSave"  class="hidden-lg hidden-md navbar-fixed-bottom btn btn-lg btn-success btn-block" ><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp Submit</a>
  </form>

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
