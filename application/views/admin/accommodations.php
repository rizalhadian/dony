<section class="content-header">

  <h1>
    Accomodations
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-4 col-xs-12">
      <!-- small box -->
      <div class="small-box"
      style="background-image: url('<?php  echo base_url(); ?>assets\/images\/hotel.jpg');
              color: white;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;"
        >
        <div class="inner" style="background: rgba(0, 0, 0, 0.4);">
          <h3 id="countaccomodations"><?php echo $countAccommodations; ?></h3>
          <p>Accomodations</p>
        </div>

        <!-- <a href="<?php echo base_url(); ?>admin/accommodations/add_new" class="btn btn-block" style="background: rgba(0, 0, 0, 0.9); color:white;">Add New <i class="fa fa-plus"></i></a> -->
        <a  data-toggle="modal" data-target="#modalNewAccommodation" class="btn btn-block" style="background: rgba(0, 0, 0, 0.9); color:white;">Add New <i class="fa fa-plus"></i></a>

      </div>
    </div>
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-yellow-gradient">
        <div class="box-header">

          <h3 class="box-title">Accommodations Need Approval</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">

            <button type="button" class="btn btn-warning btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
        <table id="example1" class="table table-bordered table-striped" style="font-size: 90%;">
        <thead>
        <tr>
          <th>Name</th>
          <th>Location</th>
          <th>Bedrooms</th>
          <th>Experiences</th>
          <th>Class</th>
          <th>Management</th>
          <th>Partner</th>
          <th>Link</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
        <!-- Tes for lots datas -->
        <?php for($i = 0; $i<1; $i++){ ?>

          <?php foreach($accommodations_need_approval as $accommodation){ ?>
            <tr id="rowAccommodation<?php echo $accommodation->id; ?>">
              <td id="dataNameAccommodation<?php echo $accommodation->id; ?>"><?php echo $accommodation->name; ?></td>
              <td id="dataLocationAccommodation<?php echo $accommodation->id; ?>"><?php foreach($accommodation->locations as $location){ echo $location.', '; } ?></td>
              <td><?php foreach($accommodation->rooms as $room){echo $room." rooms, ";} ?></td>
              <td>Wedding, Family, Beach View</td>
              <td><?php if($accommodation->class == 0){echo "Affordable";}elseif($accommodation->class == 1){echo "Luxury";}else{echo "Elite";} ?></td>
              <td><?php if($accommodation->management == 0){echo "Other Management";}else{echo "Total Bali";} ?></td>
              <td><?php echo $accommodation->partners_name." | ".$accommodation->partners_email; ?></td>
              <td id="dataLocationAccommodation<?php echo $accommodation->id; ?>">
                <center>
                <?php if($accommodation->website){ ?>
                  <a href="<?php echo $accommodation->website; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">Main Site</a>
                <?php } ?>
                <?php if($accommodation->chanel_booking_com){ ?>
                  <a href="<?php echo $accommodation->chanel_booking_com; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">booking.com</a>
                <?php } ?>
                <?php if($accommodation->chanel_airbnb_com){ ?>
                  <a href="<?php echo $accommodation->chanel_airbnb_com; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">airbnb.com</a>
                <?php } ?>
                <?php if($accommodation->chanel_flipkey_com){ ?>
                  <a href="<?php echo $accommodation->chanel_flipkey_com; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">flipkey.com</a>
                <?php } ?>
                <?php if($accommodation->chanel_other){ ?>
                  <a href="<?php echo $accommodation->chanel_other; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">Other Chanel</a>
                <?php } ?>


                </center>
              </td>



              <td>
                <center>
                <!-- <button class="btn btn-default btn-xs "  id="btnAccommodationView<?php echo $accommodation->id; ?>" value="<?php echo $accommodation->id; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View</button> -->
                <a class="btn btn-block btn-default btn-xs" style="margin-bottom:2px" href="<?php echo base_url(); ?>admin/accommodations/update/<?php echo $accommodation->id; ?>"  id="btnAccommodationUpdate<?php echo $accommodation->id; ?>" value="<?php echo $accommodation->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <button class="btn btn-block btn-success btn-xs" style="margin-bottom:2px"  id="btnAccommodationApprove" value="<?php echo $accommodation->id; ?>"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Approve</button>
                <button class="btn btn-block btn-danger btn-xs" style="margin-bottom:2px"  id="btnAccommodationDelete<?php echo $accommodation->id; ?>" value="<?php echo $accommodation->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </center>
              </td>
            </tr>
          <?php } ?>
        <?php } ?>
        </tbody>
      </table>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-blue-gradient">
        <div class="box-header">

          <h3 class="box-title">Accommodations</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">

            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
        <table id="example2" class="table table-bordered table-striped" style="font-size: 90%;">
        <thead>
        <tr>
          <th>Name</th>
          <th>Location</th>
          <th>Bedrooms</th>
          <th>Experiences</th>
          <th>Class</th>
          <th>Management</th>
          <th>Partner</th>
          <th>Status</th>
          <th>Link</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
        <!-- Tes for lots datas -->
        <?php for($i = 0; $i<1; $i++){ ?>
          <?php foreach($accommodations as $accommodation){ ?>
            <tr id="rowAccommodation<?php echo $accommodation->id; ?>" <?php if($accommodation->active == 0){echo 'class="danger"';} ?>>
              <td id="dataNameAccommodation<?php echo $accommodation->id; ?>"><?php echo $accommodation->name; ?></td>
              <td id="dataLocationAccommodation<?php echo $accommodation->id; ?>"><?php foreach($accommodation->locations as $location){ echo $location.', '; } ?></td>
              <td><?php foreach($accommodation->rooms as $room){echo $room." rooms, ";} ?></td>
              <td>Wedding, Family, Beach View, Ocean View, Cliff Front, Yearly Rent,</td>
              <td><?php if($accommodation->class == 0){echo "Affordable";}elseif($accommodation->class == 1){echo "Luxury";}else{echo "Elite";} ?></td>
              <td><?php if($accommodation->management == 0){echo "Other Management";}else{echo "Total Bali";} ?></td>
              <td><?php echo $accommodation->partners_name." | ".$accommodation->partners_email; ?></td>
              <td><?php if($accommodation->active == 0){echo "<b>Not Active</b>";}elseif($accommodation->active == 1){echo "Active";} ?></td>
              <td id="dataLocationAccommodation<?php echo $accommodation->id; ?>">
                <center>
                <?php if($accommodation->website){ ?>
                  <a href="<?php echo $accommodation->website; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">Main Site</a>
                <?php } ?>
                <?php if($accommodation->chanel_booking_com){ ?>
                  <a href="<?php echo $accommodation->chanel_booking_com; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">booking.com</a>
                <?php } ?>
                <?php if($accommodation->chanel_airbnb_com){ ?>
                  <a href="<?php echo $accommodation->chanel_airbnb_com; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">airbnb.com</a>
                <?php } ?>
                <?php if($accommodation->chanel_flipkey_com){ ?>
                  <a href="<?php echo $accommodation->chanel_flipkey_com; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">flipkey.com</a>
                <?php } ?>
                <?php if($accommodation->chanel_other){ ?>
                  <a href="<?php echo $accommodation->chanel_other; ?>" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">Other Chanel</a>
                <?php } ?>


                </center>
              </td>



              <td>
                <center>
                <!-- <button class="btn btn-default btn-xs "  id="btnAccommodationView<?php echo $accommodation->id; ?>" value="<?php echo $accommodation->id; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View</button> -->
                <a class="btn btn-block btn-default btn-xs" style="margin-bottom:2px" href="<?php echo base_url(); ?>admin/accommodations/update/<?php echo $accommodation->id; ?>"  id="btnAccommodationUpdate<?php echo $accommodation->id; ?>" value="<?php echo $accommodation->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>

                <?php if($accommodation->active == 0){ ?>
                  <button class="btn btn-block btn-success btn-xs" style="margin-bottom:2px"  id="btnAccommodationActivate" value="<?php echo $accommodation->id; ?>"><i class="fa fa-check" aria-hidden="true"></i> Activate</button>
                <?php }else{ ?>
                  <button class="btn btn-block btn-danger btn-xs" style="margin-bottom:2px"  id="btnAccommodationDeactivate" value="<?php echo $accommodation->id; ?>"><i class="fa fa-times" aria-hidden="true"></i> Deactivate</button>
                <?php } ?>

                <?php if($accommodation->management == 1){ ?>
                  <button class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px"  id="btnAccommodationSync" value="<?php echo $accommodation->id; ?>"><i class="fa fa-refresh" aria-hidden="true"></i> Sync</button>
                <?php } ?>
                <button class="btn btn-block btn-danger btn-xs" style="margin-bottom:2px"  id="btnAccommodationDelete<?php echo $accommodation->id; ?>" value="<?php echo $accommodation->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </center>
              </td>
            </tr>
          <?php } ?>
        <?php } ?>
        </tbody>
      </table>
        </div>
      </div>
    </div>
  </div>




      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->
<div class="modal fade" id="modalAccommodation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel">View Accommodation</h4>
      </div>

      <div class="modal-body row">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Accommodation</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Rooms and Prices</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Photos</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">...</div>
            <div role="tabpanel" class="tab-pane" id="profile">...</div>
            <div role="tabpanel" class="tab-pane" id="messages">...</div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Add New Accommodation by Admin or by Owner  -->
<div class="modal fade" id="modalNewAccommodation" tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Who will add the Accommodations data??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        </div>
        <div class="row">
          <div class="col-md-6"><a href="<?php echo base_url(); ?>admin/accommodations/add_new" class="btn btn-primary btn-block">Admin</a></div>
          <div class="col-md-6"><a href="<?php echo base_url(); ?>admin/accommodations/add_new_by_owner" class="btn btn-default btn-block disabled">Generate Form for Owner</a></div>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="whichDelete" id="whichDelete">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Sync -->
<div class="modal fade" id="modalSync" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel">Sync</h4>
      </div>

      <div class="modal-body row">
        <div class="col-md-12">
          <center><button class="btn btn-block btn-primary" style="margin-bottom:2px"  id="btn_sync_now" value=""><span id="loading_icon"><i class="fa fa-refresh" aria-hidden="true"></i></span> Sync Now</button></center>
          <input type="hidden" id="id_to_sync">
          <br>
          <table class="table table-bordered" id="dataSync" style="font-size: 80%;">
            <tr>
              <th>OTA</th>
              <th>OTA Id</th>
              <th>Title</th>
              <th>Link</th>
              <th>Status</th>
              <th>Status</th>

            </tr>
            <!-- Airbnb -->
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>

            </tr>
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
            </tr>
            <!-- End Airbnb -->

            <!-- FLipkey -->
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/flipkey.png" width="120px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>

            </tr>
            <!-- End Flipkey -->
          </table>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div> -->
    </div>
  </div>
</div>
<!-- End Modal Sync -->
