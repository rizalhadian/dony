<section class="content-header">
  <?php
    // echo "<br>======================================== Data View | ==================================<br>";
    // foreach($accommodations as $acc){
    //   print_r($acc->booked_dates);
    // }
    // echo "<br>====================================== End Data View ==================================<br>";
  ?>
  <h1>
    Availability
  </h1>
  <div id="alertinformations">
  </div>

  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Availability</li>
  </ol>
</section>

<!-- Main content -->

<section class="content">
  <div class="box">
    <div class="box-header">
      <form method="get" action="<?php echo base_url();?>admin/availability">
      <div class="col-lg-2">
        <div class="form-group">
          <label>Date From</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text"
              name="date"
              class="form-control pull-right"
              id="datepicker1"
              value="<?php if($this->input->get("date")){echo date('m/d/Y', strtotime($this->input->get("date")));}else{ echo date('m/d/Y'); } ?>"
            >
          </div>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="form-froup">
          <label>Accommodations Name</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-home" aria-hidden="true"></i>
            </div>
            <input type="text" name="name" class="form-control pull-right" <?php if($this->input->get("name")){ echo "value='".$this->input->get("name")."'"; } ?>>
          </div>
        </div>

      </div>


      <!-- <div class="col-lg-3">
        <div class="form-froup">
          <label>Location</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-map-o" aria-hidden="true"></i>
            </div>
            <input type="hidden" name="locations_id" id="locations_id">
            <input type="text" name="location" id="locationAdd" class="form-control pull-right" <?php if($this->input->get("location")){ echo "value='".$this->input->get("location")."'"; } ?>>
          </div>
        </div>
      </div> -->

      <div class="col-lg-3">
        <div class="form-froup">
          <label>Location</label>
          <select class="form-control select2" name="locations[]"  multiple="multiple" data-placeholder="Select Locations" style="width: 100%;">
            <!-- <option value="1">Alabama</option>
            <option value="2">Alaska</option>
            <option value="3">California</option>
            <option value="4">Delaware</option>
            <option value="5">Tennessee</option>
            <option value="6">Texas</option>
            <option value="7">Washington</option> -->

            <?php foreach($locations as $location){ ?>
              <option value="<?php echo $location->id; ?>"><?php echo $location->name.", ".$location->parent_name ?></option>
            <?php } ?>
          </select>
        </div>
      </div>


      <div class="col-lg-2">
        <div class="form-froup">
          <label>Bedrooms</label>
          <select class="form-control select2" name="bedrooms[]" multiple="multiple" data-placeholder="Select Bedrooms" style="width: 100%;">
            <option value="= 1">1 Bedrooms</option>
            <option value="= 2">2 Bedrooms</option>
            <option value="= 3">3 Bedrooms</option>
            <option value="= 4">4 Bedrooms</option>
            <option value="= 5">5 Bedrooms</option>
            <option value="> 5">> 5 Bedrooms</option>
          </select>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="form-froup">
          <label>Rates Class</label>
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-usd" aria-hidden="true"></i>
            </div>
            <select class="form-control" style="width: 100%;" name="rates">
              <option value="all">All</option>
              <option value="0">Affordable</option>
              <option value="1">Luxury</option>
              <option value="2">Elite</option>
            </select>
          </div>
        </div>
      </div>

      <div class="col-lg-">
         <input style="margin-top:25px; width:70px;" type="submit" class="btn btn-success btn-sm" value="Go" name="">
      </div>
      </form>

    </div>

    <!-- <div class="">
      <table class="table table-bordered">
        <tr>
          <td></td>
          <td><center>Available</center></td>
          <td></td>
          <td class="enquiry"><center>Enquiry</td>
          <td></td>
          <td class="onhold"><center>Onhold</td>
          <td></td>
          <td class="conf"><center>Confirmed</td>
          <td></td>
          <td class="highseason"><center>High Season</td>
          <td></td>
          <td class="peakseason"><center>Peak Season</td>
          <td></td>
          <td class="specialrate"><center>Special Rate</td>
          <td></td>
        </tr>
      </table>
    </div> -->

    <!-- /.box-header -->
    <div class="box-body">
      <div class="col-md-12 col-xs-12">
        <div class="col-md-3 col-xs-3">
          <table style="font-size:10px;" class="table table-bordered table-condensed ;">
            <tr style="font-weight:bold; " rowspan=2>
              <th  style="min-width:200px;vertical-align:middle; border-bottom:none"><center>Accommodations</center></th>
            </tr>
            <tr style="font-weight:bold; " >
              <th>.</th>
            </tr>



            <?php if($accommodations != null){ ?>
              <?php foreach($accommodations as $acc){ ?>

                <tr style="font-weight:bold; background:<?php if($acc->flag_ics_valid == 0){echo "#F5C6CB;";}else{echo "#ECF0F5;";} ?>" height="30px">

                <!-- <tr style="font-weight:bold; background:#F5C6CB;" height="30px"> -->
                  <th><?php echo $acc->name.", ".$acc->location; ?> <?php if($acc->flag_ics_valid == 0){ echo "<b>( ICS Broken )</b>"; } ?></th>
                </tr>
                <?php foreach($acc->rooms as $room){ ?>
                  <tr style="font-weight:bold;" height="30px">
                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $room->name; ?></th>
                  </tr>
                <?php } ?>
              <?php } ?>
            <?php } ?>




          </table>
        </div>
        <div class="col-md-9 col-xs-9 table-responsive">
          <table style="font-size:10px;" class="table table-bordered table-condensed">
            <tr style="font-weight:bold;" >
              <?php foreach($calendars as $key=>$month){ ?>
                <?php if($key == 0 || $key == (sizeof($calendars)-1) || $month['d'] == 1){ ?>
                  <th ><center><?php print_r($month['M']);  ?></th>
                <?php }else{ ?>
                  <th ><center></th>
                <?php } ?>
              <?php } ?>
            </tr>
            <tr style="font-weight:bold;" >
              <?php foreach($calendars as $cal){ ?>
              <th colspan="1"><center><?php echo $cal['D'];  ?></th>
              <?php } ?>
            </tr>
            <?php if($accommodations != null){ ?>
              <?php foreach($accommodations as $acc){ ?>
                <tr style="font-weight:bold; background:<?php if($acc->flag_ics_valid == 0){echo "#F5C6CB;";}else{echo "#ECF0F5;";} ?>" height="30px">
                  <th colspan="27"><center>========================&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $acc->name.", ".$acc->location; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;========================</center></th>
                </tr>
                <?php foreach($acc->rooms as $room){ ?>
                  <tr style="font-weight:bold;" height="30px">

                    <?php
                    for($cal=0; $cal<sizeof($calendars);$cal++){
                    ?>
                    <th
                      height="30px"

                      <?php
                        $before = false;
                        $after = false;
                        $bookedChooseDay = false;
                        foreach($acc->booked_dates as $booked_date){
                            if(date('Y/m/d', strtotime($booked_date)) == $calendars[$cal]['date']){
                              if($cal==0){
                                $before = true;
                                $after = true;
                              }
                              foreach($acc->booked_dates as $booked_date2){
                                if($cal!=sizeof($calendars)-1){
                                  if(date('Y/m/d', strtotime($booked_date2)) == $calendars[$cal+1]['date'] && $cal!=0){
                                    $after = true;
                                  }
                                }
                              }
                              foreach($acc->booked_dates as $booked_date3){
                                  if($cal!=0){
                                    if(date('Y/m/d', strtotime($booked_date3)) == $calendars[$cal-1]['date']){
                                      $before = true;
                                    }
                                  }
                              }
                              // echo 'style="background:grey"';
                            }
                        }
                        if(($before == true && $after == true)){
                          echo 'style="background:grey; text-align:center;"';
                        }
                        if(($before == true && $after == false)){
                          echo 'style="background: linear-gradient(to right, grey 0%,grey 50%,#000000 50%,white 50%,white 100%); text-align:center;"';
                        }
                        if(($before == false && $after == true)){
                          echo 'style="background: linear-gradient(to left, grey 0%,grey 50%,#000000 50%,white 50%,white 100%); text-align:center;"';
                        }

                      ?>


                    >
                    <?php if($acc->flag_ics_valid == 0){echo "xx";}else{ echo $calendars[$cal]['d']; } ?>


                    </th>
                    <?php
                    }
                    ?>
                  </tr>
                <?php } ?>
              <?php } ?>
            <?php } ?>




          </table>
        </div>


      </div>
    </div>
    <!-- /.box-body -->
    <!-- Button trigger modal -->
    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->
    </div>

  <!-- /.row -->
      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->



</section>
