<section class="content-header">

  <h1>
    Agents
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
        <button class="btn btn-default" data-toggle="modal" data-target="#modalExperience" id="btnExperienceAdd"><i class="fa fa-plus" aria-hidden="true" ></i> Add Agent</button>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Code</th>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
          <?php foreach($experiences as $experience){ ?>
          <tr id="row<?php echo $experience->id; ?>">
            <td id="dataCode<?php echo $experience->id; ?>"><?php echo $experience->code; ?></td>
            <td id="dataName<?php echo $experience->id; ?>"><?php echo $experience->name; ?></td>
            <td id="dataEmail<?php echo $experience->id; ?>"><?php echo $experience->email; ?></td>
            <td id="dataPhone<?php echo $experience->id; ?>"><?php echo $experience->phone; ?></td>
            <td width="50px">
              <button class="btn btn-default btn-xs btn-block"  id="btnExperienceUpdate<?php echo $experience->id; ?>" value="<?php echo $experience->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
              <button class="btn btn-danger btn-xs btn-block"  id="btnExperienceDelete<?php echo $experience->id; ?>" value="<?php echo $experience->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
            </td>
          </tr>
        <?php } ?>
        </tbody>

        <!-- <tfoot>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
        </tfoot> -->

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->

<div class="modal fade" id="modalExperience" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalExperienceLabel">Add Agent</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formExperience" role="form">
      <div class="modal-body">
        <div class="form-group">
          <label>Code</label>
          <input type="text" readonly="readonly" name="code" id="codeUpdate" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Name</label>
          <input type="hidden" name="id" id="idUpdate" class="form-control" placeholder="">
          <input type="text" name="name" id="nameUpdate" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="emailUpdate" class="form-control" placeholder="">
        </div>
        <div class="row">
          <div class="form-group col-lg-3 col-md-3">
            <label>Phone</label>
            <input type="text" name="phone[]" id="phoneUpdate0" class="form-control" style="width:60px;">
          </div>
          <div class="form-group col-lg-9 col-md-9">
            <label>&nbsp;</label>
            <input type="text" name="phone[]" id="phoneUpdate1" class="form-control">
          </div>
        </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-success">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
