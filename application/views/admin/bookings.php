<section class="content-header">

  <h1>
    Bookings
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>0</h3>

          <p>Bookings Need Invoice</p>
        </div>
        <div class="icon">
          <!-- <i class="ion ion-person-add"></i> -->
        </div>

      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>0</h3>

          <p>Bookings Issued Invoice</p>
        </div>
        <div class="icon">
          <!-- <i class="fa fa-shopping-cart"></i> -->
        </div>

      </div>
    </div>

  </div>
  <!-- /.row -->



  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-black-gradient">
        <div class="box-header">

          <h3 class="box-title">Bookings

          </h3>
          <a href="<?php echo base_url();?>admin/bookings/add_new" class="btn btn-success">
            Add New Booking <i class="fa fa-plus" aria-hidden="true"></i>
          </a>
          <!-- tools box -->
          <div class="pull-right box-tools">


          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
        <table id="example2" class="table table-bordered table-striped" style="font-size: 90%;">
        <thead>
        <tr>
          <th>Code</th>
          <th>Guest Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Payment Method</th>
          <th>Agent</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
        <!-- Tes for lots datas -->
        <?php foreach($bookings as $booking){ ?>
          <tr id="rowBooking<?php echo $booking->id; ?>">
            <td id="dataCodeBooking<?php echo $booking->id; ?>"><?php echo $booking->code; ?></td>
            <td id="dataGuestNameBooking<?php echo $booking->id; ?>"><?php echo $booking->first_name." ".$booking->last_name; ?></td>
            <td id="dataEmailBooking"><?php echo $booking->email; ?></td>
            <td id="dataPhoneBooking"><?php echo $booking->phone ?></td>
            <td id="dataPaymentMethodBooking"><?php if($booking->payment_method === 0){ echo "Credit Card"; }else if($booking->payment_method == 1){ echo "PayPal"; }else if($booking->payment_method == 2){ echo "Bank Transfer"; }?></td>
            <td id="dataAgentBooking"><?php echo $booking->agents_name." | ".$booking->agents_code; ?></td>
            <td id="dataLocationAccommodation<?php echo $booking->id; ?>">
              <center>
                <a href="" class="btn btn-primary btn-block"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="" class="btn btn-danger btn-block"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>


              </center>
            </td>




          </tr>
        <?php } ?>
        </tbody>
      </table>
        </div>
      </div>
    </div>
  </div>




      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->
<div class="modal fade" id="modalAccommodation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel">View Accommodation</h4>
      </div>

      <div class="modal-body row">
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Accommodation</a></li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Rooms and Prices</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Photos</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">...</div>
            <div role="tabpanel" class="tab-pane" id="profile">...</div>
            <div role="tabpanel" class="tab-pane" id="messages">...</div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Add New Accommodation by Admin or by Owner  -->
<div class="modal fade" id="modalNewAccommodation" tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Who will add the Accommodations data??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        </div>
        <div class="row">
          <div class="col-md-6"><a href="<?php echo base_url(); ?>admin/accommodations/add_new" class="btn btn-primary btn-block">Admin</a></div>
          <div class="col-md-6"><a href="<?php echo base_url(); ?>admin/accommodations/add_new_by_owner" class="btn btn-default btn-block disabled">Generate Form for Owner</a></div>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="whichDelete" id="whichDelete">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Sync -->
<div class="modal fade" id="modalSync" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel">Sync</h4>
      </div>

      <div class="modal-body row">
        <div class="col-md-12">
          <center><button class="btn btn-block btn-primary" style="margin-bottom:2px"  id="btn_sync_now" value=""><span id="loading_icon"><i class="fa fa-refresh" aria-hidden="true"></i></span> Sync Now</button></center>
          <input type="hidden" id="id_to_sync">
          <br>
          <table class="table table-bordered" id="dataSync" style="font-size: 80%;">
            <tr>
              <th>OTA</th>
              <th>OTA Id</th>
              <th>Title</th>
              <th>Link</th>
              <th>Status</th>
              <th>Status</th>

            </tr>
            <!-- Airbnb -->
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>

            </tr>
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
            </tr>
            <!-- End Airbnb -->

            <!-- FLipkey -->
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/flipkey.png" width="120px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>

            </tr>
            <!-- End Flipkey -->
          </table>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div> -->
    </div>
  </div>
</div>
<!-- End Modal Sync -->
