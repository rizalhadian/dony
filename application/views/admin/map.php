<section class="content-header">

  <h1>
    Map
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">

    </div>

    <!-- /.box-header -->

    <div class="box-body">

      <!-- Location -->
      <input hidden name="map_accommodation_lat" id="map_accommodation_lat" value="">
      <input hidden name="map_accommodation_lng" id="map_accommodation_lng" value="">
      <!-- map_for_villa; map_for_public;  -->
      <input hidden name="map_type" id="map_type" value="map_for_admin">
      <input hidden name="map_accommodation_id" id="map_accommodation_id" value="">
      <input hidden name="map_marker_accommodations_all" id="map_marker_accommodations_all" value="1">
      <input hidden name="map_marker_accommodations_specific" id="map_marker_accommodations_specific" value="">
      <input hidden name="map_marker_restaurants_all" id="map_marker_restaurants_all" value="1">
      <input hidden name="map_marker_restaurants_specific" id="map_marker_restaurants_specific" value="">
      <input hidden name="map_marker_beaches_all" id="map_marker_beaches_all" value="1">
      <input hidden name="map_marker_beaches_specific" id="map_marker_beaches_specific" value="">

      <div class="form-group col-md-12">
        <div class="panel-body" id="map-canvas" style="height:410px; background-color:gray">
            <br><br><br><br><br><br><br><br>
              <center><h4><span class="label label-danger">Sorry, Please Reload the Page</span></h4></center>
        </div>
      </div>


    </div>



    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->

<div class="modal fade" id="modalExperience" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalExperienceLabel">Add Interesting Place</h4>
      </div>

      <form enctype="multipart/form-data" action="" method="POST" id="formExperience" role="form">
      <div class="modal-body">
        <div class="col-md-12">

            <div class="form-group">
              <label>Name</label>
              <input type="hidden" name="id" id="idUpdate" class="form-control" placeholder="">
              <input type="text" name="name" id="nameUpdate" class="form-control" placeholder="">
            </div>
            <div class="form-group">
              <label>Type</label>
              <select class="form-control " name="type" id="type" style="width: 100%;">
                <option selected="selected" value="Restaurant">Restaurant</option>
                <option  value="Cafe">Cafe</option>
                <option  value="Club">Club</option>
                <option  value="Surf Point">Surf Point</option>
                <option  value="Event">Event</option>
              </select>
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="3" placeholder=""></textarea>
            </div>

            <div id="restaurantForm" class="row">
              <div class="modal-body">
                <div class="form-group">
                  <label>Zomato URL</label>
                  <input type="text" name="zomato_url" class="form-control" placeholder="">
                </div>
              </div>
            </div>

            <div id="surfForm" class="row">
              <div class="modal-body">
                <div class="form-group">
                  <label>Difficult</label>
                  <select class="form-control " name="surf_difficult" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Beginner">Beginner</option>
                    <option  value="Intermediate">Intermediate</option>
                    <option  value="Advance">Advance</option>
                    <option  value="All">All</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wave Direction</label>
                  <select class="form-control " name="surf_wave_direction" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Left">Left</option>
                    <option  value="Right">Right</option>
                    <option  value="All">All</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wave Type</label>
                  <select class="form-control " name="surf_wave_type" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Reef Break">Reef Break</option>
                    <option  value="Point Break">Point Break</option>
                    <option  value="Beach Break">Beach Break</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wind</label>
                  <select class="form-control " name="surf_wind" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="South East">South East</option>
                    <option  value="South West">South West</option>
                    <option  value="North East">North East</option>
                    <option  value="North West">North West</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tide</label>
                  <select class="form-control " name="surf_tide" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Low">Low</option>
                    <option  value="Mid">Mid</option>
                    <option  value="High">High</option>
                    <option  value="All">All</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="panel-body" id="map-canvas" style="height:610px; background-color:gray">
                <br><br><br><br><br><br><br><br>
                  <center><h4><span class="label label-danger">Sorry, Please Reload the Page</span></h4></center>
            </div>
          </div>



        </div>





        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnConfirmSave" class="btn btn-success">Save</button>
          <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
        </div>



      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
