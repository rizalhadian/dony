<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Total Bali-Livebalivillas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.min.css"> -->
  <!-- jQuery 3.1.1 -->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->


  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/iCheck/all.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.css">

  <!-- Kraje-File-Input -->
  <link href="<?php echo base_url();?>assets/kraje-file-input/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link href="<?php echo base_url();?>assets/kraje-file-input//themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
  <script src="<?php echo base_url();?>assets/kraje-file-input//js/plugins/sortable.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input//js/fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input//js/locales/fr.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input//js/locales/es.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input//themes/explorer-fa/theme.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input//themes/fa/theme.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

  <!-- Froala Editor -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/froala_editor.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/froala_style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/plugins/code_view.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/plugins/image_manager.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/plugins/image.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/plugins/table.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/froala_editor/css/plugins/video.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>
  <?php if($this->session->userdata('ses') == "accommodations_addnew"){ ?>
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>

  <?php } ?>


</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar.php'); ?>

<!-- Content -->
<?php include('content.php'); ?>

<!-- Footer -->
<?php
  // include('footer.php');
?>
<div>


<script>
  var base_url = "<?php echo base_url();?>";
</script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

<?php if($this->session->userdata('ses') == "accommodations_addnew"){ ?>

    <!-- js -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <?php } ?>

<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>



<!-- Bootstrap 3.3.6 -->

<script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="https://getbootstrap.com/assets/js/docs.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/demo.js"></script>

<!-- page script -->

<script>
  $("[data-toggle=popover]").popover();

  $(function () {

    $("#example1").DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });


    $('#example3').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });


    $('#example4').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

    $('#example5').DataTable({
      "paging": true,
      "lengthChange": false,
      "pageLength": 4,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": true
    });

  });

</script>

<?php if($this->session->userdata('ses') == "services"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-services.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "facilities"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-facilities.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "additional_rates"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-additional-rates.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "experiences"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-experiences.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "agents"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-agents.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "locations"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-locations.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodationtypes"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodation-types.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "activitytypes"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-activity-types.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodations"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "interesting_place_types"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-interesting-place-types.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodations_addnew" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-addnew.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "map" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-map.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodations_update" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-update.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodation_rooms_addnew"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodation-rooms.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodation_room_prices_addnew"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodation-room-prices.js"></script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "activities"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-activities.js"></script>
<?php } ?>

<?php if($this->session->userdata('ses') == "interesting_places"  || $this->session->userdata('ses') == "interesting_places_create"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-addnew.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
  <script>
  $('#btnInterestingPlaceAdd').click(function(){
    alert('asd');
    initMap();
  });

  $('#surfForm').hide();
  $('#type').on('change', function() {

    if(this.value != "18"){
      $('#restaurantForm').show();
      $('#surfForm').hide();
    }else if(this.value == "18" ){
      $('#surfForm').show();
      $('#restaurantForm').hide();
    }
  });

  </script>
<?php } ?>

<?php if($this->session->userdata('ses') == "partners"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-partners.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "availability"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-availability.js"></script>
  <!-- availability css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/totalbali-availability.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "activities_create"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-activities-create.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
<?php } ?>

<?php if($this->session->userdata('ses') == "bookings_addnew"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-bookings.js"></script>

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <!-- bootstrap datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <script>
  //Date picker
  $('#datepicker-checkin').datepicker({
    autoclose: true
  });
  $('#datepicker-checkout').datepicker({
    autoclose: true
  });
  </script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>



<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/iCheck/icheck.min.js"></script>
<script>
$(function(){
  //iCheck for checkbox and radio inputs
  new $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });
});
</script>
<script>
$(function(){
  //iCheck for checkbox and radio inputs
  new $('input[type="checkbox"].minimal-1, input[type="radio"].minimal-1').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });
});
</script>





</body>

</html>
