<section class="content-header">

  <h1>
    Blog
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-12">
      <!-- small box -->
      <div class="small-box"
      style="background-image: url('<?php  echo base_url(); ?>assets/images/partners.jpg');
              color: white;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;"
        >
        <div class="inner" style="background: rgba(0, 0, 0, 0.4);">
          <h3 id="countaccomodations"><?php echo $countPartners; ?></h3>
          <p>Partners</p>
        </div>

        <button data-toggle="modal" data-target="#modalPartner" class="btn btn-block" style="background: rgba(0, 0, 0, 0.9); color:white;" id="btnPartnerCreate">Add New <i class="fa fa-plus"></i></button>
      </div>
    </div>
    <div class="col-lg-3 col-xs-12">
      <!-- small box -->
      <div class="small-box"
      style="background-image: url('<?php  echo base_url(); ?>assets/images/activities.jpg');
              color: white;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;"
        >
        <div class="inner" style="background: rgba(0, 0, 0, 0.4);">
          <h3 id="countaccomodations">null</h3>
          <p>Activities</p>
        </div>


        <a href="<?php echo base_url(); ?>admin/activities/create" class="btn btn-block" style="background: rgba(0, 0, 0, 0.9); color:white;">Add New <i class="fa fa-plus"></i></a>

      </div>
    </div>
    <div class="col-lg-3 col-xs-12">
      <!-- small box -->
      <div class="small-box"
      style="background-image: url('<?php  echo base_url(); ?>assets/images/packages.jpg');
              color: white;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;"
        >
        <div class="inner" style="background: rgba(0, 0, 0, 0.4);">
          <h3 id="countaccomodations">null</h3>
          <p>Packages</p>
        </div>


        <a href="<?php echo base_url(); ?>admin/accommodations/rooms/prices/add_new" class="btn btn-block" style="background: rgba(0, 0, 0, 0.9); color:white;">Add New <i class="fa fa-plus"></i></a>

      </div>
    </div>

    <div class="col-lg-3 col-xs-12">
      <!-- small box -->
      <div class="small-box"
      style="background-image: url('<?php  echo base_url(); ?>assets/images/rate.jpg');
              color: white;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;"
        >
        <div class="inner" style="background: rgba(0, 0, 0, 0.4);">
          <h3 id="countaccomodations">null</h3>
          <p>Prices</p>
        </div>


        <a href="<?php echo base_url(); ?>admin/accommodations/rooms/prices/add_new" class="btn btn-block" style="background: rgba(0, 0, 0, 0.9); color:white;">Add New <i class="fa fa-plus"></i></a>

      </div>
    </div>

  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-blue-gradient">
        <div class="box-header">

          <h3 class="box-title">Partners</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
        <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Name</th>
          <th>Address</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
          <?php foreach($partners as $partner){ ?>
          <tr id="rowPartner<?php echo $partner->id; ?>">
            <td id="dataPartnerName<?php echo $partner->id; ?>"><?php echo $partner->name; ?></td>
            <td id="dataPartnerAddress<?php echo $partner->id; ?>"><?php echo $partner->address; ?></td>
            <td id="dataPartnerEmail<?php echo $partner->id; ?>"><?php echo $partner->email; ?></td>
            <td id="dataPartnerPhone<?php echo $partner->id; ?>"><?php echo $partner->phone; ?></td>



            <td width="160px">
              <!-- <button class="btn btn-default btn-xs "  id="btnAccommodationView<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View</button> -->
              <button class="btn btn-default btn-xs"  id="btnPartnerUpdate<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
              <button class="btn btn-danger btn-xs"  id="btnPartnerDelete<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
            </td>
          </tr>
        <?php } ?>
        </tbody>

      </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-blue-gradient">
        <div class="box-header">

          <h3 class="box-title">Activities</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <table id="example2" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Accommodation</th>
              <th>Room Name</th>
              <th>Location</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody id="dataRows1">

            </tbody>
          </table>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-blue-gradient">
        <div class="box-header">

          <h3 class="box-title">Packages</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <table id="example3" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Accommodation Room</th>
              <th>Rate Name</th>
              <th>Rate</th>
              <th>Type</th>
              <th>Date Start</th>
              <th>Date End</th>
              <th>Action</th>

            </tr>
            </thead>
            <tbody id="dataRows1">

            </tbody>
          </table>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-blue-gradient">
        <div class="box-header">

          <h3 class="box-title">Prices</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <table id="example4" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Accommodation Room</th>
              <th>Rate Name</th>
              <th>Rate</th>
              <th>Type</th>
              <th>Date Start</th>
              <th>Date End</th>
              <th>Action</th>

            </tr>
            </thead>
            <tbody id="dataRows1">

            </tbody>
          </table>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
