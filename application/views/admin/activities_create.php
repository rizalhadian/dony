<section class="content-header">


  <h1>
    Create Activity
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">General Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formActivity" id="formAccommodation" action="<?php echo base_url(); ?>admin/activities/create_do" method="POST">
            <div class="form-group">
              <label>Partner<input type="text" name="partners_id" id="partners_id"></label>
              <input type="text" name="partner" id="partner" class="form-control">
            </div>
            <div class="form-group">
              <label>Name</label>
              <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
              <label>Youtube</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="youtube" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
            </div>
            <div class="form-group">
              <label>Meta / Tags</label>
              <input type="text" name="tags" class="form-control">
            </div>

          </div>
          <!-- /.box-body -->


      </div>
    </div>
    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Location</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

          <div class="box-body">
            <div class="form-group">
              <label>Location</label>
              <input type="hidden" name="locations_id" id="locations_id" ></input>
              <input type="text" id="locationAdd" class="form-control">
            </div>
            <div class="form-group">
              <label>Address</label>
              <input type="text" name="address" class="form-control" >
            </div>
            <div class="form-group col-md-6">
              <label>Latitude</label>
              <input type="text" name='lat' id='latitude' class="form-control"  >
            </div>
            <div class="form-group col-md-6">
              <label>Longitude</label>
              <input type="text" name='lng' id='longitude' class="form-control"  >
            </div>
            <div class="form-group col-md-12">
              <div class="panel-body" id="map-canvas" style="height:270px; background-color:gray">
                  <br><br><br><br><br><br><br><br>
    								<center><h4><span class="label label-danger">Sorry, Please Reload the Page</span></h4></center>
    					</div>
            </div>


          </div>
          <!-- /.box-body -->


      </div>
    </div>
  </div>



  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Activity Types</h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->
        <!-- /.box-body -->
        <div class="box-footer text-black">
          <div class="row">
            <?php foreach($activity_types as $ac){ ?>
            <div class="col-md-2">
              <table class="table table-bordered">
                <tr>
                  <td style="width:100px;">
                    <input name="activity_types[]" id="activity_types" value="<?php echo $ac->id; ?>" type="checkbox" class="flat-red"> <?php echo $ac->name; ?></input>
                  </td>
                </tr>
              </table>

            </div>
          <?php } ?>
          </div>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">

          <h3 class="box-title" style="color:black">Photos<!-- Button trigger modal -->

          </h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <!-- <input type="file" name="files"> -->
                <!-- <input id="file-1" type="file" name="userFiles[]" multiple class="file" data-overwrite-initial="false" data-min-file-count="2"> -->
                <!-- <input type="file" hidden name="files[]"> -->
                <!-- <input type="file" name="fileToUpload" id="fileToUpload"> -->

              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group well" id="imagewell">
                <input type="file" name="files[]" id="files" class="btn btn-block btn-primary" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
              </div>
            </div>



            <!-- /.col -->

            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
    </div>
  </div>

  <br>
  <br>
  <button type="submit" id="btnConfirmSave" class="hidden-xs hidden-sm navbar-fixed-bottom btn btn-lg btn-success" style="margin-left:250px; padding-left:500px;padding-right:500px;margin-bottom:10px;">
    Submit
  </button>

  <a id="smBtnConfirmSave"  class="hidden-lg hidden-md navbar-fixed-bottom btn btn-lg btn-success btn-block" ><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp Submit</a>
  </form>

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
