<section class="content-header">

  <h1>
    Locations
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
        <button class="btn btn-default" data-toggle="modal" data-target="#modalLocation" id="btnLocationAdd"><i class="fa fa-plus" aria-hidden="true" ></i> Add Location</button>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Name</th>
          <th>Parent</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
        <?php foreach($locations as $location){ ?>
          <tr id="row<?php echo $location->id; ?>">
            <td id="dataName<?php echo $location->id; ?>"><?php echo $location->name; ?></td>
            <td id="dataParentName<?php echo $location->id; ?>"><?php echo $location->parent_name; ?></td>

            <td width="50px">
              <button class="btn btn-default btn-xs "  id="btnLocationUpdate<?php echo $location->id; ?>" value="<?php echo $location->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
              <button class="btn btn-danger btn-xs"  id="btnLocationDelete<?php echo $location->id; ?>" value="<?php echo $location->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
            </td>
          </tr>
        <?php } ?>
        </tbody>

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->

<div class="modal fade" id="modalLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLocationLabel">Add Location</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formLocation" role="form">
      <div class="modal-body">
        <input type="hidden" name="id" id="idUpdate" class="form-control" placeholder="">
        <input type="hidden" name="parent_id" id="parent_idUpdate" class="form-control" placeholder="">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="nameUpdate" class="form-control" placeholder="">
        </div>

        <div class="form-group">
          <label>Parent</label>
          <input type="text" name="parent_name" id="parent_nameUpdate" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Type</label>
          <select class="form-control " name="type" id="typeUpdate" style="width: 100%;">
            <option selected="selected" value="country">Country (Negara)</option>
            <option value="administrative_area_level_1">Admnistrative 1 (Provinsi)</option>
            <option value="administrative_area_level_2">Admnistrative 2 (Kabupaten / Kota)</option>
            <option value="administrative_area_level_3">Admnistrative 3 (Kecamatan)</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
