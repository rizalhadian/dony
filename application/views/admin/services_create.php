<section class="content-header">
  <h1>
    Add New Service
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">General Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/create" method="POST">

            <div class="form-group">
              <label>Name</label>
              <input type="text" name="name" class="form-control" >
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
            </div>

            <div class="form-group">
              <label>Units</label>
              <select class="form-control " name="accommodation_types_id" id="typeUpdate" style="width: 100%;">
                <option value="">Per Day</option>
                <option value="">Per Hour</option>
                <option value="">Per Person</option>
                <option value="">Per Package</option>

              </select>
            </div>






          </div>
          <!-- /.box-body -->


      </div>
    </div>

  </div>



  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Covered Areas
          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalLocation"><i class="fa fa-plus"></i>
            &nbsp;Add Covered Area
          </button><!-- Button trigger modal -->

          </h3>
          <div class="pull-right box-tools">
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <div id="roomsids">
                  <input type="text" name="accommodation_rooms_ids" id="accommodation_rooms_ids" >
                </div>
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Location</th>
                  <th>Rate</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody id="dataRooms">

                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>







  <br>
  <br>
  <button type="submit" id="btnConfirmSave" class="hidden-xs hidden-sm navbar-fixed-bottom btn btn-lg btn-success" style="margin-left:250px; padding-left:500px;padding-right:500px;margin-bottom:10px;">
    Submit
  </button>

  <a id="smBtnConfirmSave"  class="hidden-lg hidden-md navbar-fixed-bottom btn btn-lg btn-success btn-block" ><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp Submit</a>
  </form>

  <!-- Modal -->
  <div class="modal fade" id="modalLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">Add Covered Area <span class="bg-warning" style="font-size:70%;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> </span></h4>
        </div>

        <div class="modal-body row">
          <form enctype="multipart/form-data" action="" method="POST" id="formRoom" role="form">

          <div>
            <div class="form-group col-md-12">
              <label>Location</label>
              <input type="text" name="name" class="form-control" >
            </div>
            <div class="form-group col-md-12">
              <label>Rates</label>
              <input type="text" name="name" class="form-control" >
            </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnAddRoom" class="btn btn-success">Add</button>
          <button type="button" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Prices -->
  <div class="modal fade" id="modalPrices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">Rates
            <button type="button" class="btn btn-primary btn-xs" id="btnAddPriceModal" data-toggle="modal" data-target="#modalPrice"><i class="fa fa-plus"></i>
              &nbsp;Add Rate
            </button>
          </h4>
        </div>

        <div class="modal-body row">
          <input type="hidden" id="accommodation_rooms_id_for_prices">
          <div class="col-md-12">
            <div class="form-group">
              <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Rate</th>
                <th>Date Start</th>
                <th>Date End</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody id="dataRates">

              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Price -->
  <div class="modal fade" id="modalPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">New Rate
          </h4>
        </div>

        <div class="modal-body row">
          <form enctype="multipart/form-data" action="" method="POST" id="formPrice" role="form">
          <div class="form-group col-md-12">
            <input type="hidden" name="accommodation_rooms_id" id="accommodation_rooms_id_for_price" ></input>
            <input type="hidden" name="accommodations_id" value="0" ></input>
            <label>Type</label>
            <select class="form-control select2" name="type" style="width: 100%;">
              <option selected="selected" value="0">Base Rate</option>
              <option value="1">Seasonal</option>
              <option value="2">Promo</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label>Name <span class="label label-warning">"Base Rate" doesn't need name</span></label>
            <input type="text" name="name" class="form-control" >
          </div>
          <!-- <div class="form-group col-md-12">
            <label>Owner Rate</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i>
              </div>
            <input type="text" name="rate_p" class="form-control">
            </div>
          </div> -->
          <div class="form-group col-md-12">
            <label>Rate</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i>
              </div>
            <input type="text" name="rate" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-12">
            <label>Rate Extra Mature</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i>
              </div>
            <input type="text" name="rate_mature_extra" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-12">
            <label>Rate Extra Children</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i>
              </div>
            <input type="text" name="rate_children_extra" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-12">
            <label>Rate Extra Infant</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i>
              </div>
            <input type="text" name="rate_infant_extra" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-12">
            <label>Date Valid Start</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date_start" class="form-control pull-right" id="datepicker1">
              </div>
          </div>
          <div class="form-group col-md-12">
            <label>Date Valid End</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date_end" class="form-control pull-right" id="datepicker2">
              </div>
          </div>
          <div class="form-group col-md-12">
            <label>Description</label>
            <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnAddPrice" class="btn btn-success btn-block">Add</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Delete -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" class="form-control" name="idDelete" id="idDelete">
          </div>
          <div class="row">
            <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
            <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
            <div class="col-md-6"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
