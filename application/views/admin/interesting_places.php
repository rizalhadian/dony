<section class="content-header">

  <h1>
    Interesting Places
    <button class="pull-right btn" ><i class="fa fa-cog" aria-hidden="true"></i> Setting</button>
  </h1>

  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
        <a href="<?php echo base_url(); ?>admin/interesting-places/add-new" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true" ></i> Add Interesting Place</a>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Type</th>
          <th>Active</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
          <?php foreach($interesting_places as $interesting_place){ ?>
          <tr id="row<?php echo $interesting_place->id; ?>" class="<?php if($interesting_place->active == 0){echo "danger";} ?>">
            <td id="dataName<?php echo $interesting_place->id; ?>"><?php echo $interesting_place->name; ?></td>
            <td id="dataDescription<?php echo $interesting_place->id; ?>"><?php echo $interesting_place->description; ?></td>
            <td id="dataType<?php echo $interesting_place->id; ?>"><?php echo $interesting_place->interesting_place_types_id; ?></td>
            <td id="dataActive<?php echo $interesting_place->id; ?>"><?php if($interesting_place->active == 1){echo "Active";}elseif($interesting_place->active == 0){echo "Not Active";}?></td>
            <td width="50px">
              <button class="btn btn-default btn-xs btn-block"  id="btnExperienceUpdate<?php echo $interesting_place->id; ?>" value="<?php echo $interesting_place->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
              <?php if($interesting_place->active == 0){ ?>
              <button class="btn btn-success btn-xs btn-block"  id="btnExperienceActivate<?php echo $interesting_place->id; ?>" value="<?php echo $interesting_place->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Activate</button>
              <?php } ?>
              <?php if($interesting_place->active == 1){ ?>              
              <button class="btn btn-danger btn-xs btn-block"  id="btnExperienceDeactivate<?php echo $interesting_place->id; ?>" value="<?php echo $interesting_place->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Deactivate</button>
              <?php } ?>
              
              <button class="btn btn-danger btn-xs btn-block"  id="btnExperienceDelete<?php echo $interesting_place->id; ?>" value="<?php echo $interesting_place->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
            </td>
          </tr>
        <?php } ?>
        </tbody>

        <!-- <tfoot>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
        </tfoot> -->

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->

<div class="modal fade" id="modalExperience" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalExperienceLabel">Add Interesting Place</h4>
      </div>

      <form enctype="multipart/form-data" action="" method="POST" id="formExperience" role="form">
      <div class="modal-body">
        <div class="col-md-12">

            <div class="form-group">
              <label>Name</label>
              <input type="hidden" name="id" id="idUpdate" class="form-control" placeholder="">
              <input type="text" name="name" id="nameUpdate" class="form-control" placeholder="">
            </div>
            <div class="form-group">
              <label>Type</label>
              <select class="form-control " name="type" id="type" style="width: 100%;">
                <option selected="selected" value="Restaurant">Restaurant</option>
                <option  value="Cafe">Cafe</option>
                <option  value="Club">Club</option>
                <option  value="Surf Point">Surf Point</option>
                <option  value="Event">Event</option>
              </select>
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="3" placeholder=""></textarea>
            </div>

            <div id="restaurantForm" class="row">
              <div class="modal-body">
                <div class="form-group">
                  <label>Zomato URL</label>
                  <input type="text" name="zomato_url" class="form-control" placeholder="">
                </div>
              </div>
            </div>

            <div id="surfForm" class="row">
              <div class="modal-body">
                <div class="form-group">
                  <label>Difficult</label>
                  <select class="form-control " name="surf_difficult" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Beginner">Beginner</option>
                    <option  value="Intermediate">Intermediate</option>
                    <option  value="Advance">Advance</option>
                    <option  value="All">All</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wave Direction</label>
                  <select class="form-control " name="surf_wave_direction" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Left">Left</option>
                    <option  value="Right">Right</option>
                    <option  value="All">All</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wave Type</label>
                  <select class="form-control " name="surf_wave_type" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Reef Break">Reef Break</option>
                    <option  value="Point Break">Point Break</option>
                    <option  value="Beach Break">Beach Break</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Wind</label>
                  <select class="form-control " name="surf_wind" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="South East">South East</option>
                    <option  value="South West">South West</option>
                    <option  value="North East">North East</option>
                    <option  value="North West">North West</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tide</label>
                  <select class="form-control " name="surf_tide" id="typeUpdate" style="width: 100%;">
                    <option selected="selected" value="Low">Low</option>
                    <option  value="Mid">Mid</option>
                    <option  value="High">High</option>
                    <option  value="All">All</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="panel-body" id="map-canvas" style="height:610px; background-color:gray">
                <br><br><br><br><br><br><br><br>
                  <center><h4><span class="label label-danger">Sorry, Please Reload the Page</span></h4></center>
            </div>
          </div>



        </div>





        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnConfirmSave" class="btn btn-success">Save</button>
          <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
        </div>



      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
