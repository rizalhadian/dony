<section class="content-header">
  <h1>
    Add New Accomodation
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">General Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/create" method="POST">

            <div class="form-group col-lg-12 col-md-12">
              <label>Name</label>
              <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Accommodations Code</label>
              <input type="text" name="" class="form-control" disabled placeholder="Code Generated After Accommodation Added">
            </div>
            <div class="form-group col-lg-6">
              <label>Accommodations Type</label>
              <select class="form-control " name="accommodation_types_id" id="typeUpdate" style="width: 100%;">
                <!-- <option selected="selected" value="0">Choose Accommodation Type</option> -->
                <?php foreach($accommodation_types as $accommodation_type){?>
                  <option value="<?php echo $accommodation_type->id; ?>"><?php echo $accommodation_type->name; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-lg-6">
              <label>Accommodations Class</label>
              <select class="form-control " name="class" id="typeUpdate" style="width: 100%;">
                <!-- <option selected="selected" value="0">Choose Accommodation Class</option> -->
                <option value="0">Affordable</option>
                <option value="1">Luxury</option>
                <option value="2">Elite</option>
              </select>
            </div>

            <div class="form-group col-lg-12 col-md-12">
              <label>Renting Type</label>
              <select class="form-control " name="renting_type" id="renting_types_id" style="width: 100%;">
                <!-- <option selected="selected" value="0">Choose Accommodation Class</option> -->
                <option value="0">Exclusive Accommodation / All Rooms</option>
                <!-- <option value="1" disabled>Private Room</option> -->
                <option value="2">Multiple Set Rooms / Below Rooms</option>
              </select>
            </div>

            <div class="form-group col-lg-12 col-md-12">
              <table class="table" style="background:#D2D6DE;">
                <tr>
                  <td class="">
                    <b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;Renting Type&nbsp;|&nbsp;</b>
                    You can lease your accommodation with below rooms usage setting or more complex setting by choosing <b>'Flexible'</b>
                  </td>
                </tr>
              </table>
            </div>

            <div class="form-group col-lg-12 col-md-12">
              <label>Management</label>
              <table class="table table-bordered">
                <tr>
                <style type="text/css">
                  td label {
                     display: block;
                     text-align: left;
                  }
                </style>
                  <td width="50%">
                    <label>
                      <input type="radio" name="management" value="1" class="minimal" checked>&nbsp;&nbsp;&nbsp; Total Bali</input>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input type="radio" name="management" value="0" id="managementother" class="minimal">&nbsp;&nbsp;&nbsp; Other Management</input>
                    </label>
                  </td>
                </tr>
              </table>
            </div>

            <div class="form-group col-lg-12 col-md-12" >
              <label>Management / Partner Name</label>
              <input type="hidden" name="partners_id" id="partners_id">
              <input type="text" id="partnerAdd" class="form-control">
            </div>

            <div class="form-group col-lg-12 col-md-12">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
            </div>

            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Experiences</label>

              <!-- <input type="text" name="experiences" class="form-control"> -->
              <select class="form-control select2" name="experiences[]"  multiple="multiple" data-placeholder="" style="width: 100%;">
                <?php foreach($experiences as $experience){ ?>
                  <option value="<?php echo $experience->id; ?>"><?php echo $experience->name; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group col-lg-12 col-md-12">
              <label>Meta / Tags</label>
              <input type="text" name="tags" class="form-control">
            </div>


          </div>
          <!-- /.box-body -->
      </div>
    </div>
    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Locations Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

          <div class="box-body">
            <div class="form-group col-lg-12 col-md-12">
              <label>Location</label>
              <input type="hidden" name="locations_id" id="locations_id" ></input>
              <input type="text" id="locationAdd" data-placeholder="Select Location" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Address</label>
              <input type="text" name="address" class="form-control" >
            </div>
            <div class="form-group col-md-6">
              <label>Latitude</label>
              <input type="text" name='lat' id='latitude' class="form-control"  >
            </div>
            <div class="form-group col-md-6">
              <label>Longitude</label>
              <input type="text" name='lng' id='longitude' class="form-control"  >
            </div>
            <div class="form-group col-md-12">
              <div class="panel-body" id="map-canvas" style="height:610px; background-color:gray">
                  <br><br><br><br><br><br><br><br>
    								<center><h4><span class="label label-danger">Sorry, Please Reload the Page</span></h4></center>
    					</div>
            </div>


          </div>
          <!-- /.box-body -->


      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="privateRoomForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Room Informations

          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Bedrooms</label>
                    <input type="text" name="total_bedrooms" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Bathrooms</label>
                    <input type="text" name="total_bathrooms" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Beds</label>
                    <input type="text" name="total_beds" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Guest </label>
                    <input type="text" name="max_guest_normal" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Extra Guest </label>
                    <input type="text" name="max_guest_extra" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Minimum Stay</label>
                    <input type="text" name="min_stay" class="form-control">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Land Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="land_size" class="form-control" >
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Building Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="building_size" class="form-control" >
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="privateRateForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Rate Informations

          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div>
                  <input type="text" name="rates_ids" id="rates_ids" >
                </div>
                <button type="button" id="btnAddRate" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modalPrice"><i class="fa fa-plus"></i>
                  &nbsp;Add Rate
                </button>
                <br>
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Rate</th>
                  <th>Min Stay</th>
                  <th>Valid Date</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody id="dataRates">

                </tbody>
              </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="flexibleRoomForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Room Informations

          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Rooms</label>
                    <input type="text" name="flexible_total_rooms" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Bathrooms</label>
                    <input type="text" name="flexible_total_bathrooms" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Beds</label>
                    <input type="text" name="flexible_total_beds" class="form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Land Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="flexible_land_size" class="form-control" >
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Building Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="flexible_building_size" class="form-control" >
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="flexibleRoomRateForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Room Settings and Rate Informations&nbsp;&nbsp;
            <span style="font-size:80%; color:red;">You can lease your accommodation with different amount of bedrooms and diiferent rates by add Room Setting multiple times</span>
          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div id="roomsids">
                  <input type="text" name="accommodation_rooms_ids" id="accommodation_rooms_ids" >
                </div>
                <button type="button" id="btnAddRoomInfo" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalRoom"><i class="fa fa-plus"></i>
                  &nbsp;Add Room Setting
                </button>
                <br>
                <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Bedrooms</th>
                  <th>Beds</th>
                  <th>Bathooms</th>
                  <th>Min Stay</th>
                  <th>Guest</th>
                  <th>Extra Guest</th>
                  <th>Renting Type</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody id="dataRooms">

                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Extras</h3>
        </div>
          <div class="box-body">
            <?php foreach($additional_rates as $facility){ ?>
              <div class="col-md-12">
                <table class="table table-bordered">
                  <tr>
                  <style type="text/css">
                    td label {
                       display: block;
                       text-align: left;
                    }
                  </style>
                    <td style="">
                      <div class="form-group col-lg-3" >
                        <label style="margin-top:5px;">
                          <input name="additional_rates[]" value="<?php echo $facility->id; ?>" type="checkbox"></input>&nbsp;&nbsp;&nbsp; <?php echo $facility->name; ?>
                        </label>
                      </div>
                      <div class="form-group col-lg-2" >
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-usd"></i> USD
                          </div>
                        <input type="text" name="additional_rates_rate_<?php echo $facility->id; ?>" id="additional_rates_rate_<?php echo $facility->id; ?>" class="form-control" >
                        </div>
                      </div>

                      <div class="form-group col-lg-3">

                        <select class="form-control " name="metric_unit_<?php echo $facility->id; ?>" id="metric_unit_<?php echo $facility->id; ?>" style="width: 100%;">
                          <!-- <option selected="selected" value="0">Choose Accommodation Class</option> -->
                          <option value="person">Per Person</option>
                          <option value="day">Per Day</option>
                          <option value="hour">Per Hour</option>
                          <option value="person day">Per Person Per Day</option>
                          <option value="group">Per Group</option>
                          <option value="week">Per Week</option>
                          <option value="month">Per Month</option>

                        </select>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Facilities</h3>
        </div>
          <div class="box-body">
            <?php foreach($facilities as $facility){ ?>
              <div class="col-md-3">
                <table class="table table-bordered">
                  <tr>
                  <style type="text/css">
                    td label {
                       display: block;
                       text-align: left;
                    }
                  </style>
                    <td style="">
                      <label>
                        <input name="facilities[]" value="<?php echo $facility->id; ?>" type="checkbox">   &nbsp;&nbsp;&nbsp; <?php echo $facility->name; ?></input>
                      </label>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Amenities</h3>
        </div>
          <div class="box-body">
            <?php foreach($amenities as $facility){ ?>
              <div class="col-md-3">
                <table class="table table-bordered">
                  <tr>
                  <style type="text/css">
                    td label {
                       display: block;
                       text-align: left;
                    }
                  </style>
                    <td style="">
                      <label>
                        <input name="facilities[]" value="<?php echo $facility->id; ?>" type="checkbox">   &nbsp;&nbsp;&nbsp; <?php echo $facility->name; ?></input>
                      </label>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Channel Information</h3>
        </div>
          <div class="box-body">
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>ICS or ICal Link (Live Availability Links) <span style="font-size:80%; color:red;">For Private Accommodation</span></label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="ics_url" class="form-control" value="">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Accommodation Site</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="website" class="form-control">
              </div>
            </div>

            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Booking.com</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_booking_com" class="form-control" value="">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Airbnb.com</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_airbnb_com" class="form-control" value="">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Flipkey.com</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_flipkey_com" class="form-control" value="">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Other Chanel</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_other" class="form-control" value="">
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Social Media Information</h3>
        </div>
          <div class="box-body">
            <div class="form-group col-lg-12 col-md-12">
              <label>Youtube</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="youtube" class="form-control">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Facebook</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="facebook" class="form-control" value="">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Instagram</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="instagram" class="form-control" value="">
              </div>
            </div>

          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Taxes Information</h3>
        </div>
          <div class="box-body">
            <div class="form-group col-lg-6 col-md-6">
              <label>Tax</label>

              <table class="table table-bordered">
                <tr>
                <style type="text/css">
                  td label {
                     display: block;
                     text-align: left;
                  }
                </style>
                  <td width="50%">
                    <label>
                      <input type="radio" name="include_tax" value="1" class="" checked>&nbsp;&nbsp;&nbsp; Included Tax</input>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input type="radio" name="include_tax" value="0" class="">&nbsp;&nbsp;&nbsp; Not Included</input>
                    </label>
                  </td>
                </tr>
              </table>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Tax Percentage</label>
              <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" name="tax_percentage" class="form-control" value="">
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Accommodation Photos<!-- Button trigger modal -->
          </h3>
          <div class="pull-right box-tools">

            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              </div>
            </div>
            <div class="col-md-12">
              <div class="col-md-12 form-group well">
                <div class="col-md-12">
                  <div class="form-group">
                      <div class="file-loading">
                          <input id="file-1" name="userfile[]" type="file" multiple class="file" data-overwrite-initial="false">
                      </div>
                  </div>
                </div>
                <br>
              </div>
            </div>
            <input type="text" id="photoIds" name="photoIds">

            <div class="col-md-12" id="photosForm">
              <div class="col-md-12 form-group well">
                <div class="col-md-12">
                  <div class="form-group" id="uploadedPhotos">

                  </div>
                </div>
                <br>
              </div>
            </div>


          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12">
      <button type="submit" id="btnConfirmSave" class=" btn btn-lg btn-success btn-block" style="">
        Submit
      </button>
    </div>
    </form>






  <!-- <br>
  <br>
  <br>
  <button type="submit" id="btnConfirmSave" class="hidden-xs hidden-sm navbar-fixed-bottom btn btn-lg btn-success" style="margin-left:250px; padding-left:500px;padding-right:500px;margin-bottom:10px;">
    Submit
  </button> -->



  <!-- Modal Prices -->
  <div class="modal fade" id="modalPrices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">Rates
            <button type="button" class="btn btn-primary btn-xs" id="btnAddPriceModal" data-toggle="modal" data-target="#modalPrice"><i class="fa fa-plus"></i>
              &nbsp;Add Rate
            </button>
          </h4>
        </div>

        <div class="modal-body row">
          <input type="text" id="accommodation_rooms_id_for_prices">
          <div class="col-md-12">
            <div class="form-group">
              <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Name</th>
                <th>Rate</th>
                <th>Rate Ex. Adult</th>
                <th>Rate Ex. Children</th>
                <th>Rate Ex. Infant</th>
                <th>Valid Date</th>
                <th>Type</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody id="flexibleDataRates">

              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modalRoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel"><span class="bg-warning" style="font-size:100%;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> You can add the prices $ after this datas added</span></h4>
        </div>

        <div class="modal-body row">
          <form enctype="multipart/form-data" action="" method="POST" id="formRoom" role="form">

          <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#general_informations" id="tabGeneralInfo" aria-controls="general_informations" role="tab" data-toggle="tab"><u>General Informations</u></a></li>
              <li role="presentation"><a href="#facilities" id="tabFacilities" aria-controls="facilities" role="tab" data-toggle="tab"><u>Facilities and Amenities</u></a></li>

            </ul>

            <!-- Tab panes -->

            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="general_informations">
                <br>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Name&nbsp;&nbsp;<span class="label" style="font-size:80%; color:grey;">Explain your rooms like <u>4 Bedrooms Usage</u> or <u>3 Deluxe Bedrooms and 1 Normal Bedroom</u></span></label>
                    <input type="text" class="form-control" name="name">
                    <input type="hidden" class="form-control" name="id">
                    <input type="hidden" class="form-control" name="old_code">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Rooms Usage</label>
                    <input type="text" name="total_rooms" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Bathrooms Usage</label>
                    <input type="text" name="total_bathrooms" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Beds Usage</label>
                    <input type="text" name="total_beds" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Guest </label>
                    <input type="text" name="max_guest_normal" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Extra Guest </label>
                    <input type="text" name="max_guest_extra" class="form-control">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Minimum Stay</label>
                    <input type="text" name="min_stay" class="form-control">
                  </div>
                </div>
                <div class="form-group col-lg-12 col-md-12">
                  <label>Renting Type</label>
                  <table class="table table-bordered">
                    <tr>
                    <style type="text/css">
                      td label {
                         display: block;
                         text-align: left;
                      }
                    </style>
                      <td width="50%">
                        <label>
                          <input type="radio" value="0" name="renting_type" class="" checked>&nbsp;&nbsp;&nbsp; Private Accommodation</input>
                        </label>
                      </td>
                      <td>
                        <label>
                          <input type="radio" value="1" name="renting_type" class="">&nbsp;&nbsp;&nbsp; Private Room</input>
                        </label>
                      </td>
                    </tr>
                  </table>
                  <table class="table" style="background:#D2D6DE;">
                    <tr>
                      <td class="">
                        <b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;Renting Type&nbsp;|&nbsp;</b>
                        When <b>Private Accommodation</b> booked, all of the <b>Private Rooms</b> will be Unavailable. When one of <b>Private Rooms</b> booked, the others <b>Private Rooms</b> will be Available and all of the <b>Private Accommodations</b> will be Unavailable.
                      </td>
                    </tr>
                  </table>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>ICS or ICal Link (Live Availability Links) <span style="font-size:80%; color:red;">For Private Room</span></label>
                    <input type="text" name="ics_url" id="ics_url" class="form-control">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description" rows="4" placeholder=""></textarea>
                  </div>
                </div>

              </div>
              <div role="tabpanel" class="tab-pane" id="facilities">
                <br>
                <?php foreach($facilities as $facility){ ?>
                  <div class="col-md-3">
                    <table class="table table-bordered">
                      <tr>
                      <style type="text/css">
                        td label {
                           display: block;
                           text-align: left;
                        }
                      </style>
                        <td style="">
                          <label>
                            <input name="facilities[]" value="<?php echo $facility->id; ?>" type="checkbox">   &nbsp;&nbsp;&nbsp; <?php echo $facility->name; ?></input>
                          </label>
                        </td>
                      </tr>
                    </table>
                  </div>
                <?php } ?>
              </div>


            </div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnNextFacilities" class="btn btn-primary">Next</button>
          <button type="button" id="btnAddRoom" class="btn btn-success">Add</button>
          <button type="button" id="btnUpdateRoom" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Rate -->
  <div class="modal fade" id="modalPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">Add Rate
          </h4>
        </div>

        <div class="modal-body row">
          <form enctype="multipart/form-data" action="" method="POST" id="formRate" role="form">
          <div class="form-group col-md-12">

            <label>Type</label>
            <select class="form-control" name="type" id="rate_update_type" style="width: 100%;">
              <option selected="selected" value="0">Base Rate</option>
              <option value="1">Seasonal</option>
              <option value="2">Promo</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label>Name <span class="label label-warning">"Base Rate" doesn't need name</span></label>
            <input type="hidden" name="id" id="rate_update_id" ></input>
            <input type="text" name="accommodation_rooms_id" id="rate_accommodations_id" ></input>
            <input type="hidden" name="created_on" id="rate_update_created_on" ></input>
            <input type="text" name="name" id="rate_update_name" class="form-control" >
          </div>

          <div class="form-group col-md-8">
            <label>Rate</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i> USD
              </div>
            <input type="text" name="rate" id="rate_update_rate" class="form-control" >
            </div>
          </div>

          <div class="form-group col-md-4">
            <label>Minimum Stay</label>
            <div class="input-group">
              <div class="input-group-addon">
                Night
              </div>
            <input type="text" name="min_stay" id="rate_update_min_stay" class="form-control" >
            </div>
          </div>
          <div class="form-group col-md-6">
            <label>Date Valid Start</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date_start" class="form-control pull-right" id="datepicker1">
              </div>
          </div>
          <div class="form-group col-md-6">
            <label>Date Valid End</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date_end" class="form-control pull-right" id="datepicker2">
              </div>
          </div>
          <div class="form-group col-md-12">
            <label>Description</label>
            <textarea name="description" class="form-control" rows="4" placeholder="" id="rate_update_description"></textarea>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnAddPrice" class="btn btn-success btn-block">Add</button>
          <button type="button" id="btnUpdatePrice" class="btn btn-primary btn-block">Update</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Delete -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" class="form-control" name="idDelete" id="idDelete">
          </div>
          <div class="row">
            <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
            <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
            <div class="col-md-6"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
