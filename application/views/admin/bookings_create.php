<section class="content-header">

  <h1>
    Create Enquiry
  </h1>
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/bookings/create" method="POST">
  <div class="row">
    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Customer Information</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

          <div class="box-body">
            <div class="form-group col-lg-12 col-md-12">
              <label>Booking Code</label>
              <input type="text" readonly="readonly" name="booking_code" class="form-control" placeholder="Code Generated After Enquiry Created">
            </div>

            <div class="form-group col-lg-6 col-md-6">
              <label>First Name</label>
              <input type="text" name="first_name" class="form-control">
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Last Name</label>
              <input type="text" name="last_name" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Email</label>
              <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group col-lg-3 col-md-3">
              <label>Phone</label>
              <input type="text" name="phone[]" class="form-control">
            </div>
            <div class="form-group col-lg-9 col-md-9">
              <label>&nbsp;</label>
              <input type="text" name="phone[]" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Payment Method</label>
              <select class="form-control " name="payment_method" style="width: 100%;">
                <option selected="selected" value="0">Credit Card</option>
                <option value="1">Paypal</option>
                <option value="2">Bank Transfer</option>
              </select>
            </div>
          </div>
          <!-- /.box-body -->
      </div>
    </div>

    <div class="col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            Agent Information
            <button type="button" class="btn btn-xs btn-primary" id="btnNewAgent">New Agent</button>
            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modalSearchAgents">Old Agent</button>
          </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/create" method="POST">
              <div class="form-group col-lg-12 col-md-12">
                <label>Agent Name</label>
                <input type="text" readonly="readonly" id="agent_name" name="agent_name" class="form-control">
                <input type="hidden" name="agent_id" id="agent_id" class="form-control">
                <input type="hidden" name="is_new_agent" id="is_new_agent" class="form-control">
              </div>

              <div class="form-group col-lg-12 col-md-12">
                <label>Email</label>
                <input type="text" readonly="readonly" id="agent_email"  name="agent_email" class="form-control">
              </div>
              <div class="form-group col-lg-3 col-md-3">
                <label>Phone</label>
                <input type="text" readonly="readonly" id="agent_phone_0" name="agent_phone[]" class="form-control">
              </div>
              <div class="form-group col-lg-9 col-md-9">
                <label>&nbsp;</label>
                <input type="text" readonly="readonly" id="agent_phone_1" name="agent_phone[]" class="form-control">
              </div>
              <div class="form-group col-lg-12 col-md-12">
                <label>Agent Code</label>
                <input type="text" readonly="readonly" name="agent_code" id="agent_code" class="form-control">
              </div>

          </div>
          <!-- /.box-body -->
      </div>
    </div>
    <div class="col-lg-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Accommodations Booked
            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#modalSearchAccommodations"><i class="fa fa-plus" aria-hidden="true"></i> Add Accommodation</button>
          </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
          <table id="example2" class="table table-bordered table-striped" style="font-size: 90%;">
            <thead>
            <tr>
              <th>Code</th>
              <th>Name</th>
              <th>Location</th>
              <th>Bedrooms</th>
              <th>Checkin</th>
              <th>Checkout</th>
              <th>Guests</th>
              <th>Action</th>
            </tr>
            </thead>

            <tbody id="dataAccommodationsBooked">
            <!-- Tes for lots datas -->

            </tbody>
          </table>
          </div>
          <!-- /.box-body -->
      </div>
      <!-- <button type="submit" class="btn btn-block btn-success btn-lg">Submit</button> -->

    </div>

    <div class="col-lg-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Notes
          </h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            <textarea name="notes" id="" class="form-control" rows="7" placeholder=""></textarea>
          </div>
          <!-- /.box-body -->
      </div>
      <button type="submit" class="btn btn-block btn-success btn-lg">Submit</button>

    </div>




  </div>
</form>

  <!-- /.row -->








      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>





<!-- Modal Seacrh Accommodations  -->
<div class="modal fade" id="modalSearchAccommodations" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel"><i class="fa fa-search" aria-hidden="true"></i> Search Accommodations</h4>
      </div>

      <div class="modal-body row">

          <div class="col-md-12">
            <table id="example1" class="table table-bordered table-striped" style="font-size: 90%;">
              <thead>
              <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Location</th>
                <th>Bedrooms</th>

                <th>Action</th>
              </tr>
              </thead>

              <tbody id="dataRows">
              <!-- Tes for lots datas -->
              <?php for($i = 0; $i<1; $i++){ ?>
                <?php foreach($accommodations as $accommodation){ ?>
                  <tr id="rowAccommodation<?php echo $accommodation->id; ?>" <?php if($accommodation->active == 0){echo 'class="danger"';} ?>>
                    <td id="dataNameAccommodation<?php echo $accommodation->id; ?>" style="width:200px;"><?php echo $accommodation->code; ?></td>
                    <td id="dataNameAccommodation<?php echo $accommodation->id; ?>"><?php echo $accommodation->name; ?></td>
                    <td id="dataLocationAccommodation<?php echo $accommodation->id; ?>"><?php foreach($accommodation->locations as $location){ echo $location.', '; } ?></td>
                    <td><?php foreach($accommodation->rooms as $room){echo $room." rooms, ";} ?></td>
                    <td style="width:100px;">
                      <center>
                        <button class="btn btn-block btn-success btn-xs"  style="margin-bottom:2px"  id="btnAddAccommodations" value="<?php echo $accommodation->id; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Add to booking</button>
                      </center>
                    </td>
                  </tr>
                <?php } ?>
              <?php } ?>
              </tbody>
            </table>
          </div>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Modal Search Agents -->
<div class="modal fade" id="modalSearchAgents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel"><i class="fa fa-search" aria-hidden="true"></i> Search Agents</h4>
      </div>

      <div class="modal-body row">

          <div class="col-md-12">
            <table id="example3" class="table table-bordered table-striped" style="font-size: 90%;">
              <thead>
              <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody id="dataRows">
              <!-- Tes for lots datas -->
              <?php for($i = 0; $i<1; $i++){ ?>
                <?php foreach($agents as $accommodation){ ?>
                  <tr id="rowAgent<?php echo $accommodation->id; ?>">
                    <td id="dataCodeAgent<?php echo $accommodation->id; ?>" style="width:200px;"><?php echo $accommodation->code; ?></td>
                    <td id="dataNameAgent<?php echo $accommodation->id; ?>"><?php echo $accommodation->name; ?></td>
                    <td id="dataEmailAgent<?php echo $accommodation->id; ?>"><?php echo $accommodation->email; ?></td>
                    <td id="dataPhoneAgent<?php echo $accommodation->id; ?>"><?php echo $accommodation->phone; ?></td>
                    <td style="width:100px;">
                      <center>
                        <button class="btn btn-block btn-success btn-xs"  style="margin-bottom:2px"  id="btnAddAgent" value="<?php echo $accommodation->id; ?>">Submit</button>
                      </center>
                    </td>
                  </tr>
                <?php } ?>
              <?php } ?>
              </tbody>
            </table>
          </div>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAddAccommodation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel">Accommodation</h4>
      </div>

      <div class="modal-body row">
        <div class="form-group col-lg-6 col-md-6">
          <label>Code</label>
          <input type="text" readonly="readonly" name="" id="modalCode" class="form-control">
          <input type="hidden" readonly="readonly" name="" id="modalId" class="form-control">
        </div>
        <div class="form-group col-lg-6 col-md-6">
          <label>Name</label>
          <input type="text" readonly="readonly" name="" id="modalName" class="form-control">
        </div>
        <div class="form-group col-lg-12 col-md-12">
          <label>Location</label>
          <input type="text" readonly="readonly" name="" id="modalLocation" class="form-control">
        </div>
        <div class="form-group col-lg-12 col-md-12">
          <label>Bedroom Set</label>
          <select class="form-control " name="" id="modalBedroomSet" style="width: 100%;">

          </select>
        </div>

        <div class="form-group col-lg-4 col-md-4">
          <label>Adult Guest</label>
          <input type="text" id="modalAdultGuest" class="form-control pull-right">
        </div>
        <div class="form-group col-lg-4 col-md-4">
          <label>Children Guest</label>
          <input type="text" id="modalChildrenGuest" class="form-control pull-right">
        </div>
        <div class="form-group col-lg-4 col-md-4">
          <label>Infant Guest</label>
          <input type="text" id="modalInfantGuest" class="form-control pull-right">
        </div>

        <div class="form-group col-lg-6 col-md-6">
          <label>Checkin</label>
          <input type="text" class="form-control pull-right" id="datepicker-checkin">
        </div>
        <div class="form-group col-lg-6 col-md-6">
          <label>Checkout</label>
          <input type="text" class="form-control pull-right" id="datepicker-checkout">
        </div>
        <div class="form-group col-lg-12 col-md-12">
          <label>Total Rates</label>
          <div class="input-group">
            <span class="input-group-addon" style="border-right:none; text-align: right;"><b>$ USD</b></span>
            <input type="text" class="form-control pull-right" id="modalTotalRates" style="border-left:none;">
          </div>
        </div>
        <div class="form-group col-lg-12 col-md-12">
          <label>Tax</label>
          <div class="input-group">
            <input type="text" class="form-control pull-right" id="modalTax" style="width:100%; border-right:none;">
            <span class="input-group-addon" style="width:90%; text-align: left; border-left:none;"><b>%</b></span>
          </div>
        </div>
        <div class="form-group col-lg-12 col-md-12">
          <label>Agent Fee</label>
          <div class="input-group">
            <input type="text" class="form-control pull-right" id="modalAgentFee" style="width:100%; border-right:none;">
            <span class="input-group-addon" style="width:90%; text-align: left; border-left:none;"><b>%</b></span>
          </div>
        </div>
        <div class="form-group col-lg-12 col-md-12">
          <label>Notes</label>
          <textarea name="notes" id="modalNotes" class="form-control" rows="7" placeholder=""></textarea>
        </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-lg- btn-block" id="btnAddAccommodationToBookedLists">Add</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="whichDelete" id="whichDelete">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Sync -->
<div class="modal fade" id="modalSync" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalAccommodationViewLabel">Sync</h4>
      </div>

      <div class="modal-body row">
        <div class="col-md-12">
          <center><button class="btn btn-block btn-primary" style="margin-bottom:2px"  id="btn_sync_now" value=""><span id="loading_icon"><i class="fa fa-refresh" aria-hidden="true"></i></span> Sync Now</button></center>
          <input type="hidden" id="id_to_sync">
          <br>
          <table class="table table-bordered" id="dataSync" style="font-size: 80%;">
            <tr>
              <th>OTA</th>
              <th>OTA Id</th>
              <th>Title</th>
              <th>Link</th>
              <th>Status</th>
              <th>Status</th>

            </tr>
            <!-- Airbnb -->
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>

            </tr>
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
            </tr>
            <!-- End Airbnb -->

            <!-- FLipkey -->
            <tr>
              <td><img src="<?php echo base_url() ?>/assets/images/flipkey.png" width="120px" class="img-fluid" alt="Responsive image"></td>
              <td>...</td>
              <td>...</td>
              <td>...</td>
              <td>...</td>

            </tr>
            <!-- End Flipkey -->
          </table>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div> -->
    </div>
  </div>
</div>
<!-- End Modal Sync -->
