<section class="content-header">
  <h1>
    Bookings
  </h1>
  <div id="alertinformations">
  </div>

  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Bookings</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
        <button class="btn btn-default" data-toggle="modal" data-target="#modalVilla" id="btnaddvilla"><i class="fa fa-plus" aria-hidden="true"></i> Add Booking</button>

    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Booking ID</th>
          <th>Villa</th>
          <th>Guest Name</th>
          <th>Checkin</th>
          <th>Checkout</th>
          <th>Total</th>
          <th>Special Request</th>
          <th>Email</th>
          <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($books as $book){ ?>

          <tr id="rowvilla<?php echo $book->bookid; ?>" style="font-size: 90%;">
            <td><?php echo $book->bookid; ?></td>
            <td><?php echo $book->villa; ?></td>
            <td><?php echo $book->guestname; ?></td>
            <?php
              $checkin =  date('Y-M-d', strtotime($book->checkin));
              $checkout = date('Y-M-d', strtotime($book->checkout));
            ?>
            <td><?php echo $checkin; ?></td>
            <td><?php echo $checkout; ?></td>
            <td><?php echo $book->totalbookingvalue; ?></td>
            <td><?php echo $book->specialrequest; ?></td>
            <td><?php echo $book->email; ?></td>
            <td><?php echo $book->status; ?></td>
          </tr>
        <?php } ?>

        </tbody>
        <tfoot>
        <tr>
          <th>Booking ID</th>
          <th>Villa</th>
          <th>Guest Name</th>
          <th>Checkin</th>
          <th>Checkout</th>
          <th>Total</th>
          <th>Special Request</th>
          <th>Email</th>
          <th>Status</th>
        </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
    <!-- Button trigger modal -->
    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->
    </div>

  <!-- /.row -->
      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->



</section>

<!-- Modal -->
<div class="modal fade" id="modalVilla" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalBookingLabel">Add Booking</h4>
      </div>
      <form enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/book/add" method="POST" id="formbooking">
      <div class="modal-body">

        <div>

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#villa" aria-controls="villa" role="tab" data-toggle="tab">Booking Data</a></li>
            <li role="presentation"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Booking Detail</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <!-- Tab Villa -->
            <div role="tabpanel" class="tab-pane active" id="villa">
              <br>
              <div class="form-group">
                <label for="exampleInputEmail1">Booking Id</label>
                <br><div id="bookid">##########</div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Villa</label>
                <select class="form-control " name="sublocation" id="sublocation" style="width: 100%;" >
                  <option selected="selected" value="0">Choose Villa</option>
                  <?php foreach($villas as $villa){ ?>
                    <option value="<?php echo $villa->idkamar; ?>"><?php echo $villa->name; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Checkin</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker0">
                </div>
              </div><div class="form-group col-md-6">
                <label for="exampleInputEmail1">Checkout</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker1">
                </div>
              </div>
              <div class="form-group ">
                <label for="exampleInputEmail1">Guest Name</label>
                <input type="input" class="form-control" name="rooms" placeholder="Guest Name" id="rooms">
              </div>
              <div class="form-group ">
                <label for="exampleInputEmail1">Email</label>
                <input type="input" class="form-control" name="baserates" id="baserates" placeholder="Guest Email">
              </div>
              <div class="form-group ">
                <label>Number of Guests</label>
                <input type="input" class="form-control" name="baserates" id="baserates" placeholder="Number of Guests">
              </div>
              <div class="form-group ">
                <label>Status</label>
                <select class="form-control " name="sublocation" id="sublocation" style="width: 100%;" >
                  <option selected="selected" value="1">Enquiry</option>
                  <option value="2">Onhold</option>
                  <option value="3">Confirmed</option>
                  <option value="5">Dead Client</option>
                </select>
              </div>

            </div>
            <!-- End of Tab Villa -->
            <!-- Tab Detail -->
            <div role="tabpanel" class="tab-pane" id="detail">
              <br>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Nightly Rate</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                  </div>
                  <input type="input" class="form-control">
                </div>

              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Extracost</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                  </div>
                  <input type="input" class="form-control" >
                </div>

              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Total Booking Value</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                  </div>
                  <input type="input" class="form-control" >
                </div>

              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Agent</label>
                <input type="input" class="form-control"  placeholder="Agent's Name" >
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Agent Commission</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                  </div>
                  <input type="input" class="form-control"  >
                </div>

              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Special Request</label>
                <textarea class="form-control" rows="5">
                </textarea>
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Deposit Payment</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                  </div>
                  <input type="input" class="form-control" >
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Balance Payment</label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-usd" aria-hidden="true"></i>
                  </div>
                  <input type="input" class="form-control" >
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Deposit Payment Due</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker2">
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="exampleInputEmail1">Balance Payment Due</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" class="datetime" id="datepicker3">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Shortfall of Payment</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-usd"></i>
                  </div>
                  <input type="text" class="form-control pull-right" class="datetime">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Inclusion</label>
                <input type="input" class="form-control" name="address">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Flight's Detail</label>
                <textarea class="form-control" rows="5">
                </textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Notes / Remarks</label>
                <textarea class="form-control" rows="5">
                </textarea>
              </div>


            </div>
            <!-- End of Tab Details -->

          </div>
          <!-- End of tab panes -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" id="save" class="btn btn-primary">Save</button>
      </div>
    </form>
    </div>
  </div>
</div>


<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="confirmdelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>