<section class="content-header">

  <h1>
    Interesting Place Types
  </h1>

  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
        <button class="btn btn-default" data-toggle="modal" data-target="#modalLocation" id="btnLocationAdd"><i class="fa fa-plus" aria-hidden="true" ></i> Add Type</button>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Marker</th>
          <th>Name</th>
          <th>Action</th>
        </tr>
        </thead>

        <tbody id="dataRows">
        <?php foreach($interesting_place_types as $interesting_place_type){ ?>
          <tr id="row<?php echo $interesting_place_type->id; ?>">
            <td id="dataMarker<?php echo $interesting_place_type->id; ?>" width="100px"><img src="<?php echo base_url().$interesting_place_type->marker_path; ?>" class="img-fluid img-thumbnail" alt="..." style="height:64px;width:auto;"></td>
            <td id="dataName<?php echo $interesting_place_type->id; ?>"><?php echo $interesting_place_type->name; ?></td>
            <td width="150px">
              <button class="btn btn-default btn-xs btn-block"  id="btnLocationUpdate" value="<?php echo $interesting_place_type->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
              <button class="btn btn-danger btn-xs btn-block"  id="btnLocationDelete" value="<?php echo $interesting_place_type->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
            </td>
          </tr>
        <?php } ?>


        </tbody>

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal -->

<div class="modal fade" id="modalLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLocationLabel">Add Interesting Place Type</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formLocation" role="form">
      <div class="modal-body">
        <input type="hidden" name="id" id="idUpdate" class="form-control" placeholder="">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="nameUpdate" class="form-control" placeholder="">
        </div>

        <div class="form-group">
          <label>Marker Icon</label>
          <input name="file" type="file" multiple data-overwrite-initial="false">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
