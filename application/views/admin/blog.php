<section class="content-header">

  <h1>
    Manajemen Data Blog <a href="blog/addnew" class="btn btn-primary">+ Tambah Artikel Blog Baru</a>
  </h1>
  
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    
    
    

  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-blue-gradient">
        <div class="box-header">

          <h3 class="box-title"></h3>
          <!-- tools box -->
          <div class="pull-right box-tools">
            <!-- button with a dropdown -->
            <div class="btn-group">
              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bars"></i></button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="#">Add new event</a></li>
                <li><a href="#">Clear events</a></li>
                <li class="divider"></li>
                <li><a href="#">View calendar</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /. tools -->
        </div>
        <!-- /.box-header -->

        <!-- /.box-body -->
        <div class="box-footer text-black">
        <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Judul Blog</th>
          <th>Tanggal Dibuat</th>
          <th>Tanggal Diperbarui</th>
          <th>Opsi Manajemen Blog</th>
        </tr>
        </thead>

        <tbody id="dataRows">
          <?php foreach($blogs as $blog){?>
            <tr>
              <td><?php echo $blog->judul; ?></td>
              <td><?php echo $blog->created_on; ?></td>
              <td><?php echo $blog->updated_on; ?></td>
              <td><a href="<?php echo base_url(); ?>superadmin/headline/create?type=blog&id=<?php echo $blog->id; ?>&img=<?php echo $blog->img; ?>" class="btn btn-warning"><i class="fa fa-star"></i> Buat Headline</a></td>
            </tr>
          <?php } ?>
        </tbody>

      </table>
        </div>
      </div>
    </div>
  </div>
  


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
