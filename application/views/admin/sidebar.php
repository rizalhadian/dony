<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->

  <section class="sidebar">

    <!-- Sidebar user panel -->

    <div class="user-panel">

      <div class="pull-left image">

        <img src="<?php echo base_url();?>assets/images/balivillaportfolio-logo.png" class="img-circle" alt="User Image">

      </div>

      <div class="pull-left info">

        <p>First Last Name</p>

        <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->

      </div>

    </div>

    <!-- search form -->

    <!-- <form action="#" method="get" class="sidebar-form">

      <div class="input-group">

        <input type="text" name="q" class="form-control" placeholder="Search...">

            <span class="input-group-btn">

              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>

              </button>

            </span>

      </div>

    </form> -->

    <!-- /.search form -->

    <!-- sidebar menu: : style can be found in sidebar.less -->

    <ul class="sidebar-menu">

      <li class="header">MAIN NAVIGATION</li>

      <!-- <li>

        <a href="#">

          <i class="fa fa-dashboard"></i> <span>Dashboard</span>

          <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

        </a>

        <ul class="treeview-menu">

          <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>

          <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>

        </ul>

      </li> -->



      
      <li class="<?php if($this->session->userdata('ses') == "accommodations"){ echo 'active'; }?>">
        <a href="<?php echo base_url(); ?>superadmin/blog">
          <i class="fa fa-file" aria-hidden="true"></i> <span>Blog</span>
        </a>
      </li>

      <li class="<?php if($this->session->userdata('ses') == "activities"){ echo 'active'; }?>">
        <a href="<?php echo base_url(); ?>superadmin/promo">
          <i class="fa fa-tags" aria-hidden="true"></i> <span>Promo</span>
        </a>
      </li>

      <li class="<?php if($this->session->userdata('ses') == "activities"){ echo 'active'; }?>">
        <a href="<?php echo base_url(); ?>superadmin/headline">
          <i class="fa fa-tags" aria-hidden="true"></i> <span>Headline</span>
        </a>
      </li>

      <li class="<?php if($this->session->userdata('ses') == "kategori"){ echo 'active'; }?>">
        <a href="<?php echo base_url(); ?>superadmin/kategori">
          <i class="fa fa-tags" aria-hidden="true"></i> <span>Kategori</span>
        </a>
      </li>
 

      <li>
        <a href="<?php echo base_url(); ?>superadmin/user">
          <i class="fa fa-user" aria-hidden="true"></i> <span>Users</span>
        </a>
      </li>





    </ul>

  </section>

  <!-- /.sidebar -->

</aside>
