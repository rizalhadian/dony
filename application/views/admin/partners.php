<section class="content-header">

  <h1>
    Partners
    <button class="btn btn-primary" data-toggle="modal" data-target="#modalPartner" id="btnPartnerCreate"><i class="fa fa-plus" aria-hidden="true" ></i> Add Partner</button>

  </h1>

  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="box box-solid ">
    <div class="box-header bg-yellow">

        <h3 class="box-title ">Partner Need Approval</h3>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Company Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Action</th>
          </tr>
          </thead>

          <tbody id="dataRows">
            <?php foreach($partners_unconfirmed as $partner){ ?>
            <tr id="rowPartner<?php echo $partner->id; ?>">
              <td id="dataPartnerName<?php echo $partner->id; ?>"><?php echo $partner->name; ?></td>
              <td id="dataPartnerCompanyName<?php echo $partner->id; ?>"><?php echo $partner->company_name; ?></td>
              <td id="dataPartnerAddress<?php echo $partner->id; ?>"><?php echo $partner->address; ?></td>
              <td id="dataPartnerEmail<?php echo $partner->id; ?>"><?php echo $partner->email; ?></td>
              <td id="dataPartnerPhone<?php echo $partner->id; ?>"><?php echo $partner->phone; ?></td>



              <td width="160px">
                <!-- <button class="btn btn-default btn-xs "  id="btnAccommodationView<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View</button> -->
                <button class="btn btn-default btn-xs btn-block"  id="btnPartnerUpdate<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                <button class="btn btn-success btn-xs btn-block"  id="btnPartnerApprove<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Approve</button>
                <button class="btn btn-danger btn-xs btn-block"  id="btnPartnerDelete<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
              </td>
            </tr>
          <?php } ?>
        </tbody>

        <!-- <tfoot>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
        </tfoot> -->

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>

<section class="content">
  <div class="box">
    <div class="box-header">
        <h3 class="box-title ">Partner Approved</h3>
    </div>

    <!-- /.box-header -->

    <div class="box-body">
      <table id="example2" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Company Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Action</th>
          </tr>
          </thead>

          <tbody id="dataRows">
            <?php foreach($partners as $partner){ ?>
            <tr id="rowPartner<?php echo $partner->id; ?>">
              <td id="dataPartnerName<?php echo $partner->id; ?>"><?php echo $partner->name; ?></td>
              <td id="dataPartnerCompanyName<?php echo $partner->id; ?>"><?php echo $partner->company_name; ?></td>
              <td id="dataPartnerAddress<?php echo $partner->id; ?>"><?php echo $partner->address; ?></td>
              <td id="dataPartnerEmail<?php echo $partner->id; ?>"><?php echo $partner->email; ?></td>
              <td id="dataPartnerPhone<?php echo $partner->id; ?>"><?php echo $partner->phone; ?></td>



              <td width="160px">
                <!-- <button class="btn btn-default btn-xs "  id="btnAccommodationView<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View</button> -->
                <button class="btn btn-default btn-xs btn-block"  id="btnPartnerUpdate<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                <button class="btn btn-danger btn-xs btn-block"  id="btnPartnerDelete<?php echo $partner->id; ?>" value="<?php echo $partner->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
              </td>
            </tr>
          <?php } ?>
        </tbody>

        <!-- <tfoot>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
        </tfoot> -->

      </table>

    </div>

    <!-- /.box-body -->

    <!-- Button trigger modal -->

    <!-- <button class="btn btn-danger btn-xs" name="loka" id="tes"><i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

    </div>

  <!-- /.row -->

      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Company Name</label>
          <input type="text" name="company_name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-success">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="hidden" class="form-control" name="idDelete" id="idDelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
