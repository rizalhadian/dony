<!DOCTYPE html>
<html lang="en">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="Homely - Responsive Real Estate Template">
  <meta name="author" content="Rype Creative [Chris Gipple]">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Homely | Property Single Full Width</title>

  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
  <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>


  <!-- CSS file links -->
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/font-awesome.min.css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/jquery-ui.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/slick.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/chosen.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="<?php echo base_url(); ?>assets/newhomepage/villadetail/css/responsive.css" rel="stylesheet" type="text/css" media="all" />
  <link href="<?php echo base_url(); ?>assets/newhomepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/newhomepage/css/daterangepicker.css" rel="stylesheet" type="text/css"/>

	<!--slider-->

<link href='http://lifewithoutandy.com/wordpress/wp-content/plugins/advanced-responsive-video-embedder/public/assets/css/public.css?ver=5.1.1' rel='stylesheet'  type='text/css' media='all' />
<link href='<?php echo base_url(); ?>assets/newhomepage/css/lwastyle.css' rel='stylesheet' type='text/css'/>
<link href='<?php echo base_url(); ?>assets/newhomepage/css/lwastyle2.css' rel='stylesheet' id='style-css' type='text/css' />

	<style>

	.map-wrap {
      position:relative;
    }

    .overlaymap {
      width:100%;
      min-height:500px;
      position:absolute;
      top:0;
    }

	/* Modal video*/
.modalvideocss {
	z-index: 888;
    position: relative;
    display: block;
    bottom: 130px;
    float: left;
    left: 340px;
    color: #ddd;
    font-size: 14px;
    padding: 10px;
    background-color: rgba(0, 0, 0, 0.7);
    border-radius: 4px;
	cursor: pointer;
}
.modal .modal-header {
  border-bottom: none;
  position: relative;
}
.modal .modal-header .btn {
  position: absolute;
  top: 0;
  right: 0;
  margin-top: 0;
  border-top-left-radius: 0;
  border-bottom-right-radius: 0;
}
.modal .modal-footer {
  border-top: none;
  padding: 0;
}
.modal .modal-footer .btn-group > .btn:first-child {
  border-bottom-left-radius: 0;
}
.modal .modal-footer .btn-group > .btn:last-child {
  border-top-right-radius: 0;
}
	/* Modal video*/

/* rates */
@media (min-width:180px){.ratesnconclusion{
	height:1100px;
}}
@media (min-width:241px){.ratesnconclusion{
	height:920px;
}}
@media (min-width:280px){.ratesnconclusion{
	height:830px;
}}
@media (min-width:357px){.ratesnconclusion{
	height:740px;
}}
@media (min-width:435px){.ratesnconclusion{
	height:630px;
}}
@media (min-width:600px){.ratesnconclusion{
	height:455px;
}}
@media (min-width:768px){.ratesnconclusion{
	height:455px;
}}
@media (min-width:1200px){.ratesnconclusion{
	height:455px;
}}
	/* Modal drone */

	/*  .row > .column {
  padding: 0 8px;
}


.column {
  float: left;
  width: 25%;
}
*/

.row:after {
  content: "";
  display: table;
  clear: both;
}


/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 9999	;
  padding-top: 10px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: transparent;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
  z-index:12000;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}

	/* Carousel */

#quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 30px;
    /* Control buttons  */
    /* Previous button  */
    /* Next button  */
    /* Changes the position of the indicators */
    /* Changes the color of the indicators */
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-control.left {
    left: -60px;
}
#quote-carousel .carousel-control.right {
    right: -60px;
}
#quote-carousel .carousel-indicators {
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 50px;
    height: 50px;
    margin: 5px;
    cursor: pointer;
    border: 4px solid #CCC;
    border-radius: 50px;
    opacity: 0.4;
    overflow: hidden;
    transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
    background: #333333;
    width: 128px;
    height: 128px;
    border-radius: 100px;
    border-color: #f33;
    opacity: 1;
    overflow: hidden;
}
.carousel-inner {
    min-height: 20px;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}
	</style>
	<!--reviews-->

</head>
<body>
	<div class="powered hidden-xs hidden-sm" style="font-size:14px;font-style:italic;display: block;left: 112px;top: 76px;position: fixed;z-index: 1900;" >
	Powered by
	<img width="80px" src="http://www.totalbali.com/tb-home/assets/img/logo/totalbali.png" class="img-responsive">
	</div>
<nav class="navbar navbar-fixed-top green">
            <div class="container-fluid">
                <!--second nav button -->
                <div id="menu_bars" class="right">
                    <span class="t1"></span>
                    <span class="t2"></span>
                    <span class="t3"></span>
                </div>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
						<img src="<?php echo base_url(); ?>assets/newhomepage/images/balivillaportfolio2.png" alt="logo">
						</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <!-- <div class="collapse navbar-collapse navbar-ex1-collapse  ">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#home" class="scroll">Home</a></li>
                            <li><a href="#about-us" class="scroll">About</a></li>
                            <li><a href="#work" class="scroll">Our Work</a></li>
                            <li><a href="#pricing-table-section" class="scroll">Pricing</a></li>
                            <li><a href="#blog-section" class="scroll">News</a></li>
                            <li><a href="#contact-us" class="scroll">Contact Us</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="sidebar_menu">
                    <nav class="pushmenu pushmenu-right">
                        <a class="push-logo" href="#"><img src="<?php echo base_url(); ?>assets/newhomepage/images/balivillaportfolio2.png" alt="logo"></a>
                        <ul class="push_nav centered">
                            <li class="clearfix">
                                <a href="#home"  class="scroll" ><span>01.</span>Home</a>

                            </li>
                            <li class="clearfix">
                                <a href="#work" class="scroll"> <span>02.</span>Villas</a>

                            </li>
                            <li class="clearfix">
                                <a href="#" class="scroll"> <span>03.</span>Live Availability</a>

                            </li>
                            <li class="clearfix">
                                <a href="#pricing-table-section" class="scroll"> <span>04.</span>Map</a>

                            </li>
                            <li class="clearfix">
                                <a href="#blog-section" class="scroll"> <span>05.</span>Videos</a>

                            </li>
                            <li class="clearfix">
                                <a href="#contact-us" class="scroll"> <span>06.</span>Contact Us</a>

                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <ul class="social_icon black top25 bottom20 list-inline">

                            <li><a href="#"><i class="fa fa-fw fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-fw fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-fw fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                        </ul>
                    </nav>
                </div>
            </div>
        </nav>


<!--slider-->

<script type="text/javascript">
  window.LWA = { Data:{} };

  LWA.Data.Gallery = {
    feature: 'images/villa0.jpg',
    inline: [],
    thumbnails: []
  };

      LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/slider1.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/slider1.jpg'
    });

      LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/potrait2.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/potrait2.jpg'
    });

      LWA.Data.Gallery.inline.push({
      src: 'http://lifewithoutandy.com/wordpress/wp-content/uploads/2017/06/LWA-15-5.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: 'http://lifewithoutandy.com/wordpress/wp-content/uploads/2017/06/LWA-15-5.jpg'
    });

      LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/potrait1.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/potrait1.jpg'
    });

      LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/slider2.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/slider2.jpg'
    });

      LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/villa1.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/villa1.jpg'
    });

      LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/villa0.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/villa0.jpg'
    });

	LWA.Data.Gallery.inline.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/slider3.jpg',
      width: '100%',
      caption: ''
    });
    LWA.Data.Gallery.thumbnails.push({
      src: '<?php echo base_url(); ?>assets/newhomepage/images/slider3.jpg'
    });

</script>
<div id="modal-gallery" class="modal modal-transparent modal-gallery">
  <button class="modal-action modal-close"><i class="icon-close"></i></button>
  <div class="modal-gallery-count">1 / 15</div>


  <div class="modal-wrap">
<div class="modal-wrap-row" data-closeable="0">
      <div id="modal-gallery-frame" class="modal-view frame" data-closeable="0">
        <div class="royalSlider rsHor" style="height: 528px;">

		</div>

		<div class="rsGCaption"><span class="rsCaption"></span></div>

	</div>
</div>
      <button class="gallery-home button button-white" id="modal-gallery-home">back to start3</button>
      <div class="sly-controls">
        <button class="sly-prev"><i class="icon-arrow-left"></i></button>
        <button class="sly-next"><i class="icon-arrow-right"></i></button>
      </div>
    </div>
</div>

  <div id="modal-slider-next" class="gallery-next-overlay" data-closeable="0">
    <div class="royalSlider rsHor" style="width: 1349px; height: 648px;"><div class="rsOverflow" style="width: 1349px; height: 648px;"><div class="rsContainer" style="transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><div style="left: 0px;" class="rsSlide  rsActiveSlide"><div class="gallery-next-slide" data-closeable="0"></div></div><div style="left: 1357px;" class="rsSlide "><div class="gallery-next-slide" data-closeable="0">
        <div class="f-grid f-row" data-closeable="0">
          <div class="f-1 content-wrap" data-closeable="0">
            <div class="content-row" data-closeable="0">
              <div class="header-content" data-closeable="0">
  <script type="text/javascript">
    LWA.hasNextPost = true;
  </script>

  <div class="gallery-next" data-closeable="0">
    <div class="h-3">Next up...</div>
    <div class="thumb" data-closeable="0">
      <a href="http://lifewithoutandy.com/news/fashion/mambo-get-wacky-in-acid-tropic-collabo-with-byron-spencer-peter-simon-phillips/" class="thumb-feature">
        <img src="http://lifewithoutandy.com/wordpress/wp-content/uploads/2017/06/Untitled-2-1024x576.jpg">        <div class="blanket-light"></div>
      </a>
    </div>
    <a href="http://lifewithoutandy.com/news/fashion/mambo-get-wacky-in-acid-tropic-collabo-with-byron-spencer-peter-simon-phillips/" class="h-2">
	Mambo Get Wacky In 'Acid Tropic' Collabo With Byron Spencer &amp; Peter Simon Phillips</a>
    <div class="h-4">The results are out of this world.</div>
  </div>
                <button class="button button-white modal-gallery-home" id="modal-gallery-home"><i class="fa fa-previews"></i>back to start1</button>
              </div>
            </div>
          </div>
        </div>
      </div></div></div></div></div>
  </div>

</div>
<header id="header-gallery" class="header header-feature header-gallery">
  <div class="header-gallery-wrap">
    <div id="header-gallery-wrap" class="m-wrap animate-gallery">
      <div id="inline-gallery-frame" class="header-feature-bg header-gallery-frame frame" style="overflow: hidden;">
  <ul class="slidee" style="transform: translateZ(0px) translateX(-1695px); width: 11223px;">
      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/slider1.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/slider1.jpg"></div>
        <div class="blanket"></div>
      </li>

	  <li class="sly-slide header-gallery-overlay active" style="width: 449px;">

		<div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/potrait2.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/potrait2.jpg"></div>
		<div class="blanket"></div>
	  </li>

      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="http://lifewithoutandy.com/wordpress/wp-content/uploads/2017/06/LWA-15-5.jpg" src="http://lifewithoutandy.com/wordpress/wp-content/uploads/2017/06/LWA-15-5.jpg"></div>
        <div class="blanket"></div>
      </li>

      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/potrait1.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/potrait1.jpg"></div>
        <div class="blanket"></div>
      </li>

      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/slider2.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/slider2.jpg"></div>
        <div class="blanket"></div>
      </li>

      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/villa1.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/villa1.jpg"></div>
        <div class="blanket"></div>
      </li>

      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/villa0.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/villa0.jpg"></div>
        <div class="blanket"></div>
      </li>

      <li class="sly-slide header-gallery-overlay" style="width: 1122px;">

        <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url(); ?>assets/newhomepage/images/slider3.jpg" src="<?php echo base_url(); ?>assets/newhomepage/images/slider3.jpg"></div>
        <div class="blanket"></div>
      </li>
  </ul>
</div>
      <div class="m-overlay blanket-light" style="display: none;"></div>

      <div class="f-grid f-row sly-controls">
        <div id="inline-gallery-controls" class="f-1">
          <button class="gallery-home button button-white" id="inline-gallery-home"><i class="fa fa-arrow-left"></i></button>
          <button class="sly-prev"><i class="icon-arrow-left"></i></button>
          <button class="sly-next"><i class="icon-arrow-right"></i></button>
        </div>
      </div>

      <div class="f-grid f-row header-gallery-title" style="display: none;">
        <div class="f-1 content-wrap">
          <div class="content-row">
            <div class="header-content">
              <div class="header-feature-category"><a class="link h-1" href="#">
        <span class="header-feature-category-item">Villa Muse</span>
      </a>
    </div>              <div class="h-2">With the colonial style often found in the Caribbean and the Hamptons, Villa  is white and bright and will leave you feeling refreshed & pampered.</div>
            </div>
          </div>
        </div>
      </div>

      <div class="gallery-next-overlay">
        <div class="gallery-next-wrap">
          <div class="f-grid f-row">
            <div class="f-1 content-wrap">
              <div class="content-row">
                <div class="header-content">
                    <script type="text/javascript">
						LWA.hasNextPost = true;
					</script>

  <div class="gallery-next" data-closeable="0">
    <div class="h-3">Next up...</div>
    <div class="thumb" data-closeable="0">
      <a href="http://lifewithoutandy.com/news/fashion/mambo-get-wacky-in-acid-tropic-collabo-with-byron-spencer-peter-simon-phillips/" class="thumb-feature">
        <img src="http://lifewithoutandy.com/wordpress/wp-content/uploads/2017/06/Untitled-2-1024x576.jpg">        <div class="blanket-light"></div>
      </a>
    </div>
    <a href="http://lifewithoutandy.com/news/fashion/mambo-get-wacky-in-acid-tropic-collabo-with-byron-spencer-peter-simon-phillips/" class="h-1">Mambo Get Wacky In 'Acid Tropic' Collabo With Byron Spencer &amp; Peter Simon Phillips</a>
    <div class="h-4">The results are out of this world.</div>
  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="header-gallery-details">
        <div class="header-gallery-controls">
          <!--button id="gallery-thumbs" class="button button-gallery button-thumbs"><i class="icon-thumbs"></i></button-->
	  <button id="modal-gallery-button" class="button button-gallery hidden-lg hidden-md"><i class="icon-expand"></i> <font style="font-size:14px;font-family: verdana, Arial;">Launch Gallery</font> </button>
        </div>
      </div>
    </div>
  </div>


</header>
<!--slider end

<div class="container" style="margin-top:10px">
	<div class="row">
	<div class="col-lg-2">
		<img class="img-responsive" src="villadetail/images/property-img5.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="villadetail/images/property-img6.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="villadetail/images/property-img7.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="villadetail/images/property-img1.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="villadetail/images/property-img2.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="villadetail/images/property-img3.jpg" alt="" />
	</div>

	</div>
	</div>

-->
<!--slider-->

<section class="module">
  <div class="container">
	<h2>Villa Muse <br><span style="font-size:14px;color:#797979">Seminyak, Bali</span></h2>
	<div class="row">
		<div class="col-lg-8 col-md-8">
		<div class="property-single-item property-details">
				<table class="property-details-single">
					<tr>
						<td><i class="fa fa-bed"></i> <span>3</span> Beds</td>
						<td><i class="fa fa-bath"></i> <span>2</span> Baths</td>
						<td><i class="fa fa-expand"></i> <span>25,000</span> Sq Mtrs</td>
						<td><i class="fa fa-dollar"></i> <span>200 / </span> PN</td>
					</tr>
				</table>
		</div>
			<div class="row">
			<div class="widget property-single-item property-description content">
				<h4>
					<span>Description</span>
				</h4>
				<p align="justify">Luxuriously appointed bedrooms and bathrooms, numerous sitting areas to relax and a home theatre are on the main floors,
				while the stunning roof top area offers lounge and dining space adjacent to the welcoming infinity pool and sun terrace.
				With views over Seminyak and to the volcanoes beyond, here you can do nothing but relax as you’re cared for by a wonderful team of staff,
				all dedicated to ensuring you have a truly memorable stay that will live with you long after you leave Bali.
				</p>

				<p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar.
				Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat.
				Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa,
				a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper
				placerat, velit risus accumsan nisl, eget tempor lacus est vel nunc. Proin accumsan elit sed neque euismod fringilla.
				Curabitur lobortis nunc velit, et fermentum urna dapibus non. Vivamus magna lorem, elementum id gravida ac, laoreet
				tristique augue. Maecenas dictum lacus eu nunc porttitor, ut hendrerit arcu efficitur.</p>
			</div>
			</div>
		</div>


    <!-- Side Form Online Booking -->
    <div class="col-lg-4 col-md-4">
        <div class="box">
            <div class="box-header" style="background-color:#48A0DC;">
                <h3 class="box-title" style="font-size: 120%; color:white;">Start from <b style="font-size: 120%;color:black;">$<?php echo $cheapest_rate; ?></b>/night</h3>
                &nbsp;&nbsp;
                <a class="btn btn-default btn-sm" data-html="true"
                data-toggle="popover" data-placement="bottom"
                data-content="
                <table class='table table-bordered table-striped'>
                  <?php foreach($promos as $promo){ ?>
                    <tr>
                      <td width='300px'>
                        <center><?php echo date("d M Y", strtotime($promo->date_start));?> <br>-<br> <?php echo date("d M Y", strtotime($promo->date_end));?></center>
                      </td>
                      <td style='vertical-align:middle;'>
                        $<b><?php echo $promo->rate;?></b> / Night
                      </td>
                    </tr>
                  <?php } ?>

                </table>">
                <i class="fa fa-tags" aria-hidden="true"></i> Promo
                </a>


            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body" style="background-color:#f2f2f2;">

                <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/create" method="POST">
                    <input type="hidden" name="id" id="id" value="<?php echo $room->id; ?>">

                    <div class="form-group col-md-12 col-xs-12">
                        <label style="text-align:center; font-size: 100%;">Checkin - Checkout</label>
                        <input style="text-align:center; font-size: 100%;" type="text" class="form-control pull-right" id="checkinout" value="">
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <label style="text-align:center; font-size: 100%;">Guest</label>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="text-align:center; font-size: 100%;">Mature </td>
                                            <td style="text-align:right">
                                                <button type="button" class="btn btn-default" id="btn_mature_reduce">-</button>
                                            </td>
                                            <td style="text-align:center; font-size: 100%;" width="5%"><span id="txt_guest_mature">xx</span></td>
                                            <td style="text-align:left">
                                                <button type="button" class="btn btn-default" id="btn_mature_add">+</button>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td style="text-align:center; font-size: 100%;">Children </td>
                                            <td style="text-align:right">
                                                <button type="button" class="btn btn-default" id="btn_children_reduce">-</button>
                                            </td>
                                            <td style="text-align:center; font-size: 100%;" width="5%"><span id="txt_guest_children">xx</span>
                                                <input type="hidden" id="guest_children" name="guest_children">
                                            </td>
                                            <td style="text-align:left">
                                                <button type="button" class="btn btn-default" id="btn_children_add">+</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; font-size: 100%;">Infant </td>
                                            <td style="text-align:right">
                                                <button type="button" class="btn btn-default" id="btn_infant_reduce">-</button>
                                            </td>
                                            <td style="text-align:center; font-size: 100%;" width="5%"><span id="txt_guest_infant">xx</span>
                                                <input type="hidden" id="guest_infant" name="guest_infant">
                                                </span>
                                            </td>
                                            <td style="text-align:left">
                                                <button type="button" class="btn btn-default" id="btn_infant_add">+</button>
                                            </td>
                                        </tr>
                                        <tr class="danger">
                                            <td style="text-align:center; font-size: 100%;">Extra </td>
                                            <td style="text-align:right">
                                                <button type="button" class="btn btn-default" id="btn_extra_reduce">-</button>
                                            </td>
                                            <td style="text-align:center; font-size: 100%;" width="5%"><span id="txt_guest_extra">xx</span>
                                                <input type="hidden" id="guest_extra" name="guest_extra">
                                            </td>
                                            <td style="text-align:left">
                                                <button type="button" class="btn btn-default" id="btn_extra_add">+</button>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body " style="font-size: 100%;">
                                <label>Total Prices</label>
                                <table class="table table-bordered" id="total_prices_table">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <button  class="col-md-12 col-sm-12 col-xs-12  btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <b>Add your details</b>
                        </button>
                    </div>
                    <div class="collapse" id="collapseExample">
                      <div class="form-group col-md-12 col-xs-12">
                          <label style="text-align:center; font-size: 100%;">Name</label>
                          <input type="text" class="form-control" name="name">
                      </div>
                      <div class="form-group col-md-12 col-xs-12">
                          <label style="text-align:center; font-size: 100%;">Email</label>
                          <input type="text" class="form-control" name="email">
                      </div>
                      <div class="form-group col-md-12 col-xs-12">
                          <label style="text-align:center; font-size: 100%;">Retype Email</label>
                          <input type="text" class="form-control" name="email_retype">
                      </div>
                      <div class="form-group col-md-12 col-xs-12">
                          <label style="text-align:center; font-size: 100%;">Phone</label>
                          <input type="text" class="form-control" name="phone">
                      </div>
                      <div class="form-group col-md-12 col-xs-12">
                          <label style="text-align:center; font-size: 100%;">Message</label>
                          <textarea class="form-control" rows="3"></textarea>
                      </div>
                    </div>
                    <div class="form-group col-md-12 col-xs-12">
                        <button type="submit" class="col-md-12 col-sm-12 col-xs-12  btn btn-lg btn-success">
                            Booking
                        </button>

                    </div>

            </div>
        </div>
        <!-- /.box-body -->

    </div>
	</div>

		<div class="row">
			<div class="col-lg-8 col-md-8">
			<h4>Video</h4>
		<div class="row widget property-single-item">
			<div align="center">
					<iframe width="100%" height="322" src="https://www.youtube.com/embed/0BrSex0Pc5U" frameborder="0" allowfullscreen></iframe>
			<div data-toggle="modal" href="#videomodal" class="modalvideocss">
			Click <i class="fa fa-search-plus"></i>
			</div>
			</div>

<div id="videomodal" class="modal fade in">
                <div class="modal-header">
                    <!--a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title">Modal Heading</h4-->
                </div>
                    <iframe width="1349" height="90%" src="https://www.youtube.com/embed/0BrSex0Pc5U" frameborder="0" allowfullscreen></iframe>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-xs btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>
                        <button class="btn btn-xs btn-primary"><i class="fa fa-share"></i> Share</button>
                    </div>
                </div>

</div><!-- /.modal -->

		</div>
			</div>


		</div>




	<div class="row" style="margin-top:40px">
		<div class="col-lg-8 col-md-8">
				<h4>Location <br><span style="font-size:12px;color:#797979">Seminyak, Bali</span></h4>
			<div class="row widget property-single-item">
			<div class="map-wrap">
				<div class="overlaymap" onClick="style.pointerEvents='none'"></div>
				<iframe frameborder='0' scrolling='no' style='border:0;width:100%;height:450px' src='http://totalbalidirect.com/map/map_nwidget?c=1&sc=0&p=0&l=2&z=10&ce=-8.525936671766459, 115.17665147781372&w=1000&h=500&marker_selected=themuse'></iframe>
				</div>
			</div>
		</div>

	</div>


		<div class="row">
		<div class="col-lg-8 col-md-8">
			<!-- end description -->
			<!-- <div class="widget property-single-item property-amenities">
				<h4>
					<span>Amenities</span> <img class="divider-hex" src="http://rypecreative.com/homely/images/divider-half.png" alt="" />
					<div class="divider-fade"></div>
				</h4>
				<ul class="amenities-list">
					<li><i class="fa fa-check icon"></i> Balcony</li>
					<li><i class="fa fa-check icon"></i> Cable TV</li>
					<li><i class="fa fa-check icon"></i> Deck</li>
					<li><i class="fa fa-check icon"></i> Dishwasher</li>
					<li><i class="fa fa-check icon"></i> Heating</li>
					<li><i class="fa fa-close icon"></i> Internet</li>
					<li><i class="fa fa-check icon"></i> Parking</li>
					<li><i class="fa fa-check icon"></i> Pool</li>
					<li><i class="fa fa-check icon"></i> Oven</li>
					<li><i class="fa fa-close icon"></i> Gym</li>
					<li><i class="fa fa-check icon"></i> Laundry Room</li>
				</ul>
			</div>end amenities -->
			<!-- end location -->

		<div class="row">
			<div class="widget property-single-item">
				<h4>
					<span>Villa Muse Reviews</span>
				</h4>
			     <div class="col-md-12" data-wow-delay="0.2s">
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <!-- Bottom Carousel Indicators--
                            <ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/rssems/128.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg" alt="">
                                </li>
                            </ol-->

                            <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner text-center">
                                <!-- Quote 1 -->
                                <div class="item active">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !</p>
                                                <small>Someone famous</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 2 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                                <small>Someone famous</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 3 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-8 col-sm-offset-2">

                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
                                                <small>Someone famous</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                            </div>

                            <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
			</div>
		</div><!-- end reviews -->

		</div><!-- end col -->

		<div class="col-lg-4 col-md-4 sidebar sidebar-property-single">
			<!-- end widget -->



		</div><!-- end sidebar -->

	</div><!-- end row -->



  <div class="row">
  <div class="widget property-single-item col-lg-12">
				<h4>
					<span>Similar Properties</span>
				</h4>

				<div class="row">
					<div class="col-lg-4 col-md-4">
			          <div class="property shadow-hover" style="margin-bottom:20px">
			            <a href="#" class="property-img">
			              <div class="property-price"><sub>$</sub>6,500 <span>Per Night </span></div>
			              <div class="property-color-bar"></div>
			              <img class="img-responsive" style="max-height: 213px;" src="images/villa1.jpg" alt="" />
			            </a>
			            <div class="property-content">
			              <div class="property-title">
			              <h4><a href="#">Villa Ubud Bali</a></h4>
			                <p class="property-address"><i class="fa fa-map-marker icon"></i>Ubud, Bali</p>
			              </div>
			              <table class="property-details">
			                <tr>
			                  <td><i class="fa fa-bed"></i> 3 Beds</td>
			                  <td><i class="fa fa-tint"></i> 2 Baths</td>
			                  <td><i class="fa fa-expand"></i> 25,000 Sq Mtrs</td>
			                </tr>
			              </table>
			            </div>
			            <div class="property-footer">
			              <span class="left"><i class="fa fa-calendar-o icon"></i> 1 week ago</span>
			              <span class="right">
			                <a href="#"><i class="fa fa-share-alt"></i></a>
			              </span>
			              <div class="clear"></div>
			            </div>
			          </div>
			        </div>

					<div class="col-lg-4 col-md-4" style="margin-bottom:20px">
			          <div class="property shadow-hover">
			            <a href="#" class="property-img">
			              <div class="property-price"><sub>$</sub>6,500 <span>Per Night </span></div>
			              <div class="property-color-bar"></div>
			              <img class="img-responsive" style="max-height: 213px;" src="images/villa0.jpg" alt="" />
			            </a>
			            <div class="property-content">
			              <div class="property-title">
			              <h4><a href="#">Villa Ubud Bali</a></h4>
			                <p class="property-address"><i class="fa fa-map-marker icon"></i>Ubud, Bali</p>
			              </div>
			              <table class="property-details">
			                <tr>
			                  <td><i class="fa fa-bed"></i> 3 Beds</td>
			                  <td><i class="fa fa-tint"></i> 2 Baths</td>
			                  <td><i class="fa fa-expand"></i> 25,000 Sq Mtrs</td>
			                </tr>
			              </table>
			            </div>
			            <div class="property-footer">
			              <span class="left"><i class="fa fa-calendar-o icon"></i> 1 week ago</span>
			              <span class="right">
			                <a href="#"><i class="fa fa-share-alt"></i></a>
			              </span>
			              <div class="clear"></div>
			            </div>
			          </div>
			        </div>

					<div class="col-lg-4 col-md-4" style="margin-bottom:20px">
			          <div class="property shadow-hover">
					  <div align="center">
			            <a href="#" class="property-img">
			              <div class="property-price"><sub>$</sub>6,500 <span>Per Night </span></div>
			              <div class="property-color-bar"></div>
			            <img class="img-responsive" style="max-height: 213px;" src="images/villaamaya1.jpg" alt="" />
			            </a>
					  </div>
			            <div class="property-content">
			              <div class="property-title">
			              <h4><a href="#">Villa Ubud Bali</a></h4>
			                <p class="property-address"><i class="fa fa-map-marker icon"></i>Ubud, Bali</p>
			              </div>
			              <table class="property-details">
			                <tr>
			                  <td><i class="fa fa-bed"></i> 3 Beds</td>
			                  <td><i class="fa fa-tint"></i> 2 Baths</td>
			                  <td><i class="fa fa-expand"></i> 25,000 Sq Mtrs</td>
			                </tr>
			              </table>
			            </div>
			            <div class="property-footer">
			              <span class="left"><i class="fa fa-calendar-o icon"></i> 1 week ago</span>
			              <span class="right">
			                <a href="#"><i class="fa fa-share-alt"></i></a>
			              </span>
			              <div class="clear"></div>
			            </div>
			          </div>
			        </div>

			    </div>
			</div>
		</div>
</div>
</section>

<section>
    <div id="contacts">
            <div class="row">
                <section id="footer-1" class="col-md-12" style="margin-bottom: 0px; padding: 0px">
                    <div id="foot1" class="col-sm-2">
                        <h3 class="text-center">Marketing By</h3>
                        <center><hr/></center>
                        <div class="logo-img">
                            <a target="_blank" href="http://www.balivillaportfolio.com">
                                <img src="http://www.totalbali.com/tb-home/assets/img/logo/balivillaportfolio.png" class="img-responsive">
                            </a>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <h3 class="text-center">Powered By</h3>
                        <center><hr/></center>
                        <div class="logo-img">
                            <a target="_blank" href="http://www.totalbali.com">
                                <img src="http://www.totalbali.com/tb-home/assets/img/logo/totalbali.png" class="img-responsive">
                            </a>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <h3 class="text-center">Some Of Our Villas</h3>
                        <center><hr/></center>
                        <div class="col-md-12" style="text-align: center; font-size: 9pt">
                            <div class="col-md-4"><a target="_blank" href="http://www.villaamaya.com">Villa Amaya</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villaaramis.com">Villa Aramis</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villaaumbali.com">Villa Aum Bali</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villadayah.com">Villa Dayah</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villagorillabali.com">Villa Gorilla Bali</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.istanasemer.com">Villa Istana Semer</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.livinglightubud.com">Living Light Ubud</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villamonabali.com">Villa Mona Bali</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villatirtadari.com">Villa Tirtadari</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villaumalas.com">Villa Umalas</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://villaamita.com/">Villa Amita</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villamorobali.com">Villa Moro</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.themusevilla.com/">Villa The Muse</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://www.villareflectionscanggu.com">Villa Reflection Canggu</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://villakuta.com/">Villa Kuta</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://villavayana.com">Villa Vayana</a></div>
                            <div class="col-md-4"><a target="_blank" href="http://dev.totalbali.com/villatom/">Villa Tom</a></div>

                        </div>
                    </div>

                    <div id="foot2" class="col-md-3">
                        <h3 class="text-center">Affiliate Partner Pages</h3>
                        <center><hr/></center>
                        <a target="_blank" href="http://www.totalbalivillas.com"><p class="text-center">Total Bali Villas</p></a>
                        <a target="_blank" href="http://www.totalbaliactivities.com"><p class="text-center">Total Bali Activities</p></a>
                        <a target="_blank" href="http://www.totalbalidirect.com"><p class="text-center">Total Bali Direct</p></a>
                        <a target="_blank" href="http://www.indoinvestmets.com"><p class="text-center">Indo Investments</p></a>
                        <a target="_blank" href="http://www.balibio.com"><p class="text-center">Bali Bio</p></a>
                    </div>
                </section>

                <section id="footer-3" class="col-md-12" style="margin-top: 0px; padding: 0px">
                    <br>
                    <div class="col-md-10 col-md-offset-1" style="text-align: center;">
                        <p class="text-center" style="margin-top: 2em;">Exclusively Marketing the Bali Villa Portfolio A hand selected group of villas for your holiday Total Bali Direct is established to help villa owners.</p>
                        <p class="text-center">Office Phone: +6285100187444 | Whats Ap Phone: +6281338648034 | Email: <a href="mailto:totalbali@totalbali.com" target="_blank">totalbali@totalbali.com</a></p>
                    </div>
                </section>

            </div>
        </div>
</section>
<footer id="footer" style="background-color: #fff">
    <div class="container">
        <div class="footer-text" style="margin-top: 15px; margin-bottom: 10px">
            <p class="text-center" style="font-size: 13px;"> 2017 &copy;, Total Bali is a fully registered Indonesian company traded by PT Total Business Incorporated, <br/>
                with over 25 years living in Indonesia, we ensure that you have a professional and accountable team at your disposal for all your Island needs.</p>
            </div>
        </div>
</footer>


<!--<script src="villadetail/js/jquery-ui.min.js"></script>
<script src="villadetail/js/chosen.jquery.min.js"></script>
<script src="villadetail/js/isotope.min.js"></script>

-->
<script src="<?php echo base_url(); ?>assets/newhomepage/villadetail/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/newhomepage/villadetail/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/newhomepage/villadetail/js/wNumb.js"></script>
<script src="<?php echo base_url(); ?>assets/newhomepage/villadetail/js/map-single.js"></script>
<script src="<?php echo base_url(); ?>assets/newhomepage/villadetail/js/global.js"></script>
<script src="<?php echo base_url(); ?>assets/newhomepage/js/script.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<!--slider-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-41298642-1', 'auto');
    ga('send', 'pageview');

	 $('input[name="daterange"]').daterangepicker({
        });

		<!--Modal-->
		function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
  </script>

<script async src="//www.googletagservices.com/tag/js/gpt.js"> </script>

	<!--slider-->
<script type='text/javascript' src='http://lifewithoutandy.com/wordpress/wp-content/themes/warhol/static/dist/js/9e81a7e5d3e8.global.min.js'></script>
<script type='text/javascript' src='http://lifewithoutandy.com/wordpress/wp-content/themes/warhol/static/dist/js/27323ae010fb.gallery.min.js'></script>
<!--slider-->
<script>
    var base_url = "<?php echo base_url();?>";
</script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->
<!-- <script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script> -->
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/demo.js"></script>

<!-- daterange picker -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/daterangepicker.css">
<script src="<?php echo base_url();?>assets/js/daterangepicker.js"></script>
<script>

    // alert($.datepicker.formatDate('mm/dd/yy', new Date('01/01/2018')));
    //
    // if(new Date('08/03/2017') >= new Date('08/01/2017')){
    //   alert("Uweeee");
    // }else{
    //   alert('Noooo');
    // }

    $("[data-toggle=popover]").popover();
    var guest_max = 0;
    var guest_max_extra = 0;
    var guest_infant_max = 0;
    var guest_mature = 1;
    var guest_children = 0;
    var guest_infant = 0;
    var guest_extra = 0;
    var id = $('#id').val();
    var rates;
    var disabledArr;
    request = $.ajax({
        url: base_url + "api/rooms/" + id + "?get_rates&get_booked_dates",
        type: "GET",
        dataType: 'json'
    });
    request.done(function(response, textStatus, jqXHR) {
        // alert(response.booked_dates);
        rates = response.rates;
        disabledArr = response.booked_dates;
        for (j = 0; j < rates.length; j++) {
            if (rates[j].type == 0) {
                $("#rate_start_from").text(rates[j].rate);
            }
        }
    });
    $(function() {
        // Define the disabled date array
        $("#checkinout").daterangepicker({
            "opens": "center",
            isInvalidDate: function(arg) {
                console.log(arg);

                // Prepare the date comparision
                var thisMonth = arg._d.getMonth() + 1; // Months are 0 based
                if (thisMonth < 10) {
                    thisMonth = "0" + thisMonth; // Leading 0
                }
                var thisDate = arg._d.getDate();
                if (thisDate < 10) {
                    thisDate = "0" + thisDate; // Leading 0
                }
                var thisYear = arg._d.getYear() + 1900; // Years are 1900 based

                var thisCompare = thisMonth + "/" + thisDate + "/" + thisYear;
                console.log(thisCompare);

                if ($.inArray(thisCompare, disabledArr) != -1) {
                    console.log("      ^--------- DATE FOUND HERE");
                    return true;
                } else {
                    return false;
                }
            }
        });
        $('#checkinout').on('hide.daterangepicker', function(ev, picker) {
            check_prices();
        });
    });

    function check_prices() {
        //do something, like clearing an input
        $('#total_prices_table').html('');
        var checkinout = $("#checkinout").val();
        var checkinoutsplit = checkinout.split(" - ");
        var checkin = checkinoutsplit[0];
        var checkout = checkinoutsplit[1];
        var total_details = [];
        var total_detail = [];
        var total_extra_details = []
        var total_extra_detail = []

        var count_days = Math.round((new Date(checkout) - new Date(checkin)) / (1000 * 60 * 60 * 24));
        for (i = 0; i < count_days; i++) {
          var special_rate_founded = false;
            for (j = 0; j < rates.length; j++) {
                if (rates[j].type == 0) {
                    j++;
                }
                var rate_date_start = $.datepicker.formatDate('mm/dd/yy', new Date(rates[j].date_start));
                var rate_date_end = $.datepicker.formatDate('mm/dd/yy', new Date(rates[j].date_end));
                var cind = new Date(checkin);
                var d = new Date(cind).setDate(cind.getDate() + i);
                var date = $.datepicker.formatDate('mm/dd/yy', new Date(d));

                if ((new Date(date) >= new Date(rate_date_start)) && (new Date(date) <= new Date(rate_date_end))) {
                    special_rate_founded = true;
                    // alert("Price is $ "+rates[j].rate+". Extra is $"+rates[j].rate_extra);
                    // alert(JSON.stringify(rates[j]));
                    total_detail = [date, rates[j].rate];
                    total_extra_detail = [date, rates[j].rate_extra * guest_extra];
                    total_details.push(total_detail);
                    total_extra_details.push(total_extra_detail);
                    // alert(total_details);
                }
            }
            if(special_rate_founded == false){
              // alert('walaa');
              for (k = 0; k < rates.length; k++) {
                  if (rates[k].type == 0) {
                      //Base Rate
                      total_detail = [date, rates[k].rate];
                      total_extra_detail = [date, rates[k].rate_extra * parseInt(guest_extra)];
                      total_details.push(total_detail);
                      total_extra_details.push(total_extra_detail);
                      // alert(total_details);
                  }
              }
            }
            special_rate_founded = false;
        }
        var total_per_night = 0;
        var total_extra_per_night = 0;

        for (i = 0; i < total_details.length; i++) {
            total_per_night = total_per_night + parseInt(total_details[i][1]);
            total_extra_per_night = total_extra_per_night + parseInt(total_extra_details[i][1])
        }

        var total = total_per_night + total_extra_per_night;

        $('#total_prices_table').append('<tr><td>Total Per Night</td><td style="text-align:right">$' + total_per_night + '</td></tr>');
        if (guest_extra > 0) {
            $('#total_prices_table').append('<tr><td>Total Extra Per Night</td><td style="text-align:right">$' + total_extra_per_night + '</td></tr>');
        }
        $('#total_prices_table').append('<tr><td><b>Total</b></td><td style="text-align:right;border-top: thin double #000000;">$<b>' + total + '</b></td></tr>');
    }
</script>
<script>
    var room_info;
    request = $.ajax({
        url: base_url + "/api/rooms/" + id + "?get_booked_dates",
        type: "GET",
        dataType: 'json'
    });
    request.done(function(response, textStatus, jqXHR) {
        // alert(response.booked_dates);
        room_info = response.info;
        guest_max = parseInt(room_info.max_guest_normal);
        guest_max_extra = parseInt(room_info.max_guest_extra);
        guest_infant_max = parseInt(room_info.max_guest_infant);
    });

    function check_total_guest() {
        if ((guest_mature + guest_children) == guest_max) {
            $('#btn_mature_add').prop('disabled', true);
            $('#btn_children_add').prop('disabled', true);
        }
        if ((guest_mature + guest_children) < guest_max) {
            $('#btn_mature_add').prop('disabled', false);
            $('#btn_children_add').prop('disabled', false);
        }
        if (guest_mature == 1) {
            $('#btn_mature_reduce').prop('disabled', true);
        } else {
            $('#btn_mature_reduce').prop('disabled', false);
        }
        if (guest_children == 0) {
            $('#btn_children_reduce').prop('disabled', true);
        } else {
            $('#btn_children_reduce').prop('disabled', false);
        }
        if (guest_infant == guest_infant_max) {
            $('#btn_infant_add').prop('disabled', true);
        } else {
            $('#btn_infant_add').prop('disabled', false);
        }
        if (guest_infant == 0) {
            $('#btn_infant_reduce').prop('disabled', true);
        } else {
            $('#btn_infant_reduce').prop('disabled', false);
        }
        if (guest_extra == guest_max_extra) {
            $('#btn_extra_add').prop('disabled', true);
        } else {
            $('#btn_extra_add').prop('disabled', false);
        }
        if (guest_extra == 0) {
            $('#btn_extra_reduce').prop('disabled', true);
        } else {
            $('#btn_extra_reduce').prop('disabled', false);
        }

    }
    $(function() {
        $('#guest_mature').val(guest_mature);
        $('#guest_children').val(guest_children);
        $('#guest_infant').val(guest_infant);
        $('#guest_extra').val(guest_extra);

        $('#txt_guest_mature').text(guest_mature);
        $('#txt_guest_children').text(guest_children);
        $('#txt_guest_infant').text(guest_infant);
        $('#txt_guest_extra').text(guest_extra);

        $('#btn_mature_reduce').prop('disabled', true);
        $('#btn_children_reduce').prop('disabled', true);
        $('#btn_infant_reduce').prop('disabled', true);
        $('#btn_extra_reduce').prop('disabled', true);

        $('#btn_mature_add').on('click', function() {
            guest_mature++;
            $('#txt_guest_mature').text(guest_mature);
            $('#guest_mature').val(guest_mature);
            check_total_guest();
        });
        $('#btn_mature_reduce').on('click', function() {
            guest_mature--;
            $('#txt_guest_mature').text(guest_mature);
            $('#guest_mature').val(guest_mature);
            check_total_guest();
        });
        $('#btn_children_add').on('click', function() {
            guest_children++;
            $('#txt_guest_children').text(guest_children);
            $('#guest_children').val(guest_children);
            check_total_guest();
        });
        $('#btn_children_reduce').on('click', function() {
            guest_children--;
            $('#txt_guest_children').text(guest_children);
            $('#guest_children').val(guest_children);
            check_total_guest();
        });
        $('#btn_infant_add').on('click', function() {
            guest_infant++;
            $('#txt_guest_infant').text(guest_infant);
            $('#guest_infant').val(guest_infant);
            check_total_guest();
        });
        $('#btn_infant_reduce').on('click', function() {
            guest_infant--;
            $('#txt_guest_infant').text(guest_infant);
            $('#guest_infant').val(guest_infant);
            check_total_guest();
        });
        $('#btn_extra_add').on('click', function() {
            guest_extra++;
            $('#txt_guest_extra').text(guest_extra);
            $('#guest_extra').val(guest_extra);
            check_total_guest();
            check_prices();
        });
        $('#btn_extra_reduce').on('click', function() {
            guest_extra--;
            $('#txt_guest_extra').text(guest_extra);
            $('#guest_extra').val(guest_extra);
            check_total_guest();
            check_prices();
        });
    });
</script>
</body>
</html>
