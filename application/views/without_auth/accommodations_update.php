<section class="content-header">
  <center>
  <img src="<?php echo base_url(); ?>/assets/images/totalbali.png" class="img-fluid" alt="Responsive image">
  <h2>
    Partner Accomodation Update
  </h2>
  </center>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">

    <?php if($this->session->updated == 1){ ?>
      <div class="col-md-12">
        <div class="alert alert-success" role="alert">
          Thank you, your accommodation has been updated.
        </div>
      </div>
    <?php } ?>


    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Accommodations Information</h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">
          <!-- isi -->
          <div class="box-body">
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>admin/accommodations/update_post" method="POST">
              <input type="hidden" name="unique_id" id="unique_id" value="<?php echo $accommodation->unique_id; ?>">
              <input type="hidden" name="single_accommodation" value="1">
              <input type="hidden" name="id" class="form-control" value="<?php  echo $accommodation->id; ?>">

            <div class="form-group col-lg-12 col-md-12">
              <label>Name</label>
              <input type="text" name="name" class="form-control" value="<?php echo $accommodation->name; ?>">
            </div>

            <div class="form-group col-lg-6">
              <label>Accommodations Type</label>
              <select class="form-control " name="accommodation_types_id" id="typeUpdate" style="width: 100%;">
                <!-- <option selected="selected" value="0">Choose Accommodation Type</option> -->
                <?php foreach($accommodation_types as $accommodation_type){?>
                  <option value="<?php echo $accommodation_type->id; ?>"><?php echo $accommodation_type->name; ?></option>
                <?php } ?>
              </select>
            </div>


            <div class="form-group col-lg-6 col-md-6">
              <label>Renting Type
                <button type="button" id="tutorial" class="btn btn-sm btn-warning" style="font-size:80%;height:20px;padding-top:0px;margin-top:-5px;"
                        data-toggle="popover"

                        data-content="What type of accommodation do you have? Do you have a private villa, are you part of a complex? Do you rent your villa with different bedroom rates? Do you own a hotel? ">
                  <i class="fa fa-info" aria-hidden="true"></i> What is this?
                </button>
              </label>
              <select class="form-control " name="renting_type" id="renting_types_id" style="width: 100%;">
                <!-- <option selected="selected" value="0">Choose Accommodation Class</option> -->
                <option value="0" <?php if($accommodation->renting_type == 0){ echo "selected"; } ?>>Exclusive Accommodation / All Rooms</option>
                <option value="2" <?php if($accommodation->renting_type == 2){ echo "selected"; } ?>>Multiple Set Rooms / Below Rooms</option>
              </select>
            </div>


            <div class="form-group col-lg-12 col-md-12">
              <table class="table" style="background:#D2D6DE;">
                <tr>
                  <td class="">
                    <b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;Renting Type&nbsp;|&nbsp;</b>
                    You can list your accommodation with below rooms usage setting or more complex setting by choosing <b>'Flexible'</b>
                  </td>
                </tr>
              </table>
            </div>


            <div class="form-group col-lg-12 col-md-12">
              <label>Description</label>
              <textarea name="description" id="descriptionUpdate" class="form-control" rows="4" placeholder=""><?php echo $accommodation->description; ?></textarea>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Location</label>
              <input type="hidden" name="locations_id" id="locations_id" value="<?php echo $accommodation->locations_id; ?>"></input>
              <input type="text" id="locationAdd" class="form-control" value="<?php echo $accommodation->location; ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Address</label>
              <input type="text" name="address" class="form-control" value="<?php echo $accommodation->address; ?>">
            </div>
            <div class="form-group col-md-6">
              <label>Latitude</label>
              <input type="text" name='lat' id='latitude' class="form-control" value="<?php echo $accommodation->lat; ?>">
            </div>
            <div class="form-group col-md-6">
              <label>Longitude</label>
              <input type="text" name='lng' id='longitude' class="form-control" value="<?php echo $accommodation->lng; ?>">
            </div>
            <div class="form-group col-md-12">
              <div class="panel-body" id="map-canvas" style="height:320px; background-color:gray">
                  <br><br><br><br><br><br><br><br>
                    <center><h4><span class="label label-danger">Sorry, Please Reload the Page</span></h4></center>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="privateRoomForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Room Informations
            <input type="hidden" name="accommodation_rooms_id" value="<?php if($accommodation->renting_type == 0){ echo $accommodation->rooms[0]->id; } ?>">
          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Bedrooms</label>
                    <input type="text" name="total_rooms" class="form-control" value="<?php echo $accommodation->total_rooms; ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Bathrooms</label>
                    <input type="text" name="total_bathrooms" class="form-control" value="<?php echo $accommodation->total_bathrooms; ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Beds</label>
                    <input type="text" name="total_beds" class="form-control" value="<?php echo $accommodation->total_bathrooms; ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Guest </label>
                    <input type="text" name="max_guest_normal" class="form-control" value="<?php if($accommodation->renting_type == 0){ echo $accommodation->rooms[0]->max_guest_normal; } ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Extra Guest </label>
                    <input type="text" name="max_guest_extra" class="form-control" value="<?php if($accommodation->renting_type == 0){ echo $accommodation->rooms[0]->max_guest_extra; } ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Minimum Stay</label>
                    <input type="text" name="min_stay" class="form-control" value="<?php if($accommodation->renting_type == 0){ echo $accommodation->rooms[0]->min_stay; } ?>">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Land Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="land_size" class="form-control" value="<?php echo $accommodation->land_size; ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Building Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="building_size" class="form-control" value="<?php echo $accommodation->building_size; ?>">
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="privateRateForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Rate Informations

          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div>
                  <input type="hidden" name="rates_ids" id="rates_ids" >
                </div>
                <button type="button" id="btnAddRate" class="btn btn-warning btn-block" data-toggle="modal" data-target="#modalPrice"><i class="fa fa-plus"></i>
                  &nbsp;Add Rate
                </button>
                <br>
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Rate</th>
                  <th>Min Stay</th>
                  <th>Valid Date</th>
                  <th>Type</th>
                  <th style="width:100px;">Action</th >
                </tr>
                </thead>

                <tbody id="dataRates">
                    <?php
                      if($accommodation->renting_type == 0){
                    ?>
                    <?php foreach($accommodation->rooms[0]->rates as $rate){ ?>
                      <tr id="rowRate<?php echo $rate->id; ?>">
                        <td id="columnRateName<?php echo $rate->id; ?>"><?php echo $rate->name; ?></td>
                        <td id="columnRateRate<?php echo $rate->id; ?>"><?php echo $rate->rate_mature; ?></td>
                        <td id="columnRateMinStay<?php echo $rate->id; ?>"><?php echo $rate->min_stay; ?></td>
                        <td id="columnRateValidDate<?php echo $rate->id; ?>"><?php echo $rate->date_start." - ".$rate->date_end; ?></td>
                        <td id="columnRateType<?php echo $rate->id; ?>"><?php if($rate->type == 0){echo "Base Rate";}elseif($rate->type == 1){echo "High Season";}elseif($rate->type == 2){echo "Promo / Low Season";} ?></td>
                        <td  style="width:100px;">
                          <button type="button" class="btn btn-default btn-xs btn-block"  id="btnRateUpdate" value="<?php echo $rate->id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                          <button type="button" class="btn btn-danger btn-xs btn-block"  id="btnRateDelete" value="<?php echo $rate->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </td>
                      </tr>
                    <?php
                          }
                      }
                    ?>
                </tbody>
              </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="flexibleRoomForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Room Informations

          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Rooms</label>
                    <input type="text" name="flexible_total_rooms" class="form-control" value="<?php if($accommodation->renting_type == 2){echo $accommodation->total_rooms;} ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Bathrooms</label>
                    <input type="text" name="flexible_total_bathrooms" class="form-control" value="<?php if($accommodation->renting_type == 2){echo $accommodation->total_bathrooms;} ?>">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Total Beds</label>
                    <input type="text" name="flexible_total_beds" class="form-control" value="<?php if($accommodation->renting_type == 2){echo $accommodation->total_beds;} ?>">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Land Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="flexible_land_size" class="form-control" value="<?php echo $accommodation->land_size; ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Building Size</label>
                    <div class="input-group">
                      <span class="input-group-addon">m<sup>2</sup></span>
                      <input type="text" name="flexible_building_size" class="form-control" value="<?php echo $accommodation->building_size; ?>">
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12" id="flexibleRoomRateForm">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Room Settings and Rate Informations&nbsp;&nbsp;
            <span style="font-size:80%; color:red;">You can lease your accommodation with different amount of bedrooms and diiferent rates by add Room Setting multiple times</span>
          </h3>
          <div class="pull-right box-tools">

          </div>
        </div>
        <div class="box-footer text-black">

          <div class="row">
            <div class="col-md-12">
              <div class="form-group ">
                <div id="roomsids">
                  <input type="hidden" name="accommodation_rooms_ids" id="accommodation_rooms_ids" >
                </div>
                <button type="button" id="btnAddRoomInfo" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalRoom"><i class="fa fa-plus"></i>
                  &nbsp;Add Room Setting
                </button>
                <br>
                <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Bedrooms</th>
                  <th>Beds</th>
                  <th>Bathooms</th>
                  <th>Min Stay</th>
                  <th>Guest</th>
                  <th>Extra Guest</th>
                  <th>Renting Type</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody id="dataRooms">
                  <?php if($accommodation->renting_type == 2){ ?>
                    <?php foreach($accommodation->rooms as $room){ ?>
                      <tr id="rowRoom<?php echo $room->id; ?>">
                        <td id="columnNameRoom<?php echo $room->id; ?>"><?php echo $room->name; ?></td>
                        <td id="columnRoomsRoom<?php echo $room->id; ?>"><?php echo $room->total_rooms; ?></td>
                        <td id="columnBedsRoom<?php echo $room->id; ?>"><?php echo $room->total_beds; ?></td>
                        <td id="columnBathroomsRoom<?php echo $room->id; ?>"><?php echo $room->total_bathrooms; ?></td>
                        <td id="columnMinStayRoom<?php echo $room->id; ?>"><?php echo $room->min_stay; ?></td>
                        <td id="columnGuestRoom<?php echo $room->id; ?>"><?php echo $room->max_guest_normal; ?></td>
                        <td id="columnExtraGuestRoom<?php echo $room->id; ?>"><?php echo $room->max_guest_extra; ?></td>
                        <td id="columnRentingTypeRoom<?php echo $room->id; ?>"><?php if($room->renting_type == 0){echo "Private Accommodation";}else if($room->renting_type == 1){echo"Private Room";} ?></td>
                        <td width="100px">
                          <button class="btn btn-warning btn-xs btn-block"  id="btnRoomRates" value="<?php echo $room->id ?>"><i class="fa fa-usd" aria-hidden="true"></i> Rates</button>
                          <button class="btn btn-default btn-xs btn-block"  id="btnRoomUpdate" value="<?php echo $room->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>
                          <button class="btn btn-danger btn-xs btn-block"  id="btnRoomDelete" value="<?php echo $room->id ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } ?>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Extras</h3>
        </div>
          <div class="box-body">
            <?php foreach($additional_rates as $additional_rate){ ?>
              <div class="col-md-12">
                <table class="table table-bordered">
                  <tr>
                  <style type="text/css">
                    td label {
                       display: block;
                       text-align: left;
                    }
                  </style>
                    <td style="">
                      <div class="form-group col-lg-3" >
                        <label style="margin-top:5px;">
                          <input name="additional_rates[]" <?php foreach($had_additional_rates as $had_additional_rate){ if($had_additional_rate->additional_rates_id == $additional_rate->id){ echo "checked"; } } ?> value="<?php echo $additional_rate->id; ?>" type="checkbox"></input>&nbsp;&nbsp;&nbsp; <?php echo $additional_rate->name; ?>
                        </label>
                      </div>
                      <div class="form-group col-lg-2" >
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-usd"></i> USD
                          </div>
                        <input type="text" name="additional_rates_rate_<?php echo $additional_rate->id; ?>" id="additional_rates_rate_<?php echo $additional_rate->id; ?>" class="form-control" value="<?php foreach($had_additional_rates as $had_additional_rate){ if($had_additional_rate->additional_rates_id == $additional_rate->id){ echo $had_additional_rate->rate; } } ?>">
                        </div>
                      </div>

                      <div class="form-group col-lg-3">

                        <select class="form-control " name="metric_unit_<?php echo $additional_rate->id; ?>" id="metric_unit_<?php echo $additional_rate->id; ?>" style="width: 100%;">
                          <!-- <option selected="selected" value="0">Choose Accommodation Class</option> -->
                          <?php foreach($had_additional_rates as $had_additional_rate){ if($had_additional_rate->additional_rates_id == $additional_rate->id){ $metun = $had_additional_rate->metric_unit; } } ?>
                          <option <?php if(isset($metun)){ if($metun == "person"){ echo "selected"; } }  ?> value="person" >Per Person</option>
                          <option <?php if(isset($metun)){ if($metun == "day"){ echo "selected"; } }  ?> value="day" >Per Day</option>
                          <option <?php if(isset($metun)){ if($metun == "hour"){ echo "selected"; } }  ?> value="hour">Per Hour</option>
                          <option <?php if(isset($metun)){ if($metun == "person day"){ echo "selected"; } }  ?> value="person day">Per Person Per Day</option>
                          <option <?php if(isset($metun)){ if($metun == "group"){ echo "selected"; } }  ?> value="group">Per Group</option>
                          <option <?php if(isset($metun)){ if($metun == "week"){ echo "selected"; } }  ?> value="week">Per Week</option>
                          <option <?php if(isset($metun)){ if($metun == "month"){ echo "selected"; } }  ?> value="month">Per Month</option>

                        </select>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Facilities</h3>
        </div>
          <div class="box-body">
            <?php foreach($facilities as $facility){ ?>
              <div class="col-md-3">
                <table class="table table-bordered">
                  <tr>
                  <style type="text/css">
                    td label {
                       display: block;
                       text-align: left;
                    }
                  </style>
                    <td style="">
                      <label>
                        <input name="facilities[]" <?php foreach($had_facilities as $had_facility){ if($had_facility->facilities_id == $facility->id){ echo "checked"; } }?> value="<?php echo $facility->id; ?>" type="checkbox">   &nbsp;&nbsp;&nbsp; <?php echo $facility->name; ?></input>
                      </label>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Amenities</h3>
        </div>
          <div class="box-body">
            <?php foreach($amenitites as $facility){ ?>
              <div class="col-md-3">
                <table class="table table-bordered">
                  <tr>
                  <style type="text/css">
                    td label {
                       display: block;
                       text-align: left;
                    }
                  </style>
                    <td style="">
                      <label>
                        <input name="facilities[]" <?php foreach($had_facilities as $had_facility){ if($had_facility->facilities_id == $facility->id){ echo "checked"; } }?> value="<?php echo $facility->id; ?>" type="checkbox">   &nbsp;&nbsp;&nbsp; <?php echo $facility->name; ?></input>
                      </label>
                    </td>
                  </tr>
                </table>
              </div>
            <?php } ?>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Chanel Information</h3>
          <button type="button" id="infoChanel" class="btn btn-sm btn-warning" style="font-size:80%;height:20px;padding-top:0px;margin-top:-5px;"
                  data-toggle="infoChanel"
                  data-content="We would like to get some listing information from you, so that we can help provide our guests with any specials or discounts that you are running.
Please help with providing your listings for us to use, and we will follow the same terms as per the OTA. Please be informed that agreements for commissions are done separately. ">
            <i class="fa fa-info" aria-hidden="true"></i> What is this?
          </button>

        </div>
          <div class="box-body">
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>ICS or ICal Link (Live Availability Links)
                <button type="button" id="infoIcs" class="btn btn-sm btn-warning" style="font-size:80%;height:20px;padding-top:0px;margin-top:-5px;"
                        data-toggle="infoIcs"
                        data-content="Our systems require your villas to be linked live to our systems, and we use standard ICS or ICal links to do this. If you own an OTA or channel manager, they should be able to provide you with information. If they don't we can help you with this, just let us know.">
                  <i class="fa fa-info" aria-hidden="true"></i> What is this?
                </button>
              </label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="ics_url" class="form-control" value="<?php echo $accommodation->ics_url; ?>">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Accommodation Site</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="website" class="form-control" value="<?php echo $accommodation->website;  ?>">
              </div>
            </div>

            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Booking.com</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_booking_com" class="form-control" value="<?php echo $accommodation->chanel_booking_com;  ?>">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Airbnb.com</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_airbnb_com" class="form-control" value="<?php echo $accommodation->chanel_airbnb_com;  ?>">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Flipkey.com</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_flipkey_com" class="form-control" value="<?php echo $accommodation->chanel_flipkey_com;  ?>">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Other Chanel</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="chanel_other" class="form-control"  value="<?php echo $accommodation->chanel_other;  ?>">
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Social Media Information</h3>
        </div>
          <div class="box-body">
            <div class="form-group col-lg-12 col-md-12">
              <label>Youtube</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="youtube" class="form-control" value="<?php echo $accommodation->youtube;  ?>">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Facebook</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="facebook" class="form-control" value="<?php echo $accommodation->facebook;  ?>">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12 col-md-12">
              <label>Instagram</label>
              <div class="input-group">
                <span class="input-group-addon">http://</span>
                <input type="text" name="instagram" class="form-control" value="<?php echo $accommodation->instagram;  ?>">
              </div>
            </div>

          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Taxes Information</h3>
        </div>
          <div class="box-body">
            <div class="form-group col-lg-6 col-md-6">
              <label>Tax</label>

              <table class="table table-bordered">
                <tr>
                <style type="text/css">
                  td label {
                     display: block;
                     text-align: left;
                  }
                </style>
                  <td width="50%">
                    <label>
                      <input type="radio" name="include_tax" value="1" class="" <?php if($accommodation->include_tax == 1){ echo "checked"; } ?>>&nbsp;&nbsp;&nbsp; Included Tax</input>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input type="radio" name="include_tax" value="0" class="" <?php if($accommodation->include_tax == 0){ echo "checked"; } ?>>&nbsp;&nbsp;&nbsp; Not Included</input>
                    </label>
                  </td>
                </tr>
              </table>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Tax Percentage</label>
              <div class="input-group">
                <span class="input-group-addon">%</span>
                <input type="text" name="tax_percentage" class="form-control" value="<?php echo $accommodation->tax_percentage;  ?>">
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Accommodation Photos<!-- Button trigger modal -->
          </h3>
          <div class="pull-right box-tools">

            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                  <div class="file-loading">
                      <input id="file-1" name="userfile[]" type="file" multiple class="file" data-overwrite-initial="false">
                  </div>
              </div>
            </div>
            <!-- <div class="col-md-12">
              <button id="btnUpload" class=" btn btn-block btn-primary btn" style="">
                Upload
              </button>
            </div> -->
            <div class="col-md-12">
              <div class="col-md-12 form-group well">
                <input type="hidden" id="photoIds" name="photoIds">
                <td id="uploadedPhotos">
                  <?php $photo_ids_old="";  ?>
                  <?php foreach($accommodation->photos as $photo){ ?>
                    <?php if($photo_ids_old==""){$photo_ids_old = $photo->id;}else{$photo_ids_old = $photo_ids_old.",".$photo->id;}?>
                    <div class="col-lg-6 col-md-6 col-md-6" id="photo<?php echo $photo->id; ?>">
                      <table class="table table-bordered" style="background-color:white;">
                        <tr>
                          <td class="text-center" colspan="2" rowspan="1" >
                            <img src="<?php echo base_url(); ?>uploads/photos/accommodations/thumbnails/<?php echo $photo->name; ?>" class="img-fluid img-thumbnail" alt="..." style="height:200px;width:auto;">
                          </td>
                          <td class="text-center" colspan="2" rowspan="2" >
                            <table class="table table-bordered" style="margin-bottom:0px;">
                              <tr>
                                <td>
                                  Ordinal
                                </td>
                                <td>
                                  <input type="text" name="photo_ordinal[<?php echo $photo->id; ?>]" class="form-control" value="<?php echo $photo->ordinal; ?>">
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  Title
                                </td>
                                <td>
                                  <input type="text" name="photo_title[<?php echo $photo->id; ?>]" class="form-control" value="<?php echo $photo->title; ?>">
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  Type
                                </td>
                                <td>
                                  <select class="form-control" name="photo_type[<?php echo $photo->id; ?>]" style="width: 100%;">
                                    <option selected="selected" value="">-</option>
                                  </select>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>

                          <td>
                            <button type="button" id="btnPhotoDelete" value="<?php echo $photo->id; ?>" class="btn btn-danger btn-xs col-md-12"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button>
                          </td>

                        </tr>
                      </table>
                    </div>
                  <?php } ?>
                  <input type="hidden" name="photo_ids_old" value="<?php echo $photo_ids_old; ?>">
                </td>





              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-xs-12">
      <button type="submit" id="btnConfirmUpdate" class=" btn btn-lg btn-success btn-block" style="">
        Update
      </button>
    </div>







  <!-- <br>
  <br>
  <br>
  <button type="submit" id="btnConfirmSave" class="hidden-xs hidden-sm navbar-fixed-bottom btn btn-lg btn-success" style="margin-left:250px; padding-left:500px;padding-right:500px;margin-bottom:10px;">
    Submit
  </button> -->

  <a id="smBtnConfirmSave"  class="hidden-lg hidden-md navbar-fixed-bottom btn btn-lg btn-success btn-block" ><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp Submit</a>
  </form>


  <!-- Modal Prices -->
  <div class="modal fade" id="modalPrices" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">Rates
            <button type="button" class="btn btn-primary btn-xs" id="btnAddPriceModal" data-toggle="modal" data-target="#modalPrice"><i class="fa fa-plus"></i>
              &nbsp;Add Rate
            </button>
          </h4>
        </div>

        <div class="modal-body row">
          <input type="hidden" id="accommodation_rooms_id_for_prices">
          <div class="col-md-12">
            <div class="form-group">
              <table id="example2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Rate</th>
                <th>Date Start</th>
                <th>Date End</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody id="flexibleDataRates">

              </tbody>
            </table>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modalRoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel"><span class="bg-warning" style="font-size:100%;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> You can add the rates after this</span></h4>
        </div>

        <div class="modal-body row">
          <form enctype="multipart/form-data" action="" method="POST" id="formRoom" role="form">

          <div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Name&nbsp;&nbsp;<span class="label" style="font-size:80%; color:grey;">Explain your rooms like <u>4 Bedrooms Usage</u> or <u>3 Deluxe Bedrooms and 1 Normal Bedroom</u></span></label>
                <input type="text" class="form-control" name="name">
                <input type="hidden" class="form-control" name="id">
                <input type="hidden" class="form-control" name="old_code">
                <input type="hidden" name="unique_id" id="unique_id" class="form-control" value="<?php echo $accommodation->unique_id;; ?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Rooms Usage</label>
                <input type="text" name="total_rooms" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Bathrooms Usage</label>
                <input type="text" name="total_bathrooms" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Beds Usage</label>
                <input type="text" name="total_beds" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Guest </label>
                <input type="text" name="max_guest_normal" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Extra Guest </label>
                <input type="text" name="max_guest_extra" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Minimum Stay</label>
                <input type="text" name="min_stay" class="form-control">
              </div>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Renting Type</label>
              <table class="table table-bordered">
                <tr>
                <style type="text/css">
                  td label {
                     display: block;
                     text-align: left;
                  }
                </style>
                  <td width="50%">
                    <label>
                      <input type="radio" value="0" name="renting_type" class="" checked>&nbsp;&nbsp;&nbsp; Private Accommodation</input>
                    </label>
                  </td>
                  <td>
                    <label>
                      <input type="radio" value="1" name="renting_type" class="">&nbsp;&nbsp;&nbsp; Shared Accommodation</input>
                    </label>
                  </td>
                </tr>
              </table>
              <table class="table" style="background:#D2D6DE;">
                <tr>
                  <td class="">
                    <b><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;Renting Type&nbsp;|&nbsp;</b>
                    When <b>Private Accommodation</b> booked, all of the <b>Private Rooms</b> will be Unavailable. When one of <b>Private Rooms</b> booked, the others <b>Private Rooms</b> will be Available and all of the <b>Private Accommodations</b> will be Unavailable.
                  </td>
                </tr>
              </table>
            </div>

            <div class="col-md-12">
              <div class="form-group">
                <label>ICS or ICal Link (Live Availability Links) <span style="font-size:80%; color:red;">For Private Room</span></label>
                <input type="text" name="ics_url" id="ics_url" class="form-control">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" rows="4" placeholder=""></textarea>
              </div>
            </div>
            <!-- Nav tabs -->


          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnAddRoom" class="btn btn-success">Add</button>
          <button type="button" id="btnUpdateRoom" class="btn btn-primary">Update</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Price -->
  <div class="modal fade" id="modalPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modalAccommodationViewLabel">Add Rate
          </h4>
        </div>

        <div class="modal-body row">
          <form enctype="multipart/form-data" action="" method="POST" id="formRate" role="form">

          <div class="form-group col-md-12">

            <label>Type</label>
            <select class="form-control" name="type" id="rate_update_type" style="width: 100%;">
              <option selected="selected" value="0">Base Rate (Default setting for all dates)</option>
              <option value="1">Seasonal (High / Peak Season)</option>
              <option value="2">Promo (Cheaper than Base Rate)</option>
            </select>
          </div>
          <div class="form-group col-md-12">
            <label>Name <span class="label label-warning">"Base Rate" doesn't need name</span></label>
            <input type="hidden" name="id" id="rate_update_id" ></input>
            <input type="hidden" name="accommodation_rooms_id" id="rate_update_accommodations_id" <?php if($accommodation->renting_type == 0){echo 'value="'.$accommodation->rooms[0]->id.'"';}  ?>></input>
            <input type="hidden" name="created_on" id="rate_update_created_on" ></input>
            <input type="hidden" name="unique_id" id="unique_id" class="form-control" value="<?php echo $accommodation->unique_id; ?>">
            <input type="text" name="name" id="rate_update_name" class="form-control" >
          </div>

          <div class="form-group col-md-8">
            <label>Rate</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-usd"></i> USD
              </div>
            <input type="text" name="rate" id="rate_update_rate" class="form-control" >
            </div>
          </div>

          <div class="form-group col-md-4">
            <label>Minimum Stay</label>
            <div class="input-group">
              <div class="input-group-addon">
                Night
              </div>
            <input type="text" name="min_stay" id="rate_update_min_stay" class="form-control" >
            </div>
          </div>
          <div class="form-group col-md-6">
            <label>Date Valid Start</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date_start" class="form-control pull-right" id="datepicker1">
              </div>
          </div>
          <div class="form-group col-md-6">
            <label>Date Valid End</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date_end" class="form-control pull-right" id="datepicker2">
              </div>
          </div>
          <div class="form-group col-md-12">
            <label>Description</label>
            <textarea name="description" class="form-control" rows="4" placeholder="" id="rate_update_description"></textarea>
          </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnAddPrice" class="btn btn-success btn-block">Add</button>
          <button type="button" id="btnUpdatePrice" class="btn btn-primary btn-block">Update</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Delete -->
  <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="hidden" class="form-control" name="idDelete" id="idDelete">
            <input type="hidden" name="unique_id" class="form-control" value="<?php echo $accommodation->unique_id; ?>">
          </div>
          <div class="row">
            <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
            <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
            <div class="col-md-6"></div>
          </div>
        </div>
      </div>
    </div>
  </div>






      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
