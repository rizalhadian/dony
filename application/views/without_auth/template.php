<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Total Bali-Livebalivillas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.min.css"> -->
  <!-- jQuery 3.1.1 -->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/iCheck/all.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/css/skins/_all-skins.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>
  <?php if($this->session->userdata('ses') == "accommodations_addnew"){ ?>
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>

  <?php } ?>


</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">



<!-- Content -->
<?php include('content.php'); ?>

<!-- Footer -->
<?php
  // include('footer.php');
?>
<div>


<script>
  var base_url = "<?php echo base_url();?>";
</script>


<!-- Kraje-File-Input -->
<link href="<?php echo base_url();?>assets/kraje-file-input/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  <link href="<?php echo base_url();?>assets/kraje-file-input/themes/explorer-fa/theme.css" media="all" rel="stylesheet" type="text/css"/>
  <script src="<?php echo base_url();?>assets/kraje-file-input/js/plugins/sortable.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input/js/fileinput.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input/js/locales/fr.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input/js/locales/es.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input/themes/explorer-fa/theme.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/kraje-file-input/themes/fa/theme.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->


<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>



<!-- Bootstrap 3.3.6 -->

<script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/demo.js"></script>

<script>
$("#tutorial").on('click', function(){
      $('[data-toggle="popover"]').popover('show');
});
$("#infoIcs").on('click', function(){
      $('[data-toggle="infoIcs"]').popover('show');
});
$("#infoChanel").on('click', function(){
      $('[data-toggle="infoChanel"]').popover('show');
});
</script>
<!-- page script -->

<script>
  $("[data-toggle=popover]").popover();

  $(function () {

    $("#example1").DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });


    $('#example3').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });


    $('#example4').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

  });

</script>


<?php if($this->session->userdata('ses') == "api_accommodations_add" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-addnew.js"></script>

  <!-- datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <!-- iCheck 1.0.1 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/iCheck/icheck.min.js"></script>
  <script>


  $(function(){

    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
  });
  </script>
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>


<?php } ?>

<?php if($this->session->userdata('ses') == "api_accommodations_update" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-update.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>

<?php } ?>





</body>

</html>
