<br><br><br><br><br>
<div class="container">
<div class="row">
  <div class="col-md-3">
  <center>
   
    <div class="box" style="">
      <div class="box-header with-border">
        <h3 class="box-title">Penjual</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->

      <div class="box-body">
      <center><img src="<?php echo ($user->photo == null ) ? base_url().'assets/images/no-image.png' : $user->photo ; ?>" style="width:220px;" alt="..." class="img-thumbnail rounded mx-auto d-block"></center>
        <center><a href=""><h3><b><?php echo $user->username; ?></b></h3></a></center>
        <center><h6 style="margin-top:-10px;">Since <?php echo $user->created_on; ?></h6></center>
        
        <table class="table table-bordered">
          <tr>
            <td style="width:30px;"><i class="fa fa-map-marker"></i></td>
            <td style="width:20px;">:</td>
            <td><?php echo $user->location; ?></td>
          </tr>
          <tr>
            <td style="width:30px;"><i class="fa fa-phone"></i></td>
            <td style="width:20px;">:</td>
            <td><?php echo $user->phone; ?></td>
            
          </tr>
        </table>
      </div>
        <!-- /.box-body -->

    </div>
  </center>
  <a class="btn btn-block btn-primary" href="<?php echo base_url(); ?>u/<?php echo $user->username; ?>"><h4><b>Semua Produk Penjual</b></h4></a>

  </div>
  
  <div class="col-md-9" style="padding-left:0%; padding-right:0%;" style="height:500px;">
          <!-- DIRECT CHAT PRIMARY -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Pesan Kepada Penjual </h3>

              <div class="box-tools pull-right">
                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages">
                <!-- Message. Default to the left -->
                
                <!-- /.direct-chat-msg -->

                <!-- Message to the right -->

                

                <?php if(isset($messages)){ ?>
                  <?php foreach($messages as $message){?>
                  <?php if($message->pengirimid == $this->session->userdata('user')->id){ ?>
                    <div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right"><?php echo $message->username; ?></span>
                        <span class="direct-chat-timestamp pull-left"><?php echo $message->created_on; ?></span>
                      </div>
                      <img class="direct-chat-img" style="" src="<?php echo $message->photo; ?>" alt="Message User Image">
                      <div class="direct-chat-text" style="">
                        <?php echo $message->message; ?>
                      </div>
                    </div>
                  <?php }else{ ?>
                    <div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left"><?php echo $message->username; ?></span>
                        <span class="direct-chat-timestamp pull-right"><?php echo $message->created_on; ?></span>
                      </div>
                      <img class="direct-chat-img" src="<?php echo $message->photo; ?>" alt="Message User Image">
                      <div class="direct-chat-text">
                        <?php echo $message->message; ?>
                      </div>
                    </div>
                  <?php } ?>

                <?php } ?>
                <?php } ?>
                

                

                
                
                <!-- /.direct-chat-msg -->
              </div>
              <!--/.direct-chat-messages-->

              <!-- Contacts are loaded here -->
              
            <!-- /.box-body -->
            <div class="box-footer">
              <form action="<?php echo base_url()."send_message" ?>" method="post">
                <div class="input-group">
                  <input type="hidden" name="username" value="<?php echo $username; ?>">
                  <input type="hidden" name="roomid" value="<?php echo $room_id; ?>">
                  <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary btn-flat">Send</button>
                      </span>
                </div>
              </form>
            </div>
            <!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div>
</div>
</div>
