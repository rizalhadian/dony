<div class="content-wrapper" style="background:#ECF0F5">

  <?php

    if($this->session->userdata('ses') == "undercons"){
      include('undercons.php');
    }elseif($this->session->userdata('ses') == "home"){
      include('home.php');
    }elseif($this->session->userdata('ses') == "shops"){
      include('shop.php');
    }elseif($this->session->userdata('ses') == "shop_read"){
      include('shop_read.php');
    }elseif($this->session->userdata('ses') == "shops_notfound"){
      include('shop_notfound.php');
    }elseif($this->session->userdata('ses') == "shops_createnew"){
      include('shop_create.php');
    }elseif($this->session->userdata('ses') == "produk_createnew"){
      include('produk_create.php');
    }elseif($this->session->userdata('ses') == "foto_produk_upload"){
      include('foto_produk_upload.php');
    }elseif($this->session->userdata('ses') == "produk_view"){
      include('produk_view.php');
    }elseif($this->session->userdata('ses') == "produk_edit"){
      include('produk_edit.php');
    }elseif($this->session->userdata('ses') == "profile_preview"){
      include('profile_preview.php');
    }elseif($this->session->userdata('ses') == "profile_preview_other"){
      include('profile_preview.php');
    }elseif($this->session->userdata('ses') == "profile_edit"){
      include('profile_edit.php');
    }elseif($this->session->userdata('ses') == "cart"){
      include('cart.php');
    }elseif($this->session->userdata('ses') == "blogs"){
      include('blogs.php');
    }elseif($this->session->userdata('ses') == "blog"){
      include('blog.php');
    }elseif($this->session->userdata('ses') == "search"){
      include('search.php');
    }elseif($this->session->userdata('ses') == "invoice"){
      include('invoice.php');
    }elseif($this->session->userdata('ses') == "invoices"){
      include('invoices.php');
    }elseif($this->session->userdata('ses') == "change_password"){
      include('change_password.php');
    }elseif($this->session->userdata('ses') == "orders"){
      include('orders.php');
    }elseif($this->session->userdata('ses') == "order"){
      include('order.php');
    }elseif($this->session->userdata('ses') == "message"){
      include('message.php');
    }elseif($this->session->userdata('ses') == "konsultasi"){
      include('konsultasi.php');
    }elseif($this->session->userdata('ses') == "konsultasi_add"){
      include('konsultasi_add.php');
    }elseif($this->session->userdata('ses') == "konsultasi_admin"){
      include('konsultasi_admin.php');
    }elseif($this->session->userdata('ses') == "konsultasi_answer"){
      include('konsultasi_answer.php');
    }elseif($this->session->userdata('ses') == "konsultasi_read"){
      include('konsultasi_read.php');
    }
    



  ?>

</div>
