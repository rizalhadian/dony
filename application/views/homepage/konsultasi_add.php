<div class="container">
<section class="content-header">

  <h1>
    Konsultasi
  </h1>
  
  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    
    
    

  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">
      <div class="box box-solid bg-grey-gradient">
        
        <!-- /.box-header -->
        <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>konsultasi/create" method="POST">

        <!-- /.box-body -->
        <div class="box-footer text-black">
          <input type="text" name="judul" class="form-control" placeholder="Masukkan judul disini">
          <br>
          
    
      <textarea id='edit' name="deskripsi" style="margin-top: 30px;" placeholder="Masukkan konsultasi disini"></textarea>
      
      <br>
      <button class="btn btn-block btn-primary" type="submit" placeholder="">Submit</button>
    
  </div>
  </form>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/froala_editor.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/video.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/froala_editor/js/plugins/entities.min.js"></script>

  <script>
      $(function(){
        $('#edit')
          .on('froalaEditor.initialized', function (e, editor) {
           
            $('#edit').parents('form').on('submit', function () {
              console.log($('#edit').val());
              // return false;
              
            })
          })
          .froalaEditor({
            heightMin: 400,
            heightMax: 400,
            enter: $.FroalaEditor.ENTER_P, 
            placeholderText: null,
            // Set the image upload parameter.
            imageUploadParam: 'image_param',
    
            // Set the image upload URL.
            imageUploadURL: '/dony/upload-single',

            // Set request type.
            imageUploadMethod: 'POST',
            imageAllowedTypes: ['jpeg', 'jpg', 'png']
          }).on('froalaEditor.image.beforeUpload', function (e, editor, images) {
        // Return false if you want to stop the image upload.
      })
      .on('froalaEditor.image.uploaded', function (e, editor, response) {
        // Image was uploaded to the server.
        console.log(response);

      })
      .on('froalaEditor.image.inserted', function (e, editor, $img, response) {
        // Image was inserted in the editor.
      })
      .on('froalaEditor.image.replaced', function (e, editor, $img, response) {
        // Image was replaced in the editor.
      }).on('froalaEditor.image.error', function (e, editor, error, response) {
        // Bad link.
        console.log(error);
        if (error.code == 1) {  }
 
        // No link in upload response.
        else if (error.code == 2) { }
 
        // Error during image upload.
        else if (error.code == 3) {  }
 
        // Parsing response failed.
        else if (error.code == 4) {  }
 
        // Image too text-large.
        else if (error.code == 5) {  }
 
        // Invalid image type.
        else if (error.code == 6) {  }
 
        // Image can be uploaded only to same domain in IE 8 and IE 9.
        else if (error.code == 7) {  }
 
        // Response contains the original server response to the request if available.
      });

          $('.selector').on('editable.afterFileUpload', function (e, editor, response) {
// Do something here.
            console.log('asdasd');
  });
      });
  </script>
  
        </div>
      </div>
    </div>
  </div>
  


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>
</div>


