<section class="content-header">


  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    

    <div class="produk col-lg-12 col-md-12" >
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="width:100%;">

        
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>

               

                <div class="carousel-inner">
                    <?php  for($i=0; $i<3; $i++){?>
                        <!-- <div class="item active"> -->
                        
                        <?php if($i==0){ ?>
                            <div class="item active">
                        <?php }else{?>
                            <div class="item">
                        <?php } ?>

                        <a href="<?php echo base_url(); ?>blog/read/<?php echo $blogs[$i]->id; ?>"><center><img src="<?php echo $blogs[$i]->img; ?>" alt="First slide" style=" height:600px;"></a>

                    <div class="carousel-caption" style="text-align:left;">
                      <h1><?php echo $blogs[$i]->judul; ?></h1>
                      
                    </div>
                  </div>
                    <?php } ?>
                  
                  
                  
                  
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>

    </div>


  </div>
  <!-- /.row -->

  <br>
  <br>
  <?php foreach($blogs as $blog){ ?>
            
            <div class="col-md-3 ">
                <div class="box" style="width:100%">
                    <div class="panel-heading" style="height:200px;">
                      <center>
                      <a href="<?php echo base_url(); ?>blog/read/<?php echo $blog->id; ?>"><img src="<?php echo $blog->img;; ?>" alt="Responsive image" style="width:100%"></a>
                        
                          


                      </center>
                    </div>
                    <div class="box-body">
                    <li class="list-group-item" style="background-color: rgba(0, 0, 0, 0.3);">
                         <a style="text-decoration: none; color: white"><b><?php echo $blog->judul; ?></a>
                       </li>
                     
                    </div>
                </div>
            </div>
            </a>
          <?php } ?>

  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
