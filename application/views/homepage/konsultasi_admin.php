<div class="container">
    <br>
    <?php foreach($konsultasi_posts as $konpost){ ?>
    <div class="alert" role="alert" style="background:white; margin-bottom:20px;">
    
        <?php if($konpost->is_answered == 1){ ?>
        <p style="font-size:20px; background:#ccfcd2; padding:10px;"><b><u><?php echo $konpost->judul; ?></u></b> <span style="font-size:14px;">from <a style="color:blue;"><?php echo $konpost->username; ?></a></span><br><span style="font-size:12px;">Posted at <?php echo date("d-M-Y", strtotime($konpost->created_on)); ?></span></p>    
        <?php }else{ ?>
        <p style="font-size:20px; background:#EBEFF5; padding:10px;"><b><u><?php echo $konpost->judul; ?></u></b> <span style="font-size:14px;">from <a style="color:blue;"><?php echo $konpost->username; ?></a></span><br><span style="font-size:12px;">Posted at <?php echo date("d-M-Y", strtotime($konpost->created_on)); ?></span></p>
        <?php } ?>
        <?php echo $konpost->deskripsi; ?>
        <br>
        <br>
        <?php if($konpost->is_answered == 1){ ?>
        <div style="border-style: solid; border-width:1px; padding:10px;">
        <p><b>Answer : </b></p>
        <p><?php echo $konpost->jawaban; ?></p>
        </div>
        <br>
        <br>
        <?php } ?>
        <div>

        <?php if($konpost->is_answered == 1){ ?>
        <a href="<?php echo base_url().'konsultasi/answer/'.$konpost->id; ?>" class="btn btn-primary pull-right" style="text-decoration: none; margin-top:-25px; margin-left:10px;">Edit Answer</a>
        <?php }else{ ?>
        <a href="<?php echo base_url().'konsultasi/answer/'.$konpost->id; ?>" class="btn btn-primary pull-right" style="text-decoration: none; margin-top:-25px; margin-left:10px;">Answer</a>
        <?php } ?>        
        </div>
    </div>
    <?php } ?>
    

</div>