<section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="col-lg-12 col-md-12">
        <form class="form-login" action="<?php echo base_url(); ?>cart/checkout" method="post">
          <input type="hidden" name="produkid" id="produk_id" value="">
          <input type="hidden" name="urlasal" id="produk_id" value="">

          <div class="box" style="width:1000px;">
            <div class="box-header with-border" >
              <h3 class="box-title">Order</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
              <div class="col-md-5">
                <table class="table table-bordered">
                  <tr>
                    <th scope="col">Produk</th>
                    <th scope="col">Qty</th>
                  </tr>
                  <?php foreach($produks as $p){ ?>
                  <tr>
                    <td><?php echo $p->nama; ?></td>
                    <td><?php echo $p->qty; ?></td>
                  </tr>
                  <?php } ?>
                
                </table>
              </div>

              <div class="col-md-7">
                <div>
                  <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Alamat Pengiriman <i class="fa fa-map-marker"></i></h3>
                    </div>
                    
                    <table class="table table-bordered">
                    <tr>
                    <td style="width:130px;">Provinsi</td>
                    <td style="width:20px;">:</td>
                    
                    <td>
                    <select name="provinsi" id="provinsi" class="form-control">
                        <option value="null">Pilih Provinsi</option>
                        <?php foreach($provinces as $province){ ?>
                          <option value="<?php echo $province->province_id; ?>"><?php echo $province->province; ?></option>
                        <?php } ?>
                      </select>
                    </td>
                  </tr>

                  <tr>
                    <td style="width:130px;">Kota</td>
                    <td style="width:20px;">:</td>
                    
                    <td>
                    <select name="kota" id="kota" class="form-control" disabled="disabled">
                      <option value="null">Pilih Provinsi Dahulu</option>
                    </select>
                    </td>
                  </tr>

                  <tr>
                    <td style="width:130px;">Alamat</td>
                    <td style="width:20px;">:</td>
                    
                    <td>
                    <textarea name="alamat" id="alamat" class="form-control" rows="4" placeholder="" disabled><?php echo $produks[0]->alamat; ?></textarea> 
                    </td>
                  </tr>
                    </table>

                    
                  </div>
                </div>
              </div>
            </div>


          </div>
          <!-- /.box-body -->

        </form>

      </div>

    </div>
  </div>







  </div>
</section>
<!-- right col -->


<!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
        <div class="modal-body">
          <input type="text" name="idtoupdate" id="partnerId">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
          <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" classdata-toggle="modal" id="modalDelete" data-target="#exampleModal"="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <!-- <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete"> -->
        </div>
        <div class="row" >
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6" id="buttonsDelete"></div>
          
        </div>
      </div>
    </div>
  </div>
</div>