<div style="height:400px; background-color: white; background-image: url('assets/images/pets2.png'); background-size: auto 400px;  background-repeat: no-repeat; background-position: center center;" >
    <div class="container">
        <div class="row">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            
            <br>
            <form action="<?php echo base_url(); ?>konsultasi" method="get">
            <div class="col-md-11">
                <input type="text" name="search" class="form-control" placeholder="Cari Konsultasi" style="height:50px; font-size:25px; background-color:rgba(235, 239, 245, 0.9);  color:black;">                
            </div>
            <div class="col-md-1">
                <button class="btn btn-block btn-primary" style="height:50px; font-size:25px;"><i class="fa fa-search"></i></button>
            </div>
            </form>
        </div>
        
    </div>
</div>
<div class="container">
    <br>
    <br>
    <div class="row">
        <div class="col-md-4">
            <br>
            <br>
            <a style="margin-bottom:20px;" class="btn btn-block btn-default"><b>Hal yang sering di konsultasikan</b></a>
            <a class="btn btn-block btn-default"><b>Konsultasi Terbaru</b></a>
        </div>
        <div class="col-md-8">
            <p style="font-size:20px; margin-left:15px;"><b>Konsultasi Terbaru</b></p>

            <?php foreach($konsultasi_posts as $konpost){ ?>
                <div class="col-md-12">
                    <div class="alert" role="alert" style="background:white; margin-bottom:20px;">
                        <p style="font-size:16px; background:#EBEFF5; padding:10px;"><b><u><?php echo $konpost->judul; ?></u></b> <span style="font-size:12px;">from <a style="color:blue;"><?php echo $konpost->username; ?></a></span><br><span style="font-size:10px;">Posted at <?php echo date("d-M-Y", strtotime($konpost->created_on)); ?></span></p>
                        <p><?php echo $konpost->deskripsi; ?></p>
                        <br>
                        <a href="<?php echo base_url(); ?>konsultasi/read/<?php echo $konpost->id; ?>" style="text-decoration: none;" class="btn btn-primary">Lihat Jawaban</a>
                    </div>
                </div>
            <?php } ?>

            
            
        </div>
    </div>
</div>
