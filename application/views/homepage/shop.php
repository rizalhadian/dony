<section class="content-header">
<section class="content-header">


  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- Promo and Interesting -->
    <!-- <div class="promo-and-interesting col-lg-12 col-md-12">
      <div class="col-lg-6 col-md-12 col-xs-12">
        <h4 style="padding-left:20px">
          <b>Promo
        </h4>
        <center>
          <div class="small-box" style="height: 200px; width: 600px; background-image: url('<?php  echo base_url(); ?>assets/images/partners.jpg'); color: white; -webkit-background-size: cover;  -moz-background-size: cover; -o-background-size: cover background-size: cover;">
          </div>
        </center>
      </div>
      <div class="col-lg-6 col-md-12 col-xs-12">
        <h4 style="padding-left:20px">
          <b>Interesting
        </h4>
        <center>
          <div class="small-box" style="height: 200px; width: 600px;background-image: url('<?php  echo base_url(); ?>assets/images/activities.jpg');color: white;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
          </div>
        </center>
      </div>
    </div> -->
    <!-- End Promo and Interesting -->

    <div class="produk col-lg-3 col-md-3" >
      <h4 style="padding-left:40px">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Profil Anda</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <div class="box-body">
              <center><img src="<?php echo ($user->photo == null ) ? base_url().'assets/images/no-image.png' : $user->photo ; ?>" style="width:220px;" alt="..." class="img-thumbnail rounded mx-auto d-block"></center>
              <center><h3><b><?php echo $this->session->userdata('user')->username;  ?></b></h3></center>
              
              <table class="table table-bordered">
                <tr>
                  <td style="width:30px;"><i class="fa fa-map-marker"></i></td>
                  <td style="width:20px;">:</td>
                  <td><?php echo $location->city_name.", ".$location->province; ?></td>
                </tr>
                <tr>
                  <td style="width:30px;"><i class="fa fa-phone"></i></td>
                  <td style="width:20px;">:</td>
                  <td><?php echo $user->phone; ?></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
        </div>

        <a href="<?php echo base_url(); ?>produk/addnew" class="btn btn-lg btn-block btn-success"><i class="fa fa-plus-square"></i> Tambah Produk</a>
      </h4>

      
    </div>

    <div class="produk col-lg-9 col-md-9" >
      <h4 style="padding-left:40px">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Produk Yang Anda Jual </h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped" style="font-size: 90%;">
              <thead>
              <tr>
                <th>
                </th>
                <th>Nama Produk</th>
                <th>Jenis</th>
                <th>Harga</th>
                <th>Views</th>
                <th>Opsi Produk</th>
              </tr>
              </thead>

              <tbody id="dataRows">
              <!-- Tes for lots datas -->
                <?php foreach($produks as $produk){ ?>
                  <tr>
                    <td>
                    <center>
                        <img 
                          src="<?php if($produk->photo == ""){  echo base_url()."assets/images/no-image.png"; }else{ echo $produk->photo; } ?>" 
                          class="img-responsive"
                          alt="Responsive image" 
                          style="max-height:120px;"
                        >
                      </center>
                    </td>
                    <td><?php echo $produk->nama; ?></td>
                    <td>Jenis</td>
                    <td><?php echo $produk->harga; ?></td>
                    <td><i class="fa fa-eye"></i> <?php echo $produk->view_count; ?></td>
                    <td>
                      <a href="<?php echo base_url(); ?>produk/edit/<?php echo $produk->id; ?>" class="btn btn-primary btn-sm">Edit</a>
                      <!-- <a href="<?php echo base_url(); ?>produk/delete/<?php echo $produk->id; ?>" class="btn btn-danger btn-sm">Delete</a> -->
                      <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" id="modalDelete" data-target="#exampleModal" value="<?php echo $produk->id; ?>">Hapus Produk</button>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
        </div>
      </h4>
    </div>


  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Apakah Anda Yakin Ingin Menghapus Produk Ini?</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <!-- <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete"> -->
        </div>
        <div class="row" >
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button></div>
          <div class="col-md-6" id="buttonsDelete"></div>
          
        </div>
      </div>
    </div>
  </div>
</div>
