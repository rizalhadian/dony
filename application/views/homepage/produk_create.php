<section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    
    <div class="offset-lg-2 offset-md-2 col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Buat Produk</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

          <div class="box-body">
            <form class="form-login" action="<?php  echo base_url();  ?>produk/create" method="post" onkeypress="return event.keyCode != 13;">
            <div class="form-group col-lg-12 col-md-12">
              <label>Nama</label>
              <input type="text" name="nama" class="form-control">
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Produk Jenis 1</label>
              <select type="text" name="produk_jenis_1" id="produk_jenis_1" class="form-control">
                <option value="0">-</option>
                <?php foreach($produk_jenis_1 as $prodjen1){ ?>
                  <option value="<?php echo $prodjen1->id; ?>"><?php echo $prodjen1->nama; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Produk Jenis 2</label>
              <select type="text" disabled="disabled" name="produk_jenis_2" id="produk_jenis_2" class="form-control">
                <option value="0">-</option>
              </select>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Harga</label>
              <input type="text" name="harga" class="form-control">
            </div>
            <div class="form-group col-lg-3 col-md-3">
              <label>Berat (Kg)</label>
                <input type="text"  name="berat" class="form-control">
            </div>
            <div class="form-group col-lg-3 col-md-3">
              <label>Stock</label>
                <input type="text"  name="stock" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Deskripsi</label>
              <textarea name="deskripsi" id="descriptionUpdate" class="form-control" rows="4" placeholder=""></textarea>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Tags</label><br>
              <input style="width:100%" name="tags" type="text" class="form-control" value="" data-role="tagsinput" />
            </div>

            

            <button type="submit" class="btn btn-success btn-lg btn-block">Langkah Berikutnya</button>
          </form>
          </div>
          <!-- /.box-body -->
      </div>
    </div>




  </div>
</section>
    <!-- right col -->


  <!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
