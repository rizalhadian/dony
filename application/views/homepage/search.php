<section class="content-header">


  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    

    <div class="produk col-lg-12 col-md-12" >
      <h4 style="padding-left:40px">
        <b>KATEGORI
      </h4>
      <div class=" col-lg-12 col-md-12">
        <div class="col-lg-2 col-md-2" >

        <?php foreach($kategoris as $kategori){ ?>

<?php if($kategori->parent_id == null){ ?>

<div class="box box-default ">
  <div class="box-header with-border" style="background:#b8c0f9;">
    <h3 class="box-title"><?php echo $kategori->nama; ?></h3>

    <div class="box-tools">
      <button type="button" class="btn btn-box-tool" data-widget="collapse" ><i class="fa fa-minus"></i>
      </button>
    </div>
  </div>
  <div class="box-body no-padding">
    <ul class="nav nav-pills nav-stacked">
      <!-- <li class=""><a href="#">-Subkategori-</a></li>
      <li class=""><a href="#">-Subkategori-</a></li> -->

      <?php foreach($kategori->sub as $subk){ ?>
        <li class=""><a href="<?php echo base_url(); ?>p/<?php echo $kategori->nama; ?>/<?php echo $subk->nama; ?>"><?php echo $subk->nama; ?></a></li>
      <?php } ?>
      

    </ul>
  </div>
  <!-- /.box-body -->
</div>
<?php } ?>
<?php } ?>
        
        </div>
        
        <div class="col-lg-10 col-md-10" >
        <h4 style="padding-left:15px; margin-top:-30px;">
        <?php if(isset($q)){ ?>
        <b>HASIL PENCARIAN YANG DITEMUKAN UNTUK "<?php  echo $q; ?>"
        <?php } ?>

        <?php if(isset($kat)){ ?>
        <b>KATEGORI "<?php  echo $kat; ?>"
        <?php } ?>
        </h4>
        
          <?php foreach($produks as $produk){ ?>
            
            <a href = "<?php echo $produk->url; ?>" >

            <div class="col-md-3 ">
                <div class="box box-primary" style="width:100%">
                    <div class="panel-heading" style="height:200px;">
                    <center>
                        <img 
                          src="<?php if($produk->photo == ""){  echo base_url()."assets/images/no-image.png"; }else{ echo $produk->photo; } ?>" 
                          class="img-responsive"
                          alt="Responsive image" 
                          style="max-height:200px;"
                        >


                         


                      </center>
                    </div>
                    <div class="box-body">
                     <ul class="list-group list-group-unbordered">
                       <li class="list-group-item">
                         <a style="text-decoration: none; color: black"><?php echo $produk->nama; ?></a>
                       </li>
                       <li class="list-group-item">
                         <b>Rp</b> <a class="pull-right" style="text-decoration: none; color: orange"><?php echo $produk->harga; ?></a>
                       </li>
                       <li class="list-group-item" style="text-decoration: none; color: orange">
                       <a href="<?php echo base_url(); ?>u/<?php echo $produk->toko; ?>"><i class="fa fa-shopping-cart"></i><b> <?php  echo $produk->toko; ?></b></a>
                       <b style="color:black; text-alight:right; float:right;"><i class="fa fa-map-marker"></i> <?php  echo (isset($produk->province)) ? $produk->province : "-"; ?></b>
                       </li>
                     </ul>
                    </div>
                </div>
            </div>
            </a>
          <?php } ?>

        </div>
      </div>

    </div>


  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
