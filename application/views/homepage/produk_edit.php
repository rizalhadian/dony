*<section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">


    <div class="offset-lg-2 offset-md-2 col-lg-6 col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Produk</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
          <form class="form-login" action="<?php echo base_url(); ?>produk/update" method="post">
            <div class="form-group col-lg-12 col-md-12">
              <label>Nama</label>
              <input type="text" name="nama" id="nama" class="form-control" value="<?php echo $produk->nama; ?>">
              <input type="hidden" name="id" id="produk_id" value="<?php echo $produk->id; ?>">
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Hewan</label>
              <select class="form-control" name="kategori[]">
                <option value="null">Pilih Hewan</option>
                <?php foreach($subparent as $sp){ ?>
                <option value="<?php echo $sp->id; ?>"  <?php foreach($selected_kategori as $sk){ if($sk->kategori_id == $sp->id){ echo "selected"; } }  ?>><?php echo $sp->nama; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Kategori</label>
              <select class="form-control" name="kategori[]">
                <option value="null">Pilih Kategori</option>
                <?php foreach($parent as $p){ ?>
                <?php if($p != "HEWAN"){ ?>
                <option value="<?php echo $p->id; ?>" <?php foreach($selected_kategori as $sk){ if($sk->kategori_id == $p->id){ echo "selected"; } }  ?>><?php echo $p->nama; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
            
            
            <div class="form-group col-lg-6 col-md-6">
              <label>Harga</label>
              <input type="text" name="harga" class="form-control" value="<?php echo $produk->harga; ?>">
            </div>
            <div class="form-group col-lg-3 col-md-3">
              <label>Berat (Kg)</label>
              <input type="text" name="berat" class="form-control" value="<?php echo $produk->berat; ?>">
            </div>
            <div class="form-group col-lg-3 col-md-3">
              <label>Stock</label>
              <input type="text" name="stock" class="form-control" value="<?php echo $produk->stock; ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Deskripsi</label>
              <textarea name="deskripsi" id="descriptionUpdate" class="form-control" rows="4" placeholder=""><?php echo $produk->deskripsi; ?></textarea>
            </div>
            
            <div class="form-group col-lg-12 col-md-12">
              <label>Tags</label><br>
              <input style="width:100%" name="tags" type="text" class="form-control" value="<?php foreach($tags as $tag){echo $tag->name.",";} ?>" data-role="tagsinput" />
            </div>

            <!-- <button type="submit" class="btn btn-success btn-lg btn-block">Langkah Berikutnya</button> -->
        </div>
        <!-- /.box-body -->
      </div>
    </div>

    <div class="offset-lg-2 offset-md-2 col-lg-6 col-md-6">
      <img src="<?php echo base_url(); ?>assets/images/tambah_produk.png" style="width:80%">
    </div>


    <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box box-solid bg-gray">
        <div class="box-header">
          <h3 class="box-title" style="color:black">Photos
            <!-- Button trigger modal -->
          </h3>
          <div class="pull-right box-tools">

            <button type="button" class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-footer text-black">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
              </div>
            </div>
            
            <!-- <input type="text" id="photoIds" name="photoIds"> -->

            <div class="col-md-12">
              <div class="col-md-12 form-group well" id="photosForm">
                <div class="col-md-12">
                  <div class="form-group" id="uploadedPhotos">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalPartner"><i class="fa fa-upload"></i> Unggah Foto</button><br>

                    <?php foreach($photos as $photo){ ?>
                    <div class="col-md-2">
                      <div class="box">
                        <img src="<?php echo $photo->url; ?>" alt="..." class="img-thumbnail">
                      </div>
                    </div>
                    <?php } ?>
                    

                  </div>
                </div>
                <br>
              </div>
            </div>


          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-success btn-lg btn-block">Selesai</button>
    </div>
    </form>
    
  </div>
</section>
<!-- right col -->


<!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      
      <div class="col-md-12 form-group well">
                <div class="col-md-12">
                  <div class="form-group">
                    <div class="file-loading">
                      <input id="file-1" name="userfile[]" type="file" multiple class="file" data-overwrite-initial="false">
                    </div>
                  </div>
                </div>
                <br>
              </div>
            </div>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>