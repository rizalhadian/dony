  <section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="col-lg-12 col-md-12">
        <form class="form-login" action="<?php echo base_url(); ?>cart/checkout" method="post">
          <input type="hidden" name="produkid" id="produk_id" value="">
          <input type="hidden" name="urlasal" id="produk_id" value="">

          <div class="box">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Invoice</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                
              <div class="col-md-8">
              <h4>Invoice - <b><?php echo $invoice->kode; ?></b></h4>
                <br>
                <table>
                    <tr>
                        <td><h5>Invoice Number</h5></td>
                        <td style="width:50px;"><h5><center>:</center></h5></td>
                        <td><b><h5><b><?php echo $invoice->kode; ?></b></h5></td>
                    </tr>
                    <tr>
                        <td><h5>Issue Date</h5></td>
                        <td style="width:50px;"><h5><center>:</center></h5></td>
                        <td><b><h5><b><?php echo $invoice->created_on; ?></b></h5></td>
                    </tr>
                    <tr>
                        <td><h5>Status</h5></td>
                        <td style="width:50px;"><h5><center>:</center></h5></td>
                        <td><b><h5><b><?php if($invoice->is_paid==1){echo "Sudah Dibayar";}else{echo "Belum Dibayar";} ?></b></h5></td>
                    </tr>
                    <tr>
                        <td><h5>Nama Penerima</h5></td>
                        <td style="width:50px;"><h5><center>:</center></h5></td>
                        <td><b><h5><b><?php echo $invoice->nama_penerima; ?></b></h5></td>
                    </tr>
                    <tr>
                        <td><h5>Telp Penerima</h5></td>
                        <td style="width:50px;"><h5><center>:</center></h5></td>
                        <td><b><h5><b><?php echo $invoice->telp_penerima; ?></b></h5></td>
                    </tr>
                    <tr>
                        <td><h5>Alamat Penerima</h5></td>
                        <td style="width:50px;"><h5><center>:</center></h5></td>
                        <td><b><h5><b><?php echo $invoice->alamat; ?></b></h5></td>
                    </tr>
                </table>
                <hr><hr>
 
                <!-- <table class="table">
                    <?php foreach($toko as $t){ ?>
                        <tr>
                            <td>
                                <table class="table">
                                    <tr>
                                        <td>Nama Penjual</td>
                                        <td style="width:50px;"><center>:</center></td>
                                        <td><?php echo $t->tokonama; ?></td>
                                    </tr>
                                    
                                </table>
                            </td>
                            <td>
                                <table class="table table-bordered table-striped">
                                    <?php foreach($t->produk as $p){ ?>
                                        
                                        <tr>
                                            <td>Nama Produk</td>
                                            <td>:</td>
                                            <td><?php echo $p->nama; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Berat</td>
                                            <td>:</td>
                                            <td><?php echo $p->berat; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Harga</td>
                                            <td>:</td>
                                            <td><?php echo $p->harga; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                    <?php } ?>
                    
                </table> -->

                <table class="table">
                      <tr style="background:#ECF0F5;">
                              <td></td>
                              <td>Qty</td>
                              <td style="text-align:right;  ">Harga</td>
                              <td style="text-align:right;  ">Total Harga</td>
                      </tr>
                    <?php foreach($toko as $t){ ?>
                      <tr>
                        <td style="width:1500px;">Dari toko <b><?php echo $t->tokonama; ?></b></td>
                        
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>

                      <?php foreach($t->produk as $p){ ?>   
                        <tr>
                          
                         
                          <td style="padding-left:30px;"><b>> <?php echo $p->nama; ?></b></td>
                          <td><?php echo $p->qty; ?></td>
                          <td style="text-align:right;  "><?php echo $p->harga; ?></td>
                          <td style="text-align:right;  "><?php echo ($p->harga*$p->qty); ?></td>
                        </tr>
                      <?php } ?>   
                                
                      <tr style="">
                              <td style="padding-left:30px;">Ongkir dengan berat <b><?php echo $t->total_berat; ?></b> kilogram</td>
                              <td><?php echo $t->total_berat; ?></td>
                              <td style="text-align:right;  "><?php echo $t->ongkir_perkilo; ?></td>
                              <td style="text-align:right;  "><?php echo $t->total_ongkir; ?></td>
                      </tr>
  
                    <?php } ?>
                    
                    <tr style="background:#ECF0F5;">
                              <td style="text-align:right;  ">Total Transaksi</td>
                              <td></td>
                              <td ></td>
                              <td style="text-align:right;  "><b><?php echo $invoice->total; ?><b></td>
                    </tr>
                    
                </table>
                
              </div>

              


          </div>
          <!-- /.box-body -->

        </form>

      </div>

    </div>
  </div>







  </div>
</section>
<!-- right col -->


<!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
        <div class="modal-body">
          <input type="text" name="idtoupdate" id="partnerId">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
          <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>