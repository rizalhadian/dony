<section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-9 col-md-9">
      <div class="col-lg-12 col-md-12">
        <form class="form-login" action="<?php echo base_url(); ?>cart/addtocart" method="post">
        <input type="hidden" name="produkid" id="produk_id" value="<?php echo $produk[0]->id; ?>">
        <input type="hidden" name="userid_produk" id="produk_id" value="<?php echo $produk[0]->userid; ?>">
        <input type="hidden" name="produkharga" id="produk_harga" value="<?php echo $produk[0]->harga; ?>">
        <input type="hidden" name="urlasal" id="produk_id" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
        
        <div class="box">
          <div class="box-header with-border">
            <h5 class="" style="margin-left:15px;"><a ><?php echo $kat1; ?></a> > <a ><?php echo $kat2; ?></a></h5>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <div class="box-body">
              <div class="col-md-6" >
                <div class="panel panel-default">
                    <!-- <div class="panel-heading"><b>{{ Nama }}</b></div> -->
                    <div class="panel-body box-primary">
                      
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">

                          <?php if($produk[0]->photos != null){ ?>
                            
                              
                            
                            
                            <?php for($i=0; $i<sizeof($produk[0]->photos); $i++){?>
                              <?php if($i==0){ ?>
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <?php }else{ ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>"></li>                    
                              <?php } ?>

                            <?php } ?>
                          </ol>
                          <div class="carousel-inner">
                            <?php for($i=0; $i<sizeof($produk[0]->photos); $i++){?>
                              <?php if($i==0){ ?>
                                <div class="item active">
                                <center>
                                  <img src="<?php echo $produk[0]->photos[$i]->url; ?>" alt="" style="height:450px;">
                                  </center>
                                  <div class="carousel-caption">
                           
                                  </div>
                                </div>
                              <?php }else{ ?>
                                <div class="item">
                                <center>
                                  <img src="<?php echo $produk[0]->photos[$i]->url; ?>" alt="" style="height:450px;">
                                  </center>
                                  <div class="carousel-caption">
                         
                                  </div>
                                </div>                
                              <?php } ?>

                            <?php } ?>

                          <?php }else{ ?>
                    
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            </ol>
                            <div class="carousel-inner">
                            <div class="item active">
                            <center>
                            <img 
                              src="<?php  echo base_url()."assets/images/no-image.png"; ?>" 
                              class="img-responsive"
                              alt="Responsive image" 
                              style="max-height:2000px;"
                            >
                            </center>
                            
                                  <div class="carousel-caption">
                           
                                  </div>
                                </div>
                          <?php } ?>
                        
                        
                        
                        
                        
                      </div>
                      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                      </a>
                    </div>
   
                      
                    </div>
                </div>
              </div>
              <div class="col-md-6" >
                <h3><b><?php echo $produk[0]->nama;  ?><b></h3>
                <h4 style="margin-top:-10px;"><i class="fa fa-eye"></i> Dilihat sebanyak <?php echo $produk[0]->view_count; ?> kali</h4>

                <hr>
                <h2><b>Rp. <?php echo $produk[0]->harga;  ?><b></h2>
                <hr>
                <h4><b>Berat</b></h4>
                <?php echo $produk[0]->berat;  ?> gram
                <br>
                <br>
                <h4><b>Deskripsi</b></h4>
                <?php echo $produk[0]->deskripsi;  ?>

              </div>
              <div class="col-lg-12 col-md-12" style="background-color: #D5D7D8;">
                            <br>
                <div class="col-lg-3 col-md-3">
                <table class="table ">
                  <tr>
                    <td style="width:5px;">QTY<td>
                    <td style="width:2px;">:<td>
                    <td><input type="text" name="qty" id="qty" class="form-control" value="1"><td>
                  </tr>
                </table>
                
                </div>
                <div class="col-lg-9 col-md-9">
              <button type="submit" class="btn btn-lg btn-success btn-block"><i class="fa fa-cart-plus"></i> Tambah Ke Keranjang</button>

                </div>
                
                
            </div>
            </div>
            <!-- /.box-body -->
            
            </form>

        </div>

      </div>

      <div class="col-lg-12 col-md-12">
      <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Catatan Penjual</h3>
          </div>
          <div class="box-body" style="font-size:90%">
            <?php echo $produk[0]->catatan_pelapak; ?>
          </div>
      </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-3">
    <div>
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Penjual</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <div class="box-body">
              <center><img src="<?php echo ($produk[0]->photo == null ) ? base_url().'assets/images/no-image.png' : $produk[0]->photo ; ?>" style="width:220px;" alt="..." class="img-thumbnail rounded mx-auto d-block"></center>
              <center><a href="<?php echo base_url(); ?>profile/<?php echo $produk[0]->tokoid;?>"><h3><b><?php echo $produk[0]->toko; ?></b></h3></a></center>
              <center><h6 style="margin-top:-10px;">Since <?php echo $produk[0]->user_created_on; ?></h6></center>
              
              <table class="table table-bordered">
                <tr>
                  <td style="width:30px;"><i class="fa fa-map-marker"></i></td>
                  <td style="width:20px;">:</td>
                  <td><?php echo $location->city_name.", ".$location->province; ?></td>
                </tr>
                <tr>
                  <td style="width:30px;"><i class="fa fa-phone"></i></td>
                  <td style="width:20px;">:</td>
                  <td><?php echo $produk[0]->phone; ?></td>
                  
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
        <a class="btn btn-block btn-primary" href="<?php echo base_url(); ?>u/<?php echo $produk[0]->toko;?>"><h4><b>Semua Produk Penjual</b></h4></a>
        <a class="btn btn-block btn-success" href="<?php echo base_url(); ?>message/<?php echo $produk[0]->toko;?>"><h4><b><i class="fa fa-envelope"></i> Kirim Pesan</b></h4></a>                            
      </div>
    </div>





  </div>
</section>
    <!-- right col -->


  <!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
