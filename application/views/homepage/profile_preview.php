<section class="content-header">
  <section class="content-header">


    <div id="alertInformations">
    </div>

  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div style="width:700px; margin:auto">
        <div class="produk col-lg-12 col-md-12" style="">
          <h4 style="padding-left:">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Profil</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->

              <div class="box-body">
              <center><img src="<?php if($user->photo!=null){echo $user->photo;}else{echo base_url()."assets/images/no-image.png";}  ?>" style="width:220px;" id="profile-pict" alt="..." class="img-thumbnail rounded mx-auto d-block"></center>
            <!-- <br> -->
            <?php if($this->session->userdata('ses') == "profile_preview"){ ?>
            <input type="file" name="file-input" id="file-input" class="form-control">
            <input type="hidden" name="userid" id="userid" class="form-control" value="<?php echo $this->session->userdata('user')->id; ?>">
            <center><button type="button" class="btn" id="btn-change-profile" style="width:220px;"><i class="fa fa-image"></i> Ubah Foto Profil</button></center>
            <?php } ?>
            <br>
              <br>
              <form class="form-login" action="<?php echo base_url(); ?>profile_update" method="post">
              <div class="form-group col-lg-12 col-md-12">
              <label>Username</label>
              <p><?php echo $user->username; ?></p>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Email</label>
              <p><?php echo $user->email; ?></p>
            </div>
            <?php if($this->session->userdata('ses') == "profile_preview"){ ?>
            <div class="form-group col-lg-12 col-md-12">
              <label>Password</label>
              <a href="<?php echo base_url()?>auth/change_password" type="text" name="reset_password" class="btn btn-block">Klik Untuk Ubah Password</a>
            </div>
            <?php } ?>
     
            <div class="form-group col-lg-12 col-md-12">
              <label>Provinsi</label>
              <select name="provinsi" id="provinsi" class="form-control" disabled="disabled">
                        <option value="null">Pilih Provinsi</option>
                        <?php foreach($provinces as $province){ ?>
                          <option value="<?php echo $province->province_id; ?>"><?php echo $province->province; ?></option>
                        <?php } ?>
                      </select>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Kota</label>
              <select name="kota" id="kota" class="form-control" disabled="disabled">
                      <option value="null">Pilih Provinsi Dahulu</option>
                    </select>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Alamat</label>
              <p><?php echo $user->address; ?></p> 
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Nomor Telephone</label>
              <p><?php echo $user->phone; ?></p>
             </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Description</label>
              <p><?php echo $user->deskripsi; ?></p>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Latitude</label>
                <input type="text" name="lat" disabled="disabled" id="latitude" class="form-control" value="">
            </div><div class="form-group col-lg-6 col-md-6">
              <label>Longitude</label>
                <input type="text" name="lng" disabled="disabled" id="longitude" class="form-control" value="">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <style>
                  /* Always set the map height explicitly to define the size of the div
                   * element that contains the map. */
                  #map {
                    height: 300px;
                    width: 100%;
                  }
                  /* Optional: Makes the sample page fill the window. */
                </style>
                <!-- AIzaSyAMloWH7Jlq_7qxlFnCf0Vh-cdb4wgyxx0 -->
                <div id="map"></div>
                <script>
                  function initMap() {
                    var myLatLng = {lat: -8.6728372, lng: 115.2175211};

                    var map = new google.maps.Map(document.getElementById('map'), {
                      zoom: 16,
                      center: myLatLng
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: 'Toko gue disini',
                      draggable: true,
                    });

                    marker.addListener('drag', function() {
                      $('#latitude').val(marker.getPosition().lat());
                      $('#longitude').val(marker.getPosition().lng());
                    });

                  }
                </script>
                <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMloWH7Jlq_7qxlFnCf0Vh-cdb4wgyxx0&callback=initMap">
                </script>
            </div>
              </div>
              <!-- /.box-body -->
            </div>

            <?php if($this->session->userdata('ses') == "profile_preview"){ ?>
            <a href="<?php echo base_url(); ?>profile_edit" class="btn btn-lg btn-block btn-success"><i class="fa fa-plus-square"></i>
              Edit Profile
            </a>
            <?php } ?>
            <a class="btn btn-block btn-primary" href="<?php echo base_url(); ?>u/<?php echo $user->username;?>"><h4><b>Semua Produk Penjual</b></h4></a>


            </form>
          </h4>


        </div>

            
          </h4>


        </div>
      </div>
      

      <div class="login-box">
      
      </div>




    </div>
    <!-- /.row -->



    <!-- /.box -->
  </section>
  <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>