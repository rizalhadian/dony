<section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12" style="padding-left:500px;padding-right:500px;">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Update Info</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

          <div class="box-body">
            <form class="form-login" action="<?php  echo base_url();  ?>shop/create" method="post">
            <center><img src="<?php if($user->photo!=null){echo $user->photo;}else{echo base_url()."assets/images/no-image.png";}  ?>" style="width:220px;" id="profile-pict" alt="..." class="img-thumbnail rounded mx-auto d-block"></center>
            <!-- <br> -->
            <input type="file" name="file-input" id="file-input" class="form-control">
            <input type="hidden" name="userid" id="userid" class="form-control" value="<?php echo $this->session->userdata('user')->id; ?>">
            <center><button type="button" class="btn" id="btn-change-profile" style="width:220px;"><i class="fa fa-image"></i> Ubah Foto Profil</button></center>
            <br>

            <div class="form-group col-lg-12 col-md-12">
              <label>Username</label>
              <input type="text" name="username" class="form-control" value="<?php echo $this->session->userdata('user')->username; ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Email</label>
              <input type="text" disabled="disabled" name="nama" class="form-control" value="<?php echo $this->session->userdata('user')->email; ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Password</label>
              <a href="#" type="text" name="reset_password" class="btn btn-block">Klik Untuk Ubah Password</a>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Provinsi</label>
              <select name="provinsi" id="provinsi" class="form-control">
                        <option value="null">Pilih Provinsi</option>
                        <?php foreach($provinces as $province){ ?>
                          <option value="<?php echo $province->province_id; ?>"><?php echo $province->province; ?></option>
                        <?php } ?>
                      </select>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Kota</label>
              <select name="kota" id="kota" class="form-control" disabled="disabled">
                      <option value="null">Pilih Provinsi Dahulu</option>
                    </select>
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Alamat</label>
                <input type="text"  name="address" class="form-control" value="<?php echo $user->address; ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Nomor Telephone</label>
              <input type="text" name="phone" class="form-control" value="<?php echo $user->phone; ?>">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label>Description</label>
              <textarea name="deskripsi" class="form-control" rows="4" placeholder=""></textarea>
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label>Latitude</label>
                <input type="text" name="lat" id="latitude" class="form-control" value="">
            </div><div class="form-group col-lg-6 col-md-6">
              <label>Longitude</label>
                <input type="text" name="lng" id="longitude" class="form-control" value="">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <style>
                  /* Always set the map height explicitly to define the size of the div
                   * element that contains the map. */
                  #map {
                    height: 300px;
                    width: 100%;
                  }
                  /* Optional: Makes the sample page fill the window. */
                </style>
                <!-- AIzaSyAMloWH7Jlq_7qxlFnCf0Vh-cdb4wgyxx0 -->
                <div id="map"></div>
                <script>
                  function initMap() {
                    var myLatLng = {lat: -8.6728372, lng: 115.2175211};

                    var map = new google.maps.Map(document.getElementById('map'), {
                      zoom: 16,
                      center: myLatLng
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: 'Toko gue disini',
                      draggable: true,
                    });

                    marker.addListener('drag', function() {
                      $('#latitude').val(marker.getPosition().lat());
                      $('#longitude').val(marker.getPosition().lng());
                    });

                  }
                </script>
                <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMloWH7Jlq_7qxlFnCf0Vh-cdb4wgyxx0&callback=initMap">
                </script>
            </div>
            <button type="submit" class="btn btn-success btn-lg btn-block">Submit</button>
          </form>
          </div>
          <!-- /.box-body -->
      </div>
    </div>

    <div class="col-md-6 col-lg-6">

    </div>
  </div>
</section>
    <!-- right col -->


  <!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea id='edit' name="description" class="form-control" rows="3" placeholder="">
          
          </textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
