<section class="content-header">
  <div id="alertInformations">
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="col-lg-12 col-md-12">
        <form class="form-login" action="<?php echo base_url(); ?>cart/checkout" method="post">
          <input type="hidden" name="produkid" id="produk_id" value="">
          <input type="hidden" name="urlasal" id="produk_id" value="">

          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Cart</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
              <div class="col-md-8">
                <table class="table table-bordered" >


                  <?php $id=0; $n=0; foreach($produk as $p){ ?>
                    <tr style="border: 2px solid black;">
                    <?php
                      
                      if($n==0){
                        $id = $p->userid_toko;
                    
                    ?>    

                    <td style="width:250px;" rowspan="<?php echo $toko[$p->userid_toko][0]->count; ?>" style="border: 2px solid black;">
                    
                    <table class="table table-bordered">
                      <tr>
                        <td>Dari toko </td>
                        <td>:</td>
                        <td><b><u><?php echo $toko[$p->userid_toko][0]->username; ?></u><b></td>
                        <input type="hidden" name="city_ids[]" id="city_ids" value="<?php echo $p->userid_toko; ?>-<?php echo $toko[$p->userid_toko][0]->city_id; ?>">
                        
                      </tr>
                      <tr>
                        <td>Kurir</td>
                        <td>:</td>
                        <td>
                        <select name="kurir-<?php echo $p->userid_toko; ?>" id="kurir-<?php echo $p->userid_toko; ?>" class="form-control">
                        <option value="jne">JNE</option>
                        <option value="pos">POS</option>
                        <option value="tiki">TIKI</option>
                      </select>
                        </td>
                      </tr>
                      <tr>
                        <td>Servis</td>
                        <td>:</td>
                        <td>
                        <select class="form-control" id="servis-<?php echo $p->userid_toko; ?>" name="servis-<?php echo $p->userid_toko; ?>" disabled="disabled">
                          <option value="null">Atur Alamat Pengiriman Dulu</option>
                        
                      </select>
                        </td>
                      </tr>
                    </table>
                     
                    


                    </td>
                    
                    <?php  }else{
                      
                        if($id != $p->userid_toko){
                          $n = 0;
                          $id = $p->userid_toko;
                    ?>
                    <td style=";" rowspan="<?php echo $toko[$p->userid_toko][0]->count; ?>">
                    <table class="table">
                      <tr>
                        <td>Dari toko </td>
                        <td>:</td>
                        <td><b><u><?php echo $toko[$p->userid_toko][0]->username; ?></u><b></td>
                        <input type="hidden" name="city_ids[]" id="city_ids" value="<?php echo $p->userid_toko; ?>-<?php echo $toko[$p->userid_toko][0]->city_id; ?>">
                        
                        
                      </tr>
                      <tr>
                        <td>Kurir</td>
                        <td>:</td>
                        <td>
                        <select name="kurir-<?php echo $p->userid_toko; ?>" id="kurir-<?php echo $p->userid_toko; ?>" class="form-control">
                        <option value="jne">JNE</option>
                        <option value="pos">POS</option>
                        <option value="tiki">TIKI</option>
                      </select>
                        </td>
                      </tr>
                      <tr>
                        <td>Servis</td>
                        <td>:</td>
                        <td>
                        <select name="servis-<?php echo $p->userid_toko; ?>" id="servis-<?php echo $p->userid_toko; ?>" class="form-control" disabled="disabled">
                        <option value="null">Atur Alamat Pengiriman Dulu</option>
                        
                      </select>
                        </td>
                      </tr>
                    </table>
                    </td>
                    <?php
                        }
                      }
                      $n++;
                      
                    ?>
                    
                    
                    <td style="width:20px;">
                      <label style="margin-top:10px;">
                        <input type="checkbox" name="checkers[]" class="minimal" checked value="<?php echo $p->id; ?>">
                      </label>
                    </td>

                    <td>
                      <h5><?php echo $p->nama; ?></h5>
                    </td>
                    
                    <td>
                      <h5>@ <?php echo $p->berat; ?> gram x <?php echo $p->qty; ?> = <?php echo ($p->berat*$p->qty); ?> gram</h5>
                    </td>
                    <td>
                      <h5>@ Rp. <?php echo $p->harga; ?> x <?php echo $p->qty; ?> = </h5>
                    </td>
                    <td>
                      <h5>Rp. <?php echo ($p->harga*$p->qty); ?></h5>
                    </td>
                    <td>
                      <!-- <center><a href="<?php echo base_url(); ?>cart/removefromcart/<?php echo $p->id; ?>" class="btn btn-danger" style="margin:5px;"><i class="fa fa-trash"></i> Cancel</a> -->
                      <center><button type="button" value="<?php echo $p->id; ?>" id="modalDelete" data-toggle="modal" data-target="#exampleModal" class="btn btn-danger" style="margin:5px;"><i class="fa fa-trash"></i>Batalkan</a>
                    </td>
                  </tr>
                  <?php } ?>
                  

                </table>
              </div>

              <div class="col-md-4">
                <div>
                  <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Alamat Pengiriman <i class="fa fa-map-marker"></i></h3>
                    </div>
                    
                    <table class="table table-bordered">
                    <tr>
                    <td style="width:130px;">Provinsi</td>
                    <td style="width:20px;">:</td>
                    
                    <td>
                    <select name="provinsi" id="provinsi" class="form-control">
                        <option value="null">Pilih Provinsi</option>
                        <?php foreach($provinces as $province){ ?>
                          <option value="<?php echo $province->province_id; ?>"><?php echo $province->province; ?></option>
                        <?php } ?>
                      </select>
                    </td>
                  </tr>

                  <tr>
                    <td style="width:130px;">Kota</td>
                    <td style="width:20px;">:</td>
                    
                    <td>
                    <select name="kota" id="kota" class="form-control" disabled="disabled">
                      <option value="null">Pilih Provinsi Dahulu</option>
                    </select>
                    </td>
                  </tr>

                  <tr>
                    <td style="width:130px;">Alamat</td>
                    <td style="width:20px;">:</td>
                    
                    <td>
                    <textarea name="alamat" id="alamat" class="form-control" rows="4" placeholder=""><?php echo $this->session->userdata('user')->address; ?></textarea> 
                    </td>
                  </tr>
                    </table>

                    <a href="<?php echo base_url(); ?>" class="btn btn-primary btn-block">Lihat Produk Lainnya</a>
                    <button class="btn btn-warning btn-block">Checkout</button>
                  </div>
                </div>
              </div>
            </div>


          </div>
          <!-- /.box-body -->

        </form>

      </div>

    </div>
  </div>







  </div>
</section>
<!-- right col -->


<!-- /.row (main row) -->



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
        <div class="modal-body">
          <input type="text" name="idtoupdate" id="partnerId">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
          <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" classdata-toggle="modal" id="modalDelete" data-target="#exampleModal"="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Apakah Anda Yakin Ingin Membatalkan Pesanan Ini?</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <!-- <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete"> -->
        </div>
        <div class="row" >
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Tidak</button></div>
          <div class="col-md-6" id="buttonsDelete"></div>
          
        </div>
      </div>
    </div>
  </div>
</div>