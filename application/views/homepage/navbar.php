<?php
  // print_r($this->session->userdata('user')->username);
?>
<header class="main-header">

  <!-- Logo -->

  <a href="index2.html" class="logo">

    <!-- mini logo for sidebar mini 50x50 pixels -->

    <!-- <span class="logo-mini"><b>LBV</b></span>
 -->
    <!-- logo for regular state and mobile devices -->

    <!-- <span class="logo-lg">asdasd</span> -->
    <!-- <img src="<?php echo base_url() ?>/assets/images/icon.PNG" class="logo-lg" style=""> -->
  </a>

  <!-- Header Navbar: style can be found in header.less -->

  <nav class="navbar navbar-static-top">
  <!-- <img src="<?php echo base_url() ?>/assets/images/icon.PNG" class="logo-lg" style=""> -->
    


    <!-- Sidebar toggle button-->

    <form class="" action="<?php echo base_url(); ?>search" method="get">
    <div class="nav navbar-nav" style="margin-top:5px; margin-right:10px;">
    <a href="<?php echo base_url(); ?>">
    <img src="<?php echo base_url() ?>/assets/images/icon.PNG" class="logo-lg" style="width:30%;margin-left:-200px;padding-right:20px;">
    </a>
      

      
      <i class="fa fa-search" style="color: grey; padding-top:8px ; padding-left:5px; padding-right:15px; border: none; margin-right:-25px; width:25px; height:22px; position:absolute;"></i>
      
      <input type="text" name="q" class="" style=" width:450px; color:black; border:none; outline:none; padding-left:30px;"
        placeholder="search" value="<?php echo (isset($q)) ? $q : ""; ?>">
      
    </div>
    </form>



    <div class="navbar-custom-menu">

      <ul class="nav navbar-nav">

        <!-- Messages: style can be found in dropdown.less-->



        <!-- Tasks: style can be found in dropdown.less -->



        <!-- User Account: style can be found in dropdown.less -->


        <?php if(!isset($this->session->userdata('user')->username)){ ?>
        <a href="<?php echo base_url() ?>auth/register" class="btn btn-primary " style="margin-top:8px; margin-right:10px; background:#ECF0F5; color:black;>
            <span class="
          hidden-xs">Register</span>
        </a>
        <a href="<?php echo base_url() ?>auth/login" class="btn btn-success " style="margin-top:8px; margin-right:10px;color:black;">
          <span class="hidden-xs">Login</span>
        </a>
        <?php }else{ ?>
          <li class="dropdown notifications-menu ">
            <a href="#" class="dropdown-toggle " data-toggle="dropdown">
              <i class="fa fa-envelope"></i>
              Messages
              <!-- <span class="label label-danger "><?php echo $count_unread_order; ?></span> -->
            </a>
            <ul class="dropdown-menu">
              <!-- <li class="header">You have <?php echo $count_unread_order; ?> messages</li> -->
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu ">
                  <?php foreach($all_first_messages as $message){ ?>
                    <li>
                    <a href="<?php echo base_url()."message/".$message->guest[0]->username; ?>">
                      <!-- <i class="fa fa-envelope"></i><?php echo $message->messages[0]->message; ?> -->
                      <img class="direct-chat-img" style="" src="<?php echo $message->guest[0]->photo; ?>">&nbsp;&nbsp;&nbsp;<b><?php echo $message->guest[0]->username; ?></b><br>&nbsp;&nbsp;&nbsp;<?php echo $message->messages[0]->message; ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
              </li>
              <li class="footer"><a href="<?php echo base_url(); ?>order">View all</a></li>
            </ul>
          </li>
          <li class="dropdown notifications-menu ">
            <a href="#" class="dropdown-toggle " data-toggle="dropdown">
              <i class="fa fa-shopping-cart "></i>
              Order
              <span class="label label-danger "><?php echo $count_unread_order; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo $count_unread_order; ?> orders</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu ">
                  <?php foreach($orders as $order){ ?>
                    <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart"></i><?php echo $order->kode_invoice; ?> &nbsp; <i class="fa fa-user"></i><?php echo $order->username; ?>
                    </a>
                  </li>
                  <?php } ?>
                </ul>
              </li>
              <li class="footer"><a href="<?php echo base_url(); ?>order">View all</a></li>
            </ul>
          </li>
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo ($this->session->userdata('user')->photo==null) ? base_url().'assets/images/no-image.png': $this->session->userdata('user')->photo; ?>" class="user-image" style="background:white"
              alt="User Image">
            <span class="hidden-xs">
              <?php echo $this->session->userdata('user')->username; ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo ($this->session->userdata('user')->photo==null) ? base_url().'assets/images/no-image.png': $this->session->userdata('user')->photo; ?>" class="img-circle" alt="User Image" style="background:white">
              <p>
                <?php echo $this->session->userdata('user')->username; ?>
                <!-- <small>Member since Nov. 2012</small> -->
              </p>
            </li>
            <!-- Menu Body -->
            <!-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              </li> -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?php echo base_url(); ?>profile" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-left" style="margin-left:30px;">
                <a href="<?php echo base_url(); ?>shop" class="btn btn-default btn-flat">Shop</a>
              </div>
              <div class="pull-right">
                <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        
        <a href="<?php echo base_url() ?>invoice" class="btn btn-success " style="margin-top:8px; margin-right:10px; color:white;>
            <span class="
          hidden-xs"><i class="fa fa-file"></i> Pembelian</span>
        </a>
        <?php } ?>
        <a href="<?php echo base_url() ?>cart" class="btn btn-warning " style="margin-top:8px; margin-right:10px; color:white;>
            <span class="
          hidden-xs"><i class="fa fa-shopping-cart"></i> Cart</span>
        </a>
        
        <!-- Control Sidebar Toggle Button -->

        <!-- <li>

          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>

        </li>
 -->
      </ul>

    </div>

  </nav>

</header>