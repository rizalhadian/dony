<section class="content-header">
<section class="content-header">


  <div id="alertInformations">
  </div>

</section>

<!-- Main content -->
<section class="content">
  <div class="row">

    <!-- Promo and Interesting -->
    <!-- <div class="promo-and-interesting col-lg-12 col-md-12">
      <div class="col-lg-6 col-md-12 col-xs-12">
        <h4 style="padding-left:20px">
          <b>Promo
        </h4>
        <center>
          <div class="small-box" style="height: 200px; width: 600px; background-image: url('<?php  echo base_url(); ?>assets/images/partners.jpg'); color: white; -webkit-background-size: cover;  -moz-background-size: cover; -o-background-size: cover background-size: cover;">
          </div>
        </center>
      </div>
      <div class="col-lg-6 col-md-12 col-xs-12">
        <h4 style="padding-left:20px">
          <b>Interesting
        </h4>
        <center>
          <div class="small-box" style="height: 200px; width: 600px;background-image: url('<?php  echo base_url(); ?>assets/images/activities.jpg');color: white;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
          </div>
        </center>
      </div>
    </div> -->
    <!-- End Promo and Interesting -->

    <div class="produk col-lg-3 col-md-3" >
      <h4 style="padding-left:40px">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Profil</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->

            <div class="box-body">
              <center><img src="<?php echo ($user->photo == null ) ? base_url().'assets/images/no-image.png' : $user->photo ; ?>" style="width:220px;" alt="..." class="img-thumbnail rounded mx-auto d-block"></center>
              <center><a href="<?php echo base_url(); ?>profile/<?php echo $user->id;?>"><h3><b><?php echo $user->username; ?></b></h3></a></center>
              <center><h6 style="margin-top:-10px;">Since <?php echo $user->created_on; ?></h6></center>
              
              <table class="table table-bordered">
                <tr>
                  <td style="width:30px;"><i class="fa fa-map-marker"></i></td>
                  <td style="width:20px;">:</td>
                  <td><?php echo $location->city_name.", ".$location->province; ?></td>
                </tr>
                <tr>
                  <td style="width:30px;"><i class="fa fa-phone"></i></td>
                  <td style="width:20px;">:</td>
                  <td><?php echo $user->phone; ?></td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
        </div>

      </h4>

      
    </div>

<br>
    <div class="produk col-lg-9 col-md-9" >

    <?php foreach($produks as $produk){ ?>
            <a href = "<?php echo base_url(); ?>p/<?php echo $produk->produk_jenis_1_nama; ?>/<?php echo $produk->produk_jenis_2_nama; ?>/<?php echo $produk->id; ?>" >
            <div class="col-md-3 ">
                <div class="box box-primary" style="width:100%">
                    <div class="panel-heading" style="height:200px;">
                      <center>
                        <img 
                          src="<?php if($produk->photo == ""){  echo base_url()."assets/images/no-image.png"; }else{ echo $produk->photo; } ?>" 
                          class="img-responsive"
                          alt="Responsive image" 
                          style="max-height:200px;"
                        >


                         


                      </center>
                    </div>
                    <div class="box-body">
                     <ul class="list-group list-group-unbordered">
                       <li class="list-group-item">
                         <a style="text-decoration: none; color: black"><b><?php echo $produk->nama; ?></b></a>
                       </li>
                       <li class="list-group-item">
                         <b>Rp <a class="pull-right" style="text-decoration: none; color: orange"><?php echo $produk->harga; ?></a></b>
                       </li>
                       
                     </ul>
                    </div>
                </div>
            </div>
            </a>
          <?php } ?>
    </div>


  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-xs-12">

    </div>
  </div>


      <!-- /.box -->
    </section>
    <!-- right col -->
  </div>
  <!-- /.row (main row) -->
</section>



<!-- Modal Partner -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPartnerLabel">Add Partner</h4>
      </div>
      <form enctype="multipart/form-data" action="" method="POST" id="formPartner" role="form">
      <div class="modal-body">
        <input type="text" name="idtoupdate" id="partnerId">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name" id="partnerName" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Address</label>
          <input type="text" name="address" id="partnerAddress" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Email</label>
          <input type="text" name="email" id="partnerEmail" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" id="partnerPhone" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label>Description</label>
          <textarea name="description" id="partnerDescription" class="form-control" rows="3" placeholder=""></textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnPartnerConfirmSave" class="btn btn-primary">Save</button>
        <button type="button" id="btnPartnerConfirmUpdate" class="btn btn-primary">Update</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Are you sure want to delete this..??</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" name="whichdelete" id="whichdelete">
          <input type="text" class="form-control" name="idtodelete" id="idtodelete">
        </div>
        <div class="row">
          <div class="col-md-6"><button type="button" class="btn btn-default btn-block" data-dismiss="modal">Nope</button></div>
          <div class="col-md-6"><button type="button" class="btn btn-primary btn-block" id="btnConfirmDelete">Yes</button></div>
          <div class="col-md-6"></div>
        </div>
      </div>
    </div>
  </div>
</div>
