




<!-- ========================================================================================== -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/style-responsive.css" rel="stylesheet">


</head>

<body>

    <div id="login-page">
        <div class="cotainer">

            <form class="form-login" action="<?php  echo base_url();  ?>auth/change_password" method="post">

                <h2 class="form-login-heading"><center>Change Password</center></h2>
                <div class="login-wrap">
                    <input type="password" class="form-control" name="old" placeholder="Old Password" autofocus required>
                    <br>
                    <input type="password" class="form-control" name="new" placeholder="New Password" required>
                    <br>
                    <input type="password" class="form-control" name="new_confirm" placeholder="Retype New Password" required>
                    <br>
                    
                    <?php if(isset($this->session->message)){ ?>
                      <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $this->session->message; ?>
                      </div>
                    <?php } ?>
                    <button class="btn btn-theme btn-block" name="btn_login" id="btn_login" type="submit"> Submit</button>

                </div>

            </form>

        </div>
    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <!-- <script src="<?php echo base_url();?>assets/admin/js/jquery.js"></script> -->
    <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.backstretch.min.js"></script> -->
    <script>
        // $.backstretch("<?php echo base_url();?>assets/images/login-bg.jpg", {speed: 1000});
    </script>


</body>
</html>
