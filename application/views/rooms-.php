<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="Homely - Responsive Real Estate Template">
  <meta name="author" content="Rype Creative [Chris Gipple]">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Bali Villa Portfolio | <?php foreach($villa as $v){ echo $v->name; }?></title>
  <link id="favicon" rel="shortcut icon" href="<?php echo base_url();?>assets/newhomepage/images/favicon.png" type="image/png" />

  <!-- CSS file links -->
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/bootstrap.min.css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/font-awesome.min.css" rel="stylesheet" media="screen">
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/jquery-ui.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/slick.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/chosen.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="<?php echo base_url();?>assets/newhomepage/villadetail/css/responsive.css" rel="stylesheet" type="text/css" media="all" />
  <link href="<?php echo base_url();?>assets/newhomepage/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/newhomepage/css/daterangepicker.css" />

  <!--slider-->

  <link rel='stylesheet' href='http://lifewithoutandy.com/wordpress/wp-content/plugins/advanced-responsive-video-embedder/public/assets/css/public.css?ver=5.1.1' type='text/css' media='all' />
  <link rel='stylesheet' href='<?php echo base_url();?>assets/newhomepage/css/lwastyle.css' type='text/css'/>
  <link rel='stylesheet' id='style-css'  href='<?php echo base_url();?>assets/newhomepage/css/lwastyle2.css' type='text/css' />
  <style>
      /* Always set the map height explicitly to define the size of the div
      * element that contains the map. */
      #map {
        height: 100%;

      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
    <style>

     .map-wrap {
      position:relative;
    }

    .overlaymap {
      width:100%;
      min-height:500px;
      position:absolute;
      top:0;
    }

    /* Modal video*/
    .modalvideocss {
     z-index: 888;
     position: relative;
     display: block;
     bottom: 130px;
     float: left;
     left: 340px;
     color: #ddd;
     font-size: 14px;
     padding: 10px;
     background-color: rgba(0, 0, 0, 0.7);
     border-radius: 4px;
     cursor: pointer;
   }
   .modal .modal-header {
    border-bottom: none;
    position: relative;
  }
  .modal .modal-header .btn {
    position: absolute;
    top: 0;
    right: 0;
    margin-top: 0;
    border-top-left-radius: 0;
    border-bottom-right-radius: 0;
  }
  .modal .modal-footer {
    border-top: none;
    padding: 0;
  }
  .modal .modal-footer .btn-group > .btn:first-child {
    border-bottom-left-radius: 0;
  }
  .modal .modal-footer .btn-group > .btn:last-child {
    border-top-right-radius: 0;
  }
  /* Modal video*/

  /* rates */
  @media (min-width:180px){.ratesnconclusion{
   height:1100px;
 }}
 @media (min-width:241px){.ratesnconclusion{
   height:920px;
 }}
 @media (min-width:280px){.ratesnconclusion{
   height:830px;
 }}
 @media (min-width:357px){.ratesnconclusion{
   height:740px;
 }}
 @media (min-width:435px){.ratesnconclusion{
   height:630px;
 }}
 @media (min-width:600px){.ratesnconclusion{
   height:455px;
 }}
 @media (min-width:768px){.ratesnconclusion{
   height:455px;
 }}
 @media (min-width:1200px){.ratesnconclusion{
   height:455px;
 }}
 /* Modal drone */

	/*  .row > .column {
  padding: 0 8px;
}


.column {
  float: left;
  width: 25%;
}
*/

.row:after {
  content: "";
  display: table;
  clear: both;
}


/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 9999	;
  padding-top: 10px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: transparent;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
  z-index:12000;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}

/* Carousel */

#quote-carousel {
  padding: 0 10px 30px 10px;
  margin-top: 30px;
  /* Control buttons  */
  /* Previous button  */
  /* Next button  */
  /* Changes the position of the indicators */
  /* Changes the color of the indicators */
}
#quote-carousel .carousel-control {
  background: none;
  color: #CACACA;
  font-size: 2.3em;
  text-shadow: none;
  margin-top: 30px;
}
#quote-carousel .carousel-control.left {
  left: -60px;
}
#quote-carousel .carousel-control.right {
  right: -60px;
}
#quote-carousel .carousel-indicators {
  right: 50%;
  top: auto;
  bottom: 0px;
  margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
  width: 50px;
  height: 50px;
  margin: 5px;
  cursor: pointer;
  border: 4px solid #CCC;
  border-radius: 50px;
  opacity: 0.4;
  overflow: hidden;
  transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
  background: #333333;
  width: 128px;
  height: 128px;
  border-radius: 100px;
  border-color: #f33;
  opacity: 1;
  overflow: hidden;
}
.carousel-inner {
  min-height: 20px;
}
.item blockquote {
  border-left: none;
  margin: 0;
}
.item blockquote p:before {
  content: "\f10d";
  font-family: 'Fontawesome';
  float: left;
  margin-right: 10px;
}
</style>
<!--reviews-->

</head>
<body>

  <nav class="navbar navbar-fixed-top green">
    <div class="container-fluid">
      <!--second nav button -->
      <div id="menu_bars" class="right">
        <span class="t1"></span>
        <span class="t2"></span>
        <span class="t3"></span>
      </div>
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="container">

        <div class="navbar-header">
          <a class="navbar-brand" href="http://balivillaportfolio.com">
            <img src="<?php echo base_url(); ?>assets/newhomepage/images/balivillaportfolio.png" alt="logo">
          </a>
          <a  href="http://balivillaportfolio.com">
            <img style="width:50px" src="http://www.totalbali.com/tb-home/assets/img/logo/totalbali.png">
          </a>
        </div>
        <div class="sidebar_menu">
          <nav class="pushmenu pushmenu-right">
            <a class="push-logo" href="http://balivillaportfolio.com/"><img src="<?php echo base_url();?>assets/newhomepage/images/balivillaportfolio2.png" alt="logo"></a>
            <ul class="push_nav centered">
              <li class="clearfix">
                <a href="http://balivillaportfolio.com/live-availability"  ><span>01.</span>Live Availability </a>

              </li>
              <li class="clearfix">
                <a href="http://balivillaportfolio.com/"> <span>02.</span>Home</a>

              </li>
              <li class="clearfix">
                <a href="http://balivillaportfolio.com/#work"> <span>03.</span>Villas</a>

              </li>
              <li class="clearfix">
                <a href="http://balivillaportfolio.com/#pricing-table-section"> <span>04.</span>Map</a>

              </li>
              <li class="clearfix">
                <a href="http://balivillaportfolio.com/#blog-section"> <span>05.</span>Videos</a>

              </li>
              <li class="clearfix">
                <a href="http://balivillaportfolio.com/#contact-us"> <span>06.</span>Contact Us</a>

              </li>
            </ul>
            <div class="clearfix"></div>
            <ul class="social_icon black top25 bottom20 list-inline">

              <li><a href="https://www.facebook.com/totalbali/" target="_blank"><i class="fa fa-fw fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/TotalBali" target="_blank"><i class="fa fa-fw fa-twitter"></i></a></li>
              <li><a href="https://www.instagram.com/totalbali/" target="_blank"><i class="fa fa-fw fa fa-instagram"></i></a></li>
              <li><a href="https://www.youtube.com/user/Totalbalimedia" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>

            </ul>
          </nav>
        </div>
      </div>
    </nav>


    <!--slider-->

    <script type="text/javascript">
      window.LWA = { Data:{} };

      LWA.Data.Gallery = {
        feature: 'images/villa0.jpg',
        inline: [],
        thumbnails: []
      };

      <?php foreach($villa as $v){ ?>
        <?php foreach($v->photos as $photo){ ?>

          LWA.Data.Gallery.inline.push({
            src: '<?php echo base_url();?>uploads/photos/villas/<?php echo $photo->url; ?>',
            width: '100%',
            caption: ''
          });
          LWA.Data.Gallery.thumbnails.push({
            src: '<?php echo base_url();?>uploads/photos/villas/<?php echo $photo->url; ?>'
          });
          <?php } ?>
          <?php } ?>




        </script>
        <div id="modal-gallery" class="modal modal-transparent modal-gallery">
          <button class="modal-action modal-close"><i class="icon-close"></i></button>
          <div class="modal-gallery-count">1 / 15</div>


          <div class="modal-wrap">
            <div class="modal-wrap-row" data-closeable="0">
              <div id="modal-gallery-frame" class="modal-view frame" data-closeable="0">
                <div class="royalSlider rsHor" style="height: 528px;">

                </div>

                <div class="rsGCaption"><span class="rsCaption"></span></div>

              </div>
            </div>
            <button class="gallery-home button button-white" id="modal-gallery-home">back to start3</button>
            <div class="sly-controls">
              <button class="sly-prev"><i class="icon-arrow-left"></i></button>
              <button class="sly-next"><i class="icon-arrow-right"></i></button>
            </div>
          </div>
        </div>

        <div id="modal-slider-next" class="gallery-next-overlay" data-closeable="0">
          <div class="royalSlider rsHor" style="width: 1349px; height: 648px;"><div class="rsOverflow" style="width: 1349px; height: 648px;"><div class="rsContainer" style="transition-duration: 0s; transform: translate3d(0px, 0px, 0px);"><div style="left: 0px;" class="rsSlide  rsActiveSlide"><div class="gallery-next-slide" data-closeable="0"></div></div><div style="left: 1357px;" class="rsSlide "><div class="gallery-next-slide" data-closeable="0">
          <div class="f-grid f-row" data-closeable="0">
            <div class="f-1 content-wrap" data-closeable="0">
              <div class="content-row" data-closeable="0">
                <div class="header-content" data-closeable="0">
                  <script type="text/javascript">
                    LWA.hasNextPost = true;
                  </script>


                  <button class="button button-white modal-gallery-home" id="modal-gallery-home"><i class="fa fa-previews"></i>back to start1</button>
                </div>
              </div>
            </div>
          </div>
        </div></div></div></div></div>
      </div>

    </div>
    <header id="header-gallery" class="header header-feature header-gallery">
      <div class="header-gallery-wrap">
        <div id="header-gallery-wrap" class="m-wrap animate-gallery">
          <div id="inline-gallery-frame" class="header-feature-bg header-gallery-frame frame" style="overflow: hidden;">
            <ul class="slidee" style="transform: translateZ(0px) translateX(-1695px); width: 11223px;">

              <?php foreach($villa as $v){ ?>
              <?php foreach($v->photos as $photo){ ?>
              <?php
              list($width, $height) = getimagesize( base_url().'uploads/photos/villas/'.$photo->url);
              ?>
              <li class="sly-slide header-gallery-overlay" style="width: <?php if($width > $height){echo "1122";}else{echo $width;}  ?>px;">
                <div class="m-wrap"><img class="m-bg" data-img-src="<?php echo base_url();?>uploads/photos/villas/<?php echo $photo->url; ?>" src="<?php echo base_url();?>uploads/photos/villas/<?php echo $photo->url; ?>"></div>
                <div class="blanket"></div>
              </li>
              <?php } ?>

            </ul>
          </div>
          <div class="m-overlay blanket-light" style="display: none;"></div>
          <div class="f-grid f-row sly-controls">
            <div id="inline-gallery-controls" class="f-1">
              <button class="gallery-home button button-white" id="inline-gallery-home"><i class="fa fa-arrow-left"></i></button>
              <button class="sly-prev"><i class="icon-arrow-left"></i></button>
              <button class="sly-next"><i class="icon-arrow-right"></i></button>
            </div>
          </div>

          <!-- <div class="f-grid f-row header-gallery-title"> -->
          <div class="f-grid f-row  woi">
            <div class="f-1 content-wrap">
              <div class="content-row">
                <div class="header-content">
                  <div class="header-feature-category">
                    <a class="link h-1" href="#">
                      <span class="header-feature-category-item"><?php echo $v->name; ?></span>
                    </a>
                  </div>
                  <!-- <div class="h-2">With the colonial style often found in the Caribbean and the Hamptons, Villa  is white and bright and will leave you feeling refreshed & pampered.</div> -->
                </div>
              </div>
            </div>
          </div>

          <div class="gallery-next-overlay">
            <div class="gallery-next-wrap">
              <div class="f-grid f-row">
                <div class="f-1 content-wrap">
                  <div class="content-row">
                    <div class="header-content">
                      <script type="text/javascript">
                        LWA.hasNextPost = true;
                      </script>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="header-gallery-details">
            <div class="header-gallery-controls">
              <!--button id="gallery-thumbs" class="button button-gallery button-thumbs"><i class="icon-thumbs"></i></button-->
              <button id="modal-gallery-button" class="button button-gallery hidden-lg hidden-md"><i class="icon-expand"></i> <font style="font-size:14px;font-family: verdana, Arial;">Launch Gallery</font> </button>
            </div>
          </div>
        </div>
      </div>


    </header>
<!--slider end

<div class="container" style="margin-top:10px">
	<div class="row">
	<div class="col-lg-2">
		<img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/<?php echo base_url();?>assets/newhomepage/villadetail/images/property-img5.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/<?php echo base_url();?>assets/newhomepage/villadetail/images/property-img6.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/<?php echo base_url();?>assets/newhomepage/villadetail/images/property-img7.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/<?php echo base_url();?>assets/newhomepage/villadetail/images/property-img1.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/<?php echo base_url();?>assets/newhomepage/villadetail/images/property-img2.jpg" alt="" />
	</div>
	<div class="col-lg-2">
		<img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/<?php echo base_url();?>assets/newhomepage/villadetail/images/property-img3.jpg" alt="" />
	</div>

	</div>
	</div>

-->
<!--slider-->

<section class="module">
  <div class="container">
   <h2><?php echo $v->name; ?> <br><span style="font-size:14px;color:#797979"><?php echo $v->lokasi; ?>, Bali</span></h2>
   <div class="row">
    <div class="col-lg-8 col-md-8">
      <div class="property-single-item property-details">
        <table class="property-details-single">
         <tr>
          <?php
          $rate = explode('.', $v->rate);
          ?>
          <td><i class="fa fa-bed"></i> <span><?php echo $v->rooms; ?></span> Beds</td>
          <td><i class="fa fa-bath"></i> <span><?php echo $v->bathrooms; ?></span> Baths</td>
          <td><i class="fa fa-money"></i> Start From <span>$<?php echo $rate[0]; ?></span> P/N</td>

        </tr>
      </table>
    </div>
    <div class="row">
     <div class="widget property-single-item property-description content">
      <h4>
       <span>Description</span>
     </h4>
     <?php echo $v->description_new; ?>
   </div>
 </div>
</div>

<div class="col-lg-4 col-md-4">
 <div class="widget widget-sidebar advanced-search">
   <h4 style="padding:33px"><span>Make a booking now</span></h4>
   <div class="widget-content box">
    <form method='post' action="<?php echo base_url(); ?>reservation">
      <div class="form-group">
       <input type="text" name="name"  class="form-control " placeholder="Enter your name" style="background-color : #ffffff; border:2px solid #d1d1d1;">
     </div>

     <div class="form-group">
      <div class="input-group date">
        <div class="input-group-addon">
          <i class="fa fa-envelope-o" aria-hidden="true"></i>
        </div>
        <input type="text" name="email" class="form-control pull-right" class="datetime" style="background-color : #ffffff; border:2px solid #d1d1d1;" placeholder="Enter your email">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group date">
        <div class="input-group-addon">
          <i class="fa fa-phone"></i>
        </div>
        <input type="text" name="phone" class="form-control pull-right" class="datetime" style="background-color : #ffffff; border:2px solid #d1d1d1;" placeholder="Enter your phone number">
        <input type="hidden" name="villa" value="<?php echo $v->name; ?>">
      </div>
    </div>
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="text" class="form-control pull-right" name="daterange" id="daterange" style="background-color : #ffffff; border:2px solid #d1d1d1;">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group">
      <textarea name="message" rows="5" cols="5" class="form-control" placeholder="Your message here .."></textarea>
    </div>
    <div class="form-group">
     <button type="submit" class="btn btn-block btn-primary" value="Send" />Send</button>
   </div>
 </form>
 <!-- end widget content -->
</div><!-- end widget -->
</div>
</div>
</div>

<div class="row">
 <div class="col-lg-12 col-md-12">
   <h4>Video</h4>
   <div class="row widget property-single-item">
     <div align="center">
       <iframe width="100%" height="322" src="https://www.youtube.com/embed/<?php echo $v->youtube; ?>" frameborder="0" allowfullscreen></iframe>
       <div data-toggle="modal" href="#videomodal" class="modalvideocss">
         Click <i class="fa fa-search-plus"></i>
       </div>
     </div>

     <div id="videomodal" class="modal fade in">
      <div class="modal-header">
                    <!--a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title">Modal Heading</h4-->
                    </div>
                    <iframe width="1349" height="90%" src="https://www.youtube.com/embed/<?php echo $v->youtube; ?>" frameborder="0" allowfullscreen></iframe>
                    <div class="modal-footer">
                      <div class="btn-group">
                        <button class="btn btn-xs btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>
                        <button class="btn btn-xs btn-primary"><i class="fa fa-share"></i> Share</button>
                      </div>
                    </div>

                  </div><!-- /.modal -->

                </div>
              </div>


            </div>

            <div class="row" >
             <div class="col-lg-8 col-md-8">
              <h4>Rates & Inclusions</h4>
              <div class="row widget property-single-item">
                <?php echo $v->iframe_incl; ?>
              </div>
            </div>

            <div class="col-lg-4 col-md-4">
              <h4>Availbility Calendar</h4>
              <div class="row widget property-single-itemt">
                <br>
                <!-- <iframe id="calender" width="325" height="345" src="http://www.totalbalidirect.com/tbd/calendar/index.php?action=periode_tgl&var=TczBDoIwDIDhtxplusGMgkEOPUiU1zDd1uli2XAUI2xslashvINP4X5p8berMAweMUB0KNTN69LArJlmY0tSBQ7ziCx2jStJvZSbxslashnF1coNtfmv6QMfh7YAN1e6rPVTYdvHVxIANl2XRt8xplusNhZJLEGQyh0ezIy1emkbRDjigEVll7xLwQekuYhWJ6ufbHZj2utrKqG9itDwxsamexsame"  scrolling="no" frameborder="0" allowfullscreen></iframe> -->
                <?php echo $v->iframe_avail; ?>
              </div>
            </div>
</div>


<div class="row" style="margin-top:40px">
  <div class="col-lg-12 col-md-12">
    <h4>Location <br><span style="font-size:12px;color:#797979"><?php echo $v->lokasi; ?>, Bali</span></h4>
    <div class="row widget property-single-item">
     <div class="map-wrap">
      <div class="overlaymap" onClick="style.pointerEvents='none'"></div>
      <!-- <iframe frameborder='0' scrolling='no' style='border:0;width:100%;height:450px' src='http://totalbalidirect.com/map/map_nwidget?c=1&sc=0&p=0&l=2&z=10&ce=-8.525936671766459, 115.17665147781372&w=1000&h=500&marker_selected=themuse'></iframe> -->
      <div id="map" class="map" scrolling='no' style='border:0;width:100%;height:450px; background-color:#000000'>

      </div>

      <a href="https://www.google.com/maps/dir/Current+Location/<?php echo $v->latitude.",".$v->longitude; ?>" target="_blank" class="btn btn-block btn-lg btn-primary" style="border-radius: 0;"><i class="fa fa-map-o" aria-hidden="true"></i> Get Direction</a>
      <!-- <a href="https://www.google.com/maps/dir/Current+Location/" target="_blank" class="btn btn-block btn-lg btn-primary"><i class="fa fa-map-o" aria-hidden="true"></i> Get Direction</a> -->

    </div>
  </div>
</div>

</div>


<div class="row">
  <div class="col-lg-12 col-md-12">
   <!-- end description -->
			<!-- <div class="widget property-single-item property-amenities">
				<h4>
					<span>Amenities</span> <img class="divider-hex" src="<?php echo base_url();?>assets/newhomepage/http://rypecreative.com/homely/images/divider-half.png" alt="" />
					<div class="divider-fade"></div>
				</h4>
				<ul class="amenities-list">
					<li><i class="fa fa-check icon"></i> Balcony</li>
					<li><i class="fa fa-check icon"></i> Cable TV</li>
					<li><i class="fa fa-check icon"></i> Deck</li>
					<li><i class="fa fa-check icon"></i> Dishwasher</li>
					<li><i class="fa fa-check icon"></i> Heating</li>
					<li><i class="fa fa-close icon"></i> Internet</li>
					<li><i class="fa fa-check icon"></i> Parking</li>
					<li><i class="fa fa-check icon"></i> Pool</li>
					<li><i class="fa fa-check icon"></i> Oven</li>
					<li><i class="fa fa-close icon"></i> Gym</li>
					<li><i class="fa fa-check icon"></i> Laundry Room</li>
				</ul>
			</div>end amenities -->
			<!-- end location -->

      <div class="row">
       <div class="widget property-single-item">
        <h4>
         <span>Reviews</span>
       </h4>
       <div class="col-md-12" data-wow-delay="0.2s">
        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <!-- Bottom Carousel Indicators--
                            <ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="<?php echo base_url();?>assets/newhomepage/https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/https://s3.amazonaws.com/uifaces/faces/twitter/rssems/128.jpg" alt="">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="<?php echo base_url();?>assets/newhomepage/https://s3.amazonaws.com/uifaces/faces/twitter/adellecharles/128.jpg" alt="">
                                </li>
                              </ol-->

                              <!-- Carousel Slides / Quotes -->
                              <div class="carousel-inner text-center">
                                <!-- Quote 1 -->
                                <div class="item active">
                                  <blockquote>
                                    <div class="row">
                                      <div class="col-sm-8 col-sm-offset-2">

                                        <p>

			                            Villa Amaya was just what we were looking for. It was close to the beach but also near a variety of restaurants, shopping areas, and nightlife. It was the perfect hideaway, clean, well lit, and spacious. We also loved having our own pool, for the days we didn't want to venture down to the beach in the heat. I would highly recommend this villa to anyone heading to Bali!

			                            </p>
                                        <small>Eve H</small>
                                      </div>
                                    </div>
                                  </blockquote>
                                </div>
                                <!-- Quote 2 -->
                                <div class="item">
                                  <blockquote>
                                    <div class="row">
                                      <div class="col-sm-8 col-sm-offset-2">

                                        <p>

			                            We stayed at Istana Samer for a week in October 2016 and I still can't get over how beautiful everything was. We literally felt like royalty for a week. The service, the architecture, the pool, the outside pool, the jacuzzi, the nature. It was one of the most beautiful experiences I've ever had. As soon as we got there, we realised we were actually really close to both Canggu and Seminyak and tons of cool places to eat (that's for when we weren't served breakfast in bed!!!)
			                            Everything super clean, the Balinese architecture is so impressive! there were 8 of us staying there and because of the massive space we had mini sunset drinks organised almost every evening! Dreamy setup, and super affordable if you think of how spoilt we all were!! 6 star service all around!!!!! Thank you!

			                            </p>
                                        <small>Raluca V</small>
                                      </div>
                                    </div>
                                  </blockquote>
                                </div>
                                <!-- Quote 3 -->
                                <div class="item">
                                  <blockquote>
                                    <div class="row">
                                      <div class="col-sm-8 col-sm-offset-2">

                                        <p>

			                            Had an amazing stay at Villa Dayah with my partner. There is a good mix of sunshine by the pool and shade undercover with lounges.
			                            Second level was my favourite, we stayed in the upstairs bedroom, which had really good air conditioning and a beautiful bathroom, and another lounge/tv area.
			                            Had a very relaxing time in the Villa, it is also nice and close to everything in seminyak!
			                            Highly recommend to anyone travelling to bali!

			                            </p>
                                        <small>Stephpacca</small>
                                      </div>
                                    </div>
                                  </blockquote>
                                </div>

                                <div class="item">
                                  <blockquote>
                                    <div class="row">
                                      <div class="col-sm-8 col-sm-offset-2">

                                        <p>

			                            We came for a wedding just north of Seminyak and researched every private villa in the vicinity. Most were overly cramp and not that appealing. Of course there are nice ones out there, but this villa peaked our interest. We brought our young 2 children and my parents, and everyone loved it. Lots of space, well maintained landscape. The V-shape layout facing the pool brings natural light and air to each room. Entire villa is very well designed and tastefully furnished. The owner(s) have a subtle aesthetic eye, fitting for the Bali lifestyle. Pool could use a bit of upkeep, but we may have visited right before the maintenance period, and we all enjoyed a prolonged dip nevertheless. Staff was incredibly friendly. Large breakfast was delicious, with lots of choices. Also ordered in individual massages, which we weren't expecting. All in all it was a great Bali experience. Canggu is beautiful also.

			                            </p>
                                        <small>Nadine R</small>
                                      </div>
                                    </div>
                                  </blockquote>
                                </div>

                                <div class="item">
                                  <blockquote>
                                    <div class="row">
                                      <div class="col-sm-8 col-sm-offset-2">

                                        <p>
			                         
			                            The Villa Aum is undoubtedly the most amazing place I have ever stayed, the view was absolutely stunning, you could not ask for a more perfect view! The villa was of the highest standard, very clean and stylish with friendly staff who were always attentive. All the rooms were spacious and you could easily fit a large group of people and not be cramped. The infinity pool was a definite favourite, but the villa also had private access stairs down the cliff to a private beach. The beach below was beautiful and very clean! This villa is located in a very quiet, relaxing area, absolute tranquility! I could not recommend this place enough!
			                      
			                            </p>
                                        <small>Tellarina</small>
                                      </div>
                                    </div>
                                  </blockquote>
                                </div>
                              </div>

                              <!-- Carousel Buttons Next/Prev -->
                              <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                              <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                            </div>
                          </div>
                        </div>
                      </div><!-- end reviews -->

                    </div><!-- end col -->

                    <div class="col-lg-4 col-md-4 sidebar sidebar-property-single">
                     <!-- end widget -->



                   </div><!-- end sidebar -->

                 </div><!-- end row -->




               </section>

               <section>
                <div id="contacts">
                  <div class="row">
                    <section id="footer-1" class="col-md-12" style="margin-bottom: 0px; padding: 0px">
                      <div id="foot1" class="col-sm-2">
                        <h3 class="text-center">Marketing By</h3>
                        <center><hr/></center>
                        <div class="logo-img">
                          <a target="_blank" href="http://www.balivillaportfolio.com">
                            <img src="http://www.totalbali.com/tb-home/assets/img/logo/balivillaportfolio.png" class="img-responsive">
                          </a>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <h3 class="text-center">Powered By</h3>
                        <center><hr/></center>
                        <div class="logo-img">
                          <a target="_blank" href="http://www.totalbali.com">
                            <img src="http://www.totalbali.com/tb-home/assets/img/logo/totalbali.png" class="img-responsive">
                          </a>
                        </div>
                      </div>

                      <div class="col-md-5">
                        <h3 class="text-center">Some Of Our Villas</h3>
                        <center><hr/></center>
                        <div class="col-md-12" style="text-align: center; font-size: 9pt">
                          <div class="col-md-4"><a target="_blank" href="http://www.villaamaya.com">Villa Amaya</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villaaramis.com">Villa Aramis</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villaaumbali.com">Villa Aum Bali</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villadayah.com">Villa Dayah</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villagorillabali.com">Villa Gorilla Bali</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.istanasemer.com">Villa Istana Semer</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.livinglightubud.com">Living Light Ubud</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villamonabali.com">Villa Mona Bali</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villatirtadari.com">Villa Tirtadari</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villaumalas.com">Villa Umalas</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://villaamita.com/">Villa Amita</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villamorobali.com">Villa Moro</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.themusevilla.com/">Villa The Muse</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://www.villareflectionscanggu.com">Villa Reflection Canggu</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://villakuta.com/">Villa Kuta</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://villavayana.com">Villa Vayana</a></div>
                          <div class="col-md-4"><a target="_blank" href="http://dev.totalbali.com/villatom/">Villa Tom</a></div>

                        </div>
                      </div>

                      <div id="foot2" class="col-md-3">
                        <h3 class="text-center">Affiliate Partner Pages</h3>
                        <center><hr/></center>
                        <a target="_blank" href="http://www.totalbalivillas.com"><p class="text-center">Total Bali Villas</p></a>
                        <a target="_blank" href="http://www.totalbaliactivities.com"><p class="text-center">Total Bali Activities</p></a>
                        <a target="_blank" href="http://www.totalbalidirect.com"><p class="text-center">Total Bali Direct</p></a>
                        <a target="_blank" href="http://www.indoinvestmets.com"><p class="text-center">Indo Investments</p></a>
                        <a target="_blank" href="http://www.balibio.com"><p class="text-center">Bali Bio</p></a>
                      </div>
                    </section>

                    <section id="footer-3" class="col-md-12" style="margin-top: 0px; padding: 0px">
                      <br>
                      <div class="col-md-10 col-md-offset-1" style="text-align: center;">
                        <p class="text-center" style="margin-top: 2em;">Exclusively Marketing the Bali Villa Portfolio A hand selected group of villas for your holiday Total Bali Direct is established to help villa owners.</p>
                        <p class="text-center">Office Phone: +6285100187444 | Whats Ap Phone: +6281338648034 | Email: <a href="mailto:totalbali@totalbali.com" target="_blank">totalbali@totalbali.com</a></p>
                      </div>
                    </section>
                    <?php } ?>
                  </div>
                </div>
              </section>
              <footer id="footer" style="background-color: #fff">
                <div class="container">
                  <div class="footer-text" style="margin-top: 15px; margin-bottom: 10px">
                    <p class="text-center" style="font-size: 13px;"> 2017 &copy;, Total Bali is a fully registered Indonesian company traded by PT Total Business Incorporated, <br/>
                      with over 25 years living in Indonesia, we ensure that you have a professional and accountable team at your disposal for all your Island needs.</p>
                    </div>
                  </div>
                </footer>


                <script>
                  var base_url = '<?php echo base_url();?>';
                </script>
                <script>
                  <?php foreach($villa as $v){?>
                    var latv = <?php echo $v->latitude; ?>;
                    var lngv = <?php echo $v->longitude; ?>;
                    <?php } ?>
                  </script>
                  <script>

                    function initMap() {
                      var myLatLng = {lat: latv, lng: lngv};



                      var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 15,
                        center: myLatLng,
                        scrollwheel: false,
                        styles: [
                        {
                          featureType: "poi",
                          stylers: [
                          { visibility: "off" }
                          ]
                        }
                        ]
                      });

                      var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: 'Hello World!'
                      });
                    }
                  </script>
                  <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3mZwc3HFNLCDiTsUTewW48l2kvYiQVAU&callback=initMap">
                </script>



                <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
                <script src="<?php echo base_url();?>assets/newhomepage/villadetail/js/jquery-3.1.1.min.js"></script>
                <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->

                <script src="<?php echo base_url();?>assets/newhomepage/villadetail/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url();?>assets/newhomepage/villadetail/js/wNumb.js"></script>
                <!-- <script src="assets/newhomepage/villadetail/js/map-single.js"></script> -->
                <script src="<?php echo base_url();?>assets/newhomepage/villadetail/js/global.js"></script>
                <script src="<?php echo base_url();?>assets/newhomepage/js/script.js"></script>
                <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
                <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

                <!--slider-->
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-41298642-1', 'auto');
                  ga('send', 'pageview');

                  $('input[name="daterange"]').daterangepicker({
                  });

                  <!--Modal-->
                  function openModal() {
                    document.getElementById('myModal').style.display = "block";
                  }

                  function closeModal() {
                    document.getElementById('myModal').style.display = "none";
                  }

                  var slideIndex = 1;
                  showSlides(slideIndex);

                  function plusSlides(n) {
                    showSlides(slideIndex += n);
                  }

                  function currentSlide(n) {
                    showSlides(slideIndex = n);
                  }

                  function showSlides(n) {
                    var i;
                    var slides = document.getElementsByClassName("mySlides");
                    var dots = document.getElementsByClassName("demo");
                    var captionText = document.getElementById("caption");
                    if (n > slides.length) {slideIndex = 1}
                      if (n < 1) {slideIndex = slides.length}
                        for (i = 0; i < slides.length; i++) {
                          slides[i].style.display = "none";
                        }
                        for (i = 0; i < dots.length; i++) {
                          dots[i].className = dots[i].className.replace(" active", "");
                        }
                        slides[slideIndex-1].style.display = "block";
                        dots[slideIndex-1].className += " active";
                        captionText.innerHTML = dots[slideIndex-1].alt;
                      }
                    </script>

                    <script async src="http://www.googletagservices.com/tag/js/gpt.js"> </script>

                    <!--slider-->
                    <script type='text/javascript' src='http://lifewithoutandy.com/wordpress/wp-content/themes/warhol/static/dist/js/9e81a7e5d3e8.global.min.js'></script>
                    <script type='text/javascript' src='http://lifewithoutandy.com/wordpress/wp-content/themes/warhol/static/dist/js/27323ae010fb.gallery.min.js'></script>
                    <!--slider-->
                  </body>
                  </html>
