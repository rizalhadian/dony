<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Total Bali-Livebalivillas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.min.css"> -->
  <!-- jQuery 3.1.1 -->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/skins/_all-skins.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/iCheck/all.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->
  <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>
  <?php if($this->session->userdata('ses') == "accommodations_addnew"){ ?>
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.css" media="all" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/fileuploader/src/jquery.fileuploader.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/fileuploader/examples/default/js/custom.js" type="text/javascript"></script>
  <?php } ?>
</head>



<body class="hold-transition skin-blue sidebar-mini" style="background-image: url('<?php echo base_url();?>assets/images/kintamani.jpg'); height: 100%; overflow-y:auto;">
<div class="wrappr" >
<!-- Navbar -->
<header class="main-header">
  <!-- Logo -->
  <a href="index2.html" class="logo" style="background-color: rgba(255,255,255, 1);height:56px; color:black;">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>LBV</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Livebalivillas</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" style="background-color: rgba(255,255,255, 1);height:56px;">
    <!-- Sidebar toggle button-->
    <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a> -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <!-- Tasks: style can be found in dropdown.less -->
        <!-- User Account: style can be found in dropdown.less -->


        <!-- Control Sidebar Toggle Button -->

        <!-- <li>

          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>

        </li> -->
      </ul>
    </div>
  </nav>
</header>

<?php




  // include('navbar.php');
?>



<!-- Sidebar -->
<?php
  // include('sidebar.php');
?>



<!-- Content -->
<!-- <img src="<?php echo base_url();?>assets/images/kintamani.jpg" class="img-fluid" alt=""> -->
<div class="content" >
<!-- <div class="conten" > -->
  <div class="row">
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="col-md-12">
      <div class="col-md-8" style="color:white;">

        <div class="col-md-9">

          <?php if($this->session->is_new_data == 1){ ?>
            <h1><b>Thank You, </b></h1><br>
            <h3>
              We already sent you a private link and unique key to complete your accommodations info.
            </h3>
          <?php }else if($this->session->is_new_data == 0){ ?>
            <?php if(!isset($this->session->is_new_data)){ ?>
              <h1><b>Be Our Partner</b></h1><br>
              <h3>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </h3>
            <?php }else{ ?>
              <h1><b>Sorry,</b></h1><br>
              <h3>
                Email that you inserted is already existed. We already resend your private link and unique key to complete your accommodations info.
              </h3>
            <?php } ?>
          <?php } ?>

        </div>

      </div>
      <div class="col-md-4">
        <div class="col-md-12" >
          <div class="col-lg-12 col-md-12 col-xs-12"  style="background-color: rgba(0, 0, 0, 0.3);">
            <br>
            <form enctype="multipart/form-data" name="formAccommodation" id="formAccommodation" action="<?php echo base_url(); ?>partner/registering_partner" method="POST">

            <div class="form-group col-lg-6 col-md-6">
              <label style="color:white;">First Name</label>
              <input type="text" name="name[]" class="form-control">
            </div>
            <div class="form-group col-lg-6 col-md-6">
              <label style="color:white;">Last Name</label>
              <input type="text" name="name[]" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label style="color:white;">Email</label>
              <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label style="color:white;">Phone Number</label><br>
              <div class="row">
                <div class="col-md-3 col-xs-3"><input type="text" name="phone[]" class="form-control"></div>
                <div class="col-md-9 col-xs-9"><input type="text" name="phone[]" class="form-control"></div>
              </div>

            </div>
            <div class="form-group col-lg-12 col-md-12">
              <label style="color:white;">What Type Business Are You?</label>
              <select class="form-control " name="business_type" style="width: 100%;">
                <option value="0">Villa</option>
                <option value="1">Villa Complex (Same Location with Multiple Villas)</option>
                <option value="2">Property Manager (Different Location with Multiple Villas)</option>
                <option disabled value="3">Hotel</option>
                <option disabled value="4">Hostel / Bed & Breakfast</option>
              </select>
            </div>

            <div class="form-group col-lg-12 col-md-12">
              <button type="submit" class="btn btn-primary btn-block btn-lg">Let Me Join With You</button>
            </div>

          </form>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>
<?php
  // include('content.php');
?>

<!-- Footer -->
<?php
  // include('footer.php');
?>
<div>


<script>
  var base_url = "<?php echo base_url();?>";
</script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

<?php if($this->session->userdata('ses') == "accommodations_addnew"){ ?>

    <!-- js -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script> -->

  <?php } ?>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>



<!-- Bootstrap 3.3.6 -->

<script src="<?php echo base_url();?>assets/admin/adminlte/bootstrap/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/admin/adminlte/dist/js/demo.js"></script>


<!-- page script -->
<script>
  $("[data-toggle=popover]").popover();

  $(function () {

    $("#example1").DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });


    $('#example3').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });


    $('#example4').DataTable({
      "paging": true,
      "lengthChange": true,
      "pageLength": 4,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

  });
</script>

<?php if($this->session->userdata('ses') == "services"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-services.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "facilities"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-facilities.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "experiences"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-experiences.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "locations"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-locations.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodationtypes"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodation-types.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "activitytypes"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-activity-types.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodations"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodations_addnew" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-addnew.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodations_update" ){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodations-update.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodation_rooms_addnew"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodation-rooms.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "accommodation_room_prices_addnew"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-accommodation-room-prices.js"></script>
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
    $('#datepicker2').datepicker({
      autoclose: true
    });
  });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "activities"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-activities.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "partners"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-partners.js"></script>
<?php } ?>
<?php if($this->session->userdata('ses') == "availability"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-availability.js"></script>
  <!-- availability css -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/totalbali-availability.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.min.css">
  <!-- datepicker -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/adminlte/plugins/datepicker/datepicker3.css">
  <script>
  $(function(){
    $('#datepicker1').datepicker({
      autoclose: true
    });
  });
  </script>
  <!-- Select2 -->
  <script src="<?php echo base_url();?>assets/admin/adminlte/plugins/select2/select2.full.min.js"></script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });
  </script>
<?php } ?>
<?php if($this->session->userdata('ses') == "activities_create"){ ?>
  <script src="<?php echo base_url();?>assets/js/totalbali-activities-create.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnDSTVpzSHWzMAy6oVswtIvn-Ju3YVnOI&callback=initMap">
  </script>
<?php } ?>

<script src="<?php echo base_url();?>assets/admin/adminlte/plugins/iCheck/icheck.min.js"></script>
<script>
$(function(){
  //iCheck for checkbox and radio inputs
  new $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });
});
</script>
<script>
$(function(){
  //iCheck for checkbox and radio inputs
  new $('input[type="checkbox"].minimal-1, input[type="radio"].minimal-1').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });
});
</script>




</body>

</html>
