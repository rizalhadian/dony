<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminAccommodations extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('MAccessWithoutAuth');

		if (!$this->ion_auth->logged_in()){
			if(!$this->MAccessWithoutAuth->hasUniqueId()){
				$this->session->set_flashdata('error', 1);
				$this->session->set_flashdata('message', 'You must be an admin to view this page');
				redirect('/auth/login');
			}
    }

		$this->load->model('Ion_auth_model');
		$this->session->set_userdata('ses', 'accommodations');
		$this->load->model('MAccommodationTypes');
		$this->load->model('MAccommodations');
		$this->load->model('MAccommodationsNew');
		$this->load->model('MAccommodationRooms');
		$this->load->model('MAccommodationRoomsNew');
		$this->load->model('MAccommodationRoomPrices');
		$this->load->model('MServices');
		$this->load->model('MLocations');
		$this->load->model('MFacilities');
		$this->load->model('MExperiences');
		$this->load->model('MRates');
		$this->load->model('MPhotos');
		$this->load->model('MPartners');
		$this->load->model('MAdditionalRates');
		$this->load->model('MServices');
		$this->load->model('MMAccommodationsFacilities');
		$this->load->model('MMAccommodationsAdditionalRates');
		$this->load->model('MMAccommodationsExperiences');


		$this->load->model('MOTAAirbnb');

		$this->load->library('Tripadvisor');

	}

	public function index(){
		$data['accommodations'] = $this->MAccommodationsNew->readAll();
		$data['accommodations_need_approval'] = $this->MAccommodationsNew->readAllNeedApproval();
		// $data['rooms'] = $this->MAccommodationRooms->read_all();
		// $data['prices'] = $this->MAccommodationRoomPrices->read_all();
		$data['countAccommodations'] = $this->MAccommodationsNew->count();
		// print_r($data['countAccommodations']);
		// $data['countRooms'] = $this->MAccommodationRooms->count();
		// $data['countPrices'] = $this->MAccommodationRoomPrices->count();
		$accommodations = $data['accommodations'];
		$accommodations_need_approval = $data['accommodations_need_approval'];
		$arrayRooms = array();
		$arrayLocations = array();
		for($accommodation = 0; $accommodation < sizeof($accommodations); $accommodation++){
			$rooms = $this->MAccommodationRooms->read_by_accommodations_id($accommodations[$accommodation]->id);
			$locations = $this->MLocations->read_include_all_parents_by_id($accommodations[$accommodation]->locations_id);

			for($room = 0; $room < sizeof($rooms); $room++){
				array_push($arrayRooms, $rooms[$room]->total_rooms);
			}
			for($location = 0; $location < sizeof($locations); $location++){
				array_push($arrayLocations, $locations[$location]->name);
			}

			$accommodations[$accommodation]->rooms = $arrayRooms;
			$arrayRooms = array();
			$accommodations[$accommodation]->locations = $arrayLocations;
			$arrayLocations = array();
		}


		for($accommodation = 0; $accommodation < sizeof($accommodations_need_approval); $accommodation++){
			$rooms = $this->MAccommodationRooms->read_by_accommodations_id($accommodations_need_approval[$accommodation]->id);
			$locations = $this->MLocations->read_include_all_parents_by_id($accommodations_need_approval[$accommodation]->locations_id);

			for($room = 0; $room < sizeof($rooms); $room++){
				array_push($arrayRooms, $rooms[$room]->total_rooms);
			}
			for($location = 0; $location < sizeof($locations); $location++){
				array_push($arrayLocations, $locations[$location]->name);
			}

			$accommodations_need_approval[$accommodation]->rooms = $arrayRooms;
			$arrayRooms = array();
			$accommodations_need_approval[$accommodation]->locations = $arrayLocations;
			$arrayLocations = array();
		}

		// print_r($data);

		$this->load->view('/admin/index', $data);


  }

	public function add_new(){
		$this->session->set_userdata('ses', 'accommodations_addnew');
		$data['accommodation_types'] = $this->MAccommodationTypes->read_all();
		$data['services'] = $this->MServices->readAll();
		$data['facilities'] = $this->MFacilities->readAllFacilities();
		$data['amenities'] = $this->MFacilities->readAllAmenities();
		$data['additional_rates'] = $this->MAdditionalRates->readAll();
		$data['experiences'] = $this->MExperiences->read_all();
		$this->load->view('/admin/index', $data);
  }



	public function add_new_by_owner(){
		$this->session->set_userdata('ses', 'accommodations_addnew_by_owner');
		$data['accommodation_types'] = $this->MAccommodationTypes->read_all();
		$data['services'] = $this->MServices->read_all();
		$data['facilities'] = $this->MFacilities->read_all();
		$this->load->view('/admin/index', $data);
  }

	public function create(){
		$data_posted = $this->input->post();

		// Create for private accommodation
		if($this->isPrivateAccommodation()){
			//Create Accommodation
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->name = $data_posted['name'];
			$accommodation->accommodation_types_id = $data_posted['accommodation_types_id'];
			if(isset($data_posted['class'])){
				$accommodation->class = $data_posted['class'];
			}
			if(isset( $data_posted['management'])){
				$accommodation->management = $data_posted['management'];
			}else{
				$accommodation->management = 0;
			}
			if(isset( $data_posted['accommodation_complex_id'])){
				$accommodation->accommodation_complex_id = $data_posted['accommodation_complex_id'];
			}

			$accommodation->partners_id = $data_posted['partners_id'];
			$accommodation->description = $data_posted['description'];
			if(isset($data_posted['tags'])){
				$accommodation->tags = $data_posted['tags'];
			}

			if(isset($data_posted['locations_id'])){
				if($data_posted['locations_id'] != ""){
					$accommodation->locations_id = $data_posted['locations_id'];
				}else{
					$loc = $this->readLocationLike($data_posted['locations_name']);
					if(isset($loc->value)){
						$accommodation->locations_id = $loc->value;
					}else{
						$accommodation->locations_id = 1;
					}
				}
			}

			$location = new $this->MLocations;
			$location->id = $data_posted['locations_id'];
			$response_location = $location->readById();

			$accommodation->address = $data_posted['address'];
			$accommodation->lat = $data_posted['lat'];
			$accommodation->lng = $data_posted['lng'];
			$accommodation->total_rooms = $data_posted['total_bedrooms'];
			$accommodation->code =  strtolower($response_location[0]->name)."-".$data_posted['total_bedrooms']."-bedrooms-villa-".rand(0, 999);
			$accommodation->total_bathrooms = $data_posted['total_bathrooms'];
			$accommodation->total_beds = $data_posted['total_beds'];
			$accommodation->ics_url = $data_posted['ics_url'];
			$accommodation->website = $data_posted['website'];
			$accommodation->chanel_booking_com = $data_posted['chanel_booking_com'];
			$accommodation->chanel_airbnb_com = $data_posted['chanel_airbnb_com'];
			$accommodation->chanel_flipkey_com = $data_posted['chanel_flipkey_com'];
			$accommodation->chanel_other = $data_posted['chanel_other'];
			$accommodation->youtube = $data_posted['youtube'];
			$accommodation->facebook = $data_posted['facebook'];
			$accommodation->instagram = $data_posted['instagram'];
			$accommodation->include_tax = $data_posted['include_tax'];
			$accommodation->tax_percentage = $data_posted['tax_percentage'];
			$accommodation->active = 2;
			$accommodation->renting_type = $data_posted['renting_type'];
			$accommodation->building_size = $data_posted['building_size'];
			$accommodation->land_size = $data_posted['land_size'];
			$accommodation->created_on = date('Y-m-d H:i:s');
			$accommodation_response = $accommodation->create();

			// print_r($accommodation_response);
			$this->id = $accommodation_response->id;



			// $accommodation_type = new $this->MAccommodationTypes;
			// $accommodation_type->accommodation_types_id = $data_posted['accommodation_types_id'];
			// $response_accommodation_type = $accommodation_type->readById();

			// print_r($response_accommodation_type);

			//Create room
			$room = new $this->MAccommodationRoomsNew;
			$room->name = $accommodation_response->name." ".$accommodation_response->total_rooms." bedrooms ";
			$room->total_rooms = $accommodation_response->total_rooms;

			if(isset($response_location[0])){
				$room->code = $response_location[0]->name."-".$accommodation_response->total_rooms."-bedrooms-".rand(0, 999);
			}else{
				$room->code = null;
			}

			$room->total_beds = $accommodation_response->total_beds;
			$room->total_bathrooms = $accommodation_response->total_bathrooms;
			$room->min_stay = $data_posted['min_stay'];
			$room->max_guest_normal = $data_posted['max_guest_normal'];
			$room->max_guest_extra = $data_posted['max_guest_extra'];
			$room->ics_url = $accommodation_response->ics_url;
			$room->ics = $accommodation_response->ics;
			$room->description = $accommodation_response->description;
			$room->tags = $accommodation_response->tags;
			$room->accommodations_id = $accommodation_response->id;
			$room->renting_type = $accommodation_response->renting_type;
			$room->created_on = date('Y-m-d H:i:s');
			$room->active = 2;
			$room->synced = 0;
			$room_response = $room->create();

			// Update Prices / Rates
			$rates_ids = explode(',', $data_posted['rates_ids']);

			$rates_response = array();
			foreach($rates_ids as $rate_id){
				$rate = new $this->MRates;
				$rate->id = $rate_id;
				$rate->accommodations_id = $accommodation_response->id;
				$rate->accommodation_rooms_id = $room_response->id;
				$rate_response = $rate->update_ids_only();
				array_push($rates_response, $rate_response);

			}

			// print_r($rates_response);

		}
		// End of create for private accommodation

		if($this->isPrivateRoom()){

		}

		// Create for flexible accommodation
		if($this->isFlexibleAccommodation()){
			$location = new $this->MLocations;
			$location->id = $data_posted['locations_id'];
			$response_location = $location->readById();

			//Create Accommodation
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->name = $data_posted['name'];
			$accommodation->accommodation_types_id = $data_posted['accommodation_types_id'];
			if(isset($data_posted['class'])){
				$accommodation->class = $data_posted['class'];
			}
			if(isset( $data_posted['management'])){
				$accommodation->management = $data_posted['management'];
			}else{
				$accommodation->management = 0;
			}
			if(isset($data_posted['partners_id'])){
				$accommodation->partners_id = $data_posted['partners_id'];
			}
			$accommodation->description = $data_posted['description'];
			if(isset($data_posted['tags'])){
				$accommodation->tags = $data_posted['tags'];
			}
			if(isset( $data_posted['accommodation_complex_id'])){
				$accommodation->accommodation_complex_id = $data_posted['accommodation_complex_id'];
			}

			if(isset($data_posted['locations_id'])){
				if($data_posted['locations_id'] != ""){
					$accommodation->locations_id = $data_posted['locations_id'];
				}else{
					$loc = $this->readLocationLike($data_posted['locations_name']);
					if(isset($loc->value)){
						$accommodation->locations_id = $loc->value;
					}else{
						$accommodation->locations_id = 1;
					}
				}
			}

			$location = new $this->MLocations;
			$location->id = $data_posted['locations_id'];
			$response_location = $location->readById();

			$accommodation->total_rooms = $data_posted['flexible_total_rooms'];
			$accommodation->total_bathrooms = $data_posted['flexible_total_bathrooms'];
			$accommodation->total_beds = $data_posted['flexible_total_beds'];
			// $accommodation->locations_id = $data_posted['locations_id'];
			$accommodation->code =  strtolower($response_location[0]->name)."-".$data_posted['flexible_total_beds']."-bedrooms-villa-".rand(0, 999);
			$accommodation->address = $data_posted['address'];
			$accommodation->lat = $data_posted['lat'];
			$accommodation->lng = $data_posted['lng'];
			$accommodation->building_size = $data_posted['flexible_building_size'];
			$accommodation->land_size = $data_posted['flexible_land_size'];
			// $accommodation->code =  strtolower($response_location[0]->name)."-".$data_posted['total_bedrooms']."-bedrooms-villa-".rand(0, 999);
			$accommodation->ics_url = $data_posted['ics_url'];
			$accommodation->website = $data_posted['website'];
			$accommodation->chanel_booking_com = $data_posted['chanel_booking_com'];
			$accommodation->chanel_airbnb_com = $data_posted['chanel_airbnb_com'];
			$accommodation->chanel_flipkey_com = $data_posted['chanel_flipkey_com'];
			$accommodation->chanel_other = $data_posted['chanel_other'];
			$accommodation->youtube = $data_posted['youtube'];
			$accommodation->facebook = $data_posted['facebook'];
			$accommodation->instagram = $data_posted['instagram'];
			$accommodation->include_tax = $data_posted['include_tax'];
			$accommodation->tax_percentage = $data_posted['tax_percentage'];
			$accommodation->active = 2;
			$accommodation->renting_type = $data_posted['renting_type'];
			$accommodation->created_on = date('Y-m-d H:i:s');
			$accommodation_response = $accommodation->create();

			// print_r($accommodation_response);

			//Update Rooms
			$rooms_ids = explode(',', $data_posted['accommodation_rooms_ids']);
			$room_responses = array();
			for($rooms_id = 0; $rooms_id < sizeof($rooms_ids); $rooms_id++){
				$room = new $this->MAccommodationRoomsNew;
				$room->id = $rooms_ids[$rooms_id];
				$room->accommodations_id = $accommodation_response->id;
				$room_response = $room->updateAccommodationsId();
				array_push($room_responses, $room_response);
			}
			//


		}
		// End of Create for flexible accommodation

		//Create MM_Additional_Rates
		if(isset($data_posted['additional_rates'])){
			for($additional_rate = 0; $additional_rate < sizeof($data_posted['additional_rates']); $additional_rate++){
				$mm_acc_ar = $this->MMAccommodationsAdditionalRates;
				$mm_acc_ar->accommodations_id = $accommodation_response->id;
				$mm_acc_ar->additional_rates_id = $data_posted['additional_rates'][$additional_rate];
				$mm_acc_ar->rate = $data_posted['additional_rates_rate_'.$data_posted['additional_rates'][$additional_rate]];
				$mm_acc_ar->metric_unit = $data_posted['metric_unit_'.$data_posted['additional_rates'][$additional_rate]];
				$mm_acc_ar->created_on = date('Y-m-d H:i:s');
				$mm_acc_ar->create();
			}
		}

		//Create MM_Accommodations_Facilities
		if(isset($data_posted['facilities'])){
			for($facility = 0; $facility < sizeof($data_posted['facilities']); $facility++){
				$mm_acc_fac = $this->MMAccommodationsFacilities;
				$mm_acc_fac->accommodations_id = $accommodation_response->id;
				$mm_acc_fac->facilities_id = $data_posted['facilities'][$facility];
				$mm_acc_fac->created_on = date('Y-m-d H:i:s');
				$mm_acc_fac->create();
			}
		}

		//Create MM_Accommodations_Experiences
		if(isset($data_posted['experiences'])){
			for($experience = 0; $experience < sizeof($data_posted['experiences']); $experience++){
				$mm_acc_ex = $this->MMAccommodationsExperiences;
				$mm_acc_ex->accommodations_id = $accommodation_response->id;
				$mm_acc_ex->experiences_id = $data_posted['experiences'][$experience];
				$mm_acc_ex->created_on = date('Y-m-d H:i:s');
				$mm_acc_ex->create();
			}
		}

		//Update Photos
		if(isset($data_posted['photoIds']) && $data_posted['photoIds'] != ""){
			$photoIdsArr = explode(',', $data_posted['photoIds']);
			foreach($photoIdsArr as $photoId){
				$photo = $this->MPhotos;
				$photo->id = $photoId;
				$photo->ordinal = $data_posted['photo_ordinal'][$photoId];
				$photo->title = $data_posted['photo_title'][$photoId];
				$photo->type = $data_posted['photo_type'][$photoId];
				$photo->accommodations_id = $accommodation_response->id;
				print_r($photo->update());
			}
		}

		//

		// Redirect
		if(isset($data_posted['unique_id'])){
			if(isset($data_posted['is_single'])){
				redirect('/partner/update/'.$data_posted['unique_id'].'?finish', 'refresh');
			}else{
				redirect('/partner/update/'.$data_posted['unique_id'].'?accommodations', 'refresh');
			}
		}else{
			redirect('/admin/accommodations', 'refresh');
		}
	}

	private function isPrivateAccommodation(){
		$data_posted = $this->input->post();
		if($data_posted['renting_type'] == 0){
			return true;
		}else{
			return false;
		}
	}

	private function isPrivateRoom(){
		$data_posted = $this->input->post();
		if($data_posted['renting_type'] == 1){
			return true;
		}else{
			return false;
		}
	}

	private function isFlexibleAccommodation(){
		$data_posted = $this->input->post();
		if($data_posted['renting_type'] == 2){
			return true;
		}else{
			return false;
		}
	}



	public function read_id(){
		$id = $this->input->post("id");
		$response = $this->MAccommodations->read_id($id);
		echo json_encode($response);
	}

	public function read_like(){
		$word = $this->input->post('word');
		$response = $this->MAccommodations->read_like($word);
		echo json_encode($response);
	}

	public function update($id){
		$accommodation = new $this->MAccommodationsNew;
		$accommodation->id = $id;
		//Get Rooms
		$rooms = new $this->MAccommodationRoomsNew;
		$rooms->accommodations_id = $id;

		$data['accommodation'] = $accommodation->readById();
		$data['accommodation']->rooms = $rooms->readByAccommodationsId();

		//Get Partner Name
		if($data['accommodation']->partners_id != 0 && $data['accommodation']->partners_id != null && $data['accommodation']->partners_id != ""){
			$partner = new $this->MPartners;
			$partner->id = $data['accommodation']->partners_id;
			$data['accommodation']->partner = $partner->readById();
		}

		// Get rates for each room
		for($room = 0; $room < sizeof($data['accommodation']->rooms); $room++){
			$rates = new $this->MRates;
			$rates->accommodation_rooms_id = $data['accommodation']->rooms[$room]->id;
			$rates_response = $rates->readByAccommodationRoomsId();
			$data['accommodation']->rooms[$room]->rates = $rates_response;
		}

		// Get Photos
		$photos = $this->MPhotos;
		$photos->accommodations_id = $id;
		$photos_responses = $photos->readByAccommodationsId();
		$data['accommodation']->photos = $photos_responses;

		//Get Experience
		$experiences = $this->MMAccommodationsExperiences;
		$experiences->accommodations_id = $id;
		$experiences_response = $experiences->readByAccommodationsId();
		$data['had_experiences'] = $experiences_response;

		//Get Facilities & Amenities
		$facilities = $this->MMAccommodationsFacilities;
		$facilities->accommodations_id = $id;
		$facilities_response = $facilities->readByAccommodationsId();
		$data['had_facilities'] = $facilities_response;

		//Get Additional Rates
		$additional_rates = $this->MMAccommodationsAdditionalRates;
		$additional_rates->accommodations_id = $id;
		$additional_rates_response = $additional_rates->readByAccommodationsId();
		$data['had_additional_rates'] = $additional_rates_response;





		$data['facilities'] = $this->MFacilities->readAllFacilities();
		$data['amenitites'] = $this->MFacilities->readAllAmenities();
		$data['experiences'] = $this->MExperiences->read_all();
		$data['additional_rates'] = $this->MAdditionalRates->readAll();
		$this->session->set_userdata('ses', 'accommodations_update');
		$data['accommodation_types'] = $this->MAccommodationTypes->read_all();
		$this->load->view('/admin/index', $data);
		// print_r($data);

	}

	public function updatePost(){
		$data_posted = $this->input->post();
		print_r($data_posted);

		if($this->isPrivateAccommodation()){
			//Update Accommodation
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->id = $data_posted['id'];
			$accommodation->name = $data_posted['name'];
			$accommodation->accommodation_types_id = $data_posted['accommodation_types_id'];
			if(isset($data_posted['class'])){
				$accommodation->class = $data_posted['class'];
			}
			$accommodation->renting_type = $data_posted['renting_type'];
			if(isset($data_posted['management'])){
				$accommodation->management = $data_posted['management'];
			}
			if(isset($data_posted['partners_id'])){
				$accommodation->partners_id = $data_posted['partners_id'];
			}
			$accommodation->description = $data_posted['description'];
			if(isset($data_posted['tags'])){
				$accommodation->tags = $data_posted['tags'];
			}

			if(isset($data_posted['locations_id'])){
				if($data_posted['locations_id'] != ""){
					$accommodation->locations_id = $data_posted['locations_id'];
				}else{
					$loc = $this->readLocationLike($data_posted['locations_name']);
					if(isset($loc->value)){
						$accommodation->locations_id = $loc->value;
					}else{
						$accommodation->locations_id = 1;
					}
				}
			}

			$location = new $this->MLocations;
			$location->id = $data_posted['locations_id'];
			$response_location = $location->readById();

			// $accommodation->code =  strtolower($response_location[0]->name)."-".$data_posted['total_beds']."-bedrooms-villa-".rand(0, 999);
			$accommodation->address = $data_posted['address'];
			$accommodation->lat = $data_posted['lat'];
			$accommodation->lng = $data_posted['lng'];
			$accommodation->total_rooms = $data_posted['total_rooms'];
			$accommodation->total_beds = $data_posted['total_beds'];
			$accommodation->total_bathrooms = $data_posted['total_bathrooms'];
			$accommodation->ics_url = $data_posted['ics_url'];
			$accommodation->website = $data_posted['website'];
			$accommodation->chanel_booking_com = $data_posted['chanel_booking_com'];
			$accommodation->chanel_airbnb_com = $data_posted['chanel_airbnb_com'];
			$accommodation->chanel_flipkey_com = $data_posted['chanel_flipkey_com'];
			$accommodation->chanel_other = $data_posted['chanel_other'];
			$accommodation->youtube = $data_posted['youtube'];
			$accommodation->facebook = $data_posted['facebook'];
			$accommodation->instagram = $data_posted['instagram'];
			$accommodation->include_tax = $data_posted['include_tax'];
			$accommodation->land_size = $data_posted['land_size'];
			$accommodation->building_size = $data_posted['building_size'];
			$accommodation->tax_percentage = $data_posted['tax_percentage'];
			$accommodation_response = $accommodation->update();

			if($data_posted['rates_ids'] != ""){
				$rates_ids = explode(',', $data_posted['rates_ids']);
				$rates_response = array();
				foreach($rates_ids as $rate_id){
					$rate = new $this->MRates;
					$rate->id = $rate_id;
					$rate->accommodations_id = $data_posted['id'];
					$rate->accommodation_rooms_id = $data_posted['accommodation_rooms_id'];
					$rate_response = $rate->update_ids_only();
					array_push($rates_response, $rate_response);
				}
			}

			// Update Accommodation Rooms
			$room = $this->MAccommodationRoomsNew;
			$room->id = $data_posted['accommodation_rooms_id'];
			$room->total_rooms = $data_posted['total_rooms'];
			$room->total_bathrooms = $data_posted['total_bathrooms'];
			$room->total_beds = $data_posted['total_beds'];
			$room->max_guest_normal = $data_posted['max_guest_normal'];
			$room->max_guest_extra = $data_posted['max_guest_extra'];
			$room->min_stay = $data_posted['min_stay'];
			$room->accommodations_id = $data_posted['id'];
			$room->renting_type = 0;
			$room->description = $data_posted['description'];
			if(isset($data_posted['tags'])){
				$room->tags = $data_posted['tags'];
			}
			$room_response = $room->update();
		}

		if($this->isFlexibleAccommodation()){
			//Update Accommodation
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->id = $data_posted['id'];
			$accommodation->name = $data_posted['name'];
			$accommodation->accommodation_types_id = $data_posted['accommodation_types_id'];

			if(isset($data_posted['class'])){
				$accommodation->class = $data_posted['class'];
			}
			$accommodation->renting_type = $data_posted['renting_type'];
			if(isset($data_posted['management'])){
				$accommodation->management = $data_posted['management'];
			}
			if(isset($data_posted['partners_id'])){
				$accommodation->partners_id = $data_posted['partners_id'];
			}
			$accommodation->description = $data_posted['description'];
			if(isset($data_posted['tags'])){
				$accommodation->tags = $data_posted['tags'];
			}

			if(isset($data_posted['locations_id'])){
				if($data_posted['locations_id'] != ""){
					$accommodation->locations_id = $data_posted['locations_id'];
				}else{
					$loc = $this->readLocationLike($data_posted['locations_name']);
					if(isset($loc->value)){
						$accommodation->locations_id = $loc->value;
					}else{
						$accommodation->locations_id = 1;
					}
				}
			}

			$location = new $this->MLocations;
			$location->id = $data_posted['locations_id'];
			$response_location = $location->readById();

			// $accommodation->code =  strtolower($response_location[0]->name)."-".$data_posted['flexible_total_beds']."-bedrooms-villa-".rand(0, 999);
			$accommodation->address = $data_posted['address'];
			$accommodation->lat = $data_posted['lat'];
			$accommodation->lng = $data_posted['lng'];
			$accommodation->total_rooms = $data_posted['flexible_total_rooms'];
			$accommodation->total_beds = $data_posted['flexible_total_beds'];
			$accommodation->total_bathrooms = $data_posted['flexible_total_bathrooms'];
			$accommodation->land_size = $data_posted['flexible_land_size'];
			$accommodation->building_size = $data_posted['flexible_building_size'];
			$accommodation->ics_url = $data_posted['ics_url'];
			$accommodation->website = $data_posted['website'];
			$accommodation->chanel_booking_com = $data_posted['chanel_booking_com'];
			$accommodation->chanel_airbnb_com = $data_posted['chanel_airbnb_com'];
			$accommodation->chanel_flipkey_com = $data_posted['chanel_flipkey_com'];
			$accommodation->chanel_other = $data_posted['chanel_other'];
			$accommodation->youtube = $data_posted['youtube'];
			$accommodation->facebook = $data_posted['facebook'];
			$accommodation->instagram = $data_posted['instagram'];
			$accommodation->include_tax = $data_posted['include_tax'];
			$accommodation->tax_percentage = $data_posted['tax_percentage'];
			$accommodation_response = $accommodation->update();

			//Update Rooms
			if($data_posted['accommodation_rooms_ids'] != "" && $data_posted['accommodation_rooms_ids'] != null){
				$rooms_ids = explode(',', $data_posted['accommodation_rooms_ids']);
				$room_responses = array();
				for($rooms_id = 0; $rooms_id < sizeof($rooms_ids); $rooms_id++){
					$room = new $this->MAccommodationRoomsNew;
					$room->id = $rooms_ids[$rooms_id];
					$room->accommodations_id = $accommodation->id;
					$room_response = $room->updateAccommodationsId();
					array_push($room_responses, $room_response);
				}
			}
		}

		// Update MM_Additional_Rates
		if(isset($data_posted['additional_rates'])){
			// Delete All MM_Additional_Rates by accomodations_id
			$mm_acc_ar = $this->MMAccommodationsAdditionalRates;
			$mm_acc_ar->accommodations_id = $accommodation->id;
			$mm_acc_ar->deleteByAccommodationsId();
			// Recerate MM_Additional_Rates
			for($additional_rate = 0; $additional_rate < sizeof($data_posted['additional_rates']); $additional_rate++){
				$mm_acc_ar = null;
				$mm_acc_ar = $this->MMAccommodationsAdditionalRates;
				$mm_acc_ar->accommodations_id = $accommodation->id;
				$mm_acc_ar->additional_rates_id = $data_posted['additional_rates'][$additional_rate];
				$mm_acc_ar->rate = $data_posted['additional_rates_rate_'.$data_posted['additional_rates'][$additional_rate]];
				$mm_acc_ar->metric_unit = $data_posted['metric_unit_'.$data_posted['additional_rates'][$additional_rate]];
				$mm_acc_ar->created_on = date('Y-m-d H:i:s');
				$mm_acc_ar->create();
			}
		}else{
			// Delete All MM_Additional_Rates by accomodations_id
			$mm_acc_ar = $this->MMAccommodationsAdditionalRates;
			$mm_acc_ar->accommodations_id = $accommodation->id;
			$mm_acc_ar->deleteByAccommodationsId();
		}

		// Update MM_Accommodations_Facilities
		if(isset($data_posted['facilities'])){
			// Delete All MM_Accommodations_Facilities by accomodations_id
			$mm_acc_fac = $this->MMAccommodationsFacilities;
			$mm_acc_fac->accommodations_id = $accommodation->id;
			$mm_acc_fac->deleteByAccommodationsId();
			// Recerate MM_Accommodations_Facilities
			for($facility = 0; $facility < sizeof($data_posted['facilities']); $facility++){
				$mm_acc_fac = $this->MMAccommodationsFacilities;
				$mm_acc_fac->accommodations_id = $accommodation->id;
				$mm_acc_fac->facilities_id = $data_posted['facilities'][$facility];
				$mm_acc_fac->created_on = date('Y-m-d H:i:s');
				$mm_acc_fac->create();
			}
		}else{
			// Delete All MM_Accommodations_Facilities by accomodations_id
			$mm_acc_fac = $this->MMAccommodationsFacilities;
			$mm_acc_fac->accommodations_id = $accommodation->id;
			$mm_acc_fac->deleteByAccommodationsId();
		}

		// Update MM_Accommodations_Experiences
		if(isset($data_posted['experiences'])){
			// Delete All MM_Accommodations_Experiences by accommodations_id
			$mm_acc_ex = $this->MMAccommodationsExperiences;
			$mm_acc_ex->accommodations_id = $accommodation->id;
			$mm_acc_ex->deleteByAccommodationsId();
			// Recreate MM_Accommodations_Experiences
			for($experience = 0; $experience < sizeof($data_posted['experiences']); $experience++){
				$mm_acc_ex = $this->MMAccommodationsExperiences;
				$mm_acc_ex->accommodations_id = $accommodation->id;
				$mm_acc_ex->experiences_id = $data_posted['experiences'][$experience];
				$mm_acc_ex->created_on = date('Y-m-d H:i:s');
				$mm_acc_ex->create();
			}
		}else{
			// Delete All MM_Accommodations_Experiences by accommodations_id
			$mm_acc_ex = $this->MMAccommodationsExperiences;
			$mm_acc_ex->accommodations_id = $accommodation->id;
			$mm_acc_ex->deleteByAccommodationsId();
		}

		//Update Photos
		if(isset($data_posted['photo_ids_old']) && $data_posted['photo_ids_old'] != ""){
			$photoIdsArr = explode(',', $data_posted['photo_ids_old']);
			foreach($photoIdsArr as $photoId){
				$photo = new $this->MPhotos;
				$photo->id = $photoId;
				$photo->ordinal = $data_posted['photo_ordinal'][$photoId];
				$photo->title = $data_posted['photo_title'][$photoId];
				$photo->type = $data_posted['photo_type'][$photoId];
				$photo->accommodations_id = $data_posted['id'];
				$photo->update();
			}
		}
		if(isset($data_posted['photoIds']) && $data_posted['photoIds'] != ""){
			$photoIdsArr = explode(',', $data_posted['photoIds']);
			foreach($photoIdsArr as $photoId){
				$photo = $this->MPhotos;
				$photo->id = $photoId;
				if(isset($data_posted['photo_ordinal'][$photoId])){
					$photo->ordinal = $data_posted['photo_ordinal'][$photoId];

				}
				if(isset($data_posted['photo_title'][$photoId])){
					$photo->title = $data_posted['photo_title'][$photoId];
				}
				if(isset($data_posted['photo_type'][$photoId])){
					$photo->type = $data_posted['photo_type'][$photoId];
				}
				$photo->accommodations_id = $data_posted['id'];
				$photo->update();
			}
		}

		if(isset($data_posted['photo_ordinal'][0])){

		}
		if(isset($data_posted['photo_title'][0])){

		}
		if(isset($data_posted['photo_type'][0])){

		}

		// print_r($data_posted);

		if(isset($data_posted['unique_id'])){
			if(isset($data_posted['single_accommodation'])){
				$this->session->set_flashdata('updated', 1);
				$this->session->set_userdata('ses', 'accommodations_update');
				redirect('/partner/update-accommodation/'.$data_posted['unique_id'].'');
			}else{
				redirect('/partner/update/'.$data_posted['unique_id'].'?accommodations', 'refresh');
			}
		}else{
			redirect('/admin/accommodations', 'refresh');
		}

	}

	public function delete(){
		$id = $this->input->post('id');
		$deleteAccommodation =  $this->MAccommodations->delete($id);
		echo json_encode($deleteAccommodation);
	}

	public function sync(){
		// $id = $this->input->post('id');
		$id = 38;
		//Get the accommodation datas
		$accommodation = new $this->MAccommodationsNew;
		$accommodation->id = $id;
		$accommodation = $accommodation->readById();
		//

		//Get locations
		$array_locations = array();
		$locations = $this->MLocations->read_include_all_parents_by_id($accommodation->locations_id);

		for($location = 0; $location < sizeof($locations); $location++){
			array_push($array_locations, $locations[$location]->name);
		}
		$accommodation->locations = $array_locations;
		//

		//Get the room settings datas
		$rooms = new $this->MAccommodationRoomsNew;
		$rooms->accommodations_id = $id;
		$rooms = $rooms->readByAccommodationsId();
		$accommodation->rooms = $rooms;

		//Airbnb Sync
		// foreach($accommodation->rooms as $room){
		// 	$locations = "";
		// 	foreach($accommodation->locations as $location){
		// 		$locations .= $location.", ";
		// 	}
		//
		// 	$airbnb = new $this->MOTAAirbnb;
		// 	$airbnb->room_type_category = "entire_home";
		// 	$airbnb->property_type_id = 11;
		// 	$airbnb->bathrooms = $room->total_bathrooms;
		// 	$airbnb->bedrooms = $room->total_rooms;
		// 	$airbnb->beds = $room->total_beds;
		// 	$airbnb->person_capacity = $room->max_guest_normal;
		// 	$airbnb->city = $locations;
		//
		// 	if($room->airbnb_id == ""){
		// 		// Create Empty Listing
		// 		$empty_listing_json = $airbnb->createEmptyListing();
		// 		$empty_listing = json_decode($empty_listing_json);
		// 		//
		// 		// Update Airbnb Id on Room
		// 		$airbnb->id = $empty_listing->listing->id;
		// 		$room_airbnb = new $this->MAccommodationRoomsNew;
		// 		$room_airbnb->id = $room->id;
		// 		$room_airbnb->airbnb_id = $empty_listing->listing->id;
		// 		$response_update_airbnb_id = $room_airbnb->updateAirbnbId();
		// 		//
		// 	}
		//
		//
		// 	//Set Airbnb Title
		// 	$airbnb->name = $room->name;
		// 	$set_title_json = $airbnb->setTitle();
		// 	//
		// 	//Set Bedrooms, Bathrooms, Beds,
		// 	$set_rooms_json = $airbnb->setRooms();
		//
		//
		// }
		// End Airbnb Sync

		echo "<br>========= Accommodation =========<br>";
		echo json_encode($accommodation);
		echo "<br>=================================<br>";
	}

	public function upload($accommodations_id){
	    $this->load->library('upload');
	    $info_images = array();
	    $files = $_FILES;



	    $cpt = count($_FILES['userfile']['name']);
	    for($i=0; $i<$cpt; $i++){
					$rand_name = uniqid();
					// $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];

					// $width = $files['userfile']['width'][$i];
					// $height = $files['userfile']['height'][$i];
					// 500144
          //

					$config = array();
					$config['image_library'] = 'gd2';
			    $config['upload_path'] = './uploads/photos/accommodations';
			    $config['allowed_types'] = '*';
					$config['max_size'] = '5001440';
					$config['encrypt_name'] = true;
			    // $config['max_size']      = '0';
			    $config['overwrite']     = FALSE;




					$config['new_image'] = $files['userfile']['tmp_name'][$i];



	        $this->upload->initialize($config);
	        // $this->upload->do_upload();

					if(!$this->upload->do_upload()){
            echo $this->upload->display_errors();
          }else{

						$data = $this->upload->data();

						print_r($data);

						$config_resize['image_library'] = 'gd2';
						$config_resize['source_image'] = './uploads/photos/accommodations/'.$data["file_name"];
						$config_resize['create_thumb'] = FALSE;
						$config_resize['maintain_ratio'] = FALSE;

						$getsi = getimagesize($files['userfile']['tmp_name'][$i]);
						if($files['userfile']['size'][$i] > 500144){

							list($width, $height) = getimagesize($files['userfile']['tmp_name'][$i]);
							if($width > 1500 || $height > 1500){
								if($width > $height){
									$perc = 1500 / $width;
									$width_new = 1500;
									$height_new = round($height*$perc);

									$config_resize['width']     = $width_new;
	    						$config_resize['height']   = $height_new;
								}else{
									$perc = 1500 / $height;
									$height_new = 1500;
									$width_new = round($width*$perc);

									$config_resize['width']     = $width_new;
	    						$config_resize['height']   = $height_new;
								}
							}else{
								//Kurangi kualitas
								$config_resize['quality']      = 70;
							}
						}

						$config_resize['new_image'] = './uploads/photos/accommodations/'.$data["file_name"];



						$this->load->library('image_lib', $config_resize);
						$this->image_lib->initialize($config_resize);
            $this->image_lib->resize();



					}

					$config_thumbnail['image_library'] = 'gd2';
					$config_thumbnail['source_image'] = './uploads/photos/accommodations/'.$data["file_name"];
					$config_thumbnail['create_thumb'] = FALSE;
					$config_thumbnail['maintain_ratio'] = FALSE;
					list($width, $height) = getimagesize($files['userfile']['tmp_name'][$i]);
					if($width > $height){
						$perc = 500 / $width;
						$width_new = 500;
						$height_new = round($height*$perc);
						// print_r($width_new." & ".$height_new);
						$config_thumbnail['width']     = $width_new;
						$config_thumbnail['height']   = $height_new;
					}else{
						$perc = 500 / $height;
						$height_new = 500;
						$width_new = round($width*$perc);
						// print_r($width_new." & ".$height_new);
						$config_thumbnail['width']     =  $width_new;
						$config_thumbnail['height']   = $height_new;
					}

					print_r($config_thumbnail);
					$config_thumbnail['new_image'] = './uploads/photos/accommodations/thumbnail-'.$data["file_name"];
					$this->load->library('image_lib', $config_thumbnail);
					$this->image_lib->initialize($config_thumbnail);
					$this->image_lib->resize();




	        $info_images[] = $this->upload->data();


	    }
			$photo_responses = array();
			for($info_image = 0; $info_image < sizeof($info_images); $info_image++){
				$photo = new $this->MPhotos;
				$photo->name = $info_images[$info_image]['file_name'];
				$photo->path = "uploads/photos/accommodations/".$info_images[$info_image]['file_name'];
				$photo->ordinal = 0;
				$photo->accommodations_id = $accommodations_id;
				$photo->created_on = date('Y-m-d H:i:s');
				$photo_response = $photo->create();
				array_push($photo_responses, $photo_response);
			}
			// print_r($photo_responses);
			return $photo_responses;
	 }

	 private function setUploadOptions(){
	    //upload an image options
	    $config = array();
	    $config['upload_path'] = './uploads/photos/accommodations';
	    $config['allowed_types'] = '*';
			$config['max_size'] = '5001440';
			$config['encrypt_name'] = true;
	    // $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    return $config;
		}

		public function approve(){
			$id = $this->input->post("id");
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->id = $id;
			$response = $accommodation->approve();
			echo json_encode($response);
		}

		public function activate(){
			$id = $this->input->post("id");
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->id = $id;
			$response = $accommodation->activate();
			echo json_encode($response);
		}

		public function deactivate(){
			$id = $this->input->post("id");
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->id = $id;
			$response = $accommodation->deactivate();
			echo json_encode($response);
		}



		public function syncTripadvisor(){
			$auth = $this->tripadvisor;
			$auth->authenticate('https://rentals.tripadvisor.com/api/property/v1/1030059')."\n";
		}

		public function generateLinkForPartnerUpdate(){
			$id = $this->input->post("id");
			$unique_id = md5(uniqid());
			$unique_key = uniqid();


			$random_id_length = 4;
			//generate a random id encrypt it and store it in $rnd_id
			$rnd_id = crypt(uniqid(rand()), $unique_id);
			//to remove any slashes that might have come
			$rnd_id = strip_tags(stripslashes($rnd_id));
			//Removing any . or / and reversing the string
			$rnd_id = str_replace(".","",$rnd_id);
			$rnd_id = strrev(str_replace("/","",$rnd_id));
			//finally I take the first 4 characters from the $rnd_id
			$rnd_id = substr($rnd_id,0,$random_id_length);

			$access_without_auth = new $this->MAccessWithoutAuth;
			// $access_without_auth->accommodations_id = 7;
			$access_without_auth->accommodations_id = $id;
			$access_without_auth->unique_id = $unique_id;
			$access_without_auth->unique_key = strtolower($rnd_id);
			$access_without_auth->created_on = date('Y-m-d H:i:s');
			$response = $access_without_auth->create();
			echo json_encode($response);
		}

		public function hasUniqueId(){
	  	if(!$this->apiaccess->hasUniqueId()){
				echo "gada";
			}else{
				echo "ada";
			}
	  }

		public function deletePhoto(){
			$data_posted = $this->input->post();
			$photo = $this->MPhotos;
			$photo->id = $data_posted['id'];
			$response = $photo->delete();
			echo json_encode($response);
		}

		public function readLocationLike($word){
			$word_arr = explode(", ", $word);
			$readLocation = $this->MLocations->read_like($word_arr[0]);
			// echo json_encode($readLocation);
			if(isset($readLocation[0])){
				return $readLocation[0];
			}else{
				return false;
			}
		}

		public function APIReadAll(){
			$accommodations = $this->MAccommodationsNew->readAll();
			$arrayRooms = array();
			$arrayLocations = array();

			for($accommodation = 0; $accommodation < sizeof($accommodations); $accommodation++){
				$r = $this->MAccommodationRoomsNew;
				$r->accommodations_id = $accommodations[$accommodation]->id;
				$rooms = $r->readByAccommodationsId();

				$locations = $this->MLocations->read_include_all_parents_by_id($accommodations[$accommodation]->locations_id);

				for($room = 0; $room < sizeof($rooms); $room++){
					// array_push($arrayRooms, $rooms[$room]->total_rooms);
					array_push($arrayRooms, $rooms[$room]);
				}
				for($location = 0; $location < sizeof($locations); $location++){
					array_push($arrayLocations, $locations[$location]->name);
				}

				$accommodations[$accommodation]->rooms = $arrayRooms;
				$arrayRooms = array();
				$accommodations[$accommodation]->locations = $arrayLocations;
				$arrayLocations = array();
			}


			echo json_encode($accommodations);
		}



		public function updateCodeAsd(){
			$accommodations_no_code = $this->MAccommodationsNew->readWithNoCode();
			foreach($accommodations_no_code as $accommodation_no_code){
				$r = $this->MAccommodationRoomsNew;
				$r->accommodations_id = $accommodation_no_code->id;
				$rooms = $r->readByAccommodationsId();

				$accommodation = $this->MAccommodationsNew;
				$accommodation->id = $accommodation_no_code->id;
				$accommodation->code =  strtolower($accommodation_no_code->location)."-".$rooms[sizeof($rooms)-1]->total_rooms."-bedrooms-villa-".rand(0, 999);
				$accommodation->update();

			}
		}




}

?>
