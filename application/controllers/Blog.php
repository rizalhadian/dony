<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		$this->load->model('MBlog');
		
	}

	public function index(){
		$this->session->set_userdata('ses', 'blogs');
		$blogs = $this->MBlog->readAll();
		

		for($i = 0; $i<sizeof($blogs); $i++){
			$search_img = explode('http://localhost/dony/uploads/', $blogs[$i]->html);
			if(isset($search_img[1])){
				$search_img_2 = explode('"', $search_img[1]);
				// print_r($search_img_2[0]);
				$blogs[$i]->img = 'http://localhost/dony/uploads/'.$search_img_2[0];
			}else{
				$blogs[$i]->img = 'http://localhost/dony/assets/images/no-image.png';	
			}
		}

		$data['blogs'] = $blogs;
		$this->load->view('/homepage/index', $data);


	}
	  
	public function read($id){
		$this->session->set_userdata('ses', 'blog');

		$blog = $this->MBlog->readById($id);

		// print_r($blog);
		$data['blog'] = $blog;
		$this->load->view('/homepage/index', $data);

	}

	//User Functions






}

?>
