<?php

defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

class Uploads extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('MPhotos');
		$this->load->model('MUsers');
	}


	public function upload(){
		$this->load->library('upload');
		$data_posted = $this->input->post();
	    $info_images = array();
	    $files = $_FILES;

	    $cpt = count($_FILES['userfile']['name']);

		for($i=0; $i<$cpt; $i++){
			$rand_name = uniqid();
			// $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
	        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
	        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
	        $_FILES['userfile']['size']= $files['userfile']['size'][$i];

			// $width = $files['userfile']['width'][$i];
			// $height = $files['userfile']['height'][$i];
			// 500144
          	//

			$config = array();
			$config['image_library'] = 'gd2';
			$config['upload_path'] = './uploads';
			// $config['allowed_types'] = '*';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '5001440';
			$config['encrypt_name'] = true;
			// $config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			$config['new_image'] = $files['userfile']['tmp_name'][$i];

	        $this->upload->initialize($config);
	        // $this->upload->do_upload();

			if(!$this->upload->do_upload()){
				echo $this->upload->display_errors();
			}else{

				$data = $this->upload->data();

				$config_resize['image_library'] = 'gd2';
				$config_resize['source_image'] = './uploads/'.$data["file_name"];
				$config_resize['create_thumb'] = FALSE;
				$config_resize['maintain_ratio'] = FALSE;

				$getsi = getimagesize($files['userfile']['tmp_name'][$i]);
				
				if($files['userfile']['size'][$i] > 500144){
					list($width, $height) = getimagesize($files['userfile']['tmp_name'][$i]);
					if($width > 1500 || $height > 1500){
						if($width > $height){
							$perc = 1500 / $width;
							$width_new = 1500;
							$height_new = round($height*$perc);
							$config_resize['width'] = $width_new;
							$config_resize['height'] = $height_new;
						}else{
							$perc = 1500 / $height;
							$height_new = 1500;
							$width_new = round($width*$perc);
							$config_resize['width'] = $width_new;
							$config_resize['height'] = $height_new;
						}
					}else{
						//Kurangi kualitas
						$config_resize['quality']      = 70;
					}
				}

				$config_resize['new_image'] = './uploads/photos/accommodations/'.$data["file_name"];

				$this->load->library('image_lib', $config_resize);
				$this->image_lib->initialize($config_resize);
				$this->image_lib->resize();
			}

			// $config_thumbnail['image_library'] = 'gd2';
			// $config_thumbnail['source_image'] = './uploads/photos/accommodations/'.$data["file_name"];
			// $config_thumbnail['create_thumb'] = FALSE;
			// $config_thumbnail['maintain_ratio'] = FALSE;
			// $config_thumbnail['quality'] = 50;
			list($width, $height) = getimagesize($files['userfile']['tmp_name'][$i]);
			
			if($width > $height){
				$perc = 500 / $width;
				$width_new = 500;
				$height_new = round($height*$perc);
				// print_r($width_new." & ".$height_new);
				$config_thumbnail['width']     = $width_new;
				$config_thumbnail['height']   = $height_new;
			}else{
				$perc = 500 / $height;
				$height_new = 500;
				$width_new = round($width*$perc);
				// print_r($width_new." & ".$height_new);
				$config_thumbnail['width']     =  $width_new;
				$config_thumbnail['height']   = $height_new;
			}

			// $config_thumbnail['new_image'] = './uploads/thumbnails/'.$data["file_name"];
			// $this->load->library('image_lib', $config_thumbnail);
			// $this->image_lib->initialize($config_thumbnail);
			// $this->image_lib->resize();

			$info_images[] = $this->upload->data();
		}
		
		$photo_responses = array();
		for($info_image = 0; $info_image < sizeof($info_images); $info_image++){
			$photo = new $this->MPhotos;
			$photo->name = $info_images[$info_image]['file_name'];
			$photo->url = base_url()."/uploads/".$info_images[$info_image]['file_name'];
			$photo->urutan = 0;
			$photo->produkid = $data_posted['produk_id'];
			// $photo->created_on = date('Y-m-d H:i:s');
			$photo_response = $photo->create();
			array_push($photo_responses, $photo_response);
		}
			// print_r($photo_responses);
			// return $photo_responses;

		echo json_encode($photo_responses);
	 }

	 public function uploadSingle(){
		$this->load->library('upload');
		$data_posted = $this->input->post();
	    $info_images = array();
		$files = $_FILES;
		// echo json_encode($files);
		

		// $cpt = count($_FILES['userfile']['name']);

			$rand_name = uniqid();
			// $_FILES['userfile']['name']= $files['userfile']['name'][$i];
	        $_FILES['userfile']['name']= $files['image_param']['name'];
	        $_FILES['userfile']['type']= $files['image_param']['type'];
	        $_FILES['userfile']['tmp_name']= $files['image_param']['tmp_name'];
	        $_FILES['userfile']['error']= $files['image_param']['error'];
	        $_FILES['userfile']['size']= $files['image_param']['size'];

			$config = array();
			$config['image_library'] = 'gd2';
			$config['upload_path'] = './uploads';
			// $config['allowed_types'] = '*';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '5001440';
			$config['encrypt_name'] = true;
			// $config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			$config['new_image'] = $files['image_param']['tmp_name'];

			$this->upload->initialize($config);
	
			$this->upload->do_upload();

		// echo json_encode($files['image_param']['tmp_name']	);
			echo $this->upload->display_errors();



			

			// if(!$this->upload->do_upload()){
			// 	echo $this->upload->display_errors();
			// }else{
			// 	$config_resize['image_library'] = 'gd2';
			// 	$config_resize['source_image'] = './uploads/'.$data["file_name"];
			// 	$config_resize['create_thumb'] = FALSE;
			// 	$config_resize['maintain_ratio'] = FALSE;

			// 	// $getsi = getimagesize($files['userfile']['tmp_name'][0]);
				
			// 	if($files['userfile']['size'] > 500144){
			// 		list($width, $height) = getimagesize($files['userfile']['tmp_name'][0]);
			// 		if($width > 1500 || $height > 1500){
			// 			if($width > $height){
			// 				$perc = 1500 / $width;
			// 				$width_new = 1500;
			// 				$height_new = round($height*$perc);
			// 				$config_resize['width'] = $width_new;
			// 				$config_resize['height'] = $height_new;
			// 			}else{
			// 				$perc = 1500 / $height;
			// 				$height_new = 1500;
			// 				$width_new = round($width*$perc);
			// 				$config_resize['width'] = $width_new;
			// 				$config_resize['height'] = $height_new;
			// 			}
			// 		}else{
			// 			//Kurangi kualitas
			// 			$config_resize['quality']      = 70;
			// 		}
			// 	}
			// }

				

			
			$info_images = $this->upload->data();
		
		$dta['link'] = base_url()."uploads/".$info_images['file_name'];


		echo json_encode($dta);
	 }

	 public function uploadProfilePicture(){
		$this->load->library('upload');
		$data_posted = $this->input->post();
	    $info_images = array();
		$files = $_FILES;
		

		$rand_name = uniqid();
	    $_FILES['userfile']['name']= $files['image_param']['name'];
	        $_FILES['userfile']['type']= $files['image_param']['type'];
	        $_FILES['userfile']['tmp_name']= $files['image_param']['tmp_name'];
	        $_FILES['userfile']['error']= $files['image_param']['error'];
	        $_FILES['userfile']['size']= $files['image_param']['size'];

			$config = array();
			$config['image_library'] = 'gd2';
			$config['upload_path'] = './uploads';
			// $config['allowed_types'] = '*';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '5001440';
			$config['encrypt_name'] = true;
			// $config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			$config['new_image'] = $files['image_param']['tmp_name'];

			$this->upload->initialize($config);
	
			$this->upload->do_upload();
			echo $this->upload->display_errors();

				

			
			$info_images = $this->upload->data();
		
		$dta['link'] = base_url()."uploads/".$info_images['file_name'];

		$user = $this->MUsers;
		$user->id = $data_posted['id'];
		$user->photo = $dta['link'];
		$user_update_response = $user->update();


		echo json_encode($user_update_response);

	 }

	 private function setUploadOptions(){
	    //upload an image options
	    $config = array();
	    $config['upload_path'] = './uploads/photos/accommodations';
	    $config['allowed_types'] = '*';
			$config['max_size'] = '5001440';
			$config['encrypt_name'] = true;
	    // $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    return $config;
		}

	
		

		
		public function hasUniqueId(){
			if(!$this->apiaccess->hasUniqueId()){
					echo "gada";
				}else{
					echo "ada";
				}
		}

		public function deletePhoto(){
			$data_posted = $this->input->post();
			$photo = $this->MPhotos;
			$photo->id = $data_posted['id'];
			$response = $photo->delete();
			echo json_encode($response);
		}

		public function readLocationLike($word){
			$word_arr = explode(", ", $word);
			$readLocation = $this->MLocations->read_like($word_arr[0]);
			// echo json_encode($readLocation);
			if(isset($readLocation[0])){
				return $readLocation[0];
			}else{
				return false;
			}
		}




}

?>
