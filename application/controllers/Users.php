<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct() {
    parent::__construct();
    
    $this->load->model('MUsers');
    $this->load->model('MCartProduk');
    $this->load->library('RajaOngkir');

	}

  public function index(){
    $this->load->view('/frame/login');
  }

  public function login(){
    $this->load->view('/frame/login');
  }

  public function register(){
    $this->load->view('/frame/register');
  }

  public function preview(){
    if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
    }
    
    $data['user'] = $this->MUsers->readById($this->session->userdata['user']->id);
    $this->session->set_userdata('ses', 'profile_preview');
    $this->load->view('homepage/index', $data);
  }

  public function previewOther($userid){
    if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
    }
    
    $data['user'] = $this->MUsers->readById($userid);
    // echo json_encode($data['user']);
    $this->session->set_userdata('ses', 'profile_preview_other');
    $this->load->view('homepage/index', $data);
  }

  public function getCities($province_id){
    $RajaOngkir = new RajaOngkir();
    $cities = $RajaOngkir->getCities($province_id);

    header('Content-Type: application/json');
    echo json_encode($cities);
  }

  public function edit(){
    $data['user'] = $this->MUsers->readById($this->session->userdata['user']->id);

    $RajaOngkir = new RajaOngkir();

    $provinces = $RajaOngkir->getProvinces();
    $data['provinces'] = $provinces;

    $this->session->set_userdata('ses', 'profile_edit');
    // print_r();
    $this->load->view('homepage/index', $data);
        
  }

  public function update(){
    $data_posted = $this->input->post();

    $user = new $this->MUsers();
    // $user->id = $posted_data['id'];
		$user->id = $this->session->userdata('user')->id;

		$user->username = $data_posted['username'];
		$user->province_id = $data_posted['provinsi'];
		$user->city_id = $data_posted['kota'];
		$user->address = $data_posted['address'];
		$user->phone = $data_posted['phone'];
		$user->lat = $data_posted['lat'];
		$user->lng = $data_posted['lng'];
		$user->deskripsi = $data_posted['deskripsi'];
    $user->is_detail_setted = 1;
    
    $raja_ongkir = new RajaOngkir;
    $location = $raja_ongkir->getCity($data_posted['kota']);

    $user->province = $location->province;
    $user->city = $location->city_name;

    
    // echo json_encode($location);

    $user_response = $user->update();
    echo json_encode($user_response);
  }

  public function change_password(){
    // echo "change pass";
    $this->session->set_userdata('ses', 'change_password');
    $this->load->view('homepage/index');
  }

  public function update_password(){
    $posted_data = $this->input->post();

    echo json_encode($posted_data);
  }

  public function uploadProfilePicture(){
    
  }




}

?>
