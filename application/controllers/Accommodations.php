<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accommodations extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('MAccommodationsNew');
		$this->load->model('MAccommodations');
		$this->load->model('MAccommodationRoomsNew');
		$this->load->model('MRates');
		$this->load->model('MPhotos');
		$this->load->model('MFacilities');
		$this->load->model('MMAccommodationsExperiences');
		$this->load->model('MMAccommodationsFacilities');
		$this->load->model('MMAccommodationsAdditionalRates');
		$this->load->model('MBookingAccommodations');
		$this->load->model('MICSDate');
		$this->load->library('email');

	}

	public function index()
	{
		// $this->load->view('homepage/index');
		redirect('/');
	}

	public function readByCode($code){
		$this->session->set_userdata('ses', 'accommodation');
		$accommodation = $this->MAccommodationsNew;
		$accommodation->code = $code;
		$response_accommodation = $accommodation->readByCode();
		if(isset($response_accommodation[0])){
			$response_accommodation = $response_accommodation[0];

			$this->getBookingDates($response_accommodation->id);
			//Get Room Settings
			$rooms = new $this->MAccommodationRoomsNew;
			$rooms->accommodations_id = $response_accommodation->id;
			$response_accommodation->rooms = $rooms->readByAccommodationsId();

			// Get Rates for each Room Setting
			$lowest_base_rate = null;
			$lowest_rate = null;
			for($room = 0; $room < sizeof($response_accommodation->rooms); $room++){
				$rates = new $this->MRates;
				$rates->accommodation_rooms_id = $response_accommodation->rooms[$room]->id;
				$rates_response = $rates->readByAccommodationRoomsId();
				$response_accommodation->rooms[$room]->rates = $rates_response;

				foreach($rates_response as $rate){
					// Find Lowest Base Rate
					if($rate->type == 0){
						if($lowest_base_rate == null){
							$lowest_base_rate = $rate->rate_mature;
						}else{
							if($lowest_base_rate > $rate->rate_mature){
								$lowest_base_rate = $rate->rate_mature;
							}
						}
					}
					if($lowest_rate == null){
						$lowest_rate = $rate->rate_mature;
					}else{
						if($lowest_rate > $rate->rate_mature){
							$lowest_rate = $rate->rate_mature;
						}
					}
				}
			}

			$data['lowest_rate'] = $lowest_rate;
			$data['lowest_base_rate'] = $lowest_base_rate;

			$photos = $this->MPhotos;
			$photos->accommodations_id = $response_accommodation->id;
			$photos_responses = $photos->readByAccommodationsId();
			$response_accommodation->photos = $photos_responses;

			//Get Experience
			$experiences = $this->MMAccommodationsExperiences;
			$experiences->accommodations_id = $response_accommodation->id;
			$experiences_response = $experiences->readByAccommodationsId();
			$data['had_experiences'] = $experiences_response;

			//Get Facilities & Amenities
			$facilities = $this->MMAccommodationsFacilities;
			$facilities->accommodations_id = $response_accommodation->id;
			$facilities_response = $facilities->readByAccommodationsId();
			$data['had_facilities'] = $facilities_response;
			// print_r($data['had_facilities']);

			//Get Additional Rates
			$additional_rates = $this->MMAccommodationsAdditionalRates;
			$additional_rates->accommodations_id = $response_accommodation->id;
			$additional_rates_response = $additional_rates->readByAccommodationsId();
			$data['had_additional_rates'] = $additional_rates_response;

			$data['facilities'] = $this->MFacilities->readAllFacilities();
			$data['amenities'] = $this->MFacilities->readAllAmenities();
			$data['accommodation'] = $response_accommodation;
		}

		if($response_accommodation == null){
			$res = new stdClass();
			$res->response = 404;
			$res->message = "Accommodation not Found";

			echo json_encode($res);
		}else{
			// echo json_encode($data);
			$this->load->view('homepage/accommodation', $data);
		}
	}



	public function getBookingDates($id){

		//Update Calendar File
		$this->MAccommodations->updateCalendarFile($id);

		$room['info'] = $this->MAccommodations->read_id($id);
		$booked_dates = array();
		$booked_dates_unformated = array();




		$this->load->library('IcalReader');

		$randName = uniqid();
		$pathName = "./uploads/ics/".$randName.".ics";

		if($room['info']->ics == ""){
			// echo "b";
			$this->ical->get($room['info']->ics_url, $pathName);
			$accom = new $this->MAccommodationsNew;
			$accom->id = $id;
			$accom->ics = $pathName;
			$accom->update();
		}else{
			// echo "bebebebe";
			$this->ical->get($room['info']->ics_url, $room['info']->ics);
			$accom = new $this->MAccommodationsNew;
			$accom->id = $id;
			$accom->ics_updated_on = date('Y-m-d H:i:s');
			$accom->update();
		}

		$icsreader = new $this->icalreader;
		$ics = $icsreader->create($room['info']->ics);



		$vvent = $icsreader->events($ics);

		for($i = 0; $i < count($vvent); $i++){
			$booked_date_from_ics = new stdClass();
			$booked_date_from_ics->id = null;
			$booked_date_from_ics->booking_id = "ics";
			$booked_date_from_ics->gender = null;
			$booked_date_from_ics->first_name = "ics";
			$booked_date_from_ics->last_name = "ics";
			$booked_date_from_ics->checkin_date = date("Y-m-d", strtotime($vvent[$i]['DTSTART']));
			$booked_date_from_ics->checkout_date = date("Y-m-d", strtotime($vvent[$i]['DTEND']));
			$booked_date_from_ics->checkin_time = "12:08:00";
			$booked_date_from_ics->checkout_time = "12:00:00";
			$booked_date_from_ics->status = 2;


			array_push($booked_dates_unformated, $booked_date_from_ics);

		}

		$delete_bookings = new $this->MBookingAccommodations;
		$delete_bookings->accommodations_id = $id;
		$delete_bookings->deleteByAccommodationsId();

		foreach($booked_dates_unformated as $bd){
			$checkin_date = $bd->checkin_date;
			$checkout_date = $bd->checkout_date;
			$checkin_time = $bd->checkin_time ;
			$checkout_time = $bd->checkout_time;
			$time_diff = abs(strtotime($checkin_date) - strtotime($checkout_date));
			$count_days = $time_diff/86400;

			// $mics_delete = new $this->MICSDate;
			// $mics_delete->accommodations_id = $id;
			// echo json_encode($mics_delete->deleteByAccommodationsId());

			$mics = new $this->MICSDate;
			$mics->checkin_date = $checkin_date;
			$mics->checkout_date = $checkout_date;
			$mics->accommodations_id = $id;
			$mics->created_on = date('Y-m-d H:i:s');

			if($this->MICSDate->checkExistByCICO($checkin_date, $checkout_date) == false){
				$response = $mics->create();
				// print_r($response);
			}

			// print_r($response);
		}
		// echo json_encode($booked_dates);
	}

	public function sendEnquiry(){
		$data_posted = $this->input->post();

		$date_arr = explode(" - ", $data_posted['date_stay']);
		$ci = $date_arr[0];
		$co = $date_arr[1];

		$acc = new $this->MAccommodationsNew;
		$acc->id = $data_posted['accommodations_id'];
		$accommodation = $acc->readById();

		$rm = new $this->MAccommodationRoomsNew;
		$rm->id = $data_posted['accommodation_rooms_id'];
		$room = $rm->readById();

		$config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'enquiry@totalbali.com';
    $config['smtp_pass']    = 'itteambagus';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not

		$message = "
      \n
      Halo team, ada enquiry masuk dari livebalivillas.\n\n
      Email    			 : ".$data_posted['email_guest']."\n
			Villa    			 : ".$accommodation->name."\n
			Villa Code		 : ".$accommodation->code."\n
			Bedroom Set    : ".$room->name."\n
      Guest    			 : ".$data_posted['guest_num']."\n
      Checkin  			 : ".$ci."\n
      Checkout 		   : ".$co."\n
      Notes    			 : ".$data_posted['notes']."\n
    ";

		$messageClient = "
      Your enquiry is currently in process, \n
      We should answer within 15 minutes during working hours \n
      and not longer than 12 hours. \n
      Please feel free to whatsapp +6281338648034 if it is urgent. \n
      Whilst you wait checkout www.totalbali.com for more our services. \n
      \n
      Terima Kasih
      \n
      Total Bali Team
    ";

    $this->email->initialize($config);
		$this->email->set_mailtype("html");
    $this->email->from('enquiry@totalbali.com', 'Automail Livebalivillas');
    $this->email->to("totalbali@totalbali.com");
    $this->email->subject('LiveBaliVillas (Total Bali Group)');
    $this->email->message($message);
    $this->email->send();

		$this->email->initialize($config);
    $this->email->from('enquiry@totalbali.com', 'Automail Livebalivillas');
    $this->email->to($data_posted['email_guest']);
    $this->email->subject('Online Enquiry - '.$dataposted['villa'].' - '.$dataposted['name']);
    $this->email->message($messageClient);
    $this->email->send();

		$_SERVER['QUERY_STRING'];

		redirect($data_posted['url']);

	}

	public function test_doku(){
		$this->load->view('homepage/tes_doku');
	}

}
?>
