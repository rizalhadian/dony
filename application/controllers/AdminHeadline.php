<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminHeadline extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		$this->load->model('MBlog');
		$this->load->model('MHeadline');
		if (!$this->ion_auth->logged_in()){
			$this->session->set_flashdata('error', 1);
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('/auth/login');
		}else{
			if($this->session->userdata('user')->type_id != 1){
				redirect('/');
			}
		}
	}

	public function index(){
		$this->session->set_userdata('ses', 'headline');
		$blogs = $this->MBlog->readAll();
		

		// for($i = 0; $i<sizeof($blogs); $i++){
		// 	$search_img = explode('http://localhost/dony/uploads/', $blogs[$i]->html);
		// 	if(isset($search_img[1])){
		// 		$search_img_2 = explode('"', $search_img[1]);
		// 		// print_r($search_img_2[0]);
		// 		$blogs[$i]->img = 'http://localhost/dony/uploads/'.$search_img_2[0];
		// 	}else{
		// 		$blogs[$i]->img = 'http://localhost/dony/assets/images/no-image.png';	
		// 	}
		// }

		$data['blogs'] = $blogs;
		$this->load->view('/admin/index', $data);
	  }
	  
	public function addnew(){
		$data['get'] = $this->input->get();
		echo json_encode($data);
		// $this->session->set_userdata('ses', 'headline_addnew');
		// $this->load->view('/admin/index');
	}

	public function create(){
		$data['get'] = $this->input->get();
		echo json_encode($data);

		$headline = $this->MHeadline;
		if($data['get']['type'] == "blog"){
			$headline->blogid = $data["get"]["id"];
		}elseif($data['get']['type'] == "promo"){
			$headline->blogid = $data["get"]["id"];
		}
		$headline->img = $data["get"]["img"];

		$headline_response = $headline->create();
		
		echo json_encode($headline_response);
		// print_r($posted_data['html']);
	}

	public function logout(){
		$this->ion_auth->logout();
		redirect('admin');
	}

	public function undercons(){
		$this->session->set_userdata('ses', 'undercons');
		$this->load->view('/admin/index');
	}

	//User Functions






}

?>
