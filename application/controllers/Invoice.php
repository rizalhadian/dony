<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library('ion_auth');
		$this->load->library('RajaOngkir');
		$this->load->model('Ion_auth_model');
		$this->load->model('MInvoice');
		$this->load->model('MCartProduk');

		$this->load->model('MMessages');
		$this->load->model('MMessageRooms');
		$this->load->model('MMessageRoomGuests');
	}


	public function index(){
		$this->session->set_userdata('ses', 'invoices');

		// $RajaOngkir = new RajaOngkir();

		// $provinces = $RajaOngkir->getProvinces();
		// $data['provinces'] = $provinces;
		
		// if(isset($this->session->userdata('user')->username)){
		// 	// Logged In
		// 	$userid = $this->session->userdata('user')->id;
		// 	$produk = $this->MCartProduk->readByUserId($userid);
		// 	$toko = $this->MCartProduk->readByUserIdDistinctToko($userid);
		// 	$data['produk'] = $produk;
		// 	// $data['toko'] = $toko;
		// 	$toko_arr = array();
			

		// 	foreach($toko as $t){
		// 		// print_r($t) ;
		// 		// countProdukByToko
		// 		$counter = $this->MCartProduk->countProdukByToko($t->userid);
		// 		$toko_arr[$t->userid] = $counter;
		// 		// print_r($counter);
		// 	}
		// 	// print_r($toko_arr);
		// 	$data['toko'] = $toko_arr;
		// }else{
		// 	// Not Logged In
		// 	$sessid = $this->session->session_id;
		// 	$produk = $this->MCartProduk->readBySessId($sessid);
		// 	$data['produk'] = $produk;
		// 	print_r($data);
		// }
		// print_r($data);
		// $this->load->view('homepage/index', $data);

		$this->session->set_userdata('ses', 'invoices');

		$invoice = $this->MInvoice;;
		$invoice->userid = $this->session->userdata('user')->id;
		$invoice_response = $invoice->readByUserid();
		$data['invoices'] = $invoice_response;

		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}

		// Get all first message
		$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
		foreach($all_rooms as $room){
			$guests = new $this->MMessageRoomGuests;
			$guests->messageroomid = $room->id;
			$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

			$messages = new $this->MMessages;
			$messages->messageroomid = $room->id;
			$room->messages = $messages->readByMessageRoomIdLimit();

		}
		$data['all_first_messages'] = $all_rooms;

		// echo json_encode($data);
		$this->load->view('homepage/index', $data);
	}

	public function readById($id){
		$this->session->set_userdata('ses', 'invoice');
		$invoice = $this->MInvoice;;
		$invoice->userid = $this->session->userdata('user')->id;
		$invoice->id = $id;
		$invoice_response = $invoice->readById();
		$data['invoice'] = $invoice_response[0];

		$cart_produk = $this->MCartProduk;
		$cart_produk->userid = $this->session->userdata('user')->id;
		$cart_produk->invoiceid = $invoice_response[0]->id;
		$cart_produk_response = $cart_produk->readByInvoiceId();
		// $data['produk'] = $cart_produk_response;
		
		$produks = $cart_produk_response;
		$toko_ids  = array();
		
		foreach($produks as $produk){
			$toko_ids[$produk->userid_toko] = (object)[];
			$toko_ids[$produk->userid_toko]->tokoid = $produk->userid_toko;
			$toko_ids[$produk->userid_toko]->tokonama = $produk->toko_username;
			$toko_ids[$produk->userid_toko]->total_ongkir = $produk->total_ongkir;
			$toko_ids[$produk->userid_toko]->total_berat = $produk->total_berat;
			$toko_ids[$produk->userid_toko]->ongkir_perkilo = $produk->ongkir_perkilo;
			$toko_ids[$produk->userid_toko]->produk = array();
		}

		foreach($produks as $produk){
			
			array_push($toko_ids[$produk->userid_toko]->produk, $produk);
		}

		$toko = array();
		foreach($toko_ids as $toko_id){
			array_push($toko, $toko_id);
		}

		$object = (object)[];
		$object->toko = $toko;

		$data["toko"] = $toko;
		
		// Get all first message
		$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
		foreach($all_rooms as $room){
			$guests = new $this->MMessageRoomGuests;
			$guests->messageroomid = $room->id;
			$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

			$messages = new $this->MMessages;
			$messages->messageroomid = $room->id;
			$room->messages = $messages->readByMessageRoomIdLimit();

		}
		$data['all_first_messages'] = $all_rooms;

		
		$this->load->view('homepage/index', $data);

		// echo json_encode($object);
	}

}

	

  


?>