<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class KonsultasiContr extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		$this->load->model('MBlog');
		$this->load->model('MCartProduk');
		$this->load->model('MMessages');
		$this->load->model('MMessageRooms');
		$this->load->model('MMessageRoomGuests');
		$this->load->model('MKonsultasiPost');
		
	}

	public function index(){
        $user = $this->ion_auth->user()->row();

        if($user->type_id==0){
            $this->session->set_userdata('ses', 'konsultasi');

            if(isset($_GET['search'])){
                $this->session->set_userdata('ses_text', 'Pencarian');
                $keywords = explode(' ', $_GET['search']);
                $read_konsultasi_posts = $this->MKonsultasiPost->readAnsweredLike(0, $keywords);
                $data['konsultasi_posts'] = $read_konsultasi_posts;
            }else{
                $this->session->set_userdata('ses_text', 'Konsultasi Terbaru');
                $read_konsultasi_posts = $this->MKonsultasiPost->readAnsweredNewest(0);
                $data['konsultasi_posts'] = $read_konsultasi_posts;
            }

            if(isset($this->session->userdata('user')->username)){
                // Get All Orders
                $cart_produk_update = new $this->MCartProduk;
                $cart_produk_update->sessid = $this->session->session_id;
                $cart_produk_update->userid = $this->session->userdata('user')->id;		
                $cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
                $count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
                $data['count_unread_order'] = sizeof($count_unread_order);			
                $orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
                $data['orders'] = $orders;
    
                // Get all first message
                $all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
                foreach($all_rooms as $room){
                    $guests = new $this->MMessageRoomGuests;
                    $guests->messageroomid = $room->id;
                    $room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);
    
                    $messages = new $this->MMessages;
                    $messages->messageroomid = $room->id;
                    $room->messages = $messages->readByMessageRoomIdLimit();
    
                }
                $data['all_first_messages'] = $all_rooms;
            }

		    $this->load->view('homepage/index', $data);
        }else{
            $this->session->set_userdata('ses', 'konsultasi_admin');
            $read_konsultasi_posts = $this->MKonsultasiPost->readAll(0);
            $data['konsultasi_posts'] = $read_konsultasi_posts;
            // echo json_encode($read_konsultasi_post);

            if(isset($this->session->userdata('user')->username)){
                // Get All Orders
                $cart_produk_update = new $this->MCartProduk;
                $cart_produk_update->sessid = $this->session->session_id;
                $cart_produk_update->userid = $this->session->userdata('user')->id;		
                $cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
                $count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
                $data['count_unread_order'] = sizeof($count_unread_order);			
                $orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
                $data['orders'] = $orders;
    
                // Get all first message
                $all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
                foreach($all_rooms as $room){
                    $guests = new $this->MMessageRoomGuests;
                    $guests->messageroomid = $room->id;
                    $room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);
    
                    $messages = new $this->MMessages;
                    $messages->messageroomid = $room->id;
                    $room->messages = $messages->readByMessageRoomIdLimit();
    
                }
                $data['all_first_messages'] = $all_rooms;
            }
            $this->load->view('homepage/index', $data);
        }
        
    }

    public function readById($id){
        $this->session->set_userdata('ses', 'konsultasi_read');
        $konsultasi_post = $this->MKonsultasiPost;
        $konsultasi_post->id = $id;
        $konsultasi_post_response = $konsultasi_post->readById();
        $data['konsultasi_post'] = $konsultasi_post_response;

        $konsultasi_post_view = $this->MKonsultasiPost;
        $konsultasi_post_view->id = $id;
        $konsultasi_post_view->view_count = $data['konsultasi_post']->view_count + 1;
        $konsultasi_post_response = $konsultasi_post_view->update();

        if(isset($this->session->userdata('user')->username)){
            // Get All Orders
            $cart_produk_update = new $this->MCartProduk;
            $cart_produk_update->sessid = $this->session->session_id;
            $cart_produk_update->userid = $this->session->userdata('user')->id;		
            $cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
            $count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
            $data['count_unread_order'] = sizeof($count_unread_order);			
            $orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
            $data['orders'] = $orders;

            // Get all first message
            $all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
            foreach($all_rooms as $room){
                $guests = new $this->MMessageRoomGuests;
                $guests->messageroomid = $room->id;
                $room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

                $messages = new $this->MMessages;
                $messages->messageroomid = $room->id;
                $room->messages = $messages->readByMessageRoomIdLimit();

            }
            $data['all_first_messages'] = $all_rooms;
        }
        $this->load->view('homepage/index', $data);
    }
    
    public function createForm(){
        $this->session->set_userdata('ses', 'konsultasi_add');

        if(isset($this->session->userdata('user')->username)){
            // Get All Orders
            $cart_produk_update = new $this->MCartProduk;
            $cart_produk_update->sessid = $this->session->session_id;
            $cart_produk_update->userid = $this->session->userdata('user')->id;		
            $cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
            $count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
            $data['count_unread_order'] = sizeof($count_unread_order);			
            $orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
            $data['orders'] = $orders;

            // Get all first message
            $all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
            foreach($all_rooms as $room){
                $guests = new $this->MMessageRoomGuests;
                $guests->messageroomid = $room->id;
                $room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

                $messages = new $this->MMessages;
                $messages->messageroomid = $room->id;
                $room->messages = $messages->readByMessageRoomIdLimit();

            }
            $data['all_first_messages'] = $all_rooms;
        }

        $this->load->view('homepage/index', $data);
    }

    public function create(){
    
        $posted_data = $this->input->post();
        $konsultasi_post = $this->MKonsultasiPost;
        $konsultasi_post->userid = $this->ion_auth->user()->row()->id;
        $konsultasi_post->judul = $posted_data['judul'];
        $konsultasi_post->deskripsi = $posted_data['deskripsi'];
        $konsultasi_post->is_answered = 0;
        $konsultasi_post->created_on = date('Y-m-d H:i:s');
        $konsultasi_post_response = $konsultasi_post->create();

        // echo json_encode($konsultasi_post_response);
        redirect('/konsultasi');
    
    }

    public function answerForm($id){
        
        $this->session->set_userdata('ses', 'konsultasi_answer');
        

        $konsultasi_post = $this->MKonsultasiPost;
        $konsultasi_post->id = $id;
        $konsultasi_post_response = $konsultasi_post->readById();
        $data['konsultasi_post'] = $konsultasi_post_response;

        if(isset($this->session->userdata('user')->username)){
            // Get All Orders
            $cart_produk_update = new $this->MCartProduk;
            $cart_produk_update->sessid = $this->session->session_id;
            $cart_produk_update->userid = $this->session->userdata('user')->id;		
            $cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
            $count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
            $data['count_unread_order'] = sizeof($count_unread_order);			
            $orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
            $data['orders'] = $orders;

            // Get all first message
            $all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
            foreach($all_rooms as $room){
                $guests = new $this->MMessageRoomGuests;
                $guests->messageroomid = $room->id;
                $room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

                $messages = new $this->MMessages;
                $messages->messageroomid = $room->id;
                $room->messages = $messages->readByMessageRoomIdLimit();

            }
            $data['all_first_messages'] = $all_rooms;
        }

        $this->load->view('homepage/index', $data);
    }

    public function answer(){
        $posted_data = $this->input->post();

        $konsultasi_post = $this->MKonsultasiPost;
        $konsultasi_post->id = $posted_data['id'];
        $konsultasi_post->jawaban = $posted_data['jawaban'];
        $konsultasi_post->is_answered = 1;
        $konsultasi_post_response = $konsultasi_post->update();

        redirect('/konsultasi');
    }

    public function search($keyword){
        
    }
	  
}

?>
