<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminKategori extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');

		$this->load->model('MKategori');
		
	}

	public function index(){
		if (!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('error', 1);
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('/auth/login');
		}

		$kategoris = $this->MKategori->readALl();
		$data['kategoris'] = $kategoris;

		// echo json_encode($kategoris);

		$this->session->set_userdata('ses', 'kategori');
		$this->load->view('/admin/index', $data);
  	}

	public function addnew(){
		if (!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('error', 1);
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('/auth/login');
		}
		
		$this->session->set_userdata('ses', 'kategori_addnew');
		$this->load->view('/admin/index');
	}

	public function addNewSub($parent_id){
		if (!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('error', 1);
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('/auth/login');
		}

		$kategori = new $this->MKategori;
		$kategori->id = $parent_id;
		$response = $kategori->readById();

		$data['parent'] = $response[0];

		
		$this->session->set_userdata('ses', 'kategori_addnewsub');
		$this->load->view('/admin/index', $data);
	}

	// public function addNewParent(){
	// 	if (!$this->ion_auth->logged_in()){
    //         $this->session->set_flashdata('error', 1);
    //         $this->session->set_flashdata('message', 'You must be an admin to view this page');
    //         redirect('/auth/login');
	// 	}
		
	// 	$this->session->set_userdata('ses', 'kategori_addnew');
	// 	$this->load->view('/admin/index');
	// }

	public function create(){
		if (!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('error', 1);
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('/auth/login');
		}
		
		$posted_data = $this->input->post();
		
		$kategori = new $this->MKategori;
		$kategori->nama = $posted_data['nama'];
		if($posted_data['parent_id'] == null){
			$kategori->parent_id = null;
			
		}else{
			$kategori->parent_id = $posted_data['parent_id'];
		}
		$kategori->deskripsi = $posted_data['deskripsi'];
		$response = $kategori->create();

		// echo json_encode($kategori);
		redirect('/superadmin/kategori');

	}

	public function readLike(){
		$posted_data = $this->input->post();

		$kategori = new $this->MKategori;
		$kategori->nama = $posted_data['text'];
		$response = $kategori->readLikeByName($posted_data['text']);

		echo json_encode($response);
	}

	//User Functions






}

?>
