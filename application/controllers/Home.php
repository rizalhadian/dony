<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		

		// if(!isset($this->session->uniqid)){
		// 	$this->session->set_userdata('uniqid', uniqid());
		// }

		$this->load->model('MAccommodationsNew');
		$this->load->model('Ion_auth_model');
		$this->load->model('MProduk');
		$this->load->model('MHeadline');
		$this->load->model('MProdukJenis1');
		$this->load->model('MProdukJenis2');
		$this->load->model('MCartProduk');
		$this->load->model('MKategori');
		$this->load->model('MMessages');
		$this->load->model('MMessageRooms');
		$this->load->model('MMessageRoomGuests');
		

	}

	

	public function index(){
		$this->session->set_userdata('ses', 'home');
		$data_get = $this->input->get();
		if(isset($data_get['page'])){
			$data['page_current'] = $data_get['page'];
			if($data_get['page'] != 0){
				$page = ($data_get['page']-1)*16;	
			}else{
				$page = 0;
			}
		}else{
			$data['page_current'] = 0;
			$page = 0;
		}
		
		// $data['produks'] = $this->MProduk->readAll($page);
		$produks = $this->MProduk->readAll($page);
		foreach($produks as $produk){
			$namaproduk = $produk->nama;
			$namap = str_replace(' ', '-', $namaproduk);
			$produk->url = base_url()."u/".$produk->toko."/".$namap;
		}
		$data['produks'] = $produks;
		$produk_count = $this->MProduk->count();
		$data['page_count'] = ceil($produk_count / 16);
		

		$data['headlines'] = $this->MHeadline->readAll();
		
		
		$kategoris = $this->MKategori->readAll();
		// echo json_encode($kategoris);

		$kategori_arr = array();
		$subkategoris = array();
		$parentkategoris = array();
		foreach($kategoris as $kat){
			if($kat->parent_id != null){
				array_push($subkategoris, $kat);
				// unset($kat);
			}else{
				array_push($parentkategoris, $kat);
			}
		}
		foreach($parentkategoris as $pk){
			$pk->sub = $subkategoris;
		}
		


		$data['kategoris'] = $parentkategoris;

		// echo json_encode($parentkategoris);

		if(isset($this->session->userdata('user')->username)){
			// Get All Orders
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;		
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;

			// Get all first message
			$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
			foreach($all_rooms as $room){
				$guests = new $this->MMessageRoomGuests;
				$guests->messageroomid = $room->id;
				$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

				$messages = new $this->MMessages;
				$messages->messageroomid = $room->id;
				$room->messages = $messages->readByMessageRoomIdLimit();

			}
			$data['all_first_messages'] = $all_rooms;
		}

		
		
		
		


		
		// echo json_encode($messages_response);
		$this->load->view('homepage/index', $data);
		
	}

	public function registerDo(){
		// echo "asdasd";
		$data_posted = $this->input->post();
		if($data_posted['password'] == $data_posted['password_retype']){
			$data_posted = $this->input->post();
			$username = $data_posted['username'];
			$password = $data_posted['password'];
			$email = $data_posted['email'];
			$additional_data = array(
									'first_name' => '',
									'last_name' => '',
									);
			$group = array('1'); // Sets user to admin.

			$this->Ion_auth_model->register($username, $password, $email, $additional_data, $group);
			redirect('/auth/login', 'refresh');
		}else{
			$this->session->flashdata('Password Ga Sama Gan!');
			redirect('/register', 'refresh');
		}
	}

}
?>
