<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ongkir extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('MUsers');
    	$this->load->library('RajaOngkir');
		

	}


	public function getCities($province_id){
		$RajaOngkir = new RajaOngkir();
		$cities = $RajaOngkir->getCities($province_id);
	
		header('Content-Type: application/json');
		echo json_encode($cities);
		echo json_encode('a');
	}

	public function getCosts(){
		$RajaOngkir = new RajaOngkir();
		$data_posted = $this->input->post();
		$origin_id = $data_posted['origin_id'];
		$destination_id = $data_posted['destination_id'];
		$courier = $data_posted['courier'];

		$data['origin_id'] = $origin_id;
		$data['destination_id'] = $destination_id;
		$data['courier'] = $courier;

		$RajaOngkir = new RajaOngkir();
		$costs = $RajaOngkir->getCosts($origin_id, $destination_id, $courier);

		header('Content-Type: application/json');
		// echo json_encode($data);
		echo json_encode($costs);
	}
	  

}

	

  


?>