<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AccommodationRooms extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->session->set_userdata('ses', 'accommodations');
		$this->load->model('MAccommodationRooms');
		$this->load->model('MAccommodations');
		$this->load->model('MAccommodationTypes');
		$this->load->model('MAccommodationRoomPrices');
		$this->load->model('MFacilities');
		$this->load->model('MServices');
		$this->load->model('MLocations');
		$this->load->model('MBookingAccommodations');
	}

	public function add_new(){
		$this->session->set_userdata('ses', 'accommodation_rooms_addnew');
		$data['facilities'] = $this->MFacilities->read_all();
		$this->load->view('/admin/index', $data);
  }

	public function create(){
		$dataPosted = $this->input->post();
	  $createAccommodationRoom =  $this->MAccommodationRooms->create($dataPosted);
		echo json_encode($createAccommodationRoom);
		// echo json_encode($dataPosted);
	}

	public function read_by_id($id){
		echo $id;

		$data['room'] = $this->MAccommodationRooms->api_read_by_id($id);
		$rates = $this->MAccommodationRoomPrices->api_read_by_accommodation_rooms_id($id);
    $cheapest_rate = $rates[0];
		// print_r($cheapest_rate);
    $promo;
    $promos = array();
    for($i=0;$i<sizeof($rates);$i++){
      if($rates[$i]->type == 2 && $rates[$i]->date_end > date("Y-m-d")){
        $promo = $rates[$i];
        array_push($promos, $promo);
      }
      if($i<sizeof($rates)-1){
				if($cheapest_rate > $rates[$i+1]->rate){
          $cheapest_rate = $rates[$i+1]->rate;
        }
				// print_r($cheapest_rate);
      }
    }
    $data['promos'] = $promos;
    $data['cheapest_rate'] = $cheapest_rate;
		$this->load->view('rooms', $data);
		// echo "tes";
	}

	public function read_by_id_tes($id){
		echo "wwwwwwwwwwwwwwwwww";
		$data['room'] = $this->MAccommodationRooms->api_read_by_id($id);
		$rates = $this->MAccommodationRoomPrices->api_read_by_accommodation_rooms_id($id);
    $cheapest_rate = $rates[0];
		// print_r($cheapest_rate);
    $promo;
    $promos = array();
    for($i=0;$i<sizeof($rates);$i++){
      if($rates[$i]->type == 2 && $rates[$i]->date_end > date("Y-m-d")){
        $promo = $rates[$i];
        array_push($promos, $promo);
      }
      if($i<sizeof($rates)-1){
				if($cheapest_rate > $rates[$i+1]->rate){
          $cheapest_rate = $rates[$i+1]->rate;
        }
				// print_r($cheapest_rate);
      }
    }
    $data['promos'] = $promos;
    $data['cheapest_rate'] = $cheapest_rate;
		$this->load->view('room_tes', $data);
		// echo "tes";
	}

	public function read_by_accommodations_id(){
		$id = $this->input->post("id");
		$readAccommodationRoom = $this->MAccommodationRooms->read_by_accommodations_id($id);
		$readPriceBaseRate = $this->MAccommodationRoomPrices->read_by_accommodations_id_base_rate($id);
		echo json_encode($readAccommodationRoom);
	}

	public function read_like(){
		$word = $this->input->post('word');
		$readAccommodationRoom = $this->MAccommodationRooms->read_like($word);
		echo json_encode($readAccommodationRoom);
	}

	public function update($id){
		$data['room'] = $this->MAccommodationRooms->read_by_id($id);
		$this->session->set_userdata('ses', 'accommodation_rooms_update');
		$data['facilities'] = $this->MFacilities->read_all();
		$this->load->view('/admin/index', $data);
		print_r($data['room']);
	}

	public function update_do(){
		$dataPosted = $this->input->post();
		$updateAccommodationRoom =  $this->MAccommodationRooms->update($dataPosted);
		echo json_encode($updateAccommodationRoom);
	}

	public function delete(){
		$id = $this->input->post('id');
		$deleteAccommodationRoom =  $this->MAccommodationRooms->delete($id);
		echo json_encode($deleteAccommodationRoom);
	}

	public function tes($id){
		$room['info'] = $this->MAccommodationRooms->api_read_by_id($id);
		print_r($room);
	}

	public function api_read_by_id($id){
		$this->load->library('Ical');
		$keyword = $this->input->get();
		$room = array();

		$room['info'] = $this->MAccommodationRooms->api_read_by_id($id);

		if(isset($keyword['get_accommodation'])){
			$room['accommodation'] = $this->MAccommodations->api_read_by_id($room['info']->accommodations_id);
		}
		if(isset($keyword['get_location'])){
			$accommodation = $this->MAccommodations->api_read_by_id($room['info']->accommodations_id);
			$room['location'] =  $this->MLocations->api_read_by_id($accommodation->locations_id);
		}
		if(isset($keyword['get_rates'])){
			$room['rates'] = $this->MAccommodationRoomPrices->api_read_by_accommodation_rooms_id($id);
		}
		if(isset($keyword['get_facilities'])){
			$room['facilities'] = $this->MAccommodationRooms->read_room_facilities_by_id($id);
		}
		if(isset($keyword['get_photos'])){
			$room['photos']['room'] = $this->MAccommodationRooms->read_room_photos_by_id($id);
			$room['photos']['accommodations'] = $this->MAccommodations->read_accommodation_photos_by_id($room['info']->accommodations_id);
		}
		if(isset($keyword['get_booked_dates'])){
			// $room['info']->accommodations_id
			$booked_dates = array();
			$booked_dates_unformated = $this->MBookingAccommodations->read_by_accommodation_rooms_id($id);
			// Get ICS Booked Dates
			$ics = $this->ical->create($room['info']->ics);
			$vvent = $this->ical->events($ics);
			for($i = 0; $i < count($vvent); $i++){
				$booked_date_from_ics = new stdClass();
				$booked_date_from_ics->id = null;
				$booked_date_from_ics->booking_id = "ics";
				$booked_date_from_ics->gender = null;
				$booked_date_from_ics->first_name = "ics";
				$booked_date_from_ics->last_name = "ics";
				$booked_date_from_ics->checkin_date = date("Y-m-d", strtotime($vvent[$i]['DTSTART']));
				$booked_date_from_ics->checkout_date = date("Y-m-d", strtotime($vvent[$i]['DTEND']));
				$booked_date_from_ics->checkin_time = "12:08:00";
				$booked_date_from_ics->checkout_time = "12:00:00";
				$booked_date_from_ics->status = 2;
				array_push($booked_dates_unformated, $booked_date_from_ics);
			}
			foreach($booked_dates_unformated as $bd){
				$checkin_date = $bd->checkin_date;
				$checkout_date = $bd->checkout_date;
				$checkin_time = $bd->checkin_time ;
				$checkout_time = $bd->checkout_time;
				$time_diff = abs(strtotime($checkin_date) - strtotime($checkout_date));
				$count_days = $time_diff/86400;
				if($checkout_time > date("H:i:s", strtotime("12:0:0")) && $checkin_time < date("H:i:s", strtotime("12:0:0"))){
					for($i = 0; $i<$count_days+1; $i++){
						array_push($booked_dates, date('m/d/Y', strtotime($checkin_date . ' +'.$i.' day')));
					}
				}elseif($checkin_time < date("H:i:s", strtotime("12:0:0"))){
					for($i = 0; $i<$count_days; $i++){
						array_push($booked_dates, date('m/d/Y', strtotime($checkin_date . ' +'.$i.' day')));
					}
				}elseif($checkout_time > date("H:i:s", strtotime("12:0:0"))){
					for($i = 1; $i<$count_days+1; $i++){
						array_push($booked_dates, date('m/d/Y', strtotime($checkin_date . ' +'.$i.' day')));
					}
				}else{
					for($i = 1; $i<$count_days; $i++){

						array_push($booked_dates, date('m/d/Y', strtotime($checkin_date . ' +'.$i.' day')));
					}
				}
			}
			$room['booked_dates'] = $booked_dates;
		}
		print_r(json_encode($room));
	}

}

?>
