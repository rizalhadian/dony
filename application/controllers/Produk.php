<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		$this->load->model('MProduk');
		$this->load->model('MCartProduk');
		$this->load->model('MProdukJenis1');
		$this->load->model('MProdukJenis2');
		$this->load->model('MPhotos');
		$this->load->model('MTag');
		$this->load->model('MKategori');
		$this->load->model('MProdukTag');
		$this->load->model('MProdukKategori');

		if (!$this->ion_auth->logged_in()){
			if(!$this->MAccessWithoutAuth->hasUniqueId()){
				$this->session->set_flashdata('error', 1);
				$this->session->set_flashdata('message', 'You must be an admin to view this page');
				redirect('/auth/login');
			}
    	}

	}


  public function createNew(){
		$produk_jenis_1 = $this->MProdukJenis1;
		$produk_jenis_1_response = $produk_jenis_1->readAll();
		$data['produk_jenis_1'] = $produk_jenis_1_response;

		// $produk_jenis_2 = $this->MProdukJenis2;
		// $produk_jenis_2_response = $produk_jenis_2->readAll();
		// $data['produk_jenis_2'] = $produk_jenis_2_response;

    	$this->session->set_userdata('ses', 'produk_createnew');
		$this->load->view('homepage/index', $data);
  }

	public function addPhotosNew($id){
		$produk = $this->MProduk->readById($id);
		$data['produk'] = $produk;

		$this->session->set_userdata('ses', 'foto_produk_upload');
		$this->load->view('homepage/index', $data);
	}

	public function readProdukJenis2ByProdukJeni1Id($produk_jenis_1_id){
		$produk_jenis_2 = $this->MProdukJenis2;
		$produk_jenis_2->produk_jenis_1_id = $produk_jenis_1_id;
		$produk_jenis_2_response = $produk_jenis_2->readAllByProdukJenis1Id();
		echo json_encode($produk_jenis_2_response);
	}

  public function create(){
		$data_posted = $this->input->post();
		
		$tags = explode(',', $data_posted['tags']);
		
		$create_produk = $this->MProduk;
		$create_produk->userid = $this->ion_auth->user()->row()->id;
		$create_produk->produk_jenis_2_id = $data_posted['produk_jenis_2'];
		$create_produk->nama = $data_posted['nama'];
		$create_produk->stock = $data_posted['stock'];
		$create_produk->harga = $data_posted['harga'];
		$create_produk->berat = $data_posted['berat'];
		// $produk->foto = $data_posted['foto'];S
		$create_produk->deskripsi = $data_posted['deskripsi'];
		$create_produk_response = $create_produk->create();

		$produkid = $create_produk_response['id'];

		foreach($tags as $tag){
			$lower_tag = strtolower($tag);
			// echo $tag;
			$mtag = new $this->MTag;
			$mtag->name = $tag;
			$mtag_resp = $mtag->isExistByName();

			// echo json_encode($mtag_resp) ;
			if(!$mtag_resp){
				echo "========Create New Tag=========<br>";
				$create_tag = new $this->MTag;
				$create_tag->name = $lower_tag;
				$create_tag_response = $create_tag->create();
				$create_tag->is_from_admin = 0;
				print_r($create_tag_response);
				echo "<br>";	

				$create_produk_tag = new $this->MProdukTag;
				$create_produk_tag->produkid = $produkid;
				$create_produk_tag->tagid = $create_tag_response['id'];
				$create_produk_tag->is_from_admin = 0;
				$create_produk_tag_response = $create_produk_tag->create();
				print_r($create_produk_tag_response);
				echo "<br>";	

			}else{
				echo "========Read Tag Existed=========".$tag."<br>";
				$read_tag = new $this->MTag;
				$read_tag->name = $lower_tag;
				$read_tag_response = $read_tag->readByName();
				print_r($read_tag_response);
				echo "<br>";

				$create_produk_tag = new $this->MProdukTag;
				$create_produk_tag->produkid = $produkid;
				$create_produk_tag->tagid = $read_tag_response->id;
				$create_produk_tag->is_from_admin = 0;
				$create_produk_tag_response = $create_produk_tag->create();
				print_r($create_produk_tag_response);
				echo "<br>";	
			}
		}

		// echo json_encode($produk_response);
		redirect('/produk/edit/'.$produkid, 'refresh');
  }

	public function edit($produk_id){
		// echo $produk_id;
		// $produk = $this->MProduk->readById($produk_id);
		// $data['produk'] = $produk;
		$produk_jenis_1 = $this->MProdukJenis1;
		$produk_jenis_1_response = $produk_jenis_1->readAll();
		$data['produk_jenis_1'] = $produk_jenis_1_response;

		$photos = $this->MPhotos;
		$photos->produkid = $produk_id;

		$read_tags = $this->MProdukTag;
		$read_tags->produkid = $produk_id;

		$selected_kategori = $this->MProdukKategori;
		$selected_kategori->produk_id = $produk_id;
		
		$data['produk'] = $this->MProduk->readById($produk_id)[0];
		$data['photos'] = $photos->readByProdukId();
		$data['tags'] = $read_tags->readByProdukId();
		$data['selected_kategori'] = $selected_kategori->readByProdukId();
		$kategoris = $this->MKategori->readAll();

		$subkategoris = array();
		$parentkategoris = array();
		foreach($kategoris as $kat){
			if($kat->parent_id != null){
				array_push($subkategoris, $kat);
				// unset($kat);
			}else{
				array_push($parentkategoris, $kat);
			}
		}

		foreach($parentkategoris as $pk){
			// $pk->sub = array();
			$pk->sub = $subkategoris;
		}
		


		$data['parent'] = $parentkategoris;
		$data['subparent'] = $subkategoris;

		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}
		// echo json_encode($data['selected_kategori']);
		$this->session->set_userdata('ses', 'produk_edit');
		$this->load->view('homepage/index', $data);

	}

	public function update(){
		$posted_data = $this->input->post();

		echo json_encode($posted_data);

		$produk = $this->MProduk;
		$produk->id = $posted_data['id'];
		$produk->nama = $posted_data['nama'];
		$produk->harga = $posted_data['harga'];
		$produk->berat = $posted_data['berat'];
		$produk->stock = $posted_data['stock'];
		$produk->deskripsi = $posted_data['deskripsi'];
		$response = $produk->update();

		

		if(isset($posted_data['kategori'])){
			$produk_kategori = $this->MProdukKategori;
			$produk_kategori->produk_id = $posted_data['id'];
			$response_delete = $produk_kategori->deleteByProdukId();

			foreach($posted_data['kategori'] as $kat){
				$pk = new $this->MProdukKategori;
				$pk->produk_id = $posted_data['id'];
				$pk->kategori_id = $kat;
				$pk->create();
			}
		}
		

		// echo json_encode($response);
		redirect('/shop', 'refresh');
	}


	public function delete($id){
		$produk = $this->MProduk->readById($id)[0];

		if($produk->userid == $this->session->userdata['user']->id){
			echo "bisa delet";

			// $produk->delete();
			$produk_del = new $this->MProduk;
			$produk_del->id = $produk->id;
			$produk_del->delete();
		}

		redirect('/shop', 'refresh');
		
	}

}
?>
