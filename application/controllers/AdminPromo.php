<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPromo extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		if (!$this->ion_auth->logged_in()){
      $this->session->set_flashdata('error', 1);
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
      redirect('/auth/login');
    }else{
			if($this->session->userdata('user')->type_id != 1){
				redirect('/');
			}
		}
	}

	public function index(){
		$this->session->set_userdata('ses', 'promo');
		$this->load->view('/admin/index');
  	}

	public function addnew(){
		$this->session->set_userdata('ses', 'promo_addnew');
		$this->load->view('/admin/index');
	}

	public function create(){
		$posted_data = $this->input->post();

		// $blog = $this->MBlog;
		// $blog->judul = $posted_data['judul'];
		// $blog->html = $posted_data['html'];
		// $blog_response = $blog->create();

		// echo json_encode($blog_response);
		// print_r($posted_data['html']);
	}

	//User Functions






}

?>
