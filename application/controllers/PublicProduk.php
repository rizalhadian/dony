<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PublicProduk extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('RajaOngkir');

		$this->load->model('MProduk');
		$this->load->model('MProdukKategori');
		$this->load->model('MPhotos');
		$this->load->model('MCartProduk');
		$this->load->model('MKategori');
	}

	public function readProduk($a, $b, $c){
		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}
		
		$produk = $this->MProduk->readById($c);
		$created_on = date("d-M-Y", strtotime($produk[0]->user_created_on));
		$produk[0]->user_created_on = $created_on;

		$photos = $this->MPhotos;
		$photos->produkid = $produk[0]->id;

		$raja_ongkir = new RajaOngkir;
		$data['location'] = $raja_ongkir->getCity($produk[0]->city_id);
		$resp_photos = $photos->readByProdukId();
		$data['produk'] = $produk;
		$data['produk'][0]->photos = $resp_photos;

		$data['kat1'] = str_replace('%20', ' ', $a);
		$data['kat2'] = str_replace('%20', ' ', $b);

		$produk_update_view = $this->MProduk;
		$produk_update_view->id = $produk[0]->id;
		$produk_update_view->view_count = $produk[0]->view_count + 1;
		$produk_update_view->update();

		// Get all first message
		$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
		foreach($all_rooms as $room){
			$guests = new $this->MMessageRoomGuests;
			$guests->messageroomid = $room->id;
			$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

			$messages = new $this->MMessages;
			$messages->messageroomid = $room->id;
			$room->messages = $messages->readByMessageRoomIdLimit();

		}
		$data['all_first_messages'] = $all_rooms;
		echo json_encode($data['all_first_messages']);
		
		// echo json_encode($data);
		$this->session->set_userdata('ses', 'produk_view');
		$this->load->view('homepage/index', $data);
	}

	public function readKategori2($a, $b){
		

		$subkategori = str_replace('%20', ' ', $b);
		$kategori = str_replace('%20', ' ', $a);

		// echo json_encode($kategori." ".$subkategori);

		$kategori_detail = $this->MKategori->readLikeByname2($subkategori, $kategori);
		// echo json_encode($kategori_detail);

		$produks = $this->MProduk->readByKategori2($a, $b);
		// echo json_encode($produks);

		foreach($produks as $produk){
			$namaproduk = $produk->nama;
			$namap = str_replace(' ', '-', $namaproduk);
			$produk->url = base_url()."u/".$produk->toko."/".$namap;
		}

		// echo json_encode($produks);
		$data['kat'] = $subkategori." & ".$kategori;
		$data['produks'] = $produks;

		$kategoris = $this->MKategori->readAll();
		$subkategoris = array();
		$parentkategoris = array();
		foreach($kategoris as $kat){
			if($kat->parent_id != null){
				array_push($subkategoris, $kat);
				// unset($kat);
			}else{
				array_push($parentkategoris, $kat);
			}
		}

		foreach($parentkategoris as $pk){
			// $pk->sub = array();
			$pk->sub = $subkategoris;
		}
		
		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}


		$data['kategoris'] = $parentkategoris;

		
		$this->session->set_userdata('ses', 'search');
		$this->load->view('homepage/index', $data);
	}

	public function readProdukNew($toko, $produk){
		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}

		$p = str_replace('-', ' ', $produk);
		$produk = $this->MProduk->readByTokoDanProduk($toko, $p);

		$created_on = date("d-M-Y", strtotime($produk[0]->user_created_on));
		$produk[0]->user_created_on = $created_on;

		$photos = $this->MPhotos;
		$photos->produkid = $produk[0]->id;

		$raja_ongkir = new RajaOngkir;
		$data['location'] = $raja_ongkir->getCity($produk[0]->city_id);
		$resp_photos = $photos->readByProdukId();
		$data['produk'] = $produk;
		$data['produk'][0]->photos = $resp_photos;

		$produk_update_view = $this->MProduk;
		$produk_update_view->id = $produk[0]->id;
		$produk_update_view->view_count = $produk[0]->view_count + 1;
		$produk_update_view->update();

		$kat = $this->MProdukKategori;
		$kat->produk_id = $produk[0]->id;
		$kat_res = $kat->readByProdukId();

		$data['kat1'] = '';
		$data['kat2'] = '';

		$this->session->set_userdata('ses', 'produk_view');
		$this->load->view('homepage/index', $data);
	}

	public function readKategori1($a){
		$kategori = str_replace('%20', ' ', $a);
		echo $kategori;
	}
}
?>
