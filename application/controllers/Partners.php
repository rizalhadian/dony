<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('email');
    $this->load->model('MAccessWithoutAuth');
    $this->load->model('MAccommodationsNew');
    $this->load->model('MAccommodationRoomsNew');
    $this->load->model('MPartners');
    $this->load->model('MRates');
    $this->load->model('MPhotos');
    $this->load->model('MFacilities');
    $this->load->model('MExperiences');
		$this->load->model('MAccommodationTypes');
		$this->load->model('MAccessWithoutAuth');
    $this->load->model('MAccommodationComplex');
		$this->load->model('MAdditionalRates');
		$this->load->model('MMAccommodationsFacilities');
		$this->load->model('MMAccommodationsAdditionalRates');
		$this->load->model('MMAccommodationsExperiences');
		$this->load->model('MLocations');


	}

	public function index(){
		$this->load->view('/homepage/partner');
	}

	public function tes_form_1(){
		$this->load->view('/homepage/partner-form-partner-info');
	}

	public function addNew(){
		$this->load->view('/homepage/partner-form-partner-info');
	}

	public function updateAccommodation($unique_id){
    $this->session->set_userdata('ses', 'api_accommodations_add');
    // echo $unique_id;
    $access_without_auth = new $this->MAccessWithoutAuth;
    $access_without_auth->unique_id = $unique_id;
    $response_access = $access_without_auth->readByUniqueId();


    $accommodation = new $this->MAccommodationsNew;
    $accommodation->id = $response_access[0]->accommodations_id;
    $response_accommodation = $accommodation->readById();


		//Get Rooms
		$rooms = new $this->MAccommodationRoomsNew;
		$rooms->accommodations_id = $response_access[0]->accommodations_id;

		$data['accommodation'] = $accommodation->readById();
    $data['accommodation']->rooms = $rooms->readByAccommodationsId();
		$data['accommodation']->unique_id = $unique_id;

		//Get Partner Name
		if($data['accommodation']->partners_id != 0 && $data['accommodation']->partners_id != null && $data['accommodation']->partners_id != ""){
			$partner = new $this->MPartners;
			$partner->id = $data['accommodation']->partners_id;
			$data['accommodation']->partner = $partner->readById();
		}

		// Get rates for each room
		for($room = 0; $room < sizeof($data['accommodation']->rooms); $room++){
			$rates = new $this->MRates;
			$rates->accommodation_rooms_id = $data['accommodation']->rooms[$room]->id;
			$rates_response = $rates->readByAccommodationRoomsId();
			$data['accommodation']->rooms[$room]->rates = $rates_response;
		}

		// Get Photos
		$photos = $this->MPhotos;
		$photos->accommodations_id = $response_access[0]->accommodations_id;
		$photos_responses = $photos->readByAccommodationsId();
		$data['accommodation']->photos = $photos_responses;

		//Get Experience
		$experiences = $this->MMAccommodationsExperiences;
		$experiences->accommodations_id = $response_access[0]->accommodations_id;
		$experiences_response = $experiences->readByAccommodationsId();
		$data['had_experiences'] = $experiences_response;

		//Get Facilities & Amenities
		$facilities = $this->MMAccommodationsFacilities;
		$facilities->accommodations_id = $response_access[0]->accommodations_id;
		$facilities_response = $facilities->readByAccommodationsId();
		$data['had_facilities'] = $facilities_response;

		//Get Additional Rates
		$additional_rates = $this->MMAccommodationsAdditionalRates;
		$additional_rates->accommodations_id = $response_access[0]->accommodations_id;
		$additional_rates_response = $additional_rates->readByAccommodationsId();
		$data['had_additional_rates'] = $additional_rates_response;


		$data['facilities'] = $this->MFacilities->readAllFacilities();
		$data['amenitites'] = $this->MFacilities->readAllAmenities();
		$data['additional_rates'] = $this->MAdditionalRates->readAll();
		$data['experiences'] = $this->MExperiences->read_all();
		$this->session->set_userdata('ses', 'api_accommodations_update');
		$data['accommodation_types'] = $this->MAccommodationTypes->read_all();

    $this->load->view('/without_auth/index', $data);
  }

	public function updateByUniqueId($unique_id){
		$data_get = $this->input->get();

		$access_without_auth = new $this->MAccessWithoutAuth;
		$access_without_auth->unique_id = $unique_id;
		$response_access = $access_without_auth->readByUniqueId();
		// print_r($response_access);
		$partner = new $this->MPartners;
		$partner->id = $response_access[0]->partners_id;
		$response_partner = $partner->readById();
		$data['partner'] = $response_partner;
		$data['accommodation_types'] = $this->MAccommodationTypes->read_all();

		$data['unique_id'] = $unique_id;
		$complexes = new $this->MAccommodationComplex;
		$complexes->partners_id = $response_partner->id;
		$data['complexes'] = $complexes->readByPartnersId();
		$data['facilities'] = $this->MFacilities->readAllFacilities();
		$data['amenitites'] = $this->MFacilities->readAllAmenities();
		$data['additional_rates'] = $this->MAdditionalRates->readAll();
		$data['experiences'] = $this->MExperiences->read_all();

		// print_r($response_partner);

		// echo "<br>";
		if(isset($data_get['basic-info'])){
			// print_r($data['partner']);
			$this->load->view('/homepage/partner-form-basic-info', $data);
		}

		if(isset($data_get['finish'])){
			// print_r($data['partner']);
			$this->load->view('/homepage/partner-finish', $data);
		}

		if(isset($data_get['accommodations'])){
			// echo "Accommodations
			$accommodations = new $this->MAccommodationsNew;
			$accommodations->partners_id =  $response_partner->id;
			$response_accommodations = $accommodations->readByPartnersId();

			foreach ($response_accommodations as $acc) {
				// echo $acc->id."<br>";
				$photos = new $this->MPhotos;
				$photos->accommodations_id = $acc->id;
				$response_photos = $photos->readByAccommodationsId();
				// print_r($response_photos);
				$acc->photos = $response_photos;

			}
			// print_r();
			if(!isset($response_accommodations[0])){
				$data['isHaveAccommodations'] = 0;
			}else{
				$data['isHaveAccommodations'] = 1;
				$data['accommodations'] = $response_accommodations;
				// print_r($data['accommodations'][1]->photos);
			}
			if(!isset($data['complexes'][0])){
				$data['isHaveComplexes'] = 0;
			}else{
				$data['isHaveComplexes'] = 1;
			}
			// print_r($data['partner']->business_type);

			// If Complex VIlla
			if($data['partner']->business_type == 0){
				if(!isset($response_accommodations[0])){
					$this->load->view('/homepage/partner-form-accommodations-create-new-single', $data);
				}else{

					$accommodation = new $this->MAccommodationsNew;
			    $accommodation->id = $response_accommodations[0]->id;
			    $response_accommodation = $accommodation->readById();

					//Get Rooms
					$rooms = new $this->MAccommodationRoomsNew;
					$rooms->accommodations_id = $response_accommodations[0]->id;

					$data['accommodation'] = $accommodation->readById();
			    $data['accommodation']->rooms = $rooms->readByAccommodationsId();
					$data['accommodation']->unique_id = $unique_id;

					//Get Partner Name
					if($data['accommodation']->partners_id != 0 && $data['accommodation']->partners_id != null && $data['accommodation']->partners_id != ""){
						$partner = new $this->MPartners;
						$partner->id = $data['accommodation']->partners_id;
						$data['accommodation']->partner = $partner->readById();
					}

					// Get rates for each room
					for($room = 0; $room < sizeof($data['accommodation']->rooms); $room++){
						$rates = new $this->MRates;
						$rates->accommodation_rooms_id = $data['accommodation']->rooms[$room]->id;
						$rates_response = $rates->readByAccommodationRoomsId();
						$data['accommodation']->rooms[$room]->rates = $rates_response;
					}



					// Get Photos
					$photos = $this->MPhotos;
					$photos->accommodations_id = $response_accommodations[0]->id;
					$photos_responses = $photos->readByAccommodationsId();
					$data['accommodation']->photos = $photos_responses;


					//Get Experience
					$experiences = $this->MMAccommodationsExperiences;
					$experiences->accommodations_id = $response_accommodations[0]->id;
					$experiences_response = $experiences->readByAccommodationsId();
					$data['had_experiences'] = $experiences_response;

					//Get Facilities & Amenities
					$facilities = $this->MMAccommodationsFacilities;
					$facilities->accommodations_id = $response_accommodations[0]->id;
					$facilities_response = $facilities->readByAccommodationsId();
					$data['had_facilities'] = $facilities_response;

					//Get Additional Rates
					$additional_rates = $this->MMAccommodationsAdditionalRates;
					$additional_rates->accommodations_id = $response_accommodations[0]->id;
					$additional_rates_response = $additional_rates->readByAccommodationsId();
					$data['had_additional_rates'] = $additional_rates_response;



					$this->load->view('/homepage/partner-form-accommodations-update-single', $data);
				}
			}
			// If Complex VIlla
			if($data['partner']->business_type == 1){
				$this->load->view('/homepage/partner-form-accommodations', $data);
			}
			// If Property Manger
			if($data['partner']->business_type == 2){
				$this->load->view('/homepage/partner-form-accommodations', $data);
			}
		}
		if(isset($data_get['review'])){
			// echo "Review";
			$this->load->view('/homepage/partner-form-review', $data);
		}
		if(isset($data_get['new_accommodation'])){
			// echo "Review";
			$this->load->view('/homepage/partner-form-accommodations-create-new', $data);
		}
		if(isset($data_get['new_complex'])){
			// echo "Review";
			$this->load->view('/homepage/partner-form-complex-create-new', $data);
		}
		if(isset($data_get['update_complex'])){
			// echo "Review";
			$complex = new $this->MAccommodationComplex;
			$complex->id = $data_get['id'];
			$data['complex'] = $complex->readById();

			$this->load->view('/homepage/partner-form-complex-update', $data);
		}
		if(isset($data_get['update_accommodation'])){
			$accommodation = new $this->MAccommodationsNew;
	    $accommodation->id = $data_get['id'];
	    $response_accommodation = $accommodation->readById();

			//Get Rooms
			$rooms = new $this->MAccommodationRoomsNew;
			$rooms->accommodations_id = $data_get['id'];

			$data['accommodation'] = $accommodation->readById();

			if(isset($data['accommodation'])){
				$data['accommodation']->rooms = $rooms->readByAccommodationsId();
				$data['accommodation']->unique_id = $unique_id;

				//Get Partner Name
				if($data['accommodation']->partners_id != 0 && $data['accommodation']->partners_id != null && $data['accommodation']->partners_id != ""){
					$partner = new $this->MPartners;
					$partner->id = $data['accommodation']->partners_id;
					$data['accommodation']->partner = $partner->readById();
				}

				// Get rates for each room
				for($room = 0; $room < sizeof($data['accommodation']->rooms); $room++){
					$rates = new $this->MRates;
					$rates->accommodation_rooms_id = $data['accommodation']->rooms[$room]->id;
					$rates_response = $rates->readByAccommodationRoomsId();
					$data['accommodation']->rooms[$room]->rates = $rates_response;
				}

				// Get Photos
				$photos = $this->MPhotos;
				$photos->accommodations_id = $data_get['id'];;
				$photos_responses = $photos->readByAccommodationsId();
				$data['accommodation']->photos = $photos_responses;

				//Get Experience
				$experiences = $this->MMAccommodationsExperiences;
				$experiences->accommodations_id = $data_get['id'];
				$experiences_response = $experiences->readByAccommodationsId();
				$data['had_experiences'] = $experiences_response;

				//Get Facilities & Amenities
				$facilities = $this->MMAccommodationsFacilities;
				$facilities->accommodations_id = $data_get['id'];
				$facilities_response = $facilities->readByAccommodationsId();
				$data['had_facilities'] = $facilities_response;

				//Get Additional Rates
				$additional_rates = $this->MMAccommodationsAdditionalRates;
				$additional_rates->accommodations_id = $data_get['id'];
				$additional_rates_response = $additional_rates->readByAccommodationsId();
				$data['had_additional_rates'] = $additional_rates_response;
			}

			$data['facilities'] = $this->MFacilities->readAllFacilities();
			$data['amenitites'] = $this->MFacilities->readAllAmenities();
			$data['experiences'] = $this->MExperiences->read_all();
			$data['accommodation_types'] = $this->MAccommodationTypes->read_all();

			// print_r($data);
	    $this->load->view('/homepage/partner-form-accommodations-update', $data);


		}

	}

	public function create(){
		$data_posted = $this->input->post();
		$name = $this->input->post('name')[0]." ".$this->input->post('name')[1];
		$phone = $this->input->post('phone')[0]." ".$this->input->post('phone')[1];

		$partner_check = new $this->MPartners;
		$partner_check->email = $this->input->post('email');
		$partner_check_response = $partner_check->readByEmail();


		// print_r($partner_check[0]);
		if(isset($partner_check_response[0])){
			// echo "ada";
			$partner = $partner_check_response[0];
			$partner->unique_link = $this->generateLinkForPartnerUpdate($partner->id);
			$partner->is_new_data = 0;
			echo json_encode($partner);

			// $email->unique_url = $partner->unique_link->unique_id;
			$unique_link = base_url()."partner/update/".$partner->unique_link->unique_id."?basic-info";
			$this->sendEmail($this->input->post('email'), $unique_link);

			$this->session->set_flashdata('is_new_data', $partner->is_new_data);
		}else{
			// echo "gada";
			$partner = new $this->MPartners;
			$partner->name = $name;
			$partner->address = $this->input->post('address');
			$partner->email = $this->input->post('email');
			$partner->phone = $phone;
			$partner->description = $this->input->post('description');
			$partner->business_type = $this->input->post('business_type');
			$partner->created_on = date('Y-m-d H:i:s');
			$partner->active = 2;
			$response =  $partner->create();
			$response->unique_link = $this->generateLinkForPartnerUpdate($response->id);
			$response->is_new_data = 1;
			echo json_encode($response);

			// $email->unique_url = $response->unique_link->unique_id;
			$unique_link = base_url()."partner/update/".$response->unique_link->unique_id."?basic-info";
			$this->sendEmail($this->input->post('email'), $unique_link);

			$this->session->set_flashdata('is_new_data', $partner->is_new_data);
		}

		redirect("partner");
	}

	public function generateLinkForPartnerUpdate($partners_id){
		$unique_id = md5(uniqid());
		$unique_key = uniqid();
		$random_id_length = 4;
		//generate a random id encrypt it and store it in $rnd_id
		$rnd_id = crypt(uniqid(rand()), $unique_id);
		//to remove any slashes that might have come
		$rnd_id = strip_tags(stripslashes($rnd_id));
		//Removing any . or / and reversing the string
		$rnd_id = str_replace(".","",$rnd_id);
		$rnd_id = strrev(str_replace("/","",$rnd_id));
		//finally I take the first 4 characters from the $rnd_id
		$rnd_id = substr($rnd_id,0,$random_id_length);
		$access_without_auth = new $this->MAccessWithoutAuth;

		$access_without_auth->partners_id = $partners_id;
		$access_without_auth->unique_id = $unique_id;
		$access_without_auth->unique_key = strtolower($rnd_id);
		$access_without_auth->created_on = date('Y-m-d H:i:s');
		$response = $access_without_auth->create();

		return $response;
	}

	public function createComplex(){
		$data_posted = $this->input->post();
		$complex = new $this->MAccommodationComplex;
		$complex->name = $data_posted['name'];
		$complex->total_accommodations = $data_posted['total_accommodations'];
		$complex->total_rooms = $data_posted['total_rooms'];
		$complex->total_beds = $data_posted['total_beds'];

		if(isset($data_posted['locations_id'])){
			if($data_posted['locations_id'] != ""){
				$complex->locations_id = $data_posted['locations_id'];
			}else{
				$loc = $this->readLocationLike($data_posted['locations_name']);
				if(isset($loc->value)){
					$complex->locations_id = $loc->value;
				}else{
					$complex->locations_id = 1;
				}
			}
		}

		$complex->address = $data_posted['address'];
		$complex->lat = $data_posted['lat'];
		$complex->lng = $data_posted['lng'];
		$complex->partners_id = $data_posted['partners_id'];
		$complex->created_on = date('Y-m-d H:i:s');
		$response = $complex->create();
		redirect("partner/update/".$data_posted['unique_id']."?accommodations");
	}

	public function updateComplex(){
		$data_posted = $this->input->post();
		$complex = new $this->MAccommodationComplex;
		$complex->id = $data_posted['id'];
		$complex->name = $data_posted['name'];
		$complex->total_accommodations = $data_posted['total_accommodations'];
		$complex->total_rooms = $data_posted['total_rooms'];
		$complex->total_beds = $data_posted['total_beds'];

		if(isset($data_posted['locations_id'])){
			if($data_posted['locations_id'] != ""){
				$complex->locations_id = $data_posted['locations_id'];
			}else{
				$loc = $this->readLocationLike($data_posted['locations_name']);
				if(isset($loc->value)){
					$complex->locations_id = $loc->value;
				}else{
					$complex->locations_id = 1;
				}
			}
		}

		$complex->address = $data_posted['address'];
		$complex->lat = $data_posted['lat'];
		$complex->lng = $data_posted['lng'];
		$complex->partners_id = $data_posted['partners_id'];
		$complex->updated_on = date('Y-m-d H:i:s');
		$response = $complex->update();
		redirect("partner/update/".$data_posted['unique_id']."?accommodations");
	}

	public function deleteComplex(){
		$data_posted = $this->input->post();
		$complex = new $this->MAccommodationComplex;
		$complex->id = $data_posted['id'];
		$response = $complex->delete();
		// redirect("partner/update/".$data_posted['unique_id']."?accommodations");
		echo json_encode($response);
	}


	public function updateBasicInfo(){
		// print_r($this->input->post());
		$data_posted = $this->input->post();
		$partner = new $this->MPartners;
		$partner->id = $data_posted['id'];
		$partner->name = $data_posted['name'];
		$partner->email = $data_posted['email'];
		$partner->phone = $data_posted['phone'][0]." ".$data_posted['phone'][1];
		$partner->whatsapp = $data_posted['whatsapp'][0]." ".$data_posted['whatsapp'][1];
		$partner->reservation_name = $data_posted['reservation_name'];
		$partner->reservation_email = $data_posted['reservation_email'];
		$partner->reservation_phone = $data_posted['reservation_phone'][0]." ".$data_posted['reservation_phone'][1];
		$partner->reservation_whatsapp = $data_posted['reservation_whatsapp'][0]." ".$data_posted['reservation_whatsapp'][1];
		$partner->business_type = $data_posted['business_type'];
		$partner->website = $data_posted['website'];
		$partner->commission_percentage = $data_posted['commission_percentage'];
		$partner->updated_on = date('Y-m-d H:i:s');
		$response = $partner->updateNew();
		redirect("partner/update/".$data_posted['unique_id']."?accommodations");
	}

	public function readLocationLike($word){
		$word_arr = explode(", ", $word);
		$readLocation = $this->MLocations->read_like($word_arr[0]);
		// echo json_encode($readLocation);
		if(isset($readLocation[0])){
			return $readLocation[0];
		}else{
			return false;
		}
	}

	public function sendEmail($email, $unique_url){
    $config['protocol']    = 'smtp';
    $config['smtp_host']    = 'ssl://smtp.gmail.com';
    $config['smtp_port']    = '465';
    $config['smtp_timeout'] = '7';
    $config['smtp_user']    = 'enquiry@totalbali.com';
    $config['smtp_pass']    = 'itteambagus';
    $config['charset']    = 'utf-8';
    $config['newline']    = "\r\n";
    $config['mailtype'] = 'text'; // or html
    $config['validation'] = TRUE; // bool whether to validate email or not


    $messageClient = "
Welcome,<br><br>
We are sending you this link so that we can get your updated information for our new website.<br>
Below you will find a link that will help with getting all your latest and updated information for you and your villas.<br>
This link below is a unique url to update your accommodation.<br>
Please don't share this link with others, and please contact us if you have any questions.<br><br>
".$unique_url."";

    $this->email->initialize($config);
		$this->email->set_mailtype("html");
    $this->email->from('enquiry@totalbali.com', 'Automail LiveBaliVillas (Total Bali Group)');
    $this->email->to($email);
    $this->email->subject('LiveBaliVillas (Total Bali Group) - Unique Url To Update Your Accommodations');
    $this->email->message($messageClient);
    $this->email->send();
  }








}

?>
