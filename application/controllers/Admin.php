<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		if (!$this->ion_auth->logged_in()){
      $this->session->set_flashdata('error', 1);
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('/auth/login');
			
    }else{
			if($this->session->userdata('user')->type_id != 1){
				redirect('/');
			}
		}
	}

	public function index(){
		$this->load->view('/admin/index');
  }

	public function logout(){
		$this->ion_auth->logout();
		redirect('admin');
	}

	public function undercons(){
		$this->session->set_userdata('ses', 'undercons');
		$this->load->view('/admin/index');
	}

	//User Functions






}

?>
