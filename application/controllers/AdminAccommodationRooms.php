<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminAccommodationRooms extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('MAccessWithoutAuth');
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in()){
			if(!$this->MAccessWithoutAuth->hasUniqueId()){
				$this->session->set_flashdata('error', 1);
				$this->session->set_flashdata('message', 'You must be an admin to view this page');
				redirect('/auth/login');
			}
    }
		$this->session->set_userdata('ses', 'accommodations');
		$this->load->model('MAccommodationRooms');
		$this->load->model('MAccommodations');
		$this->load->model('MAccommodationTypes');
		$this->load->model('MAccommodationRoomPrices');
		$this->load->model('MFacilities');
		$this->load->model('MServices');
		$this->load->model('MAccommodationRoomsNew');
		$this->load->model('MMRoomsFacilities');



	}

	public function create(){
		$this->session->set_userdata('ses', 'accommodation_rooms_addnew');
		$data['facilities'] = $this->MFacilities->read_all();


		$this->load->view('/admin/index', $data);
  }

	public function create_do(){
		$dataPosted = $this->input->post();
	  $createAccommodationRoom =  $this->MAccommodationRooms->create($dataPosted);
		echo json_encode($createAccommodationRoom);
		// echo json_encode($dataPosted);
	}

	public function create_do_on_accommodation(){
		$data_posted = $this->input->post();
	  $createAccommodationRoom =  $this->MAccommodationRooms->create_do_on_accommodation($data_posted);
		echo json_encode($createAccommodationRoom);
	}

	public function create_new(){
		$data_posted = $this->input->post();
		$room = new $this->MAccommodationRoomsNew;
		$room->name = $data_posted['name'];
		$room->total_rooms = $data_posted['total_rooms'];
		$room->total_beds = $data_posted['total_beds'];
		$room->total_bathrooms = $data_posted['total_bathrooms'];
		$room->max_guest_normal = $data_posted['max_guest_normal'];
		$room->max_guest_extra = $data_posted['max_guest_extra'];
		$room->min_stay = $data_posted['min_stay'];
		if(isset($data_posted['ics_url'])){
			$room->ics_url = $data_posted['ics_url'];
		}



		if(isset($data_posted['renting_type'])){
			$room->renting_type = $data_posted['renting_type'];
		}
		if(isset($data_posted['flexible_renting_type'])){
			$room->renting_type = $data_posted['flexible_renting_type'];
		}

		if(isset($data_posted['ics_url'])){
			$room->ics_url = $data_posted['ics_url'];
		}
		$room->description = $data_posted['description'];
		$room->created_on = date('Y-m-d H:i:s');
		$room->active = 2;
		$room->synced = 0;
		$response = $room->create();


		// Add Facilities and Amenities
		if(isset($data_posted['facilities'])){
			$facilities = $data_posted['facilities'];
			for($index = 0; $index<sizeof($facilities); $index++){
				$facility = new $this->MMRoomsFacilities;
				$facility->accommodation_rooms_id = $response->id;
				$facility->facilities_id = $facilities[$index];
				$facility->create();
			}
		}
		//

		echo json_encode($response);
	}

	public function readById(){
		$id = $this->input->post("id");

		$room = new $this->MAccommodationRoomsNew;
		$room->id = $id;
		$response = $room->readById();
		echo json_encode($response);
		// echo json_encode('woi');
	}

	public function read_by_accommodations_id(){
		$id = $this->input->post("id");
		$readAccommodationRoom = $this->MAccommodationRooms->read_by_accommodations_id($id);
		$readPriceBaseRate = $this->MAccommodationRoomPrices->read_by_accommodations_id_base_rate($id);

		echo json_encode($readAccommodationRoom);
	}

	public function read_like(){
		$word = $this->input->post('word');
		$readAccommodationRoom = $this->MAccommodationRooms->read_like($word);
		echo json_encode($readAccommodationRoom);
	}

	public function update($id){
		$data['room'] = $this->MAccommodationRooms->read_by_id($id);
		$this->session->set_userdata('ses', 'accommodation_rooms_update');
		$data['facilities'] = $this->MFacilities->read_all();
		$this->load->view('/admin/index', $data);
		// print_r($data['room']);
	}

	public function updatePost(){
		$data_posted = $this->input->post();
		$room = new $this->MAccommodationRoomsNew;
		$room->id = $data_posted['id'];
		$room->name = $data_posted['name'];
		$room->total_rooms = $data_posted['total_rooms'];
		$room->total_beds = $data_posted['total_beds'];
		$room->total_bathrooms = $data_posted['total_bathrooms'];
		$room->max_guest_normal = $data_posted['max_guest_normal'];
		$room->max_guest_extra = $data_posted['max_guest_extra'];
		$room->min_stay = $data_posted['min_stay'];
		$room->renting_type = $data_posted['renting_type'];
		$room->updated_on = date('Y-m-d H:i:s');
		$room_response = $room->update();
		echo json_encode($room_response);
	}

	public function update_do(){
		$dataPosted = $this->input->post();
		$updateAccommodationRoom =  $this->MAccommodationRooms->update($dataPosted);

		echo json_encode($updateAccommodationRoom);
		// echo "halo";
	}

	public function delete(){
		$id = $this->input->post('id');
		$deleteAccommodationRoom =  $this->MAccommodationRooms->delete($id);
		echo json_encode($deleteAccommodationRoom);
	}


}

?>
