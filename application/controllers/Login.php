<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Login extends CI_Controller {



	function __construct() {

		parent::__construct();

		$this->load->model('M_login');

	}



	public function index()

	{

		if (isset($this->session->userdata('user_data')['username'])) {

            $this->load->view('admin/accommodations');

		}else{

			$this->load->view('login');

		}

	}



	function loginprocess(){

		$username = $this->input->post("username");

        $password = $this->input->post("password");



        //validation succeeds

        //check if username and password is correct

        $usr_result = $this->m_login->get_user($username, $password);

        if ($usr_result > 0) {

            //set the session variables

            $sessiondata = array(

                'username' => $username,

                'loginuser' => TRUE

            );

            $this->session->set_userdata('user_data', $sessiondata);

            redirect("admin/accommodations");

        } else {

            $this->session->set_flashdata('msg', '<div class="alert alert-danger"><b>Error!</b> Invalid username and password!</div>');

            redirect('login');

        }



	}



	function adduser(){

        $username = $this->input->post('username');

     	$password = $this->input->post('password');



     	$data = array(

			'username' => $username,

			'password' => md5($password),

		);

		$this->m_login->add_user($data);

		redirect('admin/accommodations');

	}



	// logout

	public function logout() {



		$this->session->unset_userdata('user_data');

		redirect('home');



	}



}

?>

