<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminAccommodationRoomPrices extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->model('MAccessWithoutAuth');

		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in()){
			if(!$this->MAccessWithoutAuth->hasUniqueId()){
				$this->session->set_flashdata('error', 1);
				$this->session->set_flashdata('message', 'You must be an admin to view this page');
				redirect('/auth/login');
			}
    }

		$this->session->set_userdata('ses', 'accommodations');
		$this->load->model('MAccommodationRoomPrices');
		$this->load->model('MFacilities');


	}

	public function add_new(){
		$this->session->set_userdata('ses', 'accommodation_room_prices_addnew');
		$data['facilities'] = $this->MFacilities->read_all();

		$this->load->view('/admin/index', $data);
  }

	public function create(){
		$dataPosted = $this->input->post();
		$createAccommodationRoomPrice =  $this->MAccommodationRoomPrices->create($dataPosted);
		echo json_encode($createAccommodationRoomPrice);
		// redirect('admin/accommodations/');
		// echo json_encode($dataPosted);

	}

	public function read_all_by_accommodation_rooms_id(){
		$id = $this->input->post("id");
		$readAccommodationRoomPrices = $this->MAccommodationRoomPrices->read_all_by_accommodation_rooms_id($id);
		echo json_encode($readAccommodationRoomPrices);
	}

	public function read_like(){
		$word = $this->input->post('word');
		$readAccommodationRoomPrice= $this->MAccommodationRoomPrices->read_like($word);
		echo json_encode($readAccommodationRoomPrice);
	}

	public function update(){
		$dataPosted = $this->input->post();
		$updateAccommodationRoomPrice = $this->MAccommodationRoomPrices->update($dataPosted);
		echo json_encode($updateAccommodationRoomPrice);
	}

	public function delete(){
		$id = $this->input->post('id');
		$deleteAccommodationRoomPrice =  $this->MAccommodationRoomPrices->delete($id);
		echo json_encode($deleteAccommodationRoomPrice);
	}

	public function delete_by_accommodation_rooms_id(){
		$accommodation_rooms_id = $this->input->post('accommodation_rooms_id');
		$deleteAccommodationRoomPrice =  $this->MAccommodationRoomPrices->delete_by_accommodation_rooms_id($accommodation_rooms_id);
		echo json_encode($deleteAccommodationRoomPrice);
	}

}

?>
