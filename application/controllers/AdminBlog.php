<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminBlog extends CI_Controller {

	function __construct() {

		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		$this->load->model('MBlog');
		if (!$this->ion_auth->logged_in()){
			$this->session->set_flashdata('error', 1);
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('/auth/login');
		}else{
			if($this->session->userdata('user')->type_id != 1){
				redirect('/');
			}
		}
	}

	public function index(){
		$this->session->set_userdata('ses', 'blog');
		$blogs = $this->MBlog->readAll();
		

		for($i = 0; $i<sizeof($blogs); $i++){
			$search_img = explode('http://localhost/dony/uploads/', $blogs[$i]->html);
			if(isset($search_img[1])){
				$search_img_2 = explode('"', $search_img[1]);
				// print_r($search_img_2[0]);
				$blogs[$i]->img = 'http://localhost/dony/uploads/'.$search_img_2[0];
			}else{
				$blogs[$i]->img = 'http://localhost/dony/assets/images/no-image.png';	
			}
		}

		$data['blogs'] = $blogs;
		$this->load->view('/admin/index', $data);
	  }
	  
	public function addnew(){
		$this->session->set_userdata('ses', 'blog_addnew');
		$this->load->view('/admin/index');
	}

	public function create(){
		$posted_data = $this->input->post();

		$blog = $this->MBlog;
		$blog->judul = $posted_data['judul'];
		$blog->html = $posted_data['html'];
		$blog_response = $blog->create();

		echo json_encode($blog_response);
		// print_r($posted_data['html']);
	}

	public function logout(){
		$this->ion_auth->logout();
		redirect('admin');
	}

	public function undercons(){
		$this->session->set_userdata('ses', 'undercons');
		$this->load->view('/admin/index');
	}

	//User Functions






}

?>
