<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library('ion_auth');
		$this->load->model('Ion_auth_model');
		$this->load->model('MProduk');
		$this->load->model('MCartProduk');
        $this->load->model('MMessages');
        $this->load->model('MMessageRooms');
        $this->load->model('MMessageRoomGuests');
        $this->load->model('MUsers');
        $this->load->library('RajaOngkir');

		if (!$this->ion_auth->logged_in()){
			if(!$this->MAccessWithoutAuth->hasUniqueId()){
				$this->session->set_flashdata('error', 1);
				$this->session->set_flashdata('message', 'You must be an admin to view this page');
				redirect('/auth/login');
			}
    	}

    }
    
    public function create(){
        $data = $this->input->post();
        
        $message = $this->MMessages;
        $message->messageroomid = $data['roomid'];
        $message->pengirimid = $this->session->userdata('user')->id;
        $message->message = $data['message'];
        $message->created_on = date("Y-m-d G:i:s.000000", time());
        // $message->created_on = '2019-03-13 00:00:00.000000';

        $message->is_read = 0;
        $message_response = $message->create();

        echo json_encode($message_response);
		redirect(base_url().'message/'.$data['username']);

    }

    public function readAll(){

    }

    public function readByUserId($username){
        $data = null;
        $data['username'] = $username;
        $data['user'] = $this->MUsers->readByUsername($username);

		$this->load->library('RajaOngkir');
        $raja_ongkir = new RajaOngkir;
        $location = $raja_ongkir->getCity($data['user']->city_id);
        $data['user']->location = $location->city_name.", ".$location->province;
        // print_r($location);

	
        if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
            $data['orders'] = $orders;
            
            $room_search = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $data['user']->id);
            
            
            if(!isset($room_search[0]->id)){
                $room = $this->MMessageRooms;
                $room->created_on = date("Y-m-d G:i:s.000000", time());
                $room_response = $room->create();

                $guest1 = new $this->MMessageRoomGuests;
                $guest1->messageroomid = $room_response['id'];
                $guest1->userid = $this->session->userdata('user')->id;
                $guest1->created_on = date("Y-m-d G:i:s.000000", time());
                $guest1_response = $guest1->create();
                
                $guest2 = new $this->MMessageRoomGuests;
                $guest2->messageroomid = $room_response['id'];
                $guest2->userid = $data['user']->id;
                $guest2->created_on = date("Y-m-d G:i:s.000000", time());
                $guest2_response = $guest2->create();
                $data['room_id'] = $room_response['id'];
                $data['messages'] = null;
            }else{
                $data['room_id'] = $room_search[0]->id;
                
                $messages = $this->MMessages;
                $messages->messageroomid = $room_search[0]->id;
                $messages_response = $messages->readByMessageRoomId();
                $data['messages'] = $messages_response;

                
                
            }

            // Get all first message
            $all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
            foreach($all_rooms as $room){
                $guests = new $this->MMessageRoomGuests;
                $guests->messageroomid = $room->id;
                $room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

                $messages = new $this->MMessages;
                $messages->messageroomid = $room->id;
                $room->messages = $messages->readByMessageRoomIdLimit();

            }
            $data['all_first_messages'] = $all_rooms;
       
        }
        


        $this->session->set_userdata('ses', 'message');
		$this->load->view('homepage/index', $data);


    }
  

	


	

}
?>
