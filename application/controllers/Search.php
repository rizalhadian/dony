<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('MProduk');
		$this->load->model('MHeadline');
		$this->load->model('MProdukJenis1');
		$this->load->model('MProdukJenis2');
		$this->load->model('MKategori');
		$this->load->model('MCartProduk');
		$this->load->model('MMessageRooms');
		$this->load->model('MMessages');
		$this->load->model('MMessageRoomGuests');

	}

	public function index(){
		$data = $this->input->get();
		$q = $data['q'];
		$produks= $this->MProduk->readLikeByName($q);
		foreach($produks as $produk){
			$namaproduk = $produk->nama;
			$namap = str_replace(' ', '-', $namaproduk);
			$produk->url = base_url()."u/".$produk->toko."/".$namap;
		}
		$data['produks'] = $produks;
		$data['headlines'] = $this->MHeadline->readAll();
		$data['produk_jenis_1s'] = $this->MProdukJenis1->readAll();
		$data['produk_jenis_2s'] = $this->MProdukJenis2->readAll();
		$kategoris = $this->MKategori->readAll();
		// echo json_encode($kategoris);

		$kategori_arr = array();
		$subkategoris = array();
		$parentkategoris = array();
		foreach($kategoris as $kat){
			if($kat->parent_id != null){
				array_push($subkategoris, $kat);
				// unset($kat);
			}else{
				array_push($parentkategoris, $kat);
			}
		}
		foreach($parentkategoris as $pk){
			$pk->sub = $subkategoris;
		}

		if(isset($this->session->userdata('user')->username)){
			// Get All Orders
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;		
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;

			// Get all first message
			$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
			foreach($all_rooms as $room){
				$guests = new $this->MMessageRoomGuests;
				$guests->messageroomid = $room->id;
				$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

				$messages = new $this->MMessages;
				$messages->messageroomid = $room->id;
				$room->messages = $messages->readByMessageRoomIdLimit();

			}
			$data['all_first_messages'] = $all_rooms;
		}
		


		$data['kategoris'] = $parentkategoris;
		$this->session->set_userdata('ses', 'search');
		$this->load->view('homepage/index', $data);
	}

	public function map()
	{
		$this->load->view('homepage/listingsmap');
	}

}
?>
