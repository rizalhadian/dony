<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library('ion_auth');
		$this->load->library('RajaOngkir');
		$this->load->model('Ion_auth_model');
		$this->load->model('MProduk');
		$this->load->model('MProdukJenis1');
		$this->load->model('MProdukJenis2');
		$this->load->model('MPhotos');
		$this->load->model('MCart');
		$this->load->model('MCartProduk');
		$this->load->model('MInvoice');

		$this->load->model('MMessages');
		$this->load->model('MMessageRooms');
		$this->load->model('MMessageRoomGuests');

	}


	public function index(){
		$this->session->set_userdata('ses', 'cart');
		

		$RajaOngkir = new RajaOngkir();

		$provinces = $RajaOngkir->getProvinces();
		$data['provinces'] = $provinces;
		
		if(isset($this->session->userdata('user')->username)){
			// Logged In
			$userid = $this->session->userdata('user')->id;
			$produk = $this->MCartProduk->readByUserId($userid);
			$toko = $this->MCartProduk->readByUserIdDistinctToko($userid);
			$data['produk'] = $produk;
			// $data['toko'] = $toko;
			$toko_arr = array();

			foreach($toko as $t){
				$counter = $this->MCartProduk->countProdukByToko($t->userid);
				$toko_arr[$t->userid] = $counter;
				$toko_arr[$t->userid][0]->count = 0;
			}

			foreach($produk as $p){
					$toko_arr[$p->userid_toko][0]->count += 1;
			}

			// print_r($toko_arr);
			$data['toko'] = $toko_arr;
		}else{
			// Not Logged In
			$sessid = $this->session->session_id;
			$produk = $this->MCartProduk->readBySessId($sessid);
			$toko = $this->MCartProduk->readBySessIdDistinctToko($sessid);
			$data['produk'] = $produk;
			// $data['toko'] = $toko;
			$toko_arr = array();
			

			foreach($toko as $t){
				// print_r($t) ;
				// countProdukByToko
				$counter = $this->MCartProduk->countProdukByToko($t->userid);
				$toko_arr[$t->userid] = $counter;
				// print_r($counter);
			}
			// print_r($toko_arr);
			$data['toko'] = $toko_arr;
		}
		// print_r($data);
		// echo json_encode($data);
		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}

		// Get all first message
		$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
		foreach($all_rooms as $room){
			$guests = new $this->MMessageRoomGuests;
			$guests->messageroomid = $room->id;
			$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

			$messages = new $this->MMessages;
			$messages->messageroomid = $room->id;
			$room->messages = $messages->readByMessageRoomIdLimit();

		}
		$data['all_first_messages'] = $all_rooms;

		$this->load->view('homepage/index', $data);
	}

	public function readOrders(){
		
		$this->session->set_userdata('ses', 'orders');
		if(isset($this->session->userdata('user')->username)){
			// $produks = $this->MCartProduk->readAllByUserIdProduk($this->session->userdata('user')->id);
			$invoices = $this->MCartProduk->readAllInvoiceByUserIdProduk($this->session->userdata('user')->id);
			$data['invoices'] = $invoices;
			// echo json_encode($invoices);

			$invoice_arr = array();

			// echo json_encode($invoice_arr);

			if(isset($this->session->userdata('user')->username)){
				$cart_produk_update = new $this->MCartProduk;
				$cart_produk_update->sessid = $this->session->session_id;
				$cart_produk_update->userid = $this->session->userdata('user')->id;
				
				$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
				
	
				$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
				$data['count_unread_order'] = sizeof($count_unread_order);
	
				
				$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
				$data['orders'] = $orders;
			}

			// Get all first message
		$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
		foreach($all_rooms as $room){
			$guests = new $this->MMessageRoomGuests;
			$guests->messageroomid = $room->id;
			$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

			$messages = new $this->MMessages;
			$messages->messageroomid = $room->id;
			$room->messages = $messages->readByMessageRoomIdLimit();

		}
		$data['all_first_messages'] = $all_rooms;

			$this->load->view('homepage/index', $data);
		}
	}

	public function readOrder($id){
		$this->session->set_userdata('ses', 'order');

		$cart_produk = $this->MCartProduk;
		$cart_produk->userid = 0;
		$cart_produk->invoiceid = $id;
		$cart_produk_response = $cart_produk->readByInvoiceId();

		$data['produks'] = $cart_produk_response;

		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}
		$this->load->view('homepage/index', $data);


		// Get all first message
		$all_rooms = $this->MMessageRooms->readByUserid($this->session->userdata('user')->id, $this->session->userdata('user')->id);
		foreach($all_rooms as $room){
			$guests = new $this->MMessageRoomGuests;
			$guests->messageroomid = $room->id;
			$room->guest = $guests->readByMessageRoomId($this->session->userdata('user')->id);

			$messages = new $this->MMessages;
			$messages->messageroomid = $room->id;
			$room->messages = $messages->readByMessageRoomIdLimit();

		}
		$data['all_first_messages'] = $all_rooms;
		
		echo json_encode($cart_produk_response);
	}

	public function addToCart(){
		$posted_data = $this->input->post();

		if(isset($this->session->userdata('user')->username)){
			//Login			
				$cartproduk = $this->MCartProduk;
				$cartproduk->sessid = $this->session->session_id;
				$cartproduk->userid = $this->session->userdata('user')->id;
				$cartproduk->produkid = $posted_data['produkid'];
				$cartproduk->userid_produk = $posted_data['userid_produk'];
				$cartproduk->cartid = 0;
				$cartproduk->status = null;
				$cartproduk->invoiceid = 0;
				$cartproduk->is_read = 0;
				$cartproduk->qty = $posted_data['qty'];
				$cartproduk_resp = $cartproduk->create();
		}else{
			// Not Login
				$cartproduk = $this->MCartProduk;
				$cartproduk->sessid = $this->session->session_id;
				$cartproduk->produkid = $posted_data['produkid'];
				$cartproduk->userid = 0;
				$cartproduk->userid_produk = $posted_data['userid_produk'];
				$cartproduk->cartid = 0;
				$cartproduk->status = null;
				$cartproduk->invoiceid = 0;
				$cartproduk->is_read = 0;
				$cartproduk->qty = $posted_data['qty'];
				$cartproduk->created_on = date("Y-m-d");
				$cartproduk_resp = $cartproduk->create();
		}
		redirect('/cart');
		

	}

	public function removeFromCart($cart_produk_id){
		// echo $cart_produk_id;

		$cartproduk = $this->MCartProduk;
		$cartproduk->id = $cart_produk_id;
		$cartproduk_remove_response = $cartproduk->delete();

		redirect('/cart');

	}

	public function checkout(){

		if(isset($this->session->userdata('user')->username)){
			$posted_data = $this->input->post();
			$city_ids_temp = $posted_data['city_ids'];
			$city_ids = array();

			for($i=0; $i<sizeof($city_ids_temp);$i++){
				$city_ids[$i] = explode('-', $city_ids_temp[$i])[0];
			}

			$produk_cheked = $posted_data['checkers'];
			$produks = array();
			$cities = array();

			foreach($city_ids as $city_id){
				$cities[$city_id] = (object)[];
				$servis_harga = $posted_data['servis-'.$city_id];
				$servis_harga_explode = explode('-', $servis_harga);
				$servis = $servis_harga_explode[0];
				
				if(isset($servis_harga_explode[1])){
					$harga = $servis_harga_explode[1];
				}

				$cities[$city_id]->servis = $servis;
				$cities[$city_id]->harga_servis_perkilo = $harga;
				$cities[$city_id]->total_harga_produk = 0;
				$cities[$city_id]->total_berat_produk = 0;
				$cities[$city_id]->produk = array();
			}

			for($i=0; $i<sizeof($produk_cheked);$i++){
				$produks[$i] = new $this->MCartProduk;
				$produks[$i]->id = $produk_cheked[$i];
				$get_produk = $produks[$i]->readById($produk_cheked[$i]);
				$produks[$i] = $get_produk[0];

				$cities[$produks[$i]->userid_toko]->total_harga_produk += ($produks[$i]->harga*$produks[$i]->qty);
				$cities[$produks[$i]->userid_toko]->total_berat_produk += (($produks[$i]->berat*$produks[$i]->qty)/1000);
				array_push($cities[$produks[$i]->userid_toko]->produk, $produks[$i]);
			}

			//Count Total Harga
			$total_harga = 0;
			foreach($cities as $city){
				$total_harga += ($city->total_harga_produk + (ceil($city->total_berat_produk)*$city->harga_servis_perkilo)); 	
			}

			// Create The Invoice
			$invoice = $this->MInvoice;
			$invoice->kode = "INV-".date("Ymdhis").rand(10,99);
			$invoice->userid = $this->ion_auth->user()->row()->id;
			$invoice->total = $total_harga;
			$invoice->is_paid = 0;
			$invoice->alamat = $posted_data['alamat'];
			$invoice->provinsi = $posted_data['provinsi'];
			$invoice->kota = $posted_data['kota'];
			$invoice->created_on = date("Y-m-d H:i:s");
			
			$invoice_response = $invoice->create();
			print_r($invoice);

			//Update The Product - Insert The Invoice Id
			foreach($cities as $city){
				foreach($city->produk as $produk){
					$cart_produk_update = new $this->MCartProduk;
					$cart_produk_update->id = $produk->id;
					$cart_produk_update->invoiceid = $invoice_response['id'];
					$cart_produk_update->total_ongkir = (ceil($city->total_berat_produk)*$city->harga_servis_perkilo);
					$cart_produk_update->total_berat = (ceil($city->total_berat_produk));
					$cart_produk_update->ongkir_perkilo = ($city->harga_servis_perkilo);
					$cart_produk_update_response = $cart_produk_update->update();
				}
			}
		}else{
			redirect('/login');
		}
	}

}

	

  


?>