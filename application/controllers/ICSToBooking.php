<?php
ini_set('max_execution_time', 0);
ini_set('memory_limit','2048M');
defined('BASEPATH') OR exit('No direct script access allowed');

class ICSToBooking extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('MAccommodationsNew');
		$this->load->model('MAccommodationRoomsNew');
		$this->load->model('MRates');
		$this->load->model('MPhotos');
		$this->load->model('MFacilities');
		$this->load->model('MMAccommodationsExperiences');
		$this->load->model('MMAccommodationsFacilities');
		$this->load->model('MMAccommodationsAdditionalRates');
		$this->load->model('MAccommodations');
		$this->load->model('MBookingAccommodations');
		$this->load->model('MICSDate');

	}

	public function index(){
		$this->load->view('homepage/index');
	}

	public function countAccommodationsActive(){
		echo json_encode($this->MAccommodationsNew->count());
	}



	public function updateICSToBooking($start_from){
		// echo $start_from;
		$accommodations = $this->MAccommodationsNew->readAllActive();
		foreach($accommodations as $acc){
			// $this->getBookingDates($acc->id);
		}
	}

	public function updateBookingFromIcsRandom(){
		$accommodations = $this->MAccommodationsNew->readRandomByICSUpdated();
		// echo json_encode($accommodations);
		foreach($accommodations as $acc){
			// $this->getBookingDates($acc->id);

			$ics_valid;

			if($this->getBookingDates($acc->id) == true){
				$ics_valid = 1;
			}else{
				$ics_valid = 0;
			}

			print_r("ics valid adalah = ".$ics_valid);

			$accom = new $this->MAccommodationsNew;
			$accom->id = $acc->id;
			$accom->flag_ics_valid = $ics_valid;
			$accom->ics_updated_on = date('Y-m-d H:i:s');
			// print_r($accom);
			$accom->update();
			// print_r($accom->update());
		}
	}

	public function getBookingDates($id){
		$ics_valid = true;

		//Update Calendar File
		$this->MAccommodations->updateCalendarFile($id);

		$room['info'] = $this->MAccommodations->read_id($id);
		$booked_dates = array();
		$booked_dates_unformated = array();

		print_r($room['info']);
		// Get ICS Booked Dates



		$this->load->library('IcalReader');

		$randName = uniqid();
		$pathName = "./uploads/ics/".$randName.".ics";

		if($room['info']->ics == ""){
			// echo "b";

			if($this->ical->get($room['info']->ics_url, $pathName)){
				$ics_valid = true;
			}else{
				return false;
			}

			$accom = new $this->MAccommodationsNew;
			$accom->id = $id;
			$accom->ics = $pathName;
			$accom->update();
		}else{
			// echo "bebebebe";
			$accom = new $this->MAccommodationsNew;
			if($this->ical->get($room['info']->ics_url, $room['info']->ics)){
				$accom->flag_ics_valid = 1;
			}else{
				$accom->flag_ics_valid = 0;
				return false;
			}


			$accom->id = $id;
			$accom->ics_updated_on = date('Y-m-d H:i:s');
			$accom->update();
		}

		$icsreader = new $this->icalreader;
		$ics = $icsreader->create($room['info']->ics);



		$vvent = $icsreader->events($ics);

		echo "<br>=================<br>";
		print_r($vvent);

		for($i = 0; $i < count($vvent); $i++){
			$booked_date_from_ics = new stdClass();
			$booked_date_from_ics->id = null;
			$booked_date_from_ics->booking_id = "ics";
			$booked_date_from_ics->gender = null;
			$booked_date_from_ics->first_name = "ics";
			$booked_date_from_ics->last_name = "ics";
			$booked_date_from_ics->checkin_date = date("Y-m-d", strtotime($vvent[$i]['DTSTART']));
			$booked_date_from_ics->checkout_date = date("Y-m-d", strtotime($vvent[$i]['DTEND']));
			$booked_date_from_ics->checkin_time = "12:08:00";
			$booked_date_from_ics->checkout_time = "12:00:00";
			$booked_date_from_ics->status = 2;

			// print_r($booked_date_from_ics->checkin_date);
			// echo " - ";
			// print_r($booked_date_from_ics->checkout_date);
			// echo " | ";
			// print_r($booked_date_from_ics);
			// echo "<br>";
      //
			array_push($booked_dates_unformated, $booked_date_from_ics);

		}
		// echo "==================================================================<br>";
		// print_r($booked_dates_unformated);
		// echo json_encode($booked_dates_unformated);
		// echo "<br>";
		// $delete_bookings = new $this->MBookingAccommodations;
		// $delete_bookings->accommodations_id = $id;
		// $delete_bookings->deleteByAccommodationsId();

		$mics_delete = new $this->MICSDate;
		$mics_delete->accommodations_id = $id;
		$mics_delete_response = $mics_delete->deleteByAccommodationsId();
		echo "<br>================== Delete ==================<br>";
		print_r($mics_delete_response);
		echo "<br>================== Delete ==================<br>";


		foreach($booked_dates_unformated as $bd){
			$checkin_date = $bd->checkin_date;
			$checkout_date = $bd->checkout_date;
			$checkin_time = $bd->checkin_time ;
			$checkout_time = $bd->checkout_time;
			$time_diff = abs(strtotime($checkin_date) - strtotime($checkout_date));
			$count_days = $time_diff/86400;
      //
			// echo "==================================================================<br>";
			// print_r($bd);
			// echo "<br>";

			// $booking = new $this->MBookingAccommodations;
			// $booking->from_ics = 1;
			// $booking->checkin_date = $checkin_date;
			// $booking->checkout_date = $checkout_date;
			// $booking->checkin_time = $checkin_time;
			// $booking->checkout_time = $checkout_time;
			// $booking->accommodations_id = $id;
			// $booking->created_on = date('Y-m-d H:i:s');

			// $mics_delete = $this->MICSDate;
			// $mics_delete->accommodations_id = $id;
			// echo json_encode($mics_delete->deleteByAccommodationsId());


			$mics = new $this->MICSDate;
			$mics->checkin_date = $checkin_date;
			$mics->checkout_date = $checkout_date;
			$mics->accommodations_id = $id;
			$mics->created_on = date('Y-m-d H:i:s');
			$response = $mics->create();

			// print_r($response);
		}
		// echo json_encode($booked_dates);
		return $ics_valid;
	}

}
?>
