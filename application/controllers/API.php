<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller {

	function __construct() {
		parent::__construct();

    $this->load->model('MAccommodationTypes');
		$this->load->model('MAccommodations');
		$this->load->model('MAccommodationsNew');
		$this->load->model('MAccommodationRooms');
		$this->load->model('MAccommodationRoomsNew');
		$this->load->model('MAccommodationRoomPrices');
		$this->load->model('MServices');
		$this->load->model('MLocations');
		$this->load->model('MFacilities');
		$this->load->model('MExperiences');
		$this->load->model('MRates');
		$this->load->model('MPhotos');
		$this->load->model('MPartners');
		$this->load->model('MAdditionalRates');
		$this->load->model('MServices');
		$this->load->model('MMAccommodationsFacilities');
		$this->load->model('MMAccommodationsAdditionalRates');
		$this->load->model('MMAccommodationsExperiences');
    $this->load->model('MOTAAirbnb');
		$this->load->model('minterestingplaces');
		$this->load->model('MBookingAccommodations');
		$this->load->model('MICSDate');


		$this->load->library('Tripadvisor');
	}

	public function apiReadBookings(){
		echo "asd";
	}

  public function readAccommodation(){
    $accommodations = $this->MAccommodationsNew->readAllActive();
    $arrayRooms = array();
    $arrayLocations = array();
    for($accommodation = 0; $accommodation < sizeof($accommodations); $accommodation++){
      $r = $this->MAccommodationRoomsNew;
      $r->accommodations_id = $accommodations[$accommodation]->id;
      $rooms = $r->readByAccommodationsId();
      $locations = $this->MLocations->read_include_all_parents_by_id($accommodations[$accommodation]->locations_id);
      for($room = 0; $room < sizeof($rooms); $room++){
        array_push($arrayRooms, $rooms[$room]->total_rooms);
      }
      for($location = 0; $location < sizeof($locations); $location++){
        array_push($arrayLocations, $locations[$location]->name);
      }

      $accommodations[$accommodation]->rooms = $arrayRooms;
      $arrayRooms = array();
      $accommodations[$accommodation]->locations = $arrayLocations;
      $arrayLocations = array();
    }


    echo json_encode($accommodations);
  }

	public function readAccommodationById($id){
		$accommodation = new $this->MAccommodationsNew;
		$accommodation->id = $id;
		$accommodation_response = $accommodation->readById();

		$rooms = new $this->MAccommodationRoomsNew;
		$rooms->accommodations_id = $id;
		$rooms_response = $rooms->readByAccommodationsId();

		$accommodation_response->rooms = $rooms_response;
		echo json_encode($accommodation_response);

	}

  public function readInterestingPlaces(){
    $ip =  $this->minterestingplaces;
    $interesting_places = $ip->read();
		// echo json_encode($restaurants->readAllRestaurants());
    echo json_encode($interesting_places);
  }

	public function readBookedDatesByAccommodationsId($id){
		$mba = $this->MICSDate;

		$mba->accommodations_id = $id;
		$dates = $mba->readByAccommodationsIdForPublic();

		// print_r($dates);

		$formatted_dates = [];
		foreach($dates as $date){
			$formatted_date = [];
			$days = abs(strtotime($date->checkout_date) - strtotime($date->checkin_date))/86400;
			if($days>1){
				for($i=1; $i<$days; $i++){
					// date('m/d/y',date(strtotime("+".$i." day", strtotime($date->checkin_date))));
					array_push($formatted_dates, date('m/d/Y',date(strtotime("+".$i." day", strtotime($date->checkin_date)))));

				}
			}else{
				array_push($formatted_dates, date("m/d/Y", strtotime($date->checkin_date)));
			}
			// echo json_encode($date);
			// echo "days=".$days;
			// echo "<br>===";
			// echo json_encode($formatted_date);
			// echo "===<br>";

		}
		echo json_encode($formatted_dates);
	}

	function readAccommodationSearch(){
		$data['get'] = $this->input->get();
		$data['locations'] = $this->MLocations->readAllExceptParent();
		$data['pages'] = ceil($this->MAccommodationsNew->count()/6);
		$response_accommodations = $this->MAccommodationsNew->readIncludingRoomsSearch();
		foreach($response_accommodations as $response_accommodation){
			//Get Room Settings
			$rooms = new $this->MAccommodationRoomsNew;
			$rooms->accommodations_id = $response_accommodation->id;
			$response_accommodation->rooms = $rooms->readByAccommodationsId();

			// Get Rates for each Room Setting
			$lowest_base_rate = null;
			$lowest_rate = null;

			for($room = 0; $room < sizeof($response_accommodation->rooms); $room++){
				$rates = new $this->MRates;
				$rates->accommodation_rooms_id = $response_accommodation->rooms[$room]->id;
				$rates_response = $rates->readByAccommodationRoomsId();
				$response_accommodation->rooms[$room]->rates = $rates_response;

				foreach($rates_response as $rate){
					// Find Lowest Base Rate
					if($rate->type == 0){
						if($lowest_base_rate == null){
							$lowest_base_rate = $rate->rate_mature;
						}else{
							if($lowest_base_rate > $rate->rate_mature){
								$lowest_base_rate = $rate->rate_mature;
							}
						}
					}
					if($lowest_rate == null){
						$lowest_rate = $rate->rate_mature;
					}else{
						if($lowest_rate > $rate->rate_mature){
							$lowest_rate = $rate->rate_mature;
						}
					}
				}
			}

			$response_accommodation->lowest_rate = $lowest_rate;
			$response_accommodation->lowest_base_rate = $lowest_base_rate;

			$photos = $this->MPhotos;
			$photos->accommodations_id = $response_accommodation->id;
			$photos_responses = $photos->readByAccommodationsIdFirst();
			$response_accommodation->photos = $photos_responses;
		}
		$data['accommodations'] = $response_accommodations;

		echo json_encode($data['accommodations']);
	}

	function countRates(){
		$data_posted = $this->input->post();


		$accommodation = $this->MAccommodationsNew;
		$accommodation->id = $data_posted['accommodations_id'];
		$accommodation_response = $accommodation->readById();




		$included_tax;
		$tax_percentage;
		if(isset($accommodation_response)){
			if($accommodation_response->include_tax == 1){
				$included_tax = 1;
				$tax_percentage = $accommodation_response->tax_percentage;
			}else{
				$included_tax = 0;
				$tax_percentage = $accommodation_response->tax_percentage;
			}
		}



		$date_start = $data_posted['date_start'];
		$date_end = $data_posted['date_end'];

		$days = abs(strtotime($date_start) - strtotime($date_end))/86400;


		$rates = [];
		$rates_total = 0;
		for($i=0; $i<$days; $i++){
			$rate;
			$day = date('Y-m-d', strtotime($date_start. ' +'.$i.' days'));

			$r = $this->MRates;
			$r->accommodation_rooms_id = $data_posted['accommodation_rooms_id'];
			$rates_response = $r->readByAccommodationRoomsIdDate($day);
			$rate = array(
				'date' => $day,
				'rate' => $rates_response[0]->rate_mature,
				'rates_id' => $rates_response[0]->id
			);
			$rates_total = $rates_total+$rates_response[0]->rate_mature;
			// echo json_encode($rate);
			array_push($rates, $rate);
		}

		$response = array(
			'rates_total' => $rates_total,
			'nights' => $days,
			'rates_average' => ceil($rates_total/$days),
			'include_tax' => $included_tax,
			'tax_percentage' => $tax_percentage,
			'tax_total' => ceil($rates_total*($tax_percentage/100)),
			'details' => $rates,
			'rates_taxes_total' => $rates_total+ceil($rates_total*($tax_percentage/100))
		);
		echo json_encode($response);
	}

	function readCreateAccommodationFromTBV(){
		$json = file_get_contents('http://localhost/tbv/accommodation-random');
		$obj = json_decode($json);

			//Create Accommodation
			$accommodation = new $this->MAccommodationsNew;
			$accommodation->name = $obj->name;
			$accommodation->accommodation_types_id = 4;
			$accommodation->description = $obj->desc;
			$accommodation->tags = $obj->tags;
			$accommodation->management = 0;
			$accommodation->partners_id = 0;
			$accommodation->include_tax = 0;
			$accommodation->tax_percentage = 0;

			$loc = $this->readLocationLike($obj->location);
			if(isset($loc->value)){
				$accommodation->locations_id = $loc->value;
			}else{
				$accommodation->locations_id = 1;
			}

			$location = new $this->MLocations;
			$location->id = $accommodation->locations_id;
			$response_location = $location->readById();

			$accommodation->lat = $obj->lat;
			$accommodation->lng = $obj->lng;
			$accommodation->total_rooms = $obj->total_bedrooms;
			$accommodation->code =  strtolower($response_location[0]->name)."-".$accommodation->total_rooms."-bedrooms-villa-".rand(0, 999);
			$accommodation->total_bathrooms = $obj->total_bathrooms;
			$accommodation->total_beds = 0;



			$accommodation->active = 2;
			$accommodation->renting_type = 0;
			$accommodation->building_size = 0;
			$accommodation->land_size = $obj->area;
			$accommodation->created_on = date('Y-m-d H:i:s');
			$accommodation_response = $accommodation->create();


			$this->id = $accommodation_response->id;

			//Create room
			$room = new $this->MAccommodationRoomsNew;
			$room->name = $accommodation_response->name." ".$accommodation_response->total_rooms." bedrooms ";
			$room->total_rooms = $accommodation_response->total_rooms;

			if(isset($response_location[0])){
				$room->code = $response_location[0]->name."-".$accommodation_response->total_rooms."-bedrooms-".rand(0, 999);
			}else{
				$room->code = null;
			}

			$room->total_beds = $accommodation_response->total_beds;
			$room->total_bathrooms = $accommodation_response->total_bathrooms;
			$room->min_stay = 0;
			$room->max_guest_normal = 0;
			$room->max_guest_extra = 0;
			$room->description = $accommodation_response->description;
			$room->tags = $accommodation_response->tags;
			$room->accommodations_id = $accommodation_response->id;
			$room->renting_type = $accommodation_response->renting_type;
			$room->created_on = date('Y-m-d H:i:s');
			$room->active = 2;
			$room->synced = 0;
			$room_response = $room->create();

			$mm_acc_fac = $this->MMAccommodationsFacilities;
			$mm_acc_fac->accommodations_id = $accommodation_response->id;
			$mm_acc_fac->facilities_id = 1;
			$mm_acc_fac->created_on = date('Y-m-d H:i:s');
			$mm_acc_fac->create();

			$change_status = file_get_contents('http://localhost/tbv/accommodation-status-change/'.$accommodation_response->id);

			if(isset($obj->photos[0])){
				foreach($obj->photos as $photo){
					$photoz = new $this->MPhotos;
					$photoz->name = $photo->images_link;
					$photoz->path = "uploads/photos/accommodations/".$photo->images_link;
					if($photo->images_primary == "y"){
						$photoz->ordinal = 1;

					}else{
						$photoz->ordinal = 2;
					}
					$photoz->accommodations_id = $accommodation_response->id;
					$photoz->created_on = date('Y-m-d H:i:s');
					$photo_response = $photoz->create();
				}
			}

	}

	public function readLocationLike($word){
		$word_arr = explode(", ", $word);
		$readLocation = $this->MLocations->read_like($word_arr[0]);
		// echo json_encode($readLocation);
		if(isset($readLocation[0])){
			return $readLocation[0];
		}else{
			return false;
		}
	}

	public function readById(){

	}

}

?>
