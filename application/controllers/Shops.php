<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shops extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->library('ion_auth');
		$this->load->library('RajaOngkir');
		
		$this->load->model('Ion_auth_model');
		$this->load->model('MShops');
		$this->load->model('MProduk');
		$this->load->model('MCartProduk');
		$this->load->model('MUsers');

	}

	public function index(){
    // if($this->checkHadToko()){
    //   $this->session->set_userdata('ses', 'shops');
		// 	$data['produks'] = $this->MProduk->readByUserId($this->session->userdata('user')->id);
		// 	// echo json_encode($data['produks']);
  	// 	$this->load->view('homepage/index', $data);
    // }else{
    //   $this->session->set_userdata('ses', 'shops_notfound');
  	// 	$this->load->view('homepage/index');
		// }

		if(isset($this->session->userdata('user')->username)){
			$cart_produk_update = new $this->MCartProduk;
			$cart_produk_update->sessid = $this->session->session_id;
			$cart_produk_update->userid = $this->session->userdata('user')->id;
			
			$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
			

			$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
			$data['count_unread_order'] = sizeof($count_unread_order);

			
			$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
			$data['orders'] = $orders;
		}

		
		$id = $this->session->userdata('user')->id;
		$user_get_response = $this->MUsers->readById($id);
		$data['user'] = $user_get_response;

		$raja_ongkir = new RajaOngkir;
		$data['location'] = $raja_ongkir->getCity($user_get_response->city_id);

		// echo json_encode($data['location']);

		if($user_get_response->is_detail_setted == 0){
			$this->session->set_userdata('ses', 'shops_notfound');
  			$this->load->view('homepage/index');
		}else{
			$this->session->set_userdata('ses', 'shops');
			$data['produks'] = $this->MProduk->readByUserId($this->session->userdata('user')->id);

			// echo json_encode($data['produks']);
  			$this->load->view('homepage/index', $data);
		}
		
		// echo json_encode($this->session);
	}

  private function checkHadToko(){
    $data_posted = $this->input->post();
    $toko = $this->MShops;
    $toko->userid = $this->session->userdata('user')->id;
    $toko_response = $toko->checkHadToko();
    return $toko_response;
  }

  public function createNew(){
		// echo "hele";
		$RajaOngkir = new RajaOngkir();

		$provinces = $RajaOngkir->getProvinces();
		$data['provinces'] = $provinces;


		$userid = $this->session->userdata('user')->id;
		$user_get_response = $this->MUsers->readById($userid);
		$data['user'] = $user_get_response;

		

   		$this->session->set_userdata('ses', 'shops_createnew');
		$this->load->view('homepage/index', $data);
  }

  public function create(){
		$data_posted = $this->input->post();
		// $toko = $this->MShops;
		// $toko->nama = $data_posted['nama'];
		// $toko->userid = $this->ion_auth->user()->row()->id;
		// $toko->address = $data_posted['address'];
		// $toko->phone = $data_posted['phone'];
		// $toko->lat = $data_posted['lat'];
		// $toko->lng = $data_posted['lng'];
		// $toko_response = $toko->create();

		$user = $this->MUsers;
		$user->id = $this->session->userdata('user')->id;
		$user->username = $data_posted['username'];
		$user->province_id = $data_posted['provinsi'];
		$user->city_id = $data_posted['kota'];
		$user->address = $data_posted['address'];
		$user->phone = $data_posted['phone'];
		$user->lat = $data_posted['lat'];
		$user->lng = $data_posted['lng'];
		$user->is_detail_setted = 1;

		$user_update_response = $user->update();

		echo json_encode($user_update_response);
  }

  public function readByUsername($username){
	$user_get_response = $this->MUsers->readByUsername($username);
	$data['user'] = $user_get_response;

	$raja_ongkir = new RajaOngkir;
	$data['location'] = $raja_ongkir->getCity($user_get_response->city_id);

	$produk_get_response = $this->MProduk->readByUserId($data['user']->id);
	$data['produks'] = $produk_get_response;

	$created_on = date("d-M-Y", strtotime($data['user']->created_on));
	$data['user']->created_on = $created_on;

	if(isset($this->session->userdata('user')->username)){
		$cart_produk_update = new $this->MCartProduk;
		$cart_produk_update->sessid = $this->session->session_id;
		$cart_produk_update->userid = $this->session->userdata('user')->id;
		
		$cart_produk_update_response = $cart_produk_update->updateUserIdBySessid();
		

		$count_unread_order = $this->MCartProduk->CountUnread($this->session->userdata('user')->id);
		$data['count_unread_order'] = sizeof($count_unread_order);

		
		$orders = $this->MCartProduk->readByUseridProduk($this->session->userdata('user')->id);
		$data['orders'] = $orders;
	}

	$this->session->set_userdata('ses', 'shop_read');
	$this->load->view('homepage/index', $data);
  }

}
?>
