<?php
		
	session_start();	

	if(isset($_POST["submit"])){
		//Checking for blank Fields..
		
		// echo $_SESSION["code"];
		// echo "<br>";
		// echo $_POST["captcha"];

		if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&$_SESSION["code"]==$_POST["captcha"]){	

			if($_POST["fullname"]==""||$_POST["email"]==""||$_POST["subject"]==""||$_POST["message"]==""){
				echo "Fill All Fields..";
			}else{
				// Check if the "Sender's Email" input field is filled out
				$email=$_POST['email']; 
				// Sanitize e-mail address
 				$email =filter_var($email, FILTER_SANITIZE_EMAIL);
 				// Validate e-mail address
				$email= filter_var($email, FILTER_VALIDATE_EMAIL);
 				if (!$email){
					echo "<script>alert('Invalid Sender's Email, Please try again'); window.location = 'index.php#cntk'</script>";
	 			}else{

	 				$headers = 'From:'. $email . "\r\n"; // Sender's Email
	 				$fullname = $_POST['fullname']; // required
					$message_post = $_POST['message'];
	 				$subject = "Contact Us Villa Moro | ".$_POST['subject'];;
	 				function clean_string($string) {
	      				$bad = array("content-type","bcc:","to:","cc:","href");
	      				return str_replace($bad,"",$string);
	 				}	

					// message lines should not exceed 70 characters (PHP rule), so wrap it
					// $message = wordwrap($message, 200);
	 				$message = "Fullname: ".clean_string($fullname)."\n";
	 				$message .= "Email: ".clean_string($email)."\n";
	 				$message .= "Message: ".clean_string($message_post)."\n";
	 				// echo $message;
	 				// Send mail by PHP Mail Function
					
					// mail("aryabwidyatmika@gmail.com", $subject, $message, $headers);
					mail("info@villamorobali.com", $subject, $message, $headers);
					
					echo "<script>alert('Your mail has been sent successfuly ! Thank you for your feedback');window.location = 'index.php#cntk'</script>";
	 			}
			}
		}else{
			echo "<script> alert('Captcha validation failed, Please try again'); window.location = 'index.php#cntk'</script>";
		}
	}

?>