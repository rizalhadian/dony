-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2019 at 06:38 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tadony`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `html` text NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `judul`, `html`, `created_on`, `updated_on`) VALUES
(2, 'adade', '<h1><strong>Textarea</strong></h1><p><strong><img src=\"http://localhost/dony/uploads/a1e8cd5d441679491003a4f78ca347ef.png\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></strong><br></p><p>The editor can also be initialized on a textarea.</p>', '2018-12-18 14:26:29', '2018-12-18 14:26:29'),
(3, 'tanpa gambar', '<h1>Textarea</h1><p>The editor can also be initialized on a textarea.</p>', '2018-12-20 14:07:34', '2018-12-20 14:07:34'),
(5, 'Bagaimana cara melakukan pembayaran', '<h1>Textarea</h1><p><img src=\"http://localhost/dony/uploads/0b83339f29dcc49cb733d362a452e200.png\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p><p>The editor can also be initialized on a textarea.</p><p><br></p><p><img src=\"http://localhost/dony/uploads/7cbc7b125e08b141377882e481a8b9c9.png\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p>', '2018-12-20 15:48:54', '2018-12-20 15:47:28'),
(6, 'How to make Popcorn', '<p><img src=\"http://localhost/dony/uploads/d668409082e32b3bd5e1d003a4911437.jpg\" style=\"width: 692px;\" class=\"fr-fic fr-dib\"></p><p>While you can pop it using any method you prefer, it&rsquo;s easier to work with unsalted, unbuttered popcorn, so an air popper usually works best. If you don&rsquo;t have one, you can also pop your corn in a pan or skillet on your stove top or put kernels into paper bag semi-closed in the microwave or 2:30 minutes .&nbsp;</p><p><br></p><ul style=\"list-style-type: circle;\"><li>To figure out how much popcorn you&rsquo;ll need for your garland, keep in mind that 1 cup typically covers 3 to 4 feet of thread.</li><li>If you pop the corn in pan, you&rsquo;ll need to add a little oil to the bottom to help the corn cook. To prevent it from becoming soggy, place the corn on a paper towel-lined plate or dish when you remove it from the pan, so some of the oil will be absorbed.</li><li>When you&rsquo;re in a hurry, you can use&nbsp;<a href=\"https://www.wikihow.com/Make-Microwave-Popcorn\" title=\"Make Microwave Popcorn\">microwave popcorn</a> or a pre-popped bag. Just make sure that it has no salt or butter.</li></ul>', '2018-12-20 16:08:31', '2018-12-20 16:08:31'),
(7, 'How to make Popcorn (2)', '<p><span class=\"fr-video fr-fvc fr-dvb fr-draggable\" contenteditable=\"false\" draggable=\"true\"><iframe width=\"640\" height=\"360\" src=\"https://www.youtube.com/embed/qVuGe_qYm0E?wmode=opaque\" frameborder=\"0\" allowfullscreen=\"\" class=\"fr-draggable\"></iframe></span><br></p><p>While you can pop it using any method you prefer, it&rsquo;s easier to work with unsalted, unbuttered popcorn, so an air popper usually works best. If you don&rsquo;t have one, you can also pop your corn in a pan or skillet on your stove top or put kernels into paper bag semi-closed in the microwave or 2:30 minutes .&nbsp;</p><p><br></p><ul style=\"list-style-type: circle;\"><li>To figure out how much popcorn you&rsquo;ll need for your garland, keep in mind that 1 cup typically covers 3 to 4 feet of thread.</li><li>If you pop the corn in pan, you&rsquo;ll need to add a little oil to the bottom to help the corn cook. To prevent it from becoming soggy, place the corn on a paper towel-lined plate or dish when you remove it from the pan, so some of the oil will be absorbed.</li><li>When you&rsquo;re in a hurry, you can use&nbsp;<a href=\"https://www.wikihow.com/Make-Microwave-Popcorn\" title=\"Make Microwave Popcorn\">microwave popcorn</a> or a pre-popped bag. Just make sure that it has no salt or butter.</li></ul>', '2018-12-20 16:10:54', '2018-12-20 16:10:54'),
(8, 'Iklan 1', '<h1><img src=\"http://localhost/dony/uploads/4b84b44038c1eb378666011d69810b52.jpg\" style=\"width: 522px;\" class=\"fr-fic fr-dib\"></h1><h1>Textarea</h1><p>The editor can also be initialized on a textarea.</p>', '2019-01-24 11:04:54', '2019-01-24 11:04:54'),
(9, 'Iklan 2', '<h1><img src=\"http://localhost/dony/uploads/10325498d70996a1e01793c2ffc15b7d.jpg\" style=\"width: 1146px;\" class=\"fr-fic fr-dib\"></h1><h1>Textarea</h1><p>The editor can also be initialized on a textarea.</p>', '2019-01-24 11:09:42', '2019-01-24 11:09:42'),
(10, 'Iklan 3', '<h1>Textarea</h1><p><img src=\"http://localhost/dony/uploads/9ad78115a13ed8e29a6cfd2103c1c281.jpg\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p><p>The editor can also be initialized on a textarea.</p>', '2019-01-24 11:10:21', '2019-01-24 11:10:21');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `sessionid` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `sessionid`, `userid`, `total_harga`, `status`, `created_on`, `updated_on`) VALUES
(7, '61a82218ec87bf98468cd60ef8ccf3081bfe2864', NULL, NULL, NULL, NULL, NULL),
(8, '6550edf76e00d6b5a0d1a9e075724f91556cd331', NULL, NULL, NULL, NULL, NULL),
(9, 'd382301abfa3aa7b4b7b79173c8e5fb947c14b7b', NULL, NULL, NULL, NULL, NULL),
(10, '509ee6c5366234eca5311706a28e134d0800088b', NULL, NULL, NULL, NULL, NULL),
(11, 'e08573d64f2e69ae140be9e89303f3e49a50efdc', NULL, NULL, NULL, NULL, NULL),
(12, 'eff23a8d6c40c4a690d7dfca9203fbcb4653dd68', NULL, NULL, NULL, NULL, NULL),
(13, 'b10a494b2d5a2975a90f86fb3628914829031e2b', NULL, NULL, NULL, NULL, NULL),
(14, '396da53ec8d6a46438bb207934cf88dd9df72d0e', NULL, NULL, NULL, NULL, NULL),
(15, 'be211083911209ec54d04a940e93d458c9a3944d', NULL, NULL, NULL, NULL, NULL),
(16, '31e419f1fa44e1405fbdc723641ad8e0209bbbc8', NULL, NULL, NULL, NULL, NULL),
(17, '07cfaa5a7d12c26bc8126e6395a5c6453d528349', NULL, NULL, NULL, NULL, NULL),
(18, '824c56e25ef07427d527e7742fbf38d6df9c769f', NULL, NULL, NULL, NULL, NULL),
(19, 'a747f9d33debd6b14313a0cee8c75cd74146c78c', NULL, NULL, NULL, NULL, NULL),
(20, 'c088a047d3c51bc2b5a95ec7cc465e8edee6346e', NULL, NULL, NULL, NULL, NULL),
(21, '87215fd9a7837d82ac23423e46a4472b67351188', NULL, NULL, NULL, NULL, NULL),
(22, '8da822e3e5e04522f6e0ee2cb510bf3770dcaafc', NULL, NULL, NULL, NULL, NULL),
(23, 'feb94aeeac3624a8e977a840e8c21bb475eed54c', NULL, NULL, NULL, NULL, NULL),
(24, 'deb3046797d8ba1346aa4dee7adcb93c4772322b', NULL, NULL, NULL, NULL, NULL),
(25, '6d7a34792626edb09e4161d2871f4cfbbb021d29', NULL, 123, NULL, NULL, NULL),
(26, '9b369c5365be05b2cf94362f76d0276e50d55a39', NULL, 123, NULL, NULL, NULL),
(27, 'c80e0ebb6ccc42b23c1f9119f85413ba64facb1e', NULL, 246, NULL, NULL, NULL),
(28, '3923534caa0ea320f92d198d4e3f2c5a2d31e167', NULL, 246, NULL, NULL, NULL),
(29, 'bc963bb626ee6868c659d24602e7a652939a2bcd', NULL, 123, NULL, NULL, NULL),
(30, '9k2699pgt3ji6kvreqg75r63n0bvmoht', NULL, 123, NULL, NULL, NULL),
(31, '3v6k0rk9nlfag2ai2oucfbi9qkgk1qha', NULL, 123, NULL, NULL, NULL),
(32, 'earmm8spdbgeemqlo15pra36v0sqs0om', NULL, 123, NULL, NULL, NULL),
(33, '587q0q3700qgatt9kpve6qeriebs44mn', NULL, 369, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_produk`
--

CREATE TABLE `cart_produk` (
  `id` int(11) NOT NULL,
  `sessid` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `cartid` int(11) DEFAULT NULL,
  `invoiceid` int(11) DEFAULT NULL,
  `produkid` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_ongkir` bigint(20) DEFAULT NULL,
  `total_berat` int(10) DEFAULT NULL,
  `ongkir_perkilo` bigint(20) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_produk`
--

INSERT INTO `cart_produk` (`id`, `sessid`, `userid`, `cartid`, `invoiceid`, `produkid`, `qty`, `total_ongkir`, `total_berat`, `ongkir_perkilo`, `created_on`, `updated_on`) VALUES
(43, '91c4ncn69fh3on524pavb7904mlu2u8r', 11, 0, 13, 34, 0, 456000, 12, 38000, '2018-12-31 16:00:00', NULL),
(44, '91c4ncn69fh3on524pavb7904mlu2u8r', 11, 0, 13, 36, 0, 912000, 24, 38000, NULL, NULL),
(45, '57jvfanmnmjdc1i0okej72n3jj2ek1s1', 11, 0, 13, 35, 0, 912000, 24, 38000, NULL, NULL),
(48, '4k92ljppip4f7sla94q0f5b6bq49urma', 11, 0, 14, 27, 0, 43000, 1, 43000, NULL, NULL),
(49, '4k92ljppip4f7sla94q0f5b6bq49urma', 11, 0, 15, 30, 0, 240000, 12, 20000, NULL, NULL),
(50, 'dr035lscqjmvf8ikrol9n1q6r95dd3oq', 11, 0, 15, 31, 0, 240000, 12, 20000, NULL, NULL),
(51, 'kr93ptf5v35d0pvjnrotrh5d11mfvaqb', NULL, 0, 0, 27, 0, NULL, NULL, NULL, NULL, NULL),
(52, 'kr93ptf5v35d0pvjnrotrh5d11mfvaqb', 0, 0, 0, 27, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL),
(53, 'v83mpjj0igsgbpf864pg6ej3o532ivjk', 0, 0, 0, 27, 0, NULL, NULL, NULL, '2019-01-20 16:00:00', NULL),
(54, 'mslebqcc0lq96ijikjfprfpt0bg9mq8h', 0, 0, 0, 27, 0, NULL, NULL, NULL, '2019-01-20 16:00:00', NULL),
(55, 'llgk1krs09bruma54l47aqq6bcpdbhho', 11, 0, 0, 25, 0, NULL, NULL, NULL, '2019-01-20 16:00:00', NULL),
(56, 'q9j78nqn1kakubrkgarkmj7ubjn4de5l', 11, 0, 0, 47, 0, NULL, NULL, NULL, '2019-01-20 16:00:00', NULL),
(57, 'e15mihpgn6ku9q0tk588fsuf2mogh189', 11, 0, 0, 57, 1, NULL, NULL, NULL, '2019-01-20 16:00:00', NULL),
(58, 'r9cpipblr0srqqlmm9ncqudg4719miqd', 11, 0, 0, 25, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `headline`
--

CREATE TABLE `headline` (
  `id` int(11) NOT NULL,
  `blogid` int(11) DEFAULT NULL,
  `produkid` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `headline`
--

INSERT INTO `headline` (`id`, `blogid`, `produkid`, `img`, `date_start`, `date_end`, `created_on`, `updated_on`) VALUES
(8, 8, NULL, 'http://localhost/dony/uploads/4b84b44038c1eb378666011d69810b52.jpg', NULL, NULL, NULL, NULL),
(9, 9, NULL, 'http://localhost/dony/uploads/10325498d70996a1e01793c2ffc15b7d.jpg', NULL, NULL, NULL, NULL),
(10, 10, NULL, 'http://localhost/dony/uploads/9ad78115a13ed8e29a6cfd2103c1c281.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `iklan`
--

CREATE TABLE `iklan` (
  `id` int(11) NOT NULL,
  `produkid` int(11) DEFAULT NULL,
  `tokoid` int(11) DEFAULT NULL,
  `type_iklan` tinyint(1) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `kode` varchar(255) NOT NULL,
  `userid` int(11) NOT NULL,
  `total` bigint(20) NOT NULL,
  `provinsi` varchar(40) DEFAULT NULL,
  `kota` varchar(40) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `nama_penerima` varchar(40) DEFAULT NULL,
  `telp_penerima` varchar(20) DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `kode`, `userid`, `total`, `provinsi`, `kota`, `alamat`, `nama_penerima`, `telp_penerima`, `is_paid`, `created_on`, `updated_on`) VALUES
(13, 'INV-2019011910391320', 11, 1510000, '2', '28', '', NULL, NULL, 0, '2019-01-19 14:39:13', NULL),
(14, 'INV-2019011910502393', 11, 53000, '7', '130', '', NULL, NULL, 0, '2019-01-19 14:50:23', NULL),
(15, 'INV-2019011910532818', 11, 1370990, '5', '210', '', NULL, NULL, 0, '2019-01-19 14:53:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '::1', 'accounting@totalbali.com', 1516244934),
(2, '::1', 'accounting@totalbali.com', 1516244954);

-- --------------------------------------------------------

--
-- Table structure for table `merek`
--

CREATE TABLE `merek` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `produkid` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `urutan` tinyint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `name`, `produkid`, `url`, `urutan`) VALUES
(84, '7d1f891d0dafabbf70c13683f3a2f4', 25, 'http://localhost/dony//uploads/7d1f891d0dafabbf70c13683f3a2f4ba.jpg', 0),
(85, '9d55cfbb911ec91df40684f467f65c', 25, 'http://localhost/dony//uploads/9d55cfbb911ec91df40684f467f65c1d.jpg', 0),
(86, '076e7fcd68475a8c4c4ab0f1aa1d05', 25, 'http://localhost/dony//uploads/076e7fcd68475a8c4c4ab0f1aa1d05a3.jpg', 0),
(87, 'a66a75d52eb98370a1290618ad3fae', 72, 'http://localhost/dony//uploads/a66a75d52eb98370a1290618ad3fae7c.jpg', 0),
(88, '6467ea8489444d86a3c5e32c2d7f52', 59, 'http://localhost/dony//uploads/6467ea8489444d86a3c5e32c2d7f52bd.jpg', 0),
(89, 'bbe22515f0a65f53aef4745547ea60', 59, 'http://localhost/dony//uploads/bbe22515f0a65f53aef4745547ea601f.jpg', 0),
(90, 'a7dea056612a4aecf8304c0037318c', 73, 'http://localhost/dony//uploads/a7dea056612a4aecf8304c0037318ce6.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `produk_jenis_2_id` int(11) DEFAULT NULL,
  `merek_id` int(11) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(6) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `view_count` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `userid`, `produk_jenis_2_id`, `merek_id`, `nama`, `stock`, `harga`, `berat`, `foto`, `deskripsi`, `view_count`, `created_on`, `updated_on`) VALUES
(25, 11, 40, NULL, 'dogs maniac', 100, 1000, 1000, NULL, 'asdasdjgajdgajhsdgas', NULL, '2018-12-28 09:08:50', NULL),
(27, 17, 40, NULL, 'dogs fresh', 1000, 10000, 1000, NULL, 'asdajsdghjasd', NULL, '2018-12-28 09:08:58', NULL),
(28, 17, 52, NULL, 'Scar', 1000, 800000, 1000, NULL, 'asdasbd\r\n', NULL, '2018-12-28 09:09:02', NULL),
(29, 18, 32, NULL, 'sks', 100000, 100000, 1000, NULL, 'asdasdas\r\n', NULL, '2018-12-28 09:09:05', NULL),
(30, 18, 18, NULL, 'retreat', 10000, 800000, 12000, NULL, 'asdnbasjd', NULL, '2018-12-28 09:09:10', NULL),
(31, 16, 32, NULL, 'better', 100000, 90990, 12000, NULL, 'bnanbdjhasha', NULL, '2018-12-28 09:09:13', NULL),
(32, 16, 52, NULL, 'maimu', 10000, 1200000, 10000, NULL, 'hwehjbjw', NULL, '2018-12-28 09:09:19', NULL),
(33, 15, 18, NULL, 'here', 676, 1900000, 1000, NULL, 'asbdkjbaskdj', NULL, '2018-12-28 09:09:24', NULL),
(34, 15, 40, NULL, 'm416', 1000, 10000, 12000, NULL, 'basjdbjhasd', NULL, '2018-12-28 09:09:27', NULL),
(35, 14, 40, NULL, 'kersxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 1000, 12000, 12000, NULL, 'hbjhadsjhbasd', NULL, '2018-12-30 19:13:21', NULL),
(36, 14, 40, NULL, 'jed', 123100, 120000, 12000, NULL, 'bajfdahfadf', NULL, '2018-12-28 09:09:35', NULL),
(37, 13, 32, NULL, 'frs', 999, 900000, 1000, NULL, 'nmkjakjsdn', NULL, '2018-12-28 09:09:39', NULL),
(38, 12, 32, NULL, 'tambah', 99, 99999, 12000, NULL, 'asfdhagfsdhgafsdh', NULL, '2018-12-28 09:09:45', NULL),
(39, 12, 42, NULL, 'lagi', 9999, 9999, 99, NULL, 'sagdhafsdhgasd', NULL, NULL, NULL),
(40, 11, NULL, NULL, '', 0, 0, 0, NULL, '', NULL, NULL, NULL),
(41, 11, NULL, NULL, '', 0, 0, 0, NULL, '', NULL, NULL, NULL),
(42, 11, NULL, NULL, '', 0, 0, 0, NULL, '', NULL, NULL, NULL),
(43, 11, NULL, NULL, '', 0, 0, 0, NULL, '', NULL, NULL, NULL),
(44, 11, NULL, NULL, '', 0, 0, 0, NULL, '', NULL, NULL, NULL),
(45, 11, NULL, NULL, '', 0, 0, 0, NULL, '', NULL, NULL, NULL),
(46, 11, 20, NULL, 'tes', 123, 123, 1231, NULL, 'asd', NULL, NULL, NULL),
(47, 11, 19, NULL, 'tesmbut', 123, 123, 123, NULL, 'deskripsimbut', NULL, NULL, NULL),
(48, 11, NULL, NULL, 'tesmbut', 123, 123, 123, NULL, 'deskripsimbut', NULL, NULL, NULL),
(49, 11, 1, NULL, 'tesmbut', 123, 123, 123, NULL, 'asdasd', NULL, NULL, NULL),
(50, 11, NULL, NULL, 'tesmbut', 123, 123, 123, NULL, 'asdasd', NULL, NULL, NULL),
(51, 11, 1, NULL, 'tesmbutt', 11, 11, 11, NULL, 'mbutdesk', NULL, NULL, NULL),
(52, 11, 18, NULL, 'tesmbutt', 11, 11, 11, NULL, 'mbutdesk', NULL, NULL, NULL),
(53, 11, 18, NULL, 'qweqwe', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(54, 11, 18, NULL, 'qweqwe', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(55, 11, 1, NULL, 'asd', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(56, 11, NULL, NULL, 'asdjkgfkmjasdsd', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(57, 11, 18, NULL, 'tes lagi mbut', 8, 345, 6, NULL, 'asdasd', NULL, NULL, NULL),
(58, 11, 32, NULL, 'jnasdasdf', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(59, 11, 18, NULL, 'or how my heart break', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(60, 11, 18, NULL, 'syat', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(61, 11, NULL, NULL, 'syat', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(62, 11, NULL, NULL, 'syat', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(63, 11, NULL, NULL, 'syat', 12, 12, 12, NULL, 'asd', NULL, NULL, NULL),
(64, 11, 1, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(65, 11, NULL, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(66, 11, 18, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(67, 11, 1, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(68, 11, 40, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(69, 11, NULL, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(70, 11, 18, NULL, 'jinhgt', 12, 12, 12, NULL, 'azasda', NULL, NULL, NULL),
(71, 11, 18, NULL, 'totlah ', 1, 12, 1, NULL, 'zxc', NULL, NULL, NULL),
(72, 11, 32, NULL, 'artot', 1, 12, 1, NULL, 'zsdz', NULL, NULL, NULL),
(73, 11, 32, NULL, 'asd', 23, 123123, 23, NULL, 'bvvn', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `produk_jenis_1`
--

CREATE TABLE `produk_jenis_1` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_jenis_1`
--

INSERT INTO `produk_jenis_1` (`id`, `nama`, `created_on`, `updated_on`) VALUES
(5, 'Merek', '2018-04-18 13:34:42', '0000-00-00 00:00:00'),
(6, 'Aksesoris Anjing', '2018-04-23 14:18:22', '0000-00-00 00:00:00'),
(7, 'Makanan Anjing ', '2018-04-23 14:18:34', '0000-00-00 00:00:00'),
(8, 'Perawatan Anjing ', '2018-04-23 14:18:49', '0000-00-00 00:00:00'),
(9, 'Obat & Vitamin Anjing ', '2018-04-23 14:19:10', '0000-00-00 00:00:00'),
(10, 'Aksesoris Kucing ', '2018-04-23 14:19:26', '0000-00-00 00:00:00'),
(11, 'Makanan Kucing ', '2018-04-23 14:19:35', '0000-00-00 00:00:00'),
(12, 'Perawatan Kucing ', '2018-04-23 14:19:47', '0000-00-00 00:00:00'),
(13, 'Obat & Vitamin Kucing ', '2018-04-23 14:20:01', '0000-00-00 00:00:00'),
(14, 'Burung', '2018-04-18 13:34:42', '0000-00-00 00:00:00'),
(15, 'Hamster', '2018-04-18 13:35:40', '0000-00-00 00:00:00'),
(16, 'Kelinci', '2018-04-18 13:35:40', '0000-00-00 00:00:00'),
(17, 'Ikan', '2018-04-18 13:35:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `produk_jenis_2`
--

CREATE TABLE `produk_jenis_2` (
  `id` int(11) NOT NULL,
  `produk_jenis_1_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_jenis_2`
--

INSERT INTO `produk_jenis_2` (`id`, `produk_jenis_1_id`, `nama`, `created_on`, `updated_on`) VALUES
(1, 5, 'Bobo', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(2, 5, 'Canidae', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(3, 5, 'Chris Christensen', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(4, 5, 'COuntry', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(5, 5, 'Daily Delight', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(6, 5, 'Dog Whisperer Caesar Millan', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(7, 5, 'Endi', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(8, 5, 'Equilibrio', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(9, 5, 'Forbis', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(10, 5, 'Happy Cat', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(11, 5, 'Happy Dog', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(12, 5, 'Kaytee', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(13, 5, 'Morning Sun', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(14, 5, 'Pro Plan', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(15, 5, 'Royal Canine', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(16, 5, 'Vegebrand', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(17, 5, 'Wellness', '2018-04-18 13:40:53', '0000-00-00 00:00:00'),
(18, 6, 'Alas Kandang', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(19, 6, 'Alat Pangkas Bulu', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(20, 6, 'Baju', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(21, 6, 'Gunting Kuku', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(22, 6, 'Kalung & Tali', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(23, 6, 'Kandang', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(24, 6, 'Cannel Box', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(25, 6, 'Mainan', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(26, 6, 'Mangkok', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(27, 6, 'Pembersih Kandang', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(28, 6, 'Sikat', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(29, 6, 'Sisir', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(30, 0, '', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(31, 6, 'Tempat Air Minum', '2018-04-18 13:46:04', '0000-00-00 00:00:00'),
(32, 7, 'Dental Stick', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(33, 7, 'Grain Free', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(34, 7, 'Basah', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(35, 7, 'Kering', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(36, 7, 'Snack', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(37, 7, 'Snack Grain Free', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(38, 7, 'Susu', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(39, 7, 'Tulang-Tulangan', '2018-04-18 13:48:44', '0000-00-00 00:00:00'),
(40, 8, 'Ketombe', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(41, 8, 'Aroma Terapi', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(42, 8, 'Conditioner', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(43, 8, 'Foam', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(44, 8, 'Kutu & Tungau', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(45, 8, 'Obat Luka', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(46, 8, 'Pembersih Gigi', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(47, 8, 'Parfum', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(48, 8, 'Pembersih Mata, Telinga, & Gigi', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(49, 8, 'Shampoo', '2018-04-18 13:53:01', '0000-00-00 00:00:00'),
(51, 0, '', '2018-04-18 13:55:04', '0000-00-00 00:00:00'),
(52, 9, 'Minyak Ikan', '2018-04-18 13:55:04', '0000-00-00 00:00:00'),
(53, 9, 'Obat Cacing', '2018-04-18 13:55:04', '0000-00-00 00:00:00'),
(54, 9, 'Obat Demodex', '2018-04-18 13:55:04', '0000-00-00 00:00:00'),
(55, 9, 'Obat Diare', '2018-04-18 13:55:04', '0000-00-00 00:00:00'),
(56, 9, 'Vitamin', '2018-04-18 13:55:04', '0000-00-00 00:00:00'),
(57, 14, 'Makanan', '2018-04-18 13:58:18', '0000-00-00 00:00:00'),
(58, 14, 'Vitamin', '2018-04-18 13:58:18', '0000-00-00 00:00:00'),
(59, 15, 'Aksesoris', '2018-04-18 13:59:18', '0000-00-00 00:00:00'),
(60, 15, 'Makanan', '2018-04-18 13:59:18', '0000-00-00 00:00:00'),
(61, 15, 'Rumah-Rumahan', '2018-04-18 13:59:18', '0000-00-00 00:00:00'),
(62, 16, 'Aksesoris', '2018-04-18 14:00:21', '0000-00-00 00:00:00'),
(63, 16, 'Makanan', '2018-04-18 14:00:21', '0000-00-00 00:00:00'),
(64, 16, 'Pembersih Kotoran', '2018-04-18 14:00:21', '0000-00-00 00:00:00'),
(65, 17, 'Makanan', '2018-04-18 14:02:23', '0000-00-00 00:00:00'),
(66, 17, 'Aquarium', '2018-04-18 14:02:23', '0000-00-00 00:00:00'),
(67, 17, 'Hiasan', '2018-04-18 14:02:23', '0000-00-00 00:00:00'),
(68, 17, 'Obat', '2018-04-18 14:02:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `produk_tag`
--

CREATE TABLE `produk_tag` (
  `id` int(11) NOT NULL,
  `produkid` int(11) NOT NULL,
  `tagid` int(11) NOT NULL,
  `is_from_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk_tag`
--

INSERT INTO `produk_tag` (`id`, `produkid`, `tagid`, `is_from_admin`) VALUES
(1, 54, 26, 0),
(2, 54, 27, 0),
(3, 54, 28, 0),
(4, 54, 29, 0),
(5, 54, 30, 0),
(6, 56, 26, 0),
(7, 56, 26, 0),
(8, 56, 26, 0),
(9, 56, 26, 0),
(10, 56, 26, 0),
(11, 57, 26, 0),
(12, 57, 26, 0),
(13, 57, 26, 0),
(14, 57, 26, 0),
(15, 57, 26, 0),
(16, 58, 26, 0),
(17, 58, 26, 0),
(18, 58, 26, 0),
(19, 58, 26, 0),
(20, 58, 26, 0),
(21, 59, 26, 0),
(22, 59, 26, 0),
(23, 59, 26, 0),
(24, 59, 26, 0),
(25, 59, 26, 0),
(26, 60, 31, 0),
(27, 60, 32, 0),
(28, 60, 33, 0),
(29, 60, 34, 0),
(30, 60, 35, 0),
(31, 61, 31, 0),
(32, 61, 31, 0),
(33, 61, 31, 0),
(34, 61, 31, 0),
(35, 61, 31, 0),
(36, 62, 31, 0),
(37, 62, 31, 0),
(38, 62, 31, 0),
(39, 62, 31, 0),
(40, 62, 31, 0),
(41, 63, 31, 0),
(42, 63, 31, 0),
(43, 63, 31, 0),
(44, 63, 31, 0),
(45, 63, 31, 0),
(46, 64, 31, 0),
(47, 64, 31, 0),
(48, 64, 31, 0),
(49, 64, 31, 0),
(50, 64, 31, 0),
(51, 65, 31, 0),
(52, 65, 31, 0),
(53, 65, 31, 0),
(54, 65, 31, 0),
(55, 65, 31, 0),
(56, 66, 31, 0),
(57, 66, 31, 0),
(58, 66, 31, 0),
(59, 66, 31, 0),
(60, 66, 31, 0),
(61, 67, 31, 0),
(62, 67, 31, 0),
(63, 67, 31, 0),
(64, 67, 31, 0),
(65, 67, 31, 0),
(66, 68, 31, 0),
(67, 68, 31, 0),
(68, 68, 31, 0),
(69, 68, 31, 0),
(70, 68, 31, 0),
(71, 69, 31, 0),
(72, 69, 32, 0),
(73, 69, 33, 0),
(74, 69, 34, 0),
(75, 69, 35, 0),
(76, 70, 31, 0),
(77, 70, 32, 0),
(78, 70, 33, 0),
(79, 70, 34, 0),
(80, 70, 35, 0),
(81, 71, 31, 0),
(82, 71, 32, 0),
(83, 71, 33, 0),
(84, 71, 34, 0),
(85, 71, 35, 0),
(86, 72, 36, 0),
(87, 72, 37, 0),
(88, 73, 38, 0);

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE `promo` (
  `id` int(11) NOT NULL,
  `produkid` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `deskripsi` int(11) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_from_admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`, `is_from_admin`) VALUES
(31, 'amsterdam', 0),
(32, 'washington', 0),
(33, 'sydney', 0),
(34, 'beijing', 0),
(35, 'cairo', 0),
(36, 'arya', 0),
(37, 'sasmita', 0),
(38, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `toko`
--

CREATE TABLE `toko` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `userid` int(11) NOT NULL,
  `kotkabid` int(11) DEFAULT NULL,
  `postalcode` varchar(10) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toko`
--

INSERT INTO `toko` (`id`, `nama`, `userid`, `kotkabid`, `postalcode`, `address`, `phone`, `lat`, `lng`, `created_on`, `updated_on`) VALUES
(7, 'Aditya Shop', 11, NULL, NULL, 'Jl. Lalang Temu 11, Jimbaran, Kuta Sel., Kabupaten', '0815327574325', -8.7929114320033, 115.16308298583374, NULL, NULL),
(8, 'Harivonda Top', 12, NULL, NULL, 'Jl. Raya Uluwatu, Jimbaran, Kuta Sel., Kabupaten B', '0856886876876', -8.804839326053443, 115.16224613662109, NULL, NULL),
(9, 'Ananta Shop', 13, NULL, NULL, 'Jl. Uluwatu II No.30X, Jimbaran, Kuta Sel., Kabupa', '0893672567673', -8.672646289124977, 115.2141522454773, NULL, NULL),
(10, 'Andik Jaya', 14, NULL, NULL, 'Jl. Celagi Basur No.3Y, Jimbaran, Kuta Sel., Kabup', '081757573254', -8.672858412313444, 115.20979633803711, NULL, NULL),
(11, 'Celangi Shop', 15, NULL, NULL, 'Jl. Uluwatu II No.1, Jimbaran, Kuta Sel., Kabupate', '0856687623648', -8.67205234355997, 115.22226324553833, NULL, NULL),
(12, 'Andry Jaya', 16, NULL, NULL, 'Jl. By Pass Ngurah Rai No.55, Jimbaran, Kuta Sel.,', '0893274t72356', -8.670525050652264, 115.22058954711304, NULL, NULL),
(13, 'Oin Shop', 17, NULL, NULL, 'Jl. Four Seasons Muaya Beach, Jimbaran, Kuta Selat', '0895645645645', -8.67137354747864, 115.21352997298584, NULL, NULL),
(14, 'Adi Jaya', 18, NULL, NULL, 'Jl. Raya Tuban No.1, Tuban, Kuta, Tuban, Kuta, Kab', '0817687678687', -8.674512969072087, 115.21181335921631, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `toko_lokasi`
--

CREATE TABLE `toko_lokasi` (
  `id` int(11) NOT NULL,
  `toko_id` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `toko_lokasi_detail`
--

CREATE TABLE `toko_lokasi_detail` (
  `id` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `long_name` varchar(30) NOT NULL,
  `short_name` varchar(30) NOT NULL,
  `toko_lokasi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `nomor_resi` varchar(30) NOT NULL,
  `transaksi_bank_kode` varchar(30) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `total_harga` int(11) NOT NULL,
  `harga_unik` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `type_id` tinyint(1) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `photo` text,
  `level` tinyint(4) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `is_detail_setted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type_id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `updated_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `photo`, `level`, `address`, `province_id`, `city_id`, `lat`, `lng`, `is_detail_setted`) VALUES
(8, 1, '::1', 'admin', '$2y$08$KKt3DYd3BADbbEPzRWvgw.3kvow75hXb6WSKg36cRftSijixrTNOS', NULL, 'admin@admin.com', NULL, NULL, NULL, NULL, 1543671409, NULL, 1548327698, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(11, 0, '::1', 'aditya', '$2y$08$kPwb5YSl2mk3feWmLhCNIOLurWt4QZcmHABD8tjpQwv4DbpREOzVm', NULL, 'aditya@gmail.com', NULL, NULL, NULL, NULL, 1546014068, NULL, 1548692601, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(12, 0, '::1', 'harivonda', '$2y$08$d1h5ZeNWYtvlmymFtxv7yeQOqGB3IrepU.8AgJOin2fz0AmvwUEiC', NULL, 'harivonda@gmail.com', NULL, NULL, NULL, NULL, 1546014448, NULL, 1546016478, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(13, 0, '::1', 'ananta', '$2y$08$87Mhhe3dWXM5IJeEWHVE4ujL3uYUYIyxn/d0VwnBW3HauevZeOeR2', NULL, 'ananta@gmail.com', NULL, NULL, NULL, NULL, 1546014568, NULL, 1546016420, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(14, 0, '::1', 'andik', '$2y$08$hywpJj92FoVdOSywGWiR0.UNUFBUJkwshPCp/U2V3uNuO/UW9/H0O', NULL, 'andik@yahoo.com', NULL, NULL, NULL, NULL, 1546014677, NULL, 1546016323, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(15, 0, '::1', 'celangi', '$2y$08$Re7.Sl6pwqh8pHq8lFJkM.OWD5OAOxm9nepVJNl0u24TW0A/cYieK', NULL, 'celangi@yahoo.com', NULL, NULL, NULL, NULL, 1546014800, NULL, 1546016225, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(16, 0, '::1', 'Andry', '$2y$08$9fScfciqzjOfX34PExLY0.6Ile3fPFwyTTBUgfGf4xrHSq.TwF.iy', NULL, 'andry@yahoo.com', NULL, NULL, NULL, NULL, 1546014890, NULL, 1546016065, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(17, 0, '::1', 'oin', '$2y$08$CcPIrPCbgBKWt16TcTPk8.Bt3xMtSxjd78D.FeXpugcpf7c81mFY6', NULL, 'oin@ymail.com', NULL, NULL, NULL, NULL, 1546014967, NULL, 1546015727, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(18, 0, '::1', 'Adi', '$2y$08$x8XKfVBSyqplzy9PrmstuuOLpgmvY0hnc/Yq/80TQ7bnXJm87zIHi', NULL, 'adi@gmail.com', NULL, NULL, NULL, NULL, 1546015351, NULL, 1546015975, 1, '', '', NULL, NULL, NULL, NULL, NULL, 1, 17, NULL, NULL, NULL),
(19, 0, '::1', 'aaa@yahoo.com', '$2y$08$Zpsu0p0F4NpV/zg5h4cX.Oh3bNiFHiSaUSLmQCRrmgkMfSXpGKnXG', NULL, 'aaa@yahoo.com', NULL, NULL, NULL, NULL, 1548333818, '2018-12-31 16:00:00', 1548333825, 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 0, '::1', 'tes1', '$2y$08$RRn9l8Foq5SW./YjySvYzedU/ggoG2y3LY9FzHpdBL3BlutTJmJjG', NULL, 'tes1@gmail.com', NULL, NULL, NULL, NULL, 1548695988, '2019-01-28 20:01:25', 1548709185, 1, '', '', NULL, '626262626262', NULL, NULL, 'kakakak', 0, 214, -8.672879624625704, 115.2185296105896, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_produk`
--
ALTER TABLE `cart_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `headline`
--
ALTER TABLE `headline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iklan`
--
ALTER TABLE `iklan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk_jenis_1`
--
ALTER TABLE `produk_jenis_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk_jenis_2`
--
ALTER TABLE `produk_jenis_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk_tag`
--
ALTER TABLE `produk_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo`
--
ALTER TABLE `promo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko`
--
ALTER TABLE `toko`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko_lokasi`
--
ALTER TABLE `toko_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko_lokasi_detail`
--
ALTER TABLE `toko_lokasi_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `cart_produk`
--
ALTER TABLE `cart_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `headline`
--
ALTER TABLE `headline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `iklan`
--
ALTER TABLE `iklan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `produk_jenis_1`
--
ALTER TABLE `produk_jenis_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `produk_jenis_2`
--
ALTER TABLE `produk_jenis_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `produk_tag`
--
ALTER TABLE `produk_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `promo`
--
ALTER TABLE `promo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `toko`
--
ALTER TABLE `toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `toko_lokasi`
--
ALTER TABLE `toko_lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `toko_lokasi_detail`
--
ALTER TABLE `toko_lokasi_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
