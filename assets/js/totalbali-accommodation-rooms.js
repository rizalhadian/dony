$( "#accommodationsNameAuto" ).autocomplete({
  source: function( request, response ) {
      $.ajax({
                url: base_url+"admin/accommodations/read_like",
                dataType: "json",
                type: "POST",
                data: {word : request.term},
                success: function(data){
                    response(data);
                    // alert(data);
                }
        });
    },
    focus: function(event, ui) {
          // prevent autocomplete from updating the textbox
          event.preventDefault();
          // manually update the textbox
          $(this).val(ui.item.label);
        },
     select: function(event, ui) {
          // prevent autocomplete from updating the textbox
          event.preventDefault();
          // manually update the textbox and hidden field
          $(this).val(ui.item.label);
          $('#accommodations_id').val(ui.item.value);
          var loc = ui.item.label;

     }
});
$("#accommodationsNameAuto").autocomplete( "option", "appendTo", ".eventInsForm" );
