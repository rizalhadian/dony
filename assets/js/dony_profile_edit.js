$( "#provinsi" ).on('change', function(){
    // alert($("#provinsi").val());
    $.ajax({
        url: base_url+"profile/getCities/"+$("#provinsi").val(),
        type: "GET",
        dataType: "JSON",
        success: function(data){
            console.log(data);

            $("#kota").prop("disabled", null);
            $("#kota").text("");
            $("#kota").append("<option value='null'>Pilih Kota</option>");
            data.forEach(element => {
                console.log(element);
                $("#kota").append("<option value='"+element.city_id+"'>"+element.city_name+"</option>");

            });
        },
        error: function(err){
            console.log(err);
        }
    })
})