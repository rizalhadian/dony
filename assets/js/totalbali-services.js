var oTable = $('#example1').dataTable();

// alert(JSON.stringify(oTable));
console.log(oTable.rows(0).data());
$('#btnServiceAdd').on('click', function(){
  $('#formService')[0].reset();
  $('#modalServiceLabel').text('Add Service');
  $('#btnConfirmUpdate').hide();
  $('#btnConfirmSave').show();
});

var idBaru = [];
$('#btnConfirmSave').on('click', function(){
  var serializedData = $('#formService').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/services/create",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalService').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      idBaru.push("row"+resp.id+"");
      // $('#dataRows').append(
      //       '<tr id="row'+resp.id+'">'+
      //         '<td id="dataName'+resp.id+'">'+resp.name+'</td>'+
      //         '<td id="dataDescription'+resp.id+'">'+resp.description+'</td>'+
      //         '<td width="50px">'+
      //           '<button class="btn btn-default btn-xs "  id="btnServiceUpdate'+resp.id+'" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'+
      //           '<button class="btn btn-danger btn-xs"  id="btnServiceDelete'+resp.id+'" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></button>'+
      //         '</td>'+
      //       '</tr>'
      // );
      $("#example1").load(location.href + " #example1");
      getAllBtnServiceDelete();
      getAllBtnServiceUpdate();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> '+resp.message+'</div>');
    }
  });
});

getAllBtnServiceDelete();
getAllBtnServiceUpdate();



function getAllBtnServiceDelete(){
  $("[id^=btnServiceDelete]").each(function () {
    $(this).click(function () {
      $('#modalDelete').modal('show');
      $("#idDelete").val($(this).val());
    });
  });
}

function getAllBtnServiceUpdate(){
  $("[id^=btnServiceUpdate]").each(function () {
    $(this).click(function () {
      $('#btnConfirmUpdate').show();
      $('#btnConfirmSave').hide();
      $('#modalServiceLabel').text('Edit Service');
      $("#idUpdate").val($(this).val());
      $('#modalService').modal('show');

      request = $.ajax({
        url: base_url+"admin/masterdata/services/read_id",
        type: "POST",
        data: {id : $(this).val() }
      });

      request.done(function (response, textStatus, jqXHR){
        var resp = jQuery.parseJSON(response);
        $('#nameUpdate').val(resp[0].name);
        $('#descriptionUpdate').val(resp[0].description);
      });

    });
  });
}

$("#btnConfirmDelete").on('click', function(){
  var id = $("#idDelete").val();
  request = $.ajax({
      url: base_url+"admin/masterdata/services/delete",
      type: "POST",
      data: {id : id}
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalDelete').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $('#row'+resp.id+'').remove();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});

$("#btnConfirmUpdate").on('click', function(){

  var serializedData = $('#formService').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/services/update",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){
    $('#modalService').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $("#dataName"+resp.id+"").text(resp.name);
      $("#dataDescription"+resp.id+"").text(resp.description);
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});
