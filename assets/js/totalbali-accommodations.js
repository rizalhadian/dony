$(document).on("click", "#btnAccommodationSync", function(){
  $('#dataSync').html('');
  $('#dataSync').append(
        '<tr>'+
        '<th>OTA</th>'+
        '<th>OTA Id</th>'+
        '<th>Rooms Setting</th>'+
        '<th>Title</th>'+
        '<th>Link</th>'+
        '<th>Synced</th>'+
        '<th>Active</th>'+
        '<th>Actions</th>'+
        '</tr>'
  );
  $('#id_to_sync').val(this.value);
  $("#loading_icon").html('');
  $("#loading_icon").html('<i class="fa fa-refresh" aria-hidden="true"></i>');
  $("#btn_sync_now").prop('disabled', false);

  request = $.ajax({
    url: base_url+"admin/room-settings/read_by_accommodations_id",
    type: "POST",
    data: {id:this.value}
  });
  request.done(function (response, textStatus, jqXHR){
    var room_settings = jQuery.parseJSON(response);

    console.log(room_settings);
    //Airbnb
    for(room_setting = 0; room_setting < room_settings.length; room_setting++){
      var airbnb_synced;
      if(room_settings[room_setting].airbnb_synced == 0){
        airbnb_synced = '<i class="fa fa-times" aria-hidden="true"></i>';
      }else{
        airbnb_synced = '<i class="fa fa-check" aria-hidden="true"></i>';
      }
      var airbnb_active;
      var airbnb_active_status;
      var btn_activate_deactivate;
      if(room_settings[room_setting].airbnb_active == 0){
        airbnb_active = '<i class="fa fa-times" aria-hidden="true"></i>';
        btn_activate_deactivate = '<button class="btn btn-block btn-success btn-xs" style="margin-bottom:2px"  id="btnAirbnbActivate" value="'+room_settings[room_setting].id+'"><i class="fa fa-check" aria-hidden="true"></i> Activate</button>'

      }else{
        airbnb_active = '<i class="fa fa-check" aria-hidden="true"></i>';
        btn_activate_deactivate = '<button class="btn btn-block btn-danger btn-xs" style="margin-bottom:2px"  id="btnAirbnbDeactivate" value="'+room_settings[room_setting].id+'"><i class="fa fa-check" aria-hidden="true"></i> Deactivate</button>'

      }

      if(room_settings[room_setting].airbnb_active == 0 || room_settings[room_setting].airbnb_synced == 0){
        airbnb_color = 'class="danger"';
      }else{
        airbnb_color = 'class=""';
      }
      var btn_sync = '<button class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px"  id="btnAirbnbSync" value="'+room_settings[room_setting].id+'"><i class="fa fa-refresh" aria-hidden="true"></i> Sync</button>'



      $('#dataSync').append(
            '<tr id="rowSync" '+airbnb_color+'>'+
              '<td><img src="http://localhost/totalbali/assets/images/airbnb.png" width="100px" class="img-fluid" alt="Responsive image"></td>'+
              '<td>'+room_settings[room_setting].airbnb_id+'</td>'+
              '<td>'+room_settings[room_setting].name+'</td>'+
              '<td>'+room_settings[room_setting].airbnb_name+'</td>'+
              '<td>'+'<a href="'+room_settings[room_setting].airbnb_url+'" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">Link</a>'+'</td>'+
              '<td>'+airbnb_synced+'</td>'+
              '<td>'+airbnb_active+'</td>'+
              '<td>'+btn_activate_deactivate+btn_sync+'</td>'+
            '</tr>'
      );
    }
    //

    //Flipkey
    // for(room_setting = 0; room_setting < room_settings.length; room_setting++){
    //   $('#dataSync').append(
    //         '<tr id="rowSync">'+
    //           '<td><img src="http://localhost/totalbali/assets/images/flipkey.png" width="100px" class="img-fluid" alt="Responsive image"></td>'+
    //           '<td>'+room_settings[room_setting].flipkey_id+'</td>'+
    //           '<td>'+room_settings[room_setting].name+'</td>'+
    //           '<td>'+'<a href="'+room_settings[room_setting].flipkey_url+'" class="btn btn-block btn-primary btn-xs" style="margin-bottom:2px" target="_blank">Link</a>'+'</td>'+
    //           '<td>'+room_settings[room_setting].flipkey_synced+'</td>'+
    //           '<td>'+room_settings[room_setting].flipkey_active+'</td>'+
    //         '</tr>'
    //   );
    // }
    //

  });
  $('#modalSync').modal('show');

});





$(document).on("click", "#btnAccommodationActivate", function(){
  request = $.ajax({
      url: base_url+"admin/accommodations/activate",
      type: "POST",
      data: {id:this.value}
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    location.reload();
  });
});

$(document).on("click", "#btnAccommodationDeactivate", function(){
  request = $.ajax({
      url: base_url+"admin/accommodations/deactivate",
      type: "POST",
      data: {id:this.value}
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    location.reload();
  });
});

$(document).on("click", "#btnAccommodationApprove", function(){
  // alert(this.value);
  request = $.ajax({
      url: base_url+"admin/accommodations/approve",
      type: "POST",
      data: {id:this.value}
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    location.reload();

    // var resp = jQuery.parseJSON(response);
    // $('#columnRateName'+resp.rate.id+'').text(resp.rate.name);
    // $('#columnRateRate'+resp.rate.id+'').text(resp.rate.rate_mature);
    // $('#columnRateRateMatureExtra'+resp.rate.id+'').text(resp.rate.rate_mature_extra);
    // $('#columnRateRateChildrenExtra'+resp.rate.id+'').text(resp.rate.rate_children_extra);
    // $('#columnRateRateInfantExtra'+resp.rate.id+'').text(resp.rate.rate_infant_extra);
    // $('#columnRateValidDate'+resp.rate.id+'').text(resp.rate.date_start+' - '+resp.rate.date_end);
    // $('#columnRateType'+resp.rate.id+'').text(resp.rate.type);

  });
});

$('#btn_sync_now').on('click', function(){
  $("#loading_icon").html('');
  $("#loading_icon").html('<i class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></i>');
  $("#btn_sync_now").prop('disabled', true);
  // alert($('#id_to_sync').val());
});


$("[id^=btnAccommodationView]").each(function () {

    $(this).click(function () {
      // alert($(this).val());
      request = $.ajax({
        url: base_url+"admin/accommodations/read_id",
        type: "POST",
        data: {id:$(this).val()}
      });
      request.done(function (response, textStatus, jqXHR){
        var accommodation = jQuery.parseJSON(response);

        alert(accommodation);
      });
      $('#modalAccommodation').modal('show');
    });

});

$("[id^=btnAccommodationDelete]").each(function () {
  $(this).click(function () {
    $("#idDelete").val($(this).val());
    $("#whichDelete").val('accommodations');
    $('#modalDelete').modal('show');
  });
});

$("[id^=btnRoomDelete]").each(function () {
  $(this).click(function () {
    $("#idDelete").val($(this).val());
    $("#whichDelete").val('accommodation_rooms');
    $('#modalDelete').modal('show');
  });
});

$("[id^=btnPriceDelete]").each(function () {
  $(this).click(function () {
    $("#idDelete").val($(this).val());
    $("#whichDelete").val('rates');
    $('#modalDelete').modal('show');
  });
});

$("#btnConfirmDelete").on('click', function(){
  var id = $("#idDelete").val();
  var whichDelete = $("#whichDelete").val();

  if(whichDelete == "accommodations"){
    request = $.ajax({
        url: base_url+"admin/accommodations/delete",
        type: "POST",
        data: {id : id}
    });
    request.done(function (response, textStatus, jqXHR){
       location.reload();
    });
  }else if(whichDelete == "accommodation_rooms"){
    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/delete",
        type: "POST",
        data: {id : id}
    });
    request.done(function (response, textStatus, jqXHR){
       location.reload();
    });
  }else if(whichDelete == "rates"){
    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/prices/delete",
        type: "POST",
        data: {id : id}
    });
    request.done(function (response, textStatus, jqXHR){
       location.reload();
    });
  }


});


$(document).on("click", "#btnAirbnbActivate", function(){
  alert(this.value);
});

$(document).on("click", "#btnAirbnbDeactivate", function(){
  alert(this.value);
});

$(document).on("click", "#btnAirbnbSync", function(){
  alert(this.value);
});
