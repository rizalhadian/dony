

function initMap() {
        var styles = [
            {
                stylers: [
                    {hue: "#0099ff"},
                    {saturation: -40}
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    {lightness: 100},
                    {visibility: "simplified"}
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});

        var uluru = {lat: -8.699850, lng: 115.180635};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 16,
          center: uluru,
          mapTypeControlOptions: {
              mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
          }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');


        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
        });

        marker.addListener('drag', function() {
          // alert(marker.getPosition());
          $('#lat').val(marker.getPosition().lat());
          $('#lng').val(marker.getPosition().lng());

        });

        $('#refresh').on('click', function () {
            // alert('weei');
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(-8.699850, 115.180635));
        });




        $("#btnsavedetailvilla").click(function(){
          var serializedData = $("#formdetailvilla").serializeArray();
          var desc = CKEDITOR.instances.editor1.getData();
          serializedData.push({name: 'desc', value: desc});
          request = $.ajax({
              url: "http://dev.totalbali.com/totalbalidirectsystem/bn/?p=villa_room&action=savedetailvilla",
              type: "post",
              data: serializedData
          });

          request.done(function (response, textStatus, jqXHR){
               // Log a message to the console
              //  console.log(desc);
               console.log(response);
           });
        })

}
