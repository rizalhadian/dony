$(document).on("change", "#produk_jenis_1", function(){
  // alert (this.value);
  if(this.value == 0){
    $("#produk_jenis_2").prop('disabled', true);
    $('#produk_jenis_2').find('option').remove();
    $('#produk_jenis_2').append(
      '<option>-</option>'
    );
  }else{
    var url = base_url+"produk/jenis-2/"+this.value;
    request = $.ajax({
        url: url,
        type: "GET"
    });
    request.done(function (response, textStatus, jqXHR){
      // console.log(response);
      $("#produk_jenis_2").prop('disabled', false);

      var resp = jQuery.parseJSON(response);
      console.log(resp);
      $('#produk_jenis_2').find('option').remove();
      for(var i=0; i<resp.length; i++){
        $('#produk_jenis_2').append(
          '<option value="'+resp[i].id+'">'+
          resp[i].nama+
          '</option>'
        );
      }
    });
  }

});
