$( "#accommodationsNameAuto" ).autocomplete({
  source: function( request, response ) {
      $.ajax({
        url: base_url+"admin/accommodations/read_like",
        dataType: "json",
        type: "POST",
        data: {word : request.term},
        success: function(data){
          response(data);
          // alert(data);
        }
      });
    },
    focus: function(event, ui) {
          // prevent autocomplete from updating the textbox
          event.preventDefault();
          // manually update the textbox
          $(this).val(ui.item.label);
        },
     select: function(event, ui) {
          // prevent autocomplete from updating the textbox
          event.preventDefault();
          // manually update the textbox and hidden field
          $(this).val(ui.item.label);
          $('#accommodations_id').val(ui.item.value);
          $('#accommodation_rooms_id').prop('disabled', false);
          var loc = ui.item.label;

          $.ajax({
            url: base_url+"admin/accommodations/rooms/read_accommodations_id",
            dataType: "json",
            type: "POST",
            data: {id : ui.item.value},
            success: function(data){
              // response(data);
              // alert(data[0].name);
              $('#accommodation_rooms_id')
                  .find('option')
                  .remove()
                  .end()
                  .append('<option value="0">Choose Room</option>')
              ;
              $.each(data, function(id, obj) {
                 $('#accommodation_rooms_id').append($("<option></option>").val(obj.id).text(obj.name));
                 
              });
            }
          });

     }
});
$("#accommodationsNameAuto").autocomplete( "option", "appendTo", ".eventInsForm" );
