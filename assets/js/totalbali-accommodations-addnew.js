
var accommodation_rooms_id_for_prices;
var idDelete;
var whichDelete;
var room_datas = [];
var rates_ids = [];
var accommodation_rooms_ids = [];
var room_update;
// accommodation_rooms_ids.splice(1, 1);

$("#btnAddRoomInfo").on('click', function(){
  $("#btnAddRoom").show();
  $("#btnUpdateRoom").hide();
  $('#formRoom')[0].reset();
  $("input[type=checkbox]", "#formRoom").attr('checked', false);
  $("#tabGeneralInfo").click();
  room_update = false;
});


$("#privateRoomForm").show();
$("#privateRateForm").show();
$("#flexibleRoomRateForm").hide();
$("#flexibleRoomForm").hide();



$('#renting_types_id').on('click', function(){
  // alert($('#renting_types_id').val());
  if($('#renting_types_id').val()==2){
    $("#privateRoomForm").hide();
    $("#privateRateForm").hide();
    $("#flexibleRoomRateForm").show();
    $("#flexibleRoomForm").show();
  }else{
    $("#privateRoomForm").show();
    $("#privateRateForm").show();
    $("#flexibleRoomRateForm").hide();
    $("#flexibleRoomForm").hide();
    $("#rate_accommodations_id").val(0);
  }
});
//
//
//
$("#btnAddRate").on('click', function(){
  // alert('wou');
  $("#btnAddPrice").show();
  $("#btnUpdatePrice").hide();
});

$("#btnAddPriceModal").on('click', function(){
  $("#btnAddPrice").show();
  $("#btnUpdatePrice").hide();
});


$('#flexible_ics_url').prop('disabled', true);

$("input[name='flexible_renting_type']").change(function(){
  // alert($('input[name=flexible_renting_type]:checked').val());
  if($('input[name=flexible_renting_type]:checked').val() == 1){
    $('#flexible_ics_url').prop('disabled', false);
  }else if ($('input[name=flexible_renting_type]:checked').val() == 0) {
    $('#flexible_ics_url').prop('disabled', true);
    $('#flexible_ics_url').val('');
  }
});




function initMap() {

        var styles = [
            {
                stylers: [
                    {hue: "#0099ff"},
                    {saturation: -40}
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    {lightness: 100},
                    {visibility: "simplified"}
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    {visibility: "on"}
                ]
            }
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});

        var uluru = {lat: -8.699850, lng: 115.180635};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 16,
          center: uluru,
          mapTypeControlOptions: {
              mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
          }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');


        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
        });

        marker.addListener('drag', function() {
          // alert(marker.getPosition());
          $('#latitude').val(marker.getPosition().lat());
          $('#longitude').val(marker.getPosition().lng());

        });

        $('#refresh').on('click', function () {
            // alert('weei');
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(-8.699850, 115.180635));
        });



        $( "#locationAdd" ).autocomplete({
          source: function( request, response ) {
              $.ajax({
                        url: base_url+"admin/masterdata/locations/read_like",
                        dataType: "json",
                        type: "POST",
                        data: {
                          word : request.term,
                          unique_id : $("#unique_id").val()
                        },
                        success: function(data){

                            response(data);
                            // alert(data);
                        }
                });
            },
            focus: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox
        					$(this).val(ui.item.label);
        				},
        		 select: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox and hidden field
        					$(this).val(ui.item.label);
        					$('#locations_id').val(ui.item.value);
                  var loc = ui.item.label;


                  $.ajax({
                            url: "http://maps.googleapis.com/maps/api/geocode/xml?address="+loc,
                            type: "GET",
                            dataType: "xml",
                            success: function(data){
                                var xml = data;
                                $("#latitude").val($(xml).find('location').find('lat').text());
                                $("#longitude").val($(xml).find('location').find('lng').text());
                                var latlng = new google.maps.LatLng($(xml).find('location').find('lat').text(), $(xml).find('location').find('lng').text());
                                marker.setPosition(latlng);
                                map.setCenter(latlng);
                            }
                  });

        		 }
        });
        $("#locationAdd").autocomplete( "option", "appendTo", ".eventInsForm" );

        $( "#partnerAdd" ).autocomplete({
          source: function( request, response ) {
              $.ajax({
                        url: base_url+"admin/masterdata/partners/read_like",
                        dataType: "json",
                        type: "POST",
                        data: {
                          word : request.term,
                          unique_id : $("#unique_id").val()
                        },
                        success: function(data){
                            response(data);
                            // alert(data);
                        }
                });
            },
            focus: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox
        					$(this).val(ui.item.label);
        				},
        		 select: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox and hidden field
        					$(this).val(ui.item.label);
        					$('#partners_id').val(ui.item.value);
                  var loc = ui.item.label;
        		 }
        });
        $("#partnerAdd").autocomplete( "option", "appendTo", ".eventInsForm" );
}
// End of Initmap





//
$('#btnAddRoom').click(function(){
  var serializedData = $('#formRoom').serializeArray();
  // alert(JSON.stringify(serializedData));
  // alert(serializedData);
  // console.log(serializedData);
  request = $.ajax({
      url: base_url+"admin/accommodations/rooms/create_do",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){

    console.log(response);

    $('#modalRoom').modal('hide');
    var resp = jQuery.parseJSON(response);
    var renting_type;

    if(resp.renting_type == 0){
      renting_type = "Private Accommodation";
    }else if(resp.renting_type == 1){
      renting_type = "Private Room";
    }

    $('#dataRooms').append(
          '<tr id="rowRoom'+resp.id+'">'+
            '<td id="columnNameRoom'+resp.id+'">'+resp.name+'</td>'+
            '<td id="columnRoomsRoom'+resp.id+'">'+resp.total_rooms+'</td>'+
            '<td id="columnBedsRoom'+resp.id+'">'+resp.total_beds+'</td>'+
            '<td id="columnBathroomsRoom'+resp.id+'">'+resp.total_bathrooms+'</td>'+
            '<td id="columnMinStayRoom'+resp.id+'">'+resp.min_stay+'</td>'+
            '<td id="columnGuestRoom'+resp.id+'">'+resp.max_guest_normal+'</td>'+
            '<td id="columnExtraGuestRoom'+resp.id+'">'+resp.max_guest_extra+'</td>'+
            '<td id="columnRentingTypeRoom'+resp.id+'">'+renting_type+'</td>'+
            '<td width="100px">'+
              '<button type="button" class="btn btn-warning btn-xs btn-block"  id="btnRoomRates" value="'+resp.id+'"><i class="fa fa-usd" aria-hidden="true"></i> Rates</button>'+
              '<button type="button" class="btn btn-default btn-xs btn-block"  id="btnRoomUpdate" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>'+
              '<button type="button" class="btn btn-danger btn-xs btn-block"  id="btnRoomDelete" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
            '</td>'+
          '</tr>'
    );
    // jQuery.grep(y, function(value) {
    //   return value != removeItem;
    // });
    accommodation_rooms_ids.push(resp.id);
    $("#accommodation_rooms_ids").val(accommodation_rooms_ids);
    // getAllbtnRoomRates();
    // getAllbtnRoomDelete();
    $('.dataTables_empty').remove();
  });

});


//
//
$(document).on("click", "#btnRoomRates", function(){
  $("#accommodation_rooms_id_for_prices").val(this.value);
  // alert (this.value);
  $("#rate_accommodations_id").val($("#accommodation_rooms_id_for_prices").val());
  $("#flexibleDataRates").html("");
  $('#modalPrices').modal('show');
  request = $.ajax({
      url: base_url+"admin/rates/read_by_accommodation_rooms_id",
      type: "POST",
      data: {
        id : this.value,
        unique_id : $("#unique_id").val()
      }
  });
  request.done(function (response, textStatus, jqXHR){
    // console.log(response);
    var resp = jQuery.parseJSON(response);
    var rate_type;

    console.log(response);
    for(var i=0; i<resp.length; i++){
      if(resp[i].type==0){
        rate_type = "Base Rate";
      }else if(resp[i].type==1){
        rate_type = "High Season";
      }else if(resp[i].type==2){
        rate_type = "Promo / Low Season";
      }


      $('#flexibleDataRates').append(
            '<tr id="rowRate'+resp[i].id+'">'+
              '<td id="columnRateName'+resp[i].id+'">'+resp[i].name+'</td>'+
              '<td id="columnRateRate'+resp[i].id+'">'+resp[i].rate_mature+'</td>'+
              '<td id="columnRateMinStay'+resp[i].id+'">'+resp[i].min_stay+'</td>'+
              '<td id="columnRateValidDate'+resp[i].id+'">'+resp[i].date_start+' - '+resp[i].date_end+'</td>'+
              '<td id="columnRateType'+resp[i].id+'">'+rate_type+'</td>'+
              '<td width="100px">'+
                '<button type="button" class="btn btn-default btn-xs btn-block"  id="btnRateUpdate" value="'+resp[i].id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>'+
                '<button type="button" class="btn btn-danger btn-xs btn-block"  id="btnRateDelete" value="'+resp[i].id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
              '</td>'+
            '</tr>'
      );
    }

  });
});
//
$(document).on('click', '#btnUpdateRoom', function(){
  var serializedData = $('#formRoom').serializeArray();
  request = $.ajax({
      url: base_url+"admin/accommodations/rooms/update_post",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){

    console.log(response);

    $('#modalRoom').modal('hide');
    var resp = jQuery.parseJSON(response);
    var renting_type;

    if(resp.renting_type == 0){
      renting_type = "Private Accommodation";
    }else if(resp.renting_type == 1){
      renting_type = "Private Room";
    }

    // $('#dataRooms').append(
          // '<tr id="rowRoom'+resp.id+'">'+
          //   '<td id="columnNameRoom'+resp.id+'">'+resp.name+'</td>'+
          //   '<td id="columnRoomsRoom'+resp.id+'">'+resp.total_rooms+'</td>'+
          //   '<td id="columnBedsRoom'+resp.id+'">'+resp.total_beds+'</td>'+
          //   '<td id="columnBathroomsRoom'+resp.id+'">'+resp.total_bathrooms+'</td>'+
          //   '<td id="columnMinStayRoom'+resp.id+'">'+resp.min_stay+'</td>'+
          //   '<td id="columnGuestRoom'+resp.id+'">'+resp.max_guest_normal+'</td>'+
          //   '<td id="columnExtraGuestRoom'+resp.id+'">'+resp.max_guest_extra+'</td>'+
          //   '<td id="columnRentingTypeRoom'+resp.id+'">'+renting_type+'</td>'+
          //   '<td width="170px">'+
          //     '<button class="btn btn-warning btn-xs btnRoomRates"  id="btnRoomRates" value="'+resp.id+'"><i class="fa fa-usd" aria-hidden="true"></i> Rates</button>&nbsp;'+
          //     '<button class="btn btn-default btn-xs"  id="btnRoomUpdate" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>&nbsp;'+
          //     '<button class="btn btn-danger btn-xs"  id="btnRoomDelete" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>&nbsp;'+
          //   '</td>'+
          // '</tr>'

    $('#columnNameRoom'+resp.id+'').text(resp.name);
    $('#columnRoomsRoom'+resp.id+'').text(resp.total_rooms);
    $('#columnBedsRoom'+resp.id+'').text(resp.total_beds);
    $('#columnBathroomsRoom'+resp.id+'').text(resp.total_bathrooms);
    $('#columnMinStayRoom'+resp.id+'').text(resp.min_stay);
    $('#columnGuestRoom'+resp.id+'').text(resp.max_guest_normal);
    $('#columnExtraGuestRoom'+resp.id+'').text(resp.max_guest_extra);
    $('#columnRentingTypeRoom'+resp.id+'').text(renting_type);

    });
});

//
$(document).on("click", "#btnRoomUpdate", function(){
  room_update = true;
  $("#btnUpdateRoom").show();
  $("#btnAddRoom").hide();


  // alert (this.value);
  $("input[type=checkbox]", "#formRoom").attr('checked', false);

  request = $.ajax({
      url: base_url+"admin/accommodations/rooms/read_id",
      type: "POST",
      data: {
        id : this.value,
        unique_id : $("#unique_id").val()
      }
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    var resp = jQuery.parseJSON(response);
    $('input[name="name"]', '#formRoom').val(resp.name);
    $('input[name="id"]', '#formRoom').val(resp.id);
    $('input[name="total_rooms"]', '#formRoom').val(resp.total_rooms);
    $('input[name="total_bathrooms"]', '#formRoom').val(resp.total_bathrooms);
    $('input[name="total_beds"]', '#formRoom').val(resp.total_beds);
    $('input[name="max_guest_normal"]', '#formRoom').val(resp.max_guest_normal);
    $('input[name="max_guest_extra"]', '#formRoom').val(resp.max_guest_extra);
    $('input[name="min_stay"]', '#formRoom').val(resp.min_stay);
    if(resp.renting_type == 1){
      $("input[name=flexible_renting_type][value='1']", "#formRoom").prop("checked",true);
    }else{
      $("input[name=flexible_renting_type][value='0']", "#formRoom").prop("checked",true);
      $('#flexible_ics_url').prop('disabled', true);
    }
    $('input[name="ics_url"]', '#formRoom').val(resp.ics_url);
    $('input[name="description"]', '#formRoom').val(resp.description);

    // alert(resp.facilities);

    for(var index =0; index < resp.facilities.length; index++){
      // alert(resp.facilities[index].facilities_id);
      $("input:checkbox[value=2]").attr("checked", true);
    }

  });
  $('#modalRoom').modal('show');
  $("#tabGeneralInfo").click();

});
//
//
//
$('#btnAddPrice').click(function(){
  var serializedData = $('#formRate').serializeArray();

  request = $.ajax({
      url: base_url+"admin/rates/create",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);

    $('#modalPrice').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){

      rates_ids.push(resp.rate.id);
      $("#rates_ids").val(rates_ids);

      $('.dataTables_empty').remove();

      var rate_type;
      if(resp.rate.type == 0){
        rate_type = "Base Rate";
      }else if(resp.rate.type == 1){
        rate_type = "High Season";
      }else if(resp.rate.type == 2){
        rate_type = "Promo / Low Season";
      }
      if($('#rate_accommodations_id').val() != 0 && $('#rate_accommodations_id').val() != ''){
        $('#flexibleDataRates').append(
              '<tr id="rowRate'+resp.rate.id+'">'+
                '<td id="columnRateName'+resp.rate.id+'">'+resp.rate.name+'</td>'+
                '<td id="columnRateRate'+resp.rate.id+'">'+resp.rate.rate_mature+'</td>'+
                '<td id="columnRateMinStay'+resp.rate.id+'">'+resp.rate.min_stay+'</td>'+
                '<td id="columnRateValidDate'+resp.rate.id+'">'+resp.rate.date_start+' - '+resp.rate.date_end+'</td>'+
                '<td id="columnRateType'+resp.rate.id+'">'+rate_type+'</td>'+
                '<td width="100px">'+
                  '<button type="button" class="btn btn-default btn-xs btn-block"  id="btnRateUpdate" value="'+resp.rate.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>'+
                  '<button type="button" class="btn btn-danger btn-xs btn-block"  id="btnRateDelete" value="'+resp.rate.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                '</td>'+
              '</tr>'
        );
      }else{
        $('#dataRates').append(
          '<tr id="rowRate'+resp.rate.id+'">'+
            '<td id="columnRateName'+resp.rate.id+'">'+resp.rate.name+'</td>'+
            '<td id="columnRateRate'+resp.rate.id+'">'+resp.rate.rate_mature+'</td>'+
            '<td id="columnRateMinStay'+resp.rate.id+'">'+resp.rate.min_stay+'</td>'+
            '<td id="columnRateValidDate'+resp.rate.id+'">'+resp.rate.date_start+' - '+resp.rate.date_end+'</td>'+
            '<td id="columnRateType'+resp.rate.id+'">'+rate_type+'</td>'+
            '<td width="100px">'+
              '<button type="button" class="btn btn-default btn-xs btn-block"  id="btnRateUpdate" value="'+resp.rate.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>'+
              '<button type="button" class="btn btn-danger btn-xs btn-block"  id="btnRateDelete" value="'+resp.rate.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
            '</td>'+
          '</tr>'
        );
      }

      $('#formRate')[0].reset();
    }else{
      alert('Price Gagal Masuk');
    }
  });
//
  $('#btnUpdatePrice').click(function(){
    // alert('masuk update');
    var serializedData = $('#formRate').serializeArray();
    request = $.ajax({
        url: base_url+"admin/rates/update",
        type: "POST",
        data: serializedData
    });
    request.done(function (response, textStatus, jqXHR){
      console.log(response);

      $('#modalPrice').modal('hide');
      var resp = jQuery.parseJSON(response);
      // console.log(resp);
      var rate_type;
      if(resp.rate.type == 0){
        rate_type = "Base Rate";
      }else if(resp.rate.type == 1){
        rate_type = "High Season";
      }else if(resp.rate.type == 2){
        rate_type = "Promo / Low Season";
      }

      $('#columnRateName'+resp.rate.id+'').text(resp.rate.name);
      $('#columnRateRate'+resp.rate.id+'').text(resp.rate.rate_mature);
      $('#columnRateMinStay'+resp.rate.id+'').text(resp.rate.min_stay);
      $('#columnRateValidDate'+resp.rate.id+'').text(resp.rate.date_start+' - '+resp.rate.date_end);
      $('#columnRateType'+resp.rate.id+'').text(rate_type);
      $('#formRate')[0].reset();
    });
  });
});
//
//
//
$('#btnAddPriceModal').click(function(){
  $("#rate_accommodations_id").val($("#accommodation_rooms_id_for_prices").val());
});

$(document).on("click", "#btnRoomDelete", function(){
  idDelete = this.value;
  whichDelete = 'room';
  // alert(idDelete);
  $('#modalDelete').modal('show');
});
$(document).on("click", "#btnRateDelete", function(){

  idDelete = this.value;
  whichDelete = 'rate';
  // alert(idDelete);
  $('#modalDelete').modal('show');
});
$(document).on("click", "#btnPhotoDelete", function(){
  idDelete = this.value;
  whichDelete = 'photo';
  $('#modalDelete').modal('show');
});
//
//
//
//
$(document).on("click", "#btnRateUpdate", function(){
  $("#btnAddPrice").hide();
  $("#btnUpdatePrice").show();

  if($("#renting_type").val() == 0){
    $("#rate_accommodations_id").val(0);
  }else if($("#renting_type").val() == 2){
    $("#rate_accommodations_id").val($("#accommodation_rooms_id_for_prices").val());
  }



  request = $.ajax({
      url: base_url+"admin/rates/read_by_id",
      type: "POST",
      data: {
        id : this.value,
        unique_id : $("#unique_id").val()
      }
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    var resp = jQuery.parseJSON(response);
    $("#rate_update_id").val(resp.id);
    $("#rate_update_created_on").val(resp.created_on);
    $("#rate_update_type").val(resp.type);
    $("#rate_update_name").val(resp.name);
    $("#rate_update_rate").val(resp.rate_mature);
    $("#rate_update_rate_mature_extra").val(resp.rate_mature_extra);
    $("#rate_update_rate_children_extra").val(resp.rate_children_extra);
    $("#rate_update_rate_infant_extra").val(resp.rate_infant_extra);
    $("#rate_update_min_stay").val(resp.min_stay);
    $("#datepicker1").val(resp.date_start);
    $("#datepicker2").val(resp.date_end);
    $("#rate_update_description").val(resp.description);
    $('#modalPrice').modal('show');
  });
});
//
$('#btnConfirmDelete').click(function(){
  if(whichDelete == "room"){
    // Delete array accommodation_rooms_ids on the variable and on the input
    for(var i = 0; i<accommodation_rooms_ids.length; i++){
      if(idDelete == accommodation_rooms_ids[i]){
        accommodation_rooms_ids.splice(i, 1);
        $('#accommodation_rooms_ids').val(accommodation_rooms_ids);
      }
    }

    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/delete",
        type: "POST",
        data: {
          id : idDelete,
          unique_id : $("#unique_id").val()
        }
    });
    request.done(function (response, textStatus, jqXHR){
      $('#modalDelete').modal('hide');
      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){

        $('#rowRoom'+resp.id+'').remove();
      }else{
        alert("Delete Failed");
      }
    });
    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/prices/delete_by_accommodation_rooms_id",
        type: "POST",
        data: {
          accommodation_rooms_id : idDelete,
          unique_id : $("#unique_id").val()
        }
    });
    request.done(function (response, textStatus, jqXHR){

    });
  }else if(whichDelete == "rate"){
    // Delete array rates_ids on the variable and on the input
    for(var i = 0; i<rates_ids.length; i++){
      if(idDelete == rates_ids[i]){
        rates_ids.splice(i, 1);
        $('#rates_ids').val(rates_ids);
      }
    }
    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/prices/delete",
        type: "POST",
        data: {
          id : idDelete,
          unique_id : $("#unique_id").val()
        }

    });
    request.done(function (response, textStatus, jqXHR){
      console.log(response);
      $('#modalDelete').modal('hide');
      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){
        $('#rowRate'+resp.id+'').remove();
      }else{
        alert("Delete Failed");
      }
    });
  }else if(whichDelete == "photo"){
    request = $.ajax({
        url: base_url+"admin/accommodations/delete_photo",
        type: "POST",
        data: {
          id : idDelete,
          unique_id : $("#unique_id").val()
        }
    });
    request.done(function (response, textStatus, jqXHR){
      console.log(response);
      $('#modalDelete').modal('hide');

      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){
        $('#photo'+resp.id+'').remove();
      }else{
        alert("Delete Failed");
      }
    });
  }

  idDelete = null;
  whichDelete = null;
});
//
//
//
function clearTableRates(){
  $('#dataRates').html('');
}


//Upload
var inPrev = [];
var inPrevConfig = [];
var photoIds = [];

  $("#file-1").fileinput({
      theme: 'fa',
      uploadUrl: base_url+'upload', // you must set a valid URL here else you will get an error
      allowedFileExtensions: ['jpg', 'png', 'gif'],
      overwriteInitial: false,
      maxFileSize: 6000,
      maxFilesNum: 20,
      showCaption: false,
      showRemove: false,
      dropZoneEnabled: true,
      previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
      initialPreviewAsData: false,
      removeTitle: true

  });

  // $('.file-drop-zone-title').remove();


  $('#file-1').on('fileuploaded', function(event, data, previewId, index) {
      var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;
      // console.log(response);
      // $("#"+previewId+", .kv-file-remove").click();
      $("#"+previewId+", .kv-file-remove").remove();

      photoIds.push(response[0].id);
      $('#photoIds').val(photoIds);

      var pathArr = response[0].path.split('/');
      var path = "";
      for(var i=0; i<pathArr.length; i++){
        if(i == (pathArr.length-1)){
          path = path+"/thumbnails/"+pathArr[i];
        }else{
          path = path+"/"+pathArr[i];
        }
      }

      $('#uploadedPhotos').append(
              '<div class="col-lg-6 col-md-6 col-md-6" id="photo'+response[0].id+'"><table class="table table-bordered" style="background-color:white;"><tr>'+
              '<td class="text-center" colspan="2" rowspan="1"  style="height:220px"><img src="'+base_url+path+'" class="img-fluid img-thumbnail" alt="..." style="height:200px;width:auto;"></td>'+
              '<td class="text-center" colspan="2" rowspan="2" >'+
              '<table class="table table-bordered" style="margin-bottom:0px;">'+
              '<tr><td>Ordinal</td>'+
              '<td><input type="text" name="photo_ordinal[]" class="form-control" value=""></td></tr>'+
              '<tr><td>Title</td>'+
              '<td><input type="text" name="photo_title[]" class="form-control" value=""></td></tr>'+
              '<tr><td>Type</td>'+
              '<td><select class="form-control" name="photo_type[]" style="width: 100%;"><option selected="selected" value="">-</option></select></td>'+
              '</tr></table></td></tr><tr>'+
              '<td><button type="button" id="btnPhotoDelete" value="'+response[0].id+'" class="btn btn-danger btn-xs col-md-12"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;Delete</button></td>'+
              '</tr></table></div>'
      );

  });

  $('#input-id').on('fileselect', function(event, numFiles, label) {
      console.log("fileselect");
  });
