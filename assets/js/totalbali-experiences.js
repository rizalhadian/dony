
$('#btnExperienceAdd').on('click', function(){
  $('#formExperience')[0].reset();
  $('#modalExperienceLabel').text('Add location');
  $('#btnConfirmUpdate').hide();
  $('#btnConfirmSave').show();
});

var idBaru = [];
$('#btnConfirmSave').on('click', function(){
  var serializedData = $('#formExperience').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/experiences/create",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalExperience').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      idBaru.push("row"+resp.id+"");
      $('#dataRows').append(
            '<tr id="row'+resp.id+'">'+
              '<td id="dataName'+resp.id+'">'+resp.name+'</td>'+
              '<td id="dataDescription'+resp.id+'">'+resp.description+'</td>'+
              '<td width="50px">'+
                '<button class="btn btn-default btn-xs "  id="btnExperienceUpdate'+resp.id+'" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'+
                '<button class="btn btn-danger btn-xs"  id="btnExperienceDelete'+resp.id+'" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></button>'+
              '</td>'+
            '</tr>'
      );

      getAllbtnExperienceDelete();
      getAllbtnExperienceUpdate();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> '+resp.message+'</div>');
    }
  });
});

getAllbtnExperienceDelete();
getAllbtnExperienceUpdate();



function getAllbtnExperienceDelete(){
  $("[id^=btnExperienceDelete]").each(function () {
    $(this).click(function () {
      $('#modalDelete').modal('show');
      $("#idDelete").val($(this).val());
    });
  });
}

function getAllbtnExperienceUpdate(){

  $("[id^=btnExperienceUpdate]").each(function () {
    $(this).click(function () {
      $('#btnConfirmUpdate').show();
      $('#btnConfirmSave').hide();
      $('#modalExperienceLabel').text('Edit location');
      $("#idUpdate").val($(this).val());
      $('#modalExperience').modal('show');

      request = $.ajax({
        url: base_url+"admin/masterdata/experiences/read_id",
        type: "POST",
        data: {id : $(this).val() }
      });

      request.done(function (response, textStatus, jqXHR){

        var resp = jQuery.parseJSON(response);
        $('#nameUpdate').val(resp[0].name);
        $('#descriptionUpdate').val(resp[0].description);
      });

    });
  });
}

$("#btnConfirmDelete").on('click', function(){
  var id = $("#idDelete").val();

  request = $.ajax({
      url: base_url+"admin/masterdata/experiences/delete",
      type: "POST",
      data: {id : id}
  });

  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    $('#modalDelete').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $('#row'+resp.id+'').remove();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});

$("#btnConfirmUpdate").on('click', function(){

  var serializedData = $('#formExperience').serializeArray();

  request = $.ajax({
      url: base_url+"admin/masterdata/experiences/update",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalExperience').modal('hide');

    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $("#dataName"+resp.id+"").text(resp.name);
      $("#dataDescription"+resp.id+"").text(resp.description);
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});

$( "#parent_nameUpdate" ).autocomplete({
  source: function( request, response ) {
      $.ajax({
                url: base_url+"admin/masterdata/experiences/read_like",
                dataType: "json",
                type: "POST",
                data: {word : request.term},
                success: function(data){

                    response(data);
                    // alert(data);
                }
        });
    },
    focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
		 select: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox and hidden field
					$(this).val(ui.item.label);
					$('#parent_idUpdate').val(ui.item.value);
		 }
});
$("#parent_nameUpdate").autocomplete( "option", "appendTo", ".eventInsForm" );
