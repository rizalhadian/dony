
var accommodation_rooms_id_for_prices;
var idDelete;
var whichDelete;
var room_datas = [];
var accommodation_rooms_ids = [];
// accommodation_rooms_ids.splice(1, 1);

$("#btnAddRoomInfo").on('click', function(){
  $("#btnAddRoom").hide();
  $("#btnUpdateRoom").hide();
});

$("#btnNextFacilities").on('click', function(){
  $("#tabFacilities").click();
  $("#btnAddRoom").show();
  $("#btnNextFacilities").hide()
});
$("#tabFacilities").on('click', function(){
  $("#btnAddRoom").show();
  $("#btnNextFacilities").hide();
});
$("#tabGeneralInfo").on('click', function(){
  $("#btnAddRoom").hide();
  $("#btnNextFacilities").show();
});

$("#privateRoomForm").show();
$("#privateRateForm").show();
$("#flexibleRoomRateForm").hide();
$("#flexibleRoomForm").hide();



$('#accommodation_types_id').on('click', function(){
  // alert($('#accommodation_types_id').val());
  if($('#accommodation_types_id').val()==2){
    $("#privateRoomForm").hide();
    $("#privateRateForm").hide();
    $("#flexibleRoomRateForm").show();
    $("#flexibleRoomForm").show();
  }else{
    $("#privateRoomForm").show();
    $("#privateRateForm").show();
    $("#flexibleRoomRateForm").hide();
    $("#flexibleRoomForm").hide();
  }
});

function initMap() {
        var styles = [
            {
                stylers: [
                    {hue: "#0099ff"},
                    {saturation: -40}
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    {lightness: 100},
                    {visibility: "simplified"}
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});

        var uluru = {lat: -8.699850, lng: 115.180635};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 16,
          center: uluru,
          mapTypeControlOptions: {
              mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
          }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');


        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
        });

        marker.addListener('drag', function() {
          // alert(marker.getPosition());
          $('#latitude').val(marker.getPosition().lat());
          $('#longitude').val(marker.getPosition().lng());

        });

        $('#refresh').on('click', function () {
            // alert('weei');
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(-8.699850, 115.180635));
        });

        $( "#locationAdd" ).autocomplete({
          source: function( request, response ) {
              $.ajax({
                        url: base_url+"api/locations/read_like",
                        dataType: "json",
                        type: "POST",
                        data: {word : request.term},
                        success: function(data){

                            response(data);
                            // alert(data);
                        }
                });
            },
            focus: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox
        					$(this).val(ui.item.label);
        				},
        		 select: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox and hidden field
        					$(this).val(ui.item.label);
        					$('#locations_id').val(ui.item.value);
                  var loc = ui.item.label;


                  $.ajax({
                            url: "http://maps.googleapis.com/maps/api/geocode/xml?address="+loc,
                            type: "GET",
                            dataType: "xml",
                            success: function(data){
                                var xml = data;
                                $("#latitude").val($(xml).find('location').find('lat').text());
                                $("#longitude").val($(xml).find('location').find('lng').text());
                                var latlng = new google.maps.LatLng($(xml).find('location').find('lat').text(), $(xml).find('location').find('lng').text());
                                marker.setPosition(latlng);
                                map.setCenter(latlng);
                            }
                    });

        		 }
        });
        $("#locationAdd").autocomplete( "option", "appendTo", ".eventInsForm" );
}

$('#btnAddRoom').click(function(){
  var serializedData = $('#formRoom').serializeArray();
  // alert(JSON.stringify(serializedData));
  // alert(serializedData);
  console.log(serializedData);
  request = $.ajax({
      url: base_url+"api/rooms/create",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    $('#modalRoom').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#dataRooms').append(
            '<tr id="rowRoom'+resp.id+'">'+
              '<td id="columnNameRoom'+resp.id+'">'+resp.name+'</td>'+
              '<td width="90px" id="columnRoomsRoom'+resp.id+'">'+resp.total_rooms+'</td>'+
              '<td id="columnDescriptionRoom'+resp.id+'">'+resp.total_bathrooms+'</td>'+
              '<td width="170px">'+
                '<button class="btn btn-warning btn-xs btnRoomRates"  id="btnRoomRates" value="'+resp.id+'"><i class="fa fa-usd" aria-hidden="true"></i> Rates</button>&nbsp;'+
                '<button class="btn btn-default btn-xs"  id="btnRoomUpdate'+resp.id+'" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>&nbsp;'+
                '<button class="btn btn-danger btn-xs"  id="btnRoomDelete" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>&nbsp;'+
              '</td>'+
            '</tr>'
      );
      accommodation_rooms_ids.push(resp.id);
      $("#accommodation_rooms_ids").val(accommodation_rooms_ids);
      // getAllbtnRoomRates();
      // getAllbtnRoomDelete();
      $('.dataTables_empty').remove();
    }
  });

});


$(document).on("click", "#btnRoomRates", function(){
  $("#accommodation_rooms_id_for_prices").val(this.value)

  $("#dataRates").html("");
  $('#modalPrices').modal('show');
  request = $.ajax({
      url: base_url+"admin/accommodations/rooms/prices/read_all_by_accommodation_rooms_id",
      type: "POST",
      data: {id : this.value}
  });
  request.done(function (response, textStatus, jqXHR){
    // console.log(response);
    var resp = jQuery.parseJSON(response);
    var type;
    for(var i=0; i<resp.length; i++){
      if(resp[i].type==0){
        type = "Base Rate";
      }else if(resp[i].type==1){
        type = "Seasonal";
      }else if(resp[i].type==2){
        type = "Promo";
      }
      $('#dataRates').append(
            '<tr id="rowRate'+resp[i].id+'">'+
              '<td id="columnNameRate'+resp[i].id+'">'+resp[i].name+'</td>'+
              '<td id="columnTypeRate'+resp[i].id+'">'+type+'</td>'+
              '<td id="columnRateRate'+resp[i].id+'">'+resp[i].rate+'</td>'+
              '<td id="columnDateStartRate'+resp[i].id+'">'+resp[i].date_start+'</td>'+
              '<td id="columnDateEndRate'+resp[i].id+'">'+resp[i].date_end+'</td>'+
              '<td width="170px">'+
                '<button class="btn btn-default btn-xs"  id="btnRateUpdate'+resp[i].id+'" value="'+resp[i].id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>&nbsp;'+
                '<button class="btn btn-danger btn-xs"  id="btnRateDelete" value="'+resp[i].id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>&nbsp;'+
              '</td>'+
            '</tr>'
      );
    }
  });
});



$('#btnAddPrice').click(function(){
  var serializedData = $('#formPrice').serializeArray();
  request = $.ajax({
      url: base_url+"admin/accommodations/rooms/prices/create",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    $('#modalPrice').modal('hide');
    var resp = jQuery.parseJSON(response);
    // console.log(response);
    if(resp.error == 0){
      $('#dataRates').append(
            '<tr id="rowRate'+resp.id+'">'+
              '<td id="columnNameRate'+resp.id+'">'+resp.name+'</td>'+
              '<td id="columnTypeRate'+resp.id+'">'+resp.type+'</td>'+
              '<td id="columnRateRate'+resp.id+'">'+resp.rate+'</td>'+
              '<td id="columnDateStartRate'+resp.id+'">'+resp.date_start+'</td>'+
              '<td id="columnDateEndRate'+resp.id+'">'+resp.date_end+'</td>'+
              '<td width="170px">'+
                '<button class="btn btn-default btn-xs"  id="btnRateUpdate'+resp.id+'" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>&nbsp;'+
                '<button class="btn btn-danger btn-xs"  id="btnRateDelete" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>&nbsp;'+
              '</td>'+
            '</tr>'
      );
    }else{
      alert('Price Gagal Masuk');
    }
  });
});

$('#btnAddPriceModal').click(function(){
  $("#accommodation_rooms_id_for_price").val($("#accommodation_rooms_id_for_prices").val());
});

$(document).on("click", "#btnRoomDelete", function(){

  idDelete = this.value;
  whichDelete = 'room';
  // alert(idDelete);
  $('#modalDelete').modal('show');
});
$(document).on("click", "#btnRateDelete", function(){
  // alert(this.value);
  idDelete = this.value;
  whichDelete = 'rate';
  // alert(idDelete);
  $('#modalDelete').modal('show');
});

$('#btnConfirmDelete').click(function(){
  if(whichDelete == "room"){
    // Delete array accommodation_rooms_ids on the variable and on the input
    for(var i = 0; i<accommodation_rooms_ids.length; i++){
      if(idDelete == accommodation_rooms_ids[i]){
        accommodation_rooms_ids.splice(i, 1);
        $('#accommodation_rooms_ids').val(accommodation_rooms_ids);
      }
    }

    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/delete",
        type: "POST",
        data: {id : idDelete}
    });
    request.done(function (response, textStatus, jqXHR){
      $('#modalDelete').modal('hide');
      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){

        $('#rowRoom'+resp.id+'').remove();
      }else{
        alert("Delete Failed");
      }
    });
    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/prices/delete_by_accommodation_rooms_id",
        type: "POST",
        data: {accommodation_rooms_id : idDelete}
    });
    request.done(function (response, textStatus, jqXHR){

    });
  }else if(whichDelete == "rate"){
    request = $.ajax({
        url: base_url+"admin/accommodations/rooms/prices/delete",
        type: "POST",
        data: {id : idDelete}
    });
    request.done(function (response, textStatus, jqXHR){
      $('#modalDelete').modal('hide');
      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){
        $('#rowRate'+resp.id+'').remove();
      }else{
        alert("Delete Failed");
      }
    });
  }
  idDelete = null;
  whichDelete = null;
});

function clearTableRates(){
  $('#dataRates').html('');
}
