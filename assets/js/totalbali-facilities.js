$('#btnFacilityAdd').on('click', function(){
  $('#formFacility')[0].reset();
  $('#modalFacilityLabel').text('Add Facility');
  $('#btnConfirmUpdate').hide();
  $('#btnConfirmSave').show();
});

var idBaru = [];
$('#btnConfirmSave').on('click', function(){
  var serializedData = $('#formFacility').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/facilities/create",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalFacility').modal('hide');
    console.log(response);
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      idBaru.push("row"+resp.id+"");
      $('#dataRows').append(
            '<tr id="row'+resp.id+'">'+
              '<td id="dataName'+resp.id+'">'+resp.name+'</td>'+
              '<td id="dataDescription'+resp.id+'">'+resp.description+'</td>'+
              '<td width="50px">'+
                '<button class="btn btn-default btn-xs "  id="btnFacilityUpdate'+resp.id+'" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'+
                '<button class="btn btn-danger btn-xs"  id="btnFacilityDelete'+resp.id+'" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></button>'+
              '</td>'+
            '</tr>'
      );

      getAllBtnFacilityDelete();
      getAllBtnFacilityUpdate();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> '+resp.message+'</div>');
    }
  });
});

getAllBtnFacilityDelete();
getAllBtnFacilityUpdate();



function getAllBtnFacilityDelete(){
  $("[id^=btnFacilityDelete]").each(function () {
    $(this).click(function () {

      $('#modalDelete').modal('show');
      $("#idDelete").val($(this).val());
    });
  });
}

function getAllBtnFacilityUpdate(){
  $("[id^=btnFacilityUpdate]").each(function () {
    $(this).click(function () {
      $('#btnConfirmUpdate').show();
      $('#btnConfirmSave').hide();
      $('#modalFacilityLabel').text('Edit Facility');
      $("#idUpdate").val($(this).val());
      $('#modalFacility').modal('show');

      request = $.ajax({
        url: base_url+"admin/masterdata/facilities/read_id",
        type: "POST",
        data: {id : $(this).val() }
      });

      request.done(function (response, textStatus, jqXHR){
        var resp = jQuery.parseJSON(response);
        $('#nameUpdate').val(resp[0].name);
        $('#descriptionUpdate').val(resp[0].description);
      });

    });
  });
}

$("#btnConfirmDelete").on('click', function(){
  var id = $("#idDelete").val();
  request = $.ajax({
      url: base_url+"admin/masterdata/facilities/delete",
      type: "POST",
      data: {id : id}
  });
  request.done(function (response, textStatus, jqXHR){
    $('#modalDelete').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $('#row'+resp.id+'').remove();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});

$("#btnConfirmUpdate").on('click', function(){

  var serializedData = $('#formFacility').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/facilities/update",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){
    $('#modalFacility').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $("#dataName"+resp.id+"").text(resp.name);
      $("#dataDescription"+resp.id+"").text(resp.description);
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});
