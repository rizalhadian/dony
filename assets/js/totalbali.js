
//Sublocation Villa
$('#location').on('click', function () {
    var id = $('#location').find(":selected").val();

    if(id != 0){
      request = $.ajax({
          url: base_url+"api/getlocation?location=",
          type: "post",
          data: {location : id}
      });
      request.done(function (response, textStatus, jqXHR){
          var datas = JSON.parse(response);
          $("#sublocation").prop('disabled', false);
          $.each(datas, function(index, data) {
            $('#sublocation').append($("<option></option>")
                    .attr("value",data['id'])
                    .text(data['nama']));
          });
       });
    }else{
      $("#sublocation").prop('disabled', true);
    }
});
// End of Sublocation Villa

// Delete Villa
$("[id^=btndelete]").each(function () {
    $(this).click(function () {
      $("#idtodelete").val($(this).val());
    });
});

$("#btnaddvilla").on('click', function(){
  // $('#formvilla')[0].reset();
  $("#modalVillaLabel").text("Add Villa");
});

$("[id^=btnupdate]").each(function () {
    $('#formvilla')[0].reset();
    $(this).click(function () {
      $("#modalVillaLabel").text("Edit Villa");
      var id = $(this).val();

      request = $.ajax({
          url: base_url+"admin/villa/get",
          type: "POST",
          data: {id : id}
      });
      request.done(function (response, textStatus, jqXHR){
        alert(response);
        var datas = JSON.parse(response);
        $.each(datas, function(index, data) {
          $("#idtoupdate").val(data.idkamar);

          $("#name").val(data.name);
          $("#rooms").val(data.rooms);
          $("#baserates").val(data.rate);
          $("#hyperlink").val(data.hyperlink);
          $("#location").val(data.parent);

          // request = $.ajax({
          //     url: base_url+"api/getlocation?location=",
          //     type: "post",
          //     data: {location : data['parent']}
          // });
          // request.done(function (response, textStatus, jqXHR){
          //     var datas = JSON.parse(response);
          //     $("#sublocation").prop('disabled', false);
          //     $.each(datas, function(index, data) {
          //       $('#sublocation').append($("<option></option>")
          //               .attr("value",data['id'])
          //               .text(data['nama']));
          //     });
          // });

          $("#occupancy").val(data['occupancy']);
          $("#bedrooms").val(data['rooms']);
          $("#bathrooms").val(data['bathrooms']);
          $("#address").val(data['address1']);
          CKEDITOR.instances.editor1.setData(data['description']);
          $("#lat").val(data['latitude']);
          $("#lng").val(data['longitude']);
          // alert(data);
        });
      });
    });
});



$("#confirmdelete").on('click', function(){
  var id = $("#idtodelete").val();

  request = $.ajax({
      url: base_url+"admin/villa/delete",
      type: "post",
      data: {id : id}
  });
  request.done(function (response, textStatus, jqXHR){
    $('#modalDelete').modal('hide');
    // $('#modalSuccess').modal('show');
    $('#rowvilla'+id+'').remove();
  });
});

$("#confirmdeleteuser").on('click', function(){
  var id = $("#idtodelete").val();

  request = $.ajax({
      url: base_url+"admin/user/delete",
      type: "post",
      data: {id : id}
  });
  request.done(function (response, textStatus, jqXHR){
    // alert(response);
    $('#modalDelete').modal('hide');
    // $('#modalSuccess').modal('show');
    $('#rowuser'+id+'').remove();
  });
});

$("#tabphotos").on("click", function(){

  // alert('we');

  // $.each(datas, function(index, data) {
  //   $('#imageview').append($("<strong>Hello</strong>");
  // });
})
//End of Delete villa

$("[id^=tdavail_]").each(function () {
    $(this).click(function () {
      var idtd = this.id;
      var id = idtd.split('_');

      request = $.ajax({
          url: base_url+"admin/book/get",
          type: "post",
          data: {id : id[1]}
      });
      request.done(function (response, textStatus, jqXHR){
        // alert(response);
        var datas = JSON.parse(response);
        $.each(datas, function(index, data) {
          $('#bookid').text(data['bookid']);
          $('#guestname').text(data['guestname']);
          $('#email').text(data['email']);
          $('#villa').text(data['villa']);
          var cico = data['checkin']+" - "+data['checkout'];
          $('#cico').text(cico);
          var status = "null";
          if(data['status'] == 1){
            status = "Enquiry";
          }else if(data['status'] == 2){
            status = "Onhold";
          }else if(data['status'] == 3){
            status = "Confirm";
          }else{
            status = data['status'];
          }
          $('#status').text(status);
          $('#guest').text(data['numberofguests']);
          $('#extracost').text('$ '+data['extracost']);
          $('#totalcost').text('$ '+data['totalbookingvalue']);
          $('#agentcommission').text('$ '+data['agentcommission']);
          $('#specialrequest').text(data['specialrequest']);






        });

          // $('').text();
       });
    });
});
