
var accommodation_rooms_id_for_prices;
var idDelete;
var whichDelete;
var room_datas = [];
var rates_ids = [];
var accommodation_rooms_ids = [];
var room_update;

var map_accommodation_lat = $('#map_accommodation_lat').val();
var map_accommodation_lng = $('#map_accommodation_lng').val();
var map_type = $('#map_type').val();
var map_accommodations_id = $('#map_accommodations_id').val();
var map_marker_accommodations_all = $('#map_marker_accommodations_all').val();
var map_marker_accommodations_specific = $('#map_marker_accommodations_specific').val();
var map_marker_restaurants_all = $('#map_marker_restaurants_all').val();
var map_marker_restaurants_specific = $('#map_marker_restaurants_specific').val();
var map_marker_beaches_all = $('#map_marker_beaches_all').val();
var map_marker_beaches_specific = $('#map_marker_beaches_specific').val();

function initMap() {
        var styles = [
            {
                stylers: [
                    {hue: "#0099ff"},
                    {saturation: -40}
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    {lightness: 100},
                    {visibility: "simplified"}
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    {visibility: "on"}
                ]
            }, {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});

        var uluru = {lat: -8.699850, lng: 115.180635};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 14,
          center: uluru,
          zoomControl: true,
          streetViewControl: false,
          fullscreenControl: false,
          mapTypeControl: false,
          mapTypeControlOptions: {
              mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
          }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        // Single Accommodation
        var single_accommodation;
        var marker_single_accommodation;
        var latlng_single_accommodation = {lat: parseFloat($('#map_accommodation_lat').val()), lng: parseFloat($('#map_accommodation_lng').val())};

        if(map_type == "map_for_villa"){
          marker_single_accommodation = new google.maps.Marker({
            position: latlng_single_accommodation,
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP,
          });
          map.setOptions({
            center: latlng_single_accommodation,
            zoom: 15
          });
        }

        // End of Single Accommodation

        function restaurants(resp){
          var restaurants = resp;
          var marker_restaurants = [];

          for(var i = 0; i<restaurants.length; i++){
            var latlng = {lat: parseFloat(restaurants[i].lat), lng: parseFloat(restaurants[i].lng)};

            marker_restaurants[i] = new google.maps.Marker({
              position: latlng,
              map: map,
              icon: base_url+"assets/maps/pin-restaurant.png",
              draggable: false,
              animation: google.maps.Animation.DROP,
            });
          }
        }

        function accommodations(resp){
          var accommodations = resp;
          var marker_accommodations = [];
          var info_window_acommodations = [];
          var content_accommodations = [];

          var highest_lat;
          var lowest_lat;
          var highest_lng;
          var lowest_lng;

          for(var i = 0; i<accommodations.length; i++){
            if(i==0){
              highest_lat = parseFloat(accommodations[i].lat);
              lowest_lat = parseFloat(accommodations[i].lat);
              highest_lng = parseFloat(accommodations[i].lng);
              lowest_lng = parseFloat(accommodations[i].lng);
            }else{
              if(highest_lat < parseFloat(accommodations[i].lat)){
                highest_lat = parseFloat(accommodations[i].lat);
              }
              if(lowest_lat > parseFloat(accommodations[i].lat)){
                lowest_lat = parseFloat(accommodations[i].lat);
              }

              if(highest_lng < parseFloat(accommodations[i].lng)){
                highest_lng = parseFloat(accommodations[i].lng);
              }
              if(lowest_lng > parseFloat(accommodations[i].lng)){
                lowest_lng = parseFloat(accommodations[i].lng);
              }
            }


            var latlng = {lat: parseFloat(accommodations[i].lat), lng: parseFloat(accommodations[i].lng)};
            var bedrooms;
            if(accommodations[i].rooms.length>1){
              bedrooms = accommodations[i].rooms[0]+'-'+accommodations[i].rooms[accommodations[i].rooms.length-1];
            }else{
              bedrooms = accommodations[i].rooms[0];
            }

            if(accommodations[i].total_rooms>10){
              marker_accommodations[accommodations[i].id] = new google.maps.Marker({
                position: latlng,
                map: map,
                index: i,
                icon: base_url+"assets/maps/accommodation-10+.png",
                draggable: false,
              });
            }else{
              marker_accommodations[accommodations[i].id] = new google.maps.Marker({
                position: latlng,
                map: map,
                index: i,
                icon: base_url+"assets/maps/accommodation-"+accommodations[i].total_rooms+".png",
                draggable: false,
              });
            }


            console.log(accommodations[i]);

            $("[id^=accommodationSquare]").each(function () {
              $(this).on("mouseenter", function () {
                var val = this.getAttribute('value');
                // console.log(val+"enter");
                // console.log(marker_accommodations[val]);
                marker_accommodations[val].setAnimation(google.maps.Animation.BOUNCE);
              });
              $(this).on("mouseleave", function () {
                var val = this.getAttribute('value');
                // console.log(val+"leave");
                marker_accommodations[val].setAnimation(null);
              });
            });
          }

          var GLOBE_WIDTH = 256;
          var pixelWidth = $("#map-container").width()-185;
          // var pixelWidth = 600;
          var new_center_point = {lat: ((highest_lat+lowest_lat)/2), lng: ((highest_lng+lowest_lng)/2) };
          console.log(pixelWidth);
          var angle;

          if(highest_lng-lowest_lng > highest_lat-lowest_lat){
            angle = highest_lng-lowest_lng;
            console.log('lng');
          }else{
            angle = highest_lat-lowest_lat;
            console.log('lat');
          }
          if (angle < 0) {
            angle += 360;
          }
          var zoom = Math.round(Math.log(pixelWidth * 360 / angle / GLOBE_WIDTH) / Math.LN2);

          if(zoom == "Infinity" || zoom > 14){
            zoom = 14;
          }

          map.setOptions({
              center: new_center_point,
              zoom: zoom
          });
        }

        function interestingPlaces(resp){
          var interesting_places = resp;
          var marker_interesting_places = [];
          var info_window_interesting_places = [];
          var content_interesting_places = [];

          for(var i = 0; i<interesting_places.length; i++){

            var latlng = {lat: parseFloat(interesting_places[i].lat), lng: parseFloat(interesting_places[i].lng)};
            var thumbnail;

            if(interesting_places[i].thumbnail != null && interesting_places[i].thumbnail != ""){
              thumbnail = interesting_places[i].thumbnail;
            }else{
              thumbnail = base_url+"/assets/images/no-image.png";
            }

            if(interesting_places[i].type == "Surf-Spot"){
              content_interesting_places[interesting_places[i].id] =
                '<div class="col-xs-12 col-md-12" style="margin-left:-15px; width:400px;">'+
                  '<table class="table">'+
                    '<tr>'+
                      '<td style="width:140px;">'+
                        '<img src="'+thumbnail+'" alt="..." class="img-thumbnail" style="width:140px;margin-left:-7px;margin-top:-8px;margin-right:-8px; margin-bottom:-8px;">'+
                      '</td>'+
                      '<td >'+
                        '<b>'+interesting_places[i].name+'</b><br>'+
                        '<table class="table table-bordered" style="margin-bottom:-8px;">'+
                          '<tr>'+
                            '<td style="font-size:80%; text-align:justify;">'+
                              'Difficult'+
                            '</td>'+
                            '<td style="font-size:80%; text-align:justify;">'+
                            '<b>'+interesting_places[i].surf_difficult+"</b>"+
                          '</td>'+
                          '</tr>'+
                          '<tr >'+
                            '<td style="font-size:85%;">'+
                              'Tide '+
                            '</td>'+
                            '<td style="font-size:85%;">'+
                              '<b>'+interesting_places[i].surf_tide+"</b>"+
                            '</td>'+
                          '</tr>'+
                          '<tr >'+
                            '<td style="font-size:85%;">'+
                              'Wave Direction '+
                            '</td>'+
                            '<td style="font-size:85%;">'+
                              '<b>'+interesting_places[i].surf_wave_direction+"</b> "+
                            '</td>'+
                          '</tr>'+
                          '<tr >'+
                            '<td style="font-size:85%;">'+
                              'Wave Type '+
                            '</td>'+
                            '<td style="font-size:85%;">'+
                              '<b>'+interesting_places[i].surf_wave_type+"</b>"+
                            '</td>'+
                          '</tr>'+
                          '<tr >'+
                            '<td style="font-size:85%;">'+
                              'Wind '+
                            '</td>'+
                            '<td style="font-size:85%;">'+
                              '<b>'+interesting_places[i].surf_wind+"</b>"+
                            '</td>'+
                          '</tr>'+
                        '</table>'+
                      '</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td colspan=2>'+

                      '</td>'+
                    '</tr>'+
                  '</table>'+
                  '</div>'+
                '</div>';
            }else{
              content_interesting_places[interesting_places[i].id] =
                '<div class="col-xs-12 col-md-12" style="margin-left:-15px; width:400px;">'+
                  '<table class="table">'+
                    '<tr>'+
                      '<td style="width:140px;">'+
                        '<img src="'+thumbnail+'" alt="..." class="img-thumbnail" style="width:140px;margin-left:-7px;margin-top:-8px;margin-right:-8px; margin-bottom:-8px;">'+
                      '</td>'+
                      '<td >'+
                        '<b>'+interesting_places[i].name+'</b><br>'+
                        '<table class="table table-bordered" style="margin-bottom:-8px;">'+
                          '<tr>'+
                            '<td style="font-size:80%; text-align:justify;">'+
                              'Cuisines'+
                            '</td>'+
                            '<td style="font-size:80%; text-align:justify;">'+
                            '<b>'+interesting_places[i].zomato_cuisines+"</b>"+
                          '</td>'+
                          '</tr>'+
                          '<tr >'+
                            '<td style="font-size:85%;">'+
                              'Rates '+
                            '</td>'+
                            '<td style="font-size:85%;">'+
                              '<b>'+interesting_places[i].zomato_rank+"</b> (Zomato)"+
                            '</td>'+
                          '</tr>'+
                        '</table>'+
                      '</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td colspan=2>'+
                        '<center><a href="'+interesting_places[i].zomato_menus_url+'" target="_blank" class="btn btn-primary btn-block" style="">Menu (Zomato)</a></center>'+
                      '</td>'+
                    '</tr>'+
                  '</table>'+
                  '</div>'+
                '</div>';
            }

            content_interesting_places[interesting_places[i].id] = new google.maps.InfoWindow({
              content: content_interesting_places[interesting_places[i].id]
            });

            marker_interesting_places[interesting_places[i].id] = new google.maps.Marker({
              position: latlng,
              map: map,
              index: i,
              icon: base_url+interesting_places[i].marker_path,
              draggable: false,
            });

            marker_interesting_places[interesting_places[i].id].addListener('click', function() {
              content_interesting_places[interesting_places[this.index].id].open(map, marker_interesting_places[interesting_places[this.index].id]);
              console.log(interesting_places[this.index]);
            });

            // console.log(interesting_places[i]);
          }
        }

        // Querying Map2
        var current_url_arr = current_url.split("?");
        var get_parameters = current_url_arr[1];
        var get_parameters_arr = current_url_arr[1].split("&");
        var parameters=[];
        for(var i=0; i<get_parameters_arr.length; i++){
          var temp = get_parameters_arr[i].split("=");
          parameters[temp[0]] = temp[1];
        }

        // Get Accommodations
        request = $.ajax({
            url: base_url+"api/accommodations/search?"+get_parameters,
            type: "GET",
        });
        request.done(function (response, textStatus, jqXHR){
          var resp = jQuery.parseJSON(response);
          accommodations(resp);
        });
        // End Get Accommodations

        // Get Interesting Places
        request = $.ajax({
          url: base_url+"api/interesting-places?"+get_parameters,
          type: "GET",
        });
        request.done(function (response, textStatus, jqXHR){
          var resp = jQuery.parseJSON(response);
          interestingPlaces(resp);
        });
        // End Get Interesting Places

        // // Get All Restaurants
        // if(map_marker_restaurants_all == 1){

        //   request = $.ajax({
        //       url: base_url+"api/interesting-places/read",
        //       type: "GET",
        //   });
        //   request.done(function (response, textStatus, jqXHR){
        //     var resp = jQuery.parseJSON(response);
        //     restaurants(resp);
        //   });

        // }
        // // End of Get All Restaurants

        // // Get All Accommodations
        // if(map_marker_accommodations_all == 1){
        //   request = $.ajax({
        //       url: base_url+"api/accommodations/read",
        //       type: "GET",
        //   });
        //   request.done(function (response, textStatus, jqXHR){
        //     var resp = jQuery.parseJSON(response);
        //     accommodations(resp);
        //   });
        // }
        // // End of Get All Accommodations







}

// End of Initmap
