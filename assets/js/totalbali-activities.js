


$('#btnPartnerCreate').on('click', function(){
  $('#formPartner')[0].reset();
  $('#modalPartnerLabel').text('Add Partner');
  $('#btnPartnerConfirmUpdate').hide();
  $('#btnPartnerConfirmSave').show();
});

getAllBtnPartnerUpdate();
getAllBtnPartnerDelete();

$("#btnPartnerConfirmSave").on('click', function(){
  var serializedData = $('#formPartner').serializeArray();
  request = $.ajax({
      url: base_url+"admin/activities/partners/create_do",
      type: "POST",
      data: serializedData
  });
  request.done(function (response, textStatus, jqXHR){
     location.reload();
     getAllBtnPartnerUpdate();
     getAllBtnPartnerDelete();
  });
});

function getAllBtnPartnerUpdate(){
  $("[id^=btnPartnerUpdate]").each(function () {
    $(this).click(function () {
      $('#btnPartnerConfirmUpdate').show();
      $('#btnPartnerConfirmSave').hide();
      $('#modalPartnerLabel').text('Edit Partner');
      $("#idUpdate").val($(this).val());
      $('#modalPartner').modal('show');

      request = $.ajax({
        url: base_url+"admin/activities/partners/read_by_id",
        type: "POST",
        data: {id : $(this).val() }
      });

      request.done(function (response, textStatus, jqXHR){
        var resp = jQuery.parseJSON(response);

        $('#partnerId').val(resp.id);
        $('#partnerName').val(resp.name);
        $('#partnerAddress').val(resp.address);
        $('#partnerEmail').val(resp.email);
        $('#partnerPhone').val(resp.phone);
        $('#partnerDescription').val(resp.description);
      });

    });
  });
}

function getAllBtnPartnerDelete(){
  $("[id^=btnPartnerDelete]").each(function () {
    $(this).click(function () {
      $('#modalDelete').modal('show');
      $("#idtodelete").val($(this).val());
      $('#whichdelete').val('partners');
    });
  });
}

$("#btnConfirmDelete").on('click', function(){
  if($("#whichdelete").val() == "partners"){
    var id = $("#idtodelete").val();
    request = $.ajax({
        url: base_url+"admin/activities/partners/delete_do",
        type: "POST",
        data: {id : id}
    });

    request.done(function (response, textStatus, jqXHR){
      $('#modalDelete').modal('hide');
      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){
        $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
        $('#rowPartner'+resp.id+'').remove();
      }else{
        $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
      }
    });
  }

});

$("#btnPartnerConfirmUpdate").on('click', function(){

  var serializedData = $('#formPartner').serializeArray();
  request = $.ajax({
      url: base_url+"admin/activities/partners/update_do",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalPartner').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $("#dataPartnerName"+resp.id+"").text(resp.name);
      $("#dataPartnerAddress"+resp.id+"").text(resp.address);
      $("#dataPartnerEmail"+resp.id+"").text(resp.email);
      $("#dataPartnerPhone"+resp.id+"").text(resp.phone);
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});
