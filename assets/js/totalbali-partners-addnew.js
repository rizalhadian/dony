
var accommodation_rooms_id_for_prices;
var idDelete;
var whichDelete;
var room_datas = [];
var rates_ids = [];
var accommodation_rooms_ids = [];
var room_update;
// accommodation_rooms_ids.splice(1, 1);


if($('#renting_types_id').val()==2){
  $("#privateRoomForm").hide();
  $("#privateRateForm").hide();
  $("#flexibleRoomRateForm").show();
  $("#flexibleRoomForm").show();
}else{
  $("#privateRoomForm").show();
  $("#privateRateForm").show();
  $("#flexibleRoomRateForm").hide();
  $("#flexibleRoomForm").hide();
}
// alert('wi');
$('#renting_types_id').on('click', function(){
  // alert($('#renting_types_id').val());
  if($('#renting_types_id').val()==2){
    $("#privateRoomForm").hide();
    $("#privateRateForm").hide();
    $("#flexibleRoomRateForm").show();
    $("#flexibleRoomForm").show();
  }else{
    $("#privateRoomForm").show();
    $("#privateRateForm").show();
    $("#flexibleRoomRateForm").hide();
    $("#flexibleRoomForm").hide();
  }
});

function initMap() {

        var styles = [
            {
                stylers: [
                    {hue: "#0099ff"},
                    {saturation: -40}
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    {lightness: 100},
                    {visibility: "simplified"}
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});

        var uluru = {lat: -8.699850, lng: 115.180635};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 16,
          center: uluru,
          mapTypeControlOptions: {
              mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
          }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');


        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          draggable: true,
          animation: google.maps.Animation.DROP,
        });

        marker.addListener('drag', function() {
          // alert(marker.getPosition());
          $('#latitude').val(marker.getPosition().lat());
          $('#longitude').val(marker.getPosition().lng());

        });



        $( "#locationAdd" ).autocomplete({
          source: function( request, response ) {
              $.ajax({
                        url: base_url+"admin/masterdata/locations/read_like",
                        dataType: "json",
                        type: "POST",
                        data: {word : request.term},
                        success: function(data){

                            response(data);
                            // alert(data);
                        }
                });
            },
            focus: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox
        					$(this).val(ui.item.label);
        				},
        		 select: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox and hidden field
        					$(this).val(ui.item.label);
        					$('#locations_id').val(ui.item.value);
                  var loc = ui.item.label;


                  $.ajax({
                            url: "http://maps.googleapis.com/maps/api/geocode/xml?address="+loc,
                            type: "GET",
                            dataType: "xml",
                            success: function(data){
                                var xml = data;
                                $("#latitude").val($(xml).find('location').find('lat').text());
                                $("#longitude").val($(xml).find('location').find('lng').text());
                                var latlng = new google.maps.LatLng($(xml).find('location').find('lat').text(), $(xml).find('location').find('lng').text());
                                marker.setPosition(latlng);
                                map.setCenter(latlng);
                            }
                    });

        		 }
        });
        $("#locationAdd").autocomplete( "option", "appendTo", ".eventInsForm" );

        $( "#partnerAdd" ).autocomplete({
          source: function( request, response ) {
              $.ajax({
                        url: base_url+"admin/masterdata/partners/read_like",
                        dataType: "json",
                        type: "POST",
                        data: {word : request.term},
                        success: function(data){
                            response(data);
                            // alert(data);
                        }
                });
            },
            focus: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox
        					$(this).val(ui.item.label);
        				},
        		 select: function(event, ui) {
        					// prevent autocomplete from updating the textbox
        					event.preventDefault();
        					// manually update the textbox and hidden field
        					$(this).val(ui.item.label);
        					$('#partners_id').val(ui.item.value);
                  var loc = ui.item.label;
        		 }
        });
        $("#partnerAdd").autocomplete( "option", "appendTo", ".eventInsForm" );



}
// End of Initmap
$(document).on("click", "#btnComplexDelete", function(){
  // alert(this.value);
  $('#whichDelete').val("complex");
  $('#idDelete').val(this.value);
  $('#modalDelete').modal('show');
});
$(document).on("click", "#btnAccommodationDelete", function(){
  // alert(this.value);
  $('#whichDelete').val("accommodation");
  $('#idDelete').val(this.value);
  $('#modalDelete').modal('show');
});


$(document).on("click", "#btnConfirmDelete", function(){
  // alert(this.value);
  if($('#whichDelete').val() == "complex"){
    request = $.ajax({
        url: base_url+"partner/delete_complex",
        type: "POST",
        data: {id : $('#idDelete').val()}
    });
    request.done(function (response, textStatus, jqXHR){
      console.log(response);
      location.reload();

    });
  }
  if($('#whichDelete').val() == "accommodation"){
    request = $.ajax({
        url: base_url+"admin/accommodations/delete",
        type: "POST",
        data: {
          id : $('#idDelete').val(),
          unique_id : $("#unique_id").val()
        }
    });
    request.done(function (response, textStatus, jqXHR){
      console.log(response);
       location.reload();
    });
  }


});




//
//
