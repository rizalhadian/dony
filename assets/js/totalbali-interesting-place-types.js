$('#btnLocationAdd').on('click', function() {
    $('#formLocation')[0].reset();
    $('#modalLocationLabel').text('Add Interesting Place Type');
    // alert('woi');
    $('#btnConfirmUpdate').hide();
    $('#btnConfirmSave').show();
});



var idBaru = [];
$('#btnConfirmSave').on('click', function() {
    var serializedData = $('#formLocation').serializeArray();
    console.log(serializedData);
    request = $.ajax({
        url: base_url + "admin/masterdata/interesting-place-types/create",
        type: "POST",
        processData: false,
        contentType: false,
        data: new FormData($("#formLocation")[0])
    });

    request.done(function(response, textStatus, jqXHR) {
        $('#modalLocation').modal('hide');
        var resp = jQuery.parseJSON(response);
        console.log(resp);
        $('#dataRows').append(
              '<tr id="row'+resp.object.id+'">'+
                '<td id="dataMarker'+resp.object.id+'" width="100px"><img src="'+base_url+resp.object.marker_path+'" class="img-fluid img-thumbnail" alt="..." style="height:64px;width:auto;"></td>'+
                '<td id="dataName'+resp.object.id+'">'+resp.object.name+'</td>'+
                '<td width="50px">'+
                  '<button class="btn btn-default btn-xs btn-block"  id="btnLocationUpdate" value="'+resp.object.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button>'+
                  '<button class="btn btn-danger btn-xs btn-block"  id="btnLocationDelete" value="'+resp.object.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                '</td>'+
              '</tr>'
        );
    });
});

$(document).on("click", "#btnLocationUpdate", function(){
  idDelete = this.value;
  $('#btnConfirmUpdate').show();
  $('#btnConfirmSave').hide();
  $('#modalLocationLabel').text('Edit Interesting Place Type');
  $("#idUpdate").val(this.value);
  $('#modalLocation').modal('show');

  request = $.ajax({
      url: base_url + "admin/masterdata/interesting-place-types/read-by-id",
      type: "POST",
      data: {
          id: this.value
      }
  });
  request.done(function(response, textStatus, jqXHR) {
      var resp = jQuery.parseJSON(response);
      $('#nameUpdate').val(resp[0].name);
      // $('#parent_idUpdate').val(resp[0].parent_id);
      // $('#parent_nameUpdate').val(resp[0].parent_name);
      // $('#typeUpdate').val(resp[0].type);
  });
});

var idDelete;
var whichDelete;

$(document).on("click", "#btnLocationDelete", function(){
  idDelete = this.value;
  whichDelete = 'room';
  $('#modalDelete').modal('show');
});

$('#btnConfirmDelete').click(function(){
  if(whichDelete == "room"){
    request = $.ajax({
        url: base_url+"admin/masterdata/interesting-place-types/delete",
        type: "POST",
        data: {
          id : idDelete,
          unique_id : $("#unique_id").val()
        }
    });
    request.done(function (response, textStatus, jqXHR){
      $('#modalDelete').modal('hide');
      var resp = jQuery.parseJSON(response);
      if(resp.error == 0){

        $('#row'+resp.id+'').remove();
      }else{
        alert("Delete Failed");
      }
    });
  }

  idDelete = null;
  whichDelete = null;
});



$("#btnConfirmUpdate").on('click', function() {
    var serializedData = $('#formLocation').serializeArray();
    request = $.ajax({
        url: base_url + "admin/masterdata/interesting-place-types/update",
        type: "POST",
        processData: false,
        contentType: false,
        data: new FormData($("#formLocation")[0])
    });

    request.done(function(response, textStatus, jqXHR) {
        $('#modalLocation').modal('hide');
        console.log(response);
        var resp = jQuery.parseJSON(response);
        if (resp.error == 0) {
            $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> ' + resp.message + '</div>');
            $("#dataName" + resp.object.id + "").text(resp.object.name);
            $("#dataMarker" + resp.object.id + "").text('');
            $("#dataMarker" + resp.object.id + "").append('<img src="'+base_url+resp.object.marker_path+'" class="img-fluid img-thumbnail" alt="..." style="height:64px;width:auto;">');

        } else {
            $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> ' + resp.message + '</div>');
        }
    });
});

$("#parent_nameUpdate").autocomplete({
    source: function(request, response) {
        $.ajax({
            url: base_url + "admin/masterdata/interesting-place-types/read_like",
            dataType: "json",
            type: "POST",
            data: {
                word: request.term
            },
            success: function(data) {

                response(data);
                // alert(data);
            }
        });
    },
    focus: function(event, ui) {
        // prevent autocomplete from updating the textbox
        event.preventDefault();
        // manually update the textbox
        $(this).val(ui.item.label);
    },
    select: function(event, ui) {
        // prevent autocomplete from updating the textbox
        event.preventDefault();
        // manually update the textbox and hidden field
        $(this).val(ui.item.label);
        $('#parent_idUpdate').val(ui.item.value);
    }
});
$("#parent_nameUpdate").autocomplete("option", "appendTo", ".eventInsForm");
