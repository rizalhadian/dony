
$('#btnLocationAdd').on('click', function(){
  $('#formLocation')[0].reset();
  $('#modalLocationLabel').text('Add location');
  // alert('woi');
  $('#btnConfirmUpdate').hide();
  $('#btnConfirmSave').show();
});

var idBaru = [];
$('#btnConfirmSave').on('click', function(){
  var serializedData = $('#formLocation').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/locations/create",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalLocation').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      idBaru.push("row"+resp.id+"");
      $('#dataRows').append(
            '<tr id="row'+resp.id+'">'+
              '<td id="dataName'+resp.id+'">'+resp.name+'</td>'+
              '<td id="dataParentName'+resp.id+'">'+resp.parent_name+'</td>'+
              '<td id="dataParentType'+resp.id+'">'+resp.type+'</td>'+
              '<td width="50px">'+
                '<button class="btn btn-default btn-xs "  id="btnLocationUpdate'+resp.id+'" value="'+resp.id+'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'+
                '<button class="btn btn-danger btn-xs"  id="btnLocationDelete'+resp.id+'" value="'+resp.id+'"><i class="fa fa-trash-o" aria-hidden="true"></i></button>'+
              '</td>'+
            '</tr>'
      );

      getAllbtnLocationDelete();
      getAllbtnLocationUpdate();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error!</strong> '+resp.message+'</div>');
    }
  });
});

getAllbtnLocationDelete();
getAllbtnLocationUpdate();



function getAllbtnLocationDelete(){
  $("[id^=btnLocationDelete]").each(function () {
    $(this).click(function () {
      $('#modalDelete').modal('show');
      $("#idDelete").val($(this).val());
    });
  });
}

function getAllbtnLocationUpdate(){

  $("[id^=btnLocationUpdate]").each(function () {
    $(this).click(function () {
      $('#btnConfirmUpdate').show();
      $('#btnConfirmSave').hide();
      $('#modalLocationLabel').text('Edit location');
      $("#idUpdate").val($(this).val());
      $('#modalLocation').modal('show');

      request = $.ajax({
        url: base_url+"admin/masterdata/locations/read_id",
        type: "POST",
        data: {id : $(this).val() }
      });

      request.done(function (response, textStatus, jqXHR){
        var resp = jQuery.parseJSON(response);
        $('#nameUpdate').val(resp[0].name);
        $('#parent_idUpdate').val(resp[0].parent_id);
        $('#parent_nameUpdate').val(resp[0].parent_name);
        $('#typeUpdate').val(resp[0].type);
      });

    });
  });
}

$("#btnConfirmDelete").on('click', function(){
  var id = $("#idDelete").val();
  request = $.ajax({
      url: base_url+"admin/masterdata/locations/delete",
      type: "POST",
      data: {id : id}
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalDelete').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $('#row'+resp.id+'').remove();
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});

$("#btnConfirmUpdate").on('click', function(){

  var serializedData = $('#formLocation').serializeArray();
  request = $.ajax({
      url: base_url+"admin/masterdata/locations/update",
      type: "POST",
      data: serializedData
  });

  request.done(function (response, textStatus, jqXHR){
    $('#modalLocation').modal('hide');
    var resp = jQuery.parseJSON(response);
    if(resp.error == 0){
      $('#alertInformations').append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success!</strong> '+resp.message+'</div>');
      $("#dataName"+resp.id+"").text(resp.name);
      $("#dataParentName"+resp.id+"").text(resp.parent_name);
      $("#dataType"+resp.id+"").text(resp.type);
    }else{
      $('#alertInformations').append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>SOMETHING WRONG!</strong> '+resp.message+'</div>');
    }
  });
});

$( "#parent_nameUpdate" ).autocomplete({
  source: function( request, response ) {
      $.ajax({
                url: base_url+"admin/masterdata/locations/read_like",
                dataType: "json",
                type: "POST",
                data: {word : request.term},
                success: function(data){

                    response(data);
                    // alert(data);
                }
        });
    },
    focus: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox
					$(this).val(ui.item.label);
				},
		 select: function(event, ui) {
					// prevent autocomplete from updating the textbox
					event.preventDefault();
					// manually update the textbox and hidden field
					$(this).val(ui.item.label);
					$('#parent_idUpdate').val(ui.item.value);
		 }
});
$("#parent_nameUpdate").autocomplete( "option", "appendTo", ".eventInsForm" );
