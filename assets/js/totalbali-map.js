
var accommodation_rooms_id_for_prices;
var idDelete;
var whichDelete;
var room_datas = [];
var rates_ids = [];
var accommodation_rooms_ids = [];
var room_update;

var map_accommodation_lat = $('#map_accommodation_lat').val();
var map_accommodation_lng = $('#map_accommodation_lng').val();
var map_type = $('#map_type').val();
var map_accommodations_id = $('#map_accommodations_id').val();
var map_marker_accommodations_all = $('#map_marker_accommodations_all').val();
var map_marker_accommodations_specific = $('#map_marker_accommodations_specific').val();
var map_marker_restaurants_all = $('#map_marker_restaurants_all').val();
var map_marker_restaurants_specific = $('#map_marker_restaurants_specific').val();
var map_marker_beaches_all = $('#map_marker_beaches_all').val();
var map_marker_beaches_specific = $('#map_marker_beaches_specific').val();

function initMap() {
        var styles = [
            {
                stylers: [
                    {hue: "#0099ff"},
                    {saturation: -40}
                ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    {lightness: 100},
                    {visibility: "simplified"}
                ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    {visibility: "on"}
                ]
            }, {
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            }
        ];
        var styledMap = new google.maps.StyledMapType(styles, {name : "Styled Map"});

        var uluru = {lat: -8.699850, lng: 115.180635};
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 14,
          center: uluru,
          zoomControl: true,
          streetViewControl: false,
          fullscreenControl: false,
          mapTypeControl: false,
          mapTypeControlOptions: {
              mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
          }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        // Single Accommodation
        var single_accommodation;
        var marker_single_accommodation;
        var latlng_single_accommodation = {lat: parseFloat($('#map_accommodation_lat').val()), lng: parseFloat($('#map_accommodation_lng').val())};

        if(map_type == "map_for_villa"){
          marker_single_accommodation = new google.maps.Marker({
            position: latlng_single_accommodation,
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP,
          });
          map.setOptions({
            center: latlng_single_accommodation,
            zoom: 15
          });
        }



        // End of Single Accommodation

        function restaurants(resp){
          var restaurants = resp;
          var marker_restaurants = [];

          for(var i = 0; i<restaurants.length; i++){
            var latlng = {lat: parseFloat(restaurants[i].lat), lng: parseFloat(restaurants[i].lng)};

            marker_restaurants[i] = new google.maps.Marker({
              position: latlng,
              map: map,
              icon: base_url+"assets/maps/pin-restaurant.png",
              draggable: false,
              animation: google.maps.Animation.DROP,
            });
          }
        }

        function accommodations(resp){
          var accommodations = resp;
          var marker_accommodations = [];
          var info_window_acommodations = [];
          var content_accommodations = [];

          for(var i = 0; i<accommodations.length; i++){

            if(map_type = "map_for_villa"){
              if(accommodations[i].id == $("#map_accommodation_id").val()){
                continue;
              }
            }


            var latlng = {lat: parseFloat(accommodations[i].lat), lng: parseFloat(accommodations[i].lng)};
            var bedrooms;
            if(accommodations[i].rooms.length>1){
              bedrooms = accommodations[i].rooms[0]+'-'+accommodations[i].rooms[accommodations[i].rooms.length-1];
            }else{
              bedrooms = accommodations[i].rooms[0];
            }

            content_accommodations[i] =
                '<div class="col-xs-12 col-md-12" style="margin-left:-15px; width:400px;">'+
                  '<table class="table">'+
                    '<tr>'+
                      '<td style="width:140px;">'+
                        '<img src="'+base_url+'assets/images/no-image.png" alt="..." class="img-thumbnail" style="width:140px;margin-left:-7px;margin-top:-8px;margin-right:-8px; margin-bottom:-8px;">'+
                      '</td>'+
                      '<td >'+
                        '<b>'+accommodations[i].name+'</b><br>'+
                        '<table class="table table-bordered" style="margin-bottom:-8px;">'+
                          '<tr>'+
                            '<td colspan=2 style="font-size:80%; text-align:justify;">'+
                              'Lorem ipsum dolor sit amet, ludus sapientem mei et. Eu mei iriure delectus tincidunt. Mundi corrumpit et has. Nam an quot equidem. <a href="#">...</a>'+
                            '</td>'+
                          '</tr>'+
                          '<tr >'+
                            '<td style="font-size:85%;">'+
                              '<i class="fa fa-bed" aria-hidden="true"></i> '+bedrooms+
                            '</td>'+
                            '<td style="font-size:85%;">'+
                              '<i class="fa fa-usd" aria-hidden="true"></i> Start From 999 USD / Night'+
                            '</td>'+
                          '</tr>'+
                        '</table>'+
                      '</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td colspan=2>'+
                        '<center><a href="http://www.google.com" class="btn btn-primary btn-block" style="">Book Now</a></center>'
                      '</td>'+
                    '</tr>'+
                  '</table>'+
                  '</div>'+
                '</div>'

            ;

            content_accommodations[i] = new google.maps.InfoWindow({
              content: content_accommodations[i]
            });

            marker_accommodations[i] = new google.maps.Marker({
              position: latlng,
              map: map,
              index: i,
              icon: base_url+"assets/maps/pin-tes.png",
              draggable: false,
              animation: google.maps.Animation.DROP,
            });

            marker_accommodations[i].addListener('click', function() {
              content_accommodations[this.index].open(map, marker_accommodations[this.index]);
              console.log(accommodations[this.index]);
            });


          }
        }

        // Get All Restaurants
        if(map_marker_restaurants_all == 1){

          request = $.ajax({
              url: base_url+"api/interesting-places/read",
              type: "GET",
          });
          request.done(function (response, textStatus, jqXHR){
            var resp = jQuery.parseJSON(response);
            restaurants(resp);
          });

        }
        // End of Get All Restaurants

        // Get All Accommodations
        if(map_marker_accommodations_all == 1){
          request = $.ajax({
              url: base_url+"api/accommodations/read",
              type: "GET",
          });
          request.done(function (response, textStatus, jqXHR){
            var resp = jQuery.parseJSON(response);
            accommodations(resp);
          });
        }
        // End of Get All Accommodations

}

// End of Initmap
