

$(document).on("click", "#btnAddAccommodations", function(){
  request = $.ajax({
      url: base_url+"api/accommodations/read/"+this.value,
      type: "GET",

  });

  request.done(function (response, textStatus, jqXHR){
    console.log(response);
    var resp = jQuery.parseJSON(response);
    $('#modalCode').val(resp.code);
    $('#modalId').val(resp.id);
    $('#modalName').val(resp.name);
    $('#modalLocation').val(resp.location);

    $('#modalBedroomSet').children('option').remove();
    for(var i = 0; i < resp.rooms.length; i++){
      $('#modalBedroomSet').append($("<option/>", {
        value: resp.rooms[i].id,
        text: resp.rooms[i].name
      }));
    }

    $('#modalAddAccommodation').modal('show');
  });
});



$(document).on("click", "#btnAddAccommodationToBookedLists", function(){
  $(".dataTables_empty").hide();
  $("#dataAccommodationsBooked").append(
    '<tr id="rowAccommodation'+$('#modalId').val()+'" >'+
      '<td>'+$("#modalCode").val()+
        '<input type="hidden" name="accommodation_ids[]" value="'+$("#modalId").val()+'">'+
        '<input type="hidden" name="adult_guests[]" value="'+$("#modalAdultGuest").val()+'">'+
        '<input type="hidden" name="children_guests[]" value="'+$("#modalChildrenGuest").val()+'">'+
        '<input type="hidden" name="infant_guests[]" value="'+$("#modalInfantGuest").val()+'">'+
      '</td>'+
      '<td>'+$("#modalName").val()+'</td>'+
      '<td>'+$("#modalLocation").val()+'</td>'+
      '<td>'+$("#modalBedroomSet option[value='"+$("#modalBedroomSet").val()+"']").text()+'<input type="hidden" name="bedroom_sets[]" value="'+$("#modalBedroomSet").val()+'"></td>'+
      '<td>'+formatDate(new Date($("#datepicker-checkin").val()))+'<input type="hidden" name="checkins[]" value="'+$("#datepicker-checkin").val()+'"></td>'+
      '<td>'+formatDate(new Date($("#datepicker-checkout").val()))+'<input type="hidden" name="checkouts[]" value="'+$("#datepicker-checkout").val()+'"></td>'+
      '<td>'+$("#modalAdultGuest").val()+' Adult; '+$("#modalChildrenGuest").val()+' Children; '+$("#modalInfantGuest").val()+' Infant;</td>'+
      '<td>'+
        '<button type="button" disabled id="btnEditAccommodationBooked" value="'+$('#modalId').val()+'" class="btn btn-primary btn-block btn-xs">Edit</button>'+
        '<button type="button" id="btnDeleteAccommodationBooked" value="'+$('#modalId').val()+'" class="btn btn-danger btn-block btn-xs">Delete</button>'+
      '</td>'+
    '</tr>'
  );
  $("#modalAddAccommodation").modal('hide');
});

$(document).on("click", "#btnDeleteAccommodationBooked", function(){
  // alert(this.value);
  $("#rowAccommodation"+this.value).remove();
});

$(document).on("click", "#btnEditAccommodationBooked", function(){
  // alert(this.value);
  $("#modalAddAccommodation").modal("show");
});

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

$(document).on("click", "#btnAddAgent", function(){
  // alert(this.value);
  $("#agent_name").attr("readonly", true);
  $("#agent_email").attr("readonly", true);
  $("#agent_phone_0").attr("readonly", true);
  $("#agent_phone_1").attr("readonly", true);

  $("#agent_id").val(this.value);
  $("#agent_code").val(document.getElementById('dataCodeAgent'+this.value).innerText);
  $("#agent_name").val(document.getElementById('dataNameAgent'+this.value).innerText);
  $("#agent_email").val(document.getElementById('dataEmailAgent'+this.value).innerText);
  var phone = document.getElementById('dataPhoneAgent'+this.value).innerText.split(" ");
  $("#agent_phone_0").val(phone[0]);
  $("#agent_phone_1").val(phone[1]);
  $("#is_new_agent").val(0);
  $("#modalSearchAgents").modal("hide");

})

$(document).on("click", "#btnNewAgent", function(){
  $("#is_new_agent").val(1);
  $("#agent_name").attr("readonly", false);
  $("#agent_name").val("");
  $("#agent_email").attr("readonly", false);
  $("#agent_email").val("");
  $("#agent_phone_0").attr("readonly", false);
  $("#agent_phone_0").val("");
  $("#agent_phone_1").attr("readonly", false);
  $("#agent_phone_1").val("");
  $("#agent_code").val("");
});
