<?php
class CI_Tripadvisor{

  public function authenticate($url){
    // $endpoint_url = "https://rentals.tripadvisor.com/api/property/v1";
    $client = "1030059";
    $secret = "axMMdxRmOOF4qw8q";
    $requestBody = "";

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt ($ch, CURLOPT_CAINFO, realpath(__DIR__ . '/../..')."/assets/certificates/cacert-2017-09-20.pem");

    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      "Authorization: ".$this->getAuthHeader($url, $client, $secret, $requestBody),
      "Content-Type: application/json"
    ]);


    $response = curl_exec($ch);
    $info = curl_getinfo($ch);

    echo "================================Response Info=====================================<br>";
    print_r($info);
    echo "<br><br>=============================Response===================================<br>";
    echo $response;
    echo "<br><br>=================================Header================================<br>";
    echo json_encode($this->getAuthHeader($url, $client, $secret, $requestBody));

    curl_close($ch);

  }

  // public function authenticate_new(){
  //   $client = "1030059";
  //   $secret = "axMMdxRmOOF4qw8q";
  //   $requestBody = "";
  //
  //   $endpoint_url = "https://rentals.tripadvisor.com/api/property/v1";
  //
  //
	// 	$url = $endpoint_url;
	// 	//  Initiate curl
	// 	$ch = curl_init();
	// 	// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //   curl_setopt ($ch, CURLOPT_CAINFO, realpath(__DIR__ . '/../..')."/assets/certificates/cacert-2017-09-20.pem");
	// 	curl_setopt($ch, CURLOPT_URL,$url);
  //   curl_setopt($ch, CURLOPT_HTTPHEADER, [
  //     "Authorization: ".$this->getAuthHeader($url, $client, $secret, $requestBody),
  //     "Content-Type: application/json"
  //   ]);
  //
	// 	$result = curl_exec($ch);
	// 	curl_close($ch);
  //
	// 	echo json_encode($result);
  //   echo "<br>";
  //
  // }



  private function getAuthHeader($url, $client, $secret, $requestBody){
    $algorithm = "VRS-HMAC-SHA512";
    $timestamp = $this->getFormattedTimestamp();
    return sprintf("%s timestamp=%s, client=%s, signature=%s", $algorithm, $timestamp, $client, $this->getSignature($url, $timestamp, $requestBody, $secret));
  }

  private function getFormattedTimestamp(){
    date_default_timezone_set("UTC");
    $currentDate = new DateTime();
    return $currentDate->format("Y-m-d\TH:i:s\Z");
  }

  private function getSignature($url, $timestamp, $requestBody, $secret){
    $method = 'GET';
    $path = parse_url($url, PHP_URL_PATH);
    $query = parse_url($url, PHP_URL_QUERY);
    $bodyDigest = hash('sha512', $requestBody);
    $requestData = implode("\n", [$method, $path, $query, $timestamp, $bodyDigest]);
    $requestDigest = hash('sha512', $requestData);

    // echo "<br> ==================== path =========================<br>";
    // print_r($path);
    // echo "<br> ==================== path =========================<br>";
    //
    // echo "<br> ==================== query =========================<br>";
    // print_r($query);
    // echo "<br> ==================== query =========================<br>";
    //
    // echo "<br> ==================== bodyDigest =========================<br>";
    // print_r($bodyDigest);
    // echo "<br> ==================== bodyDigest =========================<br>";
    //
    //
    // echo "<br> ==================== requestData =========================<br>";
    // print_r($requestData);
    // echo "<br> ==================== requestData =========================<br>";
    //
    // echo "<br> ==================== requestDigest =========================<br>";
    // print_r($requestDigest);
    // echo "<br> ==================== requestDigest =========================<br>";
    //
    // echo "<br> ==================== hash_hmac =========================<br>";
    // print_r(hash_hmac('sha512', strtolower($requestDigest), $secret));
    // echo "<br> ==================== hash_hmac =========================<br>";



    return hash_hmac('sha512', strtolower($requestDigest), $secret);

  }


}
?>
